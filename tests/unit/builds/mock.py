from unittest.mock import patch, Mock, MagicMock, PropertyMock

from occam.object import Object
from occam.builds.manager import BuildManager
from occam.builds.records.build import BuildRecord

from tests.unit.objects.mock import ObjectMock

from tests.helper import multihash

class BuildMock(Mock):
  def __init__(self, info, *args, **kwargs):
    super(Mock, self).__init__(*args, spec_set=Object, **kwargs)
    self.info = info
    self.id = info.get('id')
    self.uid = info.get('uid')
    self.revision = info.get('revision')
    self.path = info.get('path')
    self.infoRevision = self.revision

class BuildManagerMock(Mock):
  """ A fake BuildManager that manages mocked builds for you.

  The point of this is to act like the build manager and retrieve mocked builds
  based on objects in your fake repository. The test can set up a set of fake
  objects and then create and attach this fake build manager to other managers
  to see if unit tests operate correctly.
  """

  def __init__(self, objs=[], *args, **kwargs):
    # Format info
    for obj, builds in objs:
      objectInfo = obj
      if isinstance(obj, list) or isinstance(obj, tuple):
        objectInfo = obj[0]

      if not 'id' in objectInfo:
        objectInfo['id'] = multihash()

      if not 'uid' in objectInfo:
        objectInfo['uid'] = multihash()

      if not 'revision' in objectInfo:
        objectInfo['revision'] = multihash()

    # Mock each object
    objectList = list(map(lambda t:
                            [ObjectMock(t[0]), t[1]] if not isinstance(obj, list) and not isinstance(obj, tuple) else [ObjectMock(t[0][0]), t[1]],
                          objs))
    fileList = list(map(lambda obj:
                          {} if not isinstance(obj, list) and not isinstance(obj, tuple) or len(obj) < 2 else obj[1],
                        objs))

    super(Mock, self).__init__(*args, spec_set=BuildManager, **kwargs)

    def retrieveAll(object, *args, **kwargs):
      for obj, builds in objectList:
        if obj.id == object.id:
          return list(map(lambda build: BuildRecord(build), builds))
      return None

    self.retrieveAll = MagicMock(side_effect=retrieveAll)

    def retrieve(object, task, identity=None):
      for obj, builds in objectList:
        if obj.id == object.id:
          for build in builds:
            if build.get('id') == task.id and build.get('revision') == task.revision:
              return list(map(lambda build: BuildRecord(build), builds))

      return None

    self.retrieve = MagicMock(side_effect=retrieve)

    def pathFor(uid, revision, buildId, create = False, cache = False):
      for obj, builds in objectList:
        if obj.uid == uid:
          if create:
            return f"/build/path/for/{uid}/{buildId}"

          for build in builds:
            if build.get('id') == buildId:
              return f"/build/path/for/{uid}/{buildId}"

      return None

    self.pathFor = MagicMock(side_effect=pathFor)
