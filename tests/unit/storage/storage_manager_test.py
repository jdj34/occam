import unittest

from occam.storage.manager import StorageManager
from occam.storage.manager import StorageError

from unittest.mock import patch, Mock, MagicMock, mock_open
from unittest.mock import create_autospec

# import sys
# sys.modules['occam.resources.plugins.svn'] = Mock()

class TestStorageManager:

  class Obj:
    def __init__(self):
      self.backend = "backend"
      self.account_info = "account_info"

  class TestHandlerFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_None(self):
      self.assertEqual(self.storageManager.handlerFor("HandlerFor"),None)

    def test_should_return_object(self):
      # self.storageManager.configurationFor = Mock(return_value = TestStorageManager.Obj())
      self.assertIsInstance(self.storageManager.handlerFor('local'),object)

  class TestConfigurationFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_dict(self):
      self.assertIsInstance(self.storageManager.configurationFor("test"),dict)

  class TestInitialize(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_dict(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.initialize.return_value = "something"
      self.assertIsInstance(self.storageManager.initialize(),dict)
      self.storageManager.handlerFor.assert_called()

  class TestDiscover(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_list(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.discover.return_value = "handlerForDiscover"
      self.assertIsInstance(self.storageManager.discover(1),list)

  class TestIsLocal(unittest.TestCase):
    storageManager = StorageManager()

    # if StorageManager.handlerFor is used:
    #   TypeError: handlerFor() missing 1 required positional argument: 'storageName'
    # handlerFor is not static -> changed for self.handlerFor
    def test_should_return_False(self):
      self.assertEqual(self.storageManager.isLocal(1),False)

    def test_should_return_True(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.isLocal.return_value = True
      self.assertEqual(self.storageManager.isLocal(1),True)

  class TestBuildPathFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_string_path(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.buildPathFor.return_value = "path/to/somewhere"
      self.assertIsInstance(self.storageManager.buildPathFor(1,2,3),str)
      self.storageManager.handlerFor.assert_called()

    def test_should_return_None_when_all_handlers_return_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.buildPathFor.return_value = None
      self.assertEqual(self.storageManager.buildPathFor(1,2,3),None)
      self.storageManager.handlerFor.assert_called()

  class TestRepositoryPathFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.repositoryPathFor.return_value = None
      self.assertEqual(self.storageManager.repositoryPathFor(1,2),None)
      self.storageManager.handlerFor.assert_called()

    def test_should_return_path_string(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.repositoryPathFor.return_value = "path/to/somewhere"
      self.assertIsInstance(self.storageManager.repositoryPathFor(1,2),str)
      self.storageManager.handlerFor.assert_called()

  class TestResourcePathFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_resourcePath_string(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.resourcePathFor.return_value = "something"
      self.assertIsInstance(self.storageManager.resourcePathFor(1),str)
      self.storageManager.handlerFor.assert_called()

  class TestPathFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_raise_valueError(self):
      with self.assertRaises(ValueError) as cm:
        t = self.storageManager.pathFor(1,2,accountInfo="test")
        self.assertEqual(t.exception.error_code,"cannot have an accountInfo specified without a backend")

    def test_should_return_string_path_for(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.buildPathFor.return_value = "path/for"
      self.assertIsInstance(self.storageManager.pathFor(1,2,buildId=1,backend="test"),str)
      self.storageManager.handlerFor.assert_called()

    def test_should_return_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pathFor.return_value = None
      self.storageManager.storageHashFor = Mock(return_value = [{"value":[1,2,3,4]}])
      self.assertEqual(self.storageManager.pathFor(1,2,backend="test"),None)
      self.storageManager.handlerFor.assert_called()
      self.storageManager.storageHashFor.assert_called()

  class TestPin(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_True(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pin.return_value = True
      self.assertEqual(self.storageManager.pin(1),True)
      self.storageManager.handlerFor.assert_called()

    def test_should_return_False(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pin.return_value = False
      self.assertEqual(self.storageManager.pin(1),False)
      self.storageManager.handlerFor.assert_called()

  class TestPurge(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_True(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.purge.return_value = True
      self.assertEqual(self.storageManager.purge(1),True)
      self.storageManager.handlerFor.assert_called()

    def test_should_return_False(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.purge.return_value = False
      self.assertEqual(self.storageManager.purge(1),False)
      self.storageManager.handlerFor.assert_called()

  class TestPull(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_True(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pull.return_value = True
      self.assertEqual(self.storageManager.pull(1),True)
      self.storageManager.handlerFor.assert_called()

    def test_should_return_False(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pull.return_value = False
      self.assertEqual(self.storageManager.pull(1),False)
      self.storageManager.handlerFor.assert_called()

  class TestPush(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_storageHash_and_accountHash_if_handler_not_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.push.return_value = ("storageHash","accountHash","storedRevision")
      t = self.storageManager.push(1,"revision","identity","path",backend="Local")
      self.assertIsInstance(t[0],str)
      self.assertIsInstance(t[1],str)
      self.storageManager.handlerFor.assert_called()

  class TestPushResource(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_storageHash_and_accountHash_if_handler_not_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pushResource.return_value = ("storageHash","accountHash")
      t = self.storageManager.pushResource(1,"revision","identity","path")
      self.assertIsInstance(t[0],str)
      self.assertIsInstance(t[1],str)
      self.storageManager.handlerFor.assert_called()

  class TestPushBuild(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_storageHash_and_accountHash_if_handler_not_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pushBuild.return_value = ("storageHash","accountHash")
      t = self.storageManager.pushBuild(1,"revision","identity","path")
      self.assertIsInstance(t[0],str)
      self.assertIsInstance(t[1],str)
      self.storageManager.handlerFor.assert_called()

  class TestInstantiate(unittest.TestCase):
    storageManager = StorageManager()

    @patch('occam.storage.manager.GitRepository')
    def test_should_return_path_parameter(self,git_repo):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pathFor.return_value = "repositoryPath"
      self.storageManager.pathFor = Mock(return_value = "revisionPath")
      git_repo.return_value.clone.return_value = 'clone'
      git_repo.return_value.reset.return_value = 'reset'
      self.assertEqual(self.storageManager.instantiate(1,"revision","path"),"path")

    def test_should_return_None(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.pathFor.return_value = None
      self.storageManager.pathFor = Mock(return_value = None)
      self.assertEqual(self.storageManager.instantiate(1,"revision","path"),None)

  class TestClone(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_string(self):
      self.storageManager.storageHashFor = Mock()
      self.storageManager.storageHashFor.side_effect = [1,None,3,4]
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.clone.return_value = "cloned"
      self.assertIsInstance(self.storageManager.clone(1,"revision","path"),str)
      self.storageManager.storageHashFor.assert_called()
      self.storageManager.handlerFor.assert_called()

    def test_should_return_None(self):
      self.storageManager.storageHashFor = Mock()
      self.storageManager.storageHashFor.side_effect = [None,None,None,None]
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.clone.return_value = "cloned"
      self.assertEqual(self.storageManager.clone(1,"revision","path"),None)

  class TestDestroyBackendEntry(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_True(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.unestablish.return_value = "unestablish"
      self.storageManager.datastore = Mock()
      self.storageManager.datastore.destroy.return_value = True
      self.assertEqual(self.storageManager.destroyBackendEntry(TestStorageManager.Obj()),True)

  class TestCreateBackendEntry(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_added_record(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.establish.return_value = "unestablish"
      self.storageManager.datastore = Mock()
      self.storageManager.datastore.createStorageBackendEntry.return_value = TestStorageManager.Obj()
      self.assertIsInstance(self.storageManager.createBackendEntry("backend","person",'info'),object)

  class TestUpdateBackendEntry(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_uptaded_record(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.establish.return_value = "unestablish"
      self.storageManager.datastore = Mock()
      self.storageManager.datastore.update.return_value = TestStorageManager.Obj()
      self.assertIsInstance(self.storageManager.updateBackendEntry(TestStorageManager.Obj()),object)

  class TestBackendEntry(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_retrieved_information(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.datastore = Mock()
      self.storageManager.datastore.retrieveStorageBackendEntry.return_value = 'informationRetrieved'
      self.assertIsInstance(self.storageManager.backendEntry("backend","id","person"),str)

  class TestDefaultBackendEntry(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_dict_if_handler_not_None(self):
      StorageManager.register("foo", Mock(), accountInfo = {'foo': 'bar'})
      self.assertIsInstance(self.storageManager.defaultBackendEntry("foo"), dict)
      StorageManager.handlers = {}

  class TestBackendEntries(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_retrieved_information_stored(self):
      self.storageManager.handlerFor = Mock()
      self.storageManager.datastore = Mock()
      self.storageManager.datastore.retrieveStorageBackendEntries.return_value = 'informationRetrieved'
      self.assertIsInstance(self.storageManager.backendEntries("backend","person"),str)

  class TestRetrieveJSON(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_json(self):
      self.storageManager.retrieveFile = Mock(return_value = '{"teste":"test2"}'.encode('utf-8'))
      self.assertIsInstance(self.storageManager.retrieveJSON(1,2,3),dict)

  class TestStorageHashFor(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_storageHash(self):
      self.assertIsInstance(self.storageManager.storageHashFor("uuid","revision","storageType"),dict)

  class TestRetrieveHistory(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_history_array(self):
      self.storageManager.storageHashFor = Mock()
      self.storageManager.storageHashFor.return_value = {"id": 1}
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value.retrieveHistory.return_value = ["history"]
      self.assertIsInstance(self.storageManager.retrieveHistory(1,2),list)
      self.storageManager.storageHashFor.assert_called()
      self.storageManager.handlerFor.assert_called()

    def test_should_raise_IOError(self):
      with self.assertRaises(IOError) as cm:
        self.storageManager.storageHashFor = Mock()
        self.storageManager.storageHashFor.return_value = {"id": 1}
        self.storageManager.handlerFor = Mock()
        self.storageManager.handlerFor.return_value.retrieveHistory.side_effect = IOError
        t = self.storageManager.retrieveHistory(1,2)
        self.assertEqual(t.exception.error_code,"File Not Found")
        self.storageManager.storageHashFor.assert_called()

  class TestRetrieveDirectory(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_list_of_retrieved_directories_when_buildId_is_None(self):
      self.storageManager.storageHashFor = Mock()
      self.storageManager.storageHashFor.return_value = {"id": 1}
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value = Mock()
      self.storageManager.handlerFor.return_value.retrieveDirectory.return_value = ["retrieveDirectory"]
      self.assertIsInstance(self.storageManager.retrieveDirectory("uid","revision","/path/to/somewhere"),list)

    def test_should_raise_IOError(self):
      with self.assertRaises(IOError):
        self.storageManager.storageHashFor = Mock()
        self.storageManager.storageHashFor.return_value = {"id": 1}
        self.storageManager.handlerFor = Mock()
        self.storageManager.handlerFor.return_value = Mock()
        self.storageManager.handlerFor.return_value.retrieveDirectory.side_effect = IOError
        t = self.storageManager.retrieveDirectory("uid","revision","/path/to/somewhere",buildId=1)
        self.assertEqual(t.exception.error_code,"File Not Found")
        self.storageManager.handler.assert_called()

  class TestRetrieveFileStat(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_dictionary(self):
      self.storageManager.storageHashFor = Mock()
      self.storageManager.storageHashFor.return_value = {"id": 1}
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value = Mock()
      self.storageManager.handlerFor.return_value.retrieveFileStat.return_value = {"file1": "file1"}
      self.assertIsInstance(self.storageManager.retrieveFileStat(1, 2, "/path/to/somewhere"), dict)

    def test_should_raise_exception(self):
      with self.assertRaises(IOError):
        self.storageManager.storageHashFor = Mock()
        self.storageManager.storageHashFor.return_value = {"id": 1}
        self.storageManager.handlerFor = Mock()
        self.storageManager.handlerFor.return_value = Mock()
        self.storageManager.handlerFor.return_value.retrieveFileStat.side_effect = IOError
        t = self.storageManager.retrieveFileStat(1,2,"/path/to/somewhere")
        self.assertEqual(t.exception.error_code,"File Not Found")
        self.storageManager.storageHashFor.assert_called()

  class TestRetrieveFile(unittest.TestCase):
    storageManager = StorageManager()

    def test_should_return_string(self):
      self.storageManager.storageHashFor = Mock()
      self.storageManager.storageHashFor.return_value = {"id": 1}
      self.storageManager.handlerFor = Mock()
      self.storageManager.handlerFor.return_value = Mock()
      self.storageManager.handlerFor.return_value.retrieveFile.return_value = "file"
      self.assertIsInstance(self.storageManager.retrieveFile(1, 2, "/path/to/somewhere"), str)

    def test_should_raise_exception_when_retrieveFile_fails(self):
      with self.assertRaises(IOError):
        self.storageManager.storageHashFor = Mock()
        self.storageManager.storageHashFor.return_value = {"id": 1}
        self.storageManager.handlerFor = Mock()
        self.storageManager.handlerFor.return_value = Mock()
        self.storageManager.handlerFor.return_value.retrieveFile.side_effect = IOError
        t = self.storageManager.retrieveFile(1,2,"/path/to/somewhere")
        self.assertEqual(t.exception.error_code,"File Not Found")
        self.storageManager.storageHashFor.assert_called()


class TestStorageError:
  class TestStorage(unittest.TestCase):
    storageManager = StorageError("message")

    def test_should_return_string(self):
      self.assertIsInstance(self.storageManager.__str__(),str)
