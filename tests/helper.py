import os, random
random.seed(os.environ["PYTHONHASHSEED"])

from uuid import uuid4

# Create a URI by hashing the public key with a multihash
from occam.storage.plugins.ipfs_vendor.multihash import multihash as vendor_multihash

from occam.storage.plugins.ipfs_vendor.base58 import b58encode

def uuid():
  """ Generates a random uuid as a string.
  """
  return str(uuid4())

def multihash(hashType="sha256"):
  """ Generates a random multihash as a string.
  """

  token = uuid()
  if hashType == "sha1":
    # E.g. 5dsrskwNzUS2FQJQTnWprkwwHp3kzh
    hashType = vendor_multihash.SHA1
  else:
    # E.g. Qmf9xTypKKj7HSN4QvoMWSTc2TnovP51yeniqTjqYsePoo
    hashType = vendor_multihash.SHA2_256

  hashedBytes = vendor_multihash.encode(token, hashType)
  ret = b58encode(bytes(hashedBytes))

  return ret

def random_email():
  """ Generates a random valid email address.
  """

  return f"{uuid().replace('-', '.')}@{multihash()}.com"

def initialize(objects, accounts=None):
  """ Initializes a complete system.

  When an 'id' or 'uid' are not given, they will be randomly generated using the
  multihash helper.

  Every specified account will automatically have a Person object created which
  is added to the object repository automatically.

  Arguments:
    objects (list): A set of dict elements comprising of the object info.
    accounts (list): A set of dict elements comprising of account info.

  Returns:
    tuple: The newly crafted ObjectManagerMock and AccountManagerMock.
  """

  from tests.unit.objects.mock import ObjectManagerMock
  from tests.unit.accounts.mock import AccountManagerMock

  # Create Person object for every account
  people = list(map(lambda account: {
      'name': account.get('username', uuid()),
      'type': 'person',
      'id': multihash(),
      'uid': multihash()
    }, accounts))

  objects.extend(people)

  accountsMock = AccountManagerMock(people)
  objectsMock = ObjectManagerMock(objects)

  return objectsMock, accountsMock

def Any(cls):
  """ A matcher that reports True when the argument is of the given type.

  Usage:
    # Call the function
    foo.someFunction()

    # Then assert that it was called with an int
    foo.someFunction().assert_called_with(Any(int))
  """

  class Any(cls):
    def __eq__(self, other):
      return isinstance(other, cls)

  return Any

def record_matcher(cls, key, value):
  """ A matcher that compares a given record and that it contains a given value.
  """

  def record_matcher(other):
    return isinstance(other, cls) and getattr(other, key) == value

  return record_matcher
