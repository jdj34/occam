# Modifying Occam Objects

While the web interface conveniently provides a way to edit files, you may wish to use your own tools to hack on Occam objects. Object URLs can be `git clone`ed:

```
git clone https://occam.cs.pitt.edu/QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g
```

You may see a warning like the following:
```
Cloning into 'QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g'...
remote: Enumerating objects: 130, done.
remote: Counting objects: 100% (130/130), done.
remote: Compressing objects: 100% (59/59), done.
remote: Total 130 (delta 69), reused 130 (delta 69)
Receiving objects: 100% (130/130), 17.56 KiB | 8.78 MiB/s, done.
Resolving deltas: 100% (69/69), done.
warning: remote HEAD refers to nonexistent ref, unable to checkout.
```

This is ok, it just means the git folder you checked out is not immediately ready for usage. To fix the ref issue:

```
cd QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g
git checkout origin<TAB>
```

You can check which branches can be checked out by using `git branch -a`. If you have checked out the correct remote branch, you should see the same history from `git log` as is recorded on the object's history page on the web <https://occam.cs.pitt.edu/QmSk23u78xfHA2RVSBVdDeDJGpoHWQUYWWVD8dAgmanY4g/history>.
