#################################
Occam Advanced User Documentation
#################################

This documentation details advanced use of Occam with the command line. For help with basic usage please see our `tutorial page <https://occam.cs.pitt.edu/publications/tutorials>`_.

.. toctree::
   :maxdepth: 2

   Importing Existing Objects <docs/user/importing-objects.md>
   Modifying Objects <docs/user/modifying-objects.md>
