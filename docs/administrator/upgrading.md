# Upgrading to a New Version

Occam is always under development. Some changes require administrator action to migrate between versions.

The following aspects of the system are sensitive to changes:

  * Install Prerequisites
  * Python Requirements
  * Configuration Options
  * Database Schemas

The first two items can be handled by running the `install.sh` script again. Configuration changes and database changes however require more manual intervention.

## Configuration Changes.

From time to time the configuration options used by Occam will change. When upgrading, make sure to review the differences between the sample configuration and the configuration you have modified locally. All options should be documented in the sample configuration. If you see a configuration option that is no longer in the sample, it means that most likely that option has been renamed or removed. See the changelog for guidance or contact our team at <occam@list.pitt.edu>.

## Database Migrations

When we change our database schemas we will also publish migration scripts that look for old schemas running in the active database and migrate them to the new schema. These scripts can be found in `scripts/migrations` in the Occam source folder.

In the event we make a change so drastic that it is not possible to migrate existing data, we will provide a tool for handling porting to the newer version and explain how to safely migrate your data.
