# Policies

System policies may be specified under `$OCCAM_ROOT/policies` as YAML files. You can use this feature to specify policies such as data retention guarantees, terms and conditions, and acceptable content policies. The file must include title and text keys.

Below is an example of how you would specify a simple content policy that users must agree to in order to sign up:

```
# content-policy.yml

title: "Acceptable Use Policy"

text: "This service may only be used for the purposes of genetics research. Use of this service for any other purposes is strictly prohibited."
```

Policies are presented to the user upon signup, and may also be viewed under "https://YOUR_URL/system/policies".
