# Add key type to each key
from occam.databases.manager import up, table

import occam.accounts.records.account

@up("accounts")
def addEmailColumnsToAccounts():
  return {
    # The email associated with the account
    "email": {
      "type": "string",
      "length": 128,
      "unique": True
    },

    # The updated email to verify
    "email_pending": {
      "type": "string",
      "length": 128
    },

    # The expiration data for the email verification token.
    "email_token_expiration": {
      "type": "datetime"
    },

    # Semi-colon (;) delimited list of emailed event subscriptions.
    "subscriptions": {
      "type": "text"
    },
  }

@table("account_events")
def addAccountEventsTable():
  return {
    # The username.
    "username": {
      "foreign": {
        "key": "username",
        "table": "accounts"
      }
    },

    # The originating IP address.
    "ip_address": {
      "type": "string",
      "length": 46,
      "null": False
    },

    # The event we are logging. This could be a successful login, failed login,
    # or password reset.
    "event_type": {
      "type": "string",
      "length": 32,
      "values": ["login_success", "login_failure", "password_change"],
      "null": False
    },

    # The time of the last failed login attempt.
    "event_time": {
      "type": "datetime",
      "null": False
    }
  }