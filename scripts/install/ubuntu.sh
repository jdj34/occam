#!/bin/bash
set -eu

# Install required packages
sudo apt install python3 python3-pip python3-virtualenv build-essential libssl-dev libffi-dev python3-dev python3-venv sqlite3

# Setup from here
./scripts/install/common.sh
