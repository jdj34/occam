# Occam

Occam is an archival toolchain designed to support digital preservation of computational artifacts. Visit [occam.cs.pitt.edu](https://occam.cs.pitt.edu) to see a live instance.
## Quickstart Installation

For a detailed guide on installing and running Occam, check out the administration guide on [installation](docs/administrator/installing.md).

### Prerequisites

Using your operating system's package manager, install the following dependencies:
  * `docker` (be sure to add the Occam user to the docker group)
  * `git`
  * `python` (3.7 or above)
    * along with `pip`
  * `sqlite3`
  * `unzip`

### Install procedure
Run the following commands in your terminal.

```
cd ~
git clone https://gitlab.com/occam-archive/occam.git
cd occam
sh ./install.sh
```

You should now have a fully initialized Occam instance installed in `~/.occam`.

Add Occam to your PATH.

```
export PATH="$(pwd)/bin:$PATH"
```
## Usage

Detailed tutorial videos and slides are available on our [tutorials page](https://occam.cs.pitt.edu/publications/tutorials)

For a listing of command line options, type:

```
./bin/occam
```

For continuing with the command line interface, see the [User's Guide](docs/user.md).

To use a web interface, see the [Occam web client repository](https://gitlab.com/occam-archive/occam-web-client) for further instructions.

## Project Layout

```
.
|- bin                   - Occam bin folder for adding to the system PATH
|- docs                  - code documentation
|- dev-requirements.txt  - Python packages required by the project
|- locales               - i18n translation files for localization
|- plugins               - place Occam plugins here
|- pytest.ini            - config for pytest
\- scripts               - standalone scripts
   |- build              - build scripts for hand building dependencies
   |- install            - installation scripts used by install.sh
   |- migrations         - database migration scripts used when upgrading to a new Occam version
   |- bootstrap.sh       - instance bootstrapping script for pulling standard Occam objects
   |- updateYear.sh      - script for updating the copyright year on all source files
|- setup.py              - Python setuptools script for packaging
|- src                   - root for all code
|- terms.yml.sample      - sample policy YAML file
|- occam.json            - object info file - yes, Occam is also an Occam object!
|- occam.py              - main application entrypoint
\- occam                 - application code
   |- occam.py           - main application entrypoint
   |- accounts           - accounts component    (handles account generation)
   |- backends           - backends component    (handles VM plugins)
   |- caches             - caches component      (manages cache services)
   |- commands           - commands component    (organizes cli commands)
   |- configurations     - config component      (handles configuration objects)
   |- daemon             - daemon component      (daemon service)
   |- databases          - database component    (handles db access)
   |- jobs               - jobs component        (handles running tasks)
   |- links              - links component       (manages links to objects)
   |- manifests          - manifests component   (generates tasks)
   |- network            - network component     (world network access)
   |- nodes              - nodes component       (organizes known nodes)
   |- notes              - notes component       (manages metadata)
   |- objects            - objects component     (manages objects)
   |- permissions        - permissions component (manages access control)
   |- resources          - resources component   (stores/retrieves data)
   |- storage            - storage component     (handles data storage)
   |- system             - system component      (general system maintenance)
   |- workflows          - workflows component   (manages workflows)
|- tests                 - unit tests
```


## Code Documentation

See the [contribution guide's](docs/contributing.md) section on documentation.

## Contributing

See the [contribution guide](docs/contributing.md) for details on how to make changes to Occam.

## Code Profiling

See the [performance measurement guide](docs/performance.md).

## Quick Troubleshooting
### Verbose Mode

You can tell Occam to log more information about what it is doing by passing the `-V` flag to any command. This also works when launching the daemon.

### Stack Traces

Exceptions get squashed and turned into simple error messages by default once they reach the daemon manager code. To get a verbose stack trace for these errors instead, set the environment variable `export OCCAM_DEBUG=1`.

### Query Debugging

You can invoke an Occam command and see the executed queries by using the `OCCAM_QUERY` environment variable:

```
OCCAM_QUERY=1 occam objects search --name "DRAMSim"
```

This will print out every executed query.

## Acknowledgements

All attribution and crediting for contributors is located within [occam-web-client](https://gitlab.com/occam-archive/occam-web-client/blob/develop/views/static/credits.slim) or by visiting `/acknowledgements` in any Occam website.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](docs/code-of-conduct.md). By participating in this project you agree to abide by its terms.

## License

Occam is licensed under the AGPL 3.0. Refer to the [LICENSE.txt](LICENSE.txt) file in the root of the repository for specific details.
