# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO: rename nextNodes getNextNodes or something similar
# TODO: refactor and test workflow generation
# TODO: make Workflow class a subclass of Object

import datetime
import os, json
import shutil

from occam.datetime import Datetime

from types import SimpleNamespace

from occam.config     import Config
from occam.log        import loggable

from occam.manager import manager, uses

from occam.accounts.manager import AccountManager
from occam.objects.write_manager  import ObjectWriteManager
from occam.manifests.manager      import ManifestManager
from occam.configurations.manager import ConfigurationManager
from occam.databases.manager      import DatabaseManager
from occam.jobs.manager           import JobManager
from occam.network.manager        import NetworkManager
from occam.nodes.manager          import NodeManager
from occam.discover.manager       import DiscoverManager

from occam.workflows.datatypes.workflow import Workflow
from occam.workflows.datatypes.node import Node

@loggable
@uses(NetworkManager)
@uses(ObjectWriteManager)
@uses(ManifestManager)
@uses(ConfigurationManager)
@uses(DatabaseManager)
@uses(JobManager)
@uses(NodeManager)
@uses(DiscoverManager)
@uses(AccountManager)
@manager("workflows")
class WorkflowManager:
  """ This OCCAM manager handles workflows, configurations, and job generation.
  """

  def createWorkflow(self, object, person=None):
    info = self.objects.infoFor(object)
    try:
      data = self.objects.retrieveJSONFrom(object, info['file'])
    except:
      data = {}
    return Workflow(object, info, data, self.objects.objectTagFor(object))

  # We will turn a workflow into a set of workflows for each possible
  # configuration (taking into account ranges and the like)

  # We will then turn each of these into a set of tasks which can be queued
  # together

  def dataFor(self, workflowObject, person = None):
    """ Returns the data for the workflow.
    """

    workflow = self.createWorkflow(workflowObject, person=person)
    return workflow.data

  def isNodeWorkflow(self, node, person=None):
    """ Returns True if the given node describes a workflow object.
    """
    return node.isType('workflow')

  def resolveNode(self, node, person):
    """ Attains object metadata for the object described by the Node.

    Returns a new Node that contains the object information.
    """

    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   version  = node.version,
                                   person   = person)

    # Discover the object metadata if we cannot resolve the object
    info = self.objects.infoFor(object)

    # Recreate the node with the info so it is cached there
    return Node(node.raw, node.index, node.workflow, info)

  def isNodeRunnable(self, node, person = None):
    """ Returns True if the given node describes a running object.
    """

    # If this node is attached as input, then it does not run
    # It is input instead.

    # However, it may need to run its initial phase to determine
    # its command if it has a 'run.script' key and the wire
    # consuming it has an 'occam/runnable' tag.
    if len(node.self.get('connections', [])) > 0:
      if 'run' in node.info and 'script' in node.info['run']:
        return True

      return False

    return 'run' in node.info

  def jobsForNode(self, runId, nodeIndex):
    """ Returns a list of unique job ids pertaining to the given node.
    """

    import sql

    # Look up all run job records
    runJobs = sql.Table('run_jobs')

    session = self.database.session()

    query = runJobs.select(runJobs.job_id)
    query.where = (runJobs.run_id == runId) & \
                  (runJobs.node_index == nodeIndex)

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    return list(set([x["job_id"] for x in rows]))

  def generateRunForNode(self, workflow, info, run, person = None):
    """ Returns a list of objects and inputs to run for the given node.

    When withInput is specified, it indicates new data into an output wire.
    It will then use that data on the corresponding input wire(s) and permute
    the remaining wires.
    """

    # REMEMBER: if the same wire is connected to two or more inputs, it shares
    #           the data. It does not permute the possible data.

    ret = []

    node         = info.get('node')
    existingTask = info.get('task')
    taskPath     = info.get('path')

    # Get the node object
    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   version  = node.version,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)

    # Returns empty array when node is not runnable
    if not (self.isNodeRunnable(node, person=person) or self.isNodeWorkflow(node, person=person)):
      return ret

    nodeCanRun = True
    inputs = []
    for wireIndex, wire in enumerate(node.inputs):
      inputs.append([])

      # Look at configuration inputs and permute them when necessary to generate the
      # actual inputs for the run.
      if wire.get('type') == "configuration" and len(wire.get('connections', [])) == 0:
        # Permute default configurations

        # Add an imaginary source attached to this node
        inputs[-1].append([])

        schema = self.configurations.schemaFor(object, wireIndex, person = person)
        baseConfiguration = self.configurations.defaultsFor(schema)

        inputMetadata = objectInfo.get('inputs', [])[wireIndex]

        for configuration in self.configurations.permute({}, baseConfiguration):
          WorkflowManager.Log.write("Generated default configuration for %s for %s" % (wire.get('name'), node.name))
          # Generate the configuration object and store that as a possible input
          # We will permute all the configuration inputs when we build tasks

          # This would generate a uuid that hashes the configuration
          #import uuid
          #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

          # Create the configuration object
          configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
            "file": inputMetadata.get('file', 'config.json'),
            "schema": {
              "id": objectInfo.get('id'),
              "revision": object.revision,
              "file": inputMetadata.get('schema'),
              "type": "application/json",
              "name": objectInfo.get('name')
            }
          }, identity = person.identity)

          # Store the generated configuration
          self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
          self.objects.write.commit(configObject, person.identity, message="Adds configuration")
          self.objects.write.store(configObject, person.identity)

          # TODO: Make sure to tie the subset of updated parameters to the output

          # Add it is an input set for this wire
          inputs[-1][-1].append([configObject])

      # Otherwise, look at any other type of input
      else:
        objects = self.resolveInputs(run, workflow, node, wireIndex, person)

        if len(objects) == 0 and len(wire.get('connections', [])) != 0:
          nodeCanRun = False

        for i, input_object in enumerate(objects):
          # Make room for the source
          inputs[-1].append([])

          if wire.get('type') == "configuration":
            # We need to append all permutations of this object

            schema = self.configurations.schemaFor(input_object, person = person)
            data = self.configurations.dataFor(input_object)
            baseConfiguration = self.configurations.defaultsFor(schema)
            subObjectInfo = self.objects.infoFor(input_object)

            inputMetadata = objectInfo.get('inputs', [])[wireIndex]
            ranges = self.configurations.rangedValuesFor(data, schema)
            configurationSpace = self.configurations.expandRangedValue(ranges)
            for configuration in self.configurations.permute(configurationSpace, baseConfiguration):
              WorkflowManager.Log.write("Generated configuration for %s for %s" % (wire.get('name'), node.name))
              # Generate the configuration object and store that as a possible input
              # We will permute all the configuration inputs when we build tasks

              # This would generate a uuid that hashes the configuration
              # We should ensure that common configurations are not constantly recreated
              #import uuid
              #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

              # Create the configuration object
              configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
                "file": inputMetadata.get('file', 'config.json'),
                "schema": {
                  "id": objectInfo.get('id'),
                  "revision": input_object.revision,
                  "file": inputMetadata.get('schema'),
                  "type": "application/json",
                  "name": objectInfo.get('name')
                },
                "generator": [{
                  "id": input_object.id,
                  "revision": input_object.revision,
                  "file": subObjectInfo.get('file'),
                  "name": subObjectInfo.get('name'),
                  "type": subObjectInfo.get('type'),
                }]
              }, identity = person.identity)

              # Store the generated configuration
              self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
              self.objects.write.commit(configObject, person.identity, message="Adds configuration")
              self.objects.write.store(configObject, person.identity)

              # TODO: Make sure to tie the subset of updated parameters to the output

              # Add it is an input set for this wire
              inputs[-1][-1].append([configObject])
          else: # Not configuration
            input_info = self.objects.infoFor(input_object)
            input_info['revision'] = input_object.revision
            input_info['id']  = input_object.id
            input_info['uid'] = input_object.uid

            # If this is a runnable self object, we need to include the run
            # report as the command.
            source = self.resolveInputSource(workflow, node, wireIndex, i)
            if input_info.get('run', {}).get('script') and self.isInputSourceSelf(workflow, node, wireIndex, i):
              # Get the job that ran this and determine its report
              jobs = self.nodesFor(run.id, nodeIndex = source.index).get('nodes', {}).get(source.index, {}).get('jobs', [])
              for job in jobs:
                # TODO: handle these permutations
                job_db = self.jobs.jobFromId(job['id'])
                task = self.jobs.taskFor(job_db, person=person)
                task = self.objects.infoFor(task)
                rootPath = os.path.join(job_db.path, "..")
                runReport = self.jobs.pullRunReport(task, rootPath)

                # We want to lock the input's index to what it was before
                objectIndex = task['runs'].get('index')
                if objectIndex:
                  input_info['index'] = objectIndex

                if isinstance(runReport, list) and len(runReport) > 0:
                  runReport = runReport[0]

                if runReport:
                  input_info['run'] = input_info.get('run', {})
                  input_info['run'].update(runReport)

                # We then, also, want to add the input's runtime
                # dependencies as normal dependencies.
                input_info['dependencies'] = input_info.get('dependencies', [])
                input_info['dependencies'].extend(input_info.get('run', {}).get('dependencies', []))

            inputs[-1][-1].append([input_info])

    # Permute the input sets and generate partial tasks
    # TODO: permute
    # for now we will take the first item in each input
    runs = self.permuteRun(inputs)
    ret = []

    # TODO: ignore nodes that already executed until loops are fully supported
    import sql
    runJobsTable = sql.Table("run_jobs")
    session = self.database.session()
    query = runJobsTable.select(where = (runJobsTable.run_id == run.id))
    self.database.execute(session, query)

    rows = self.database.many(session, size=1000)
    for row in rows:
      if row.get("node_index") == node.index:
        return [ret]

    for run in runs:
      newNode = node.raw.copy()
      newNode['input'] = run
      if existingTask:
        newNode['task'] = existingTask
        newNode['path'] = taskPath
      ret.append(newNode)

    # If there are no inputs connected, then it still needs to run
    if (nodeCanRun and len(runs)==0):
      newNode = node.raw.copy()
      newNode['input'] = []
      if existingTask:
        newNode['task'] = existingTask
        newNode['path'] = taskPath
      ret.append(newNode)

    return [ret]

  def permuteRun(self, runSpace):
    """
    """

    if len(runSpace) == 0:
      return []

    # For each permutation of the remaining inputs,
    # We create a run by adding this input
    runs = self.permuteRun(runSpace[:-1])
    currentCount = len(runs)

    currentWire = runSpace[-1]

    for index, wire in enumerate(currentWire):
      for sourceIndex, item in enumerate(wire):
        # If we have no permutations yet, just add the input list as the
        # first run. We will permute this set with other inputs below.
        if currentCount == 0:
          runs.append([item])

        # Add the run to the current list (if there exists one)
        for input in runs[0:currentCount]:
          if sourceIndex == 0:
            # First, go through each run and add the given item
            # This updates the basic runs, but for the next permutation
            # of this particular input, we go to the bottom else case.
            input.append(item)
          else:
            # Duplicate the existing run input set.
            runs.append(input[:])

            # Recall above that we added the item to the existing set,
            # So now we simply replace that added item with this new item
            runs[-1][-1] = item

    if len(currentWire) == 0:
      if currentCount == 0:
        runs.append([[]])

      for input in runs[0:currentCount]:
        input.append([])

    return runs

  def tailNodes(self, workflowObject, person = None):
    """ Returns a list of nodes that have a dangling output.
    """

    workflow = self.createWorkflow(workflowObject, person=person)
    return workflow.getTailNodes()

  def runJobFor(self, jobId):
    """ Returns the RunJobRecord for the given job.
    """

    return self.datastore.retrieveRunJobFor(jobId)

  def nodesIn(self, workflow, person = None):
    """ Returns the nodes within the given workflow.
    """
    return workflow.nodes

  def nodeAt(self, workflow, nodeIndex, person = None):
    """ Returns information about the node at the given index in the given workflow.
    """
    return self.nodesIn(workflow, person = person)[nodeIndex]

  def queue(self, object, person=None, target=None):
    """ Queues a job to initialize a workflow run and creates run metadata.

    This will create a job that will initiate the workflow. It will create a
    run that will track the progress of the workflow.

    Arguments:
      object (Object): The workflow object to queue.
      person (Person): The actor queuing the workflow.
      target (str): The target to use for queuing the workflow.

    Returns:
      RunRecord: The metadata for the progress of the workflow.
    """

    # Retrieve the workflow instance
    workflow = self.createWorkflow(object)

    # Get the configuration for the host/port of this node.
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    # Creates a run record that keeps track of the workflow progress
    experiment = self.objects.parentFor(object)
    run = self.createRun(experiment or object, workflow, person)

    # Creates a job that initializes the workflow
    readOnlyToken = ""
    if person:
      account = self.accounts.retrieveAccount(identity = person.identity)
      readOnlyToken = self.accounts.generateToken(account, experiment or object, "readOnly")

    if readOnlyToken:
      readOnlyToken = f"?T={readOnlyToken}"

    job = self.createJob(experiment or object,
                         initialize = "workflows",
                         initializeTag = f"occam://{thisHost}:{thisPort}/workflows/status/{(experiment or object).id}/{run.id}{readOnlyToken}",
                         runOwner = run,
                         person = person,
                         target = target)

    # Creates a record that maps a run to a job for later lookup
    self.mapJobToRun(run.id, job.id, None)

    # Return the run record
    return run

  def launch(self, object, run, person=None):
    """ This function takes a workflow and queues it.

    It will look at the workflow metadata and queue jobs for each node in the
    workflow graph that has no other dependencies.

    Arguments:
      object(Object): A workflow object
      run (RunRecord): The run record that tracks the queued workflow.
      person (Object): The actor launching the workflow.
    """

    # Get a representation of the workflow
    workflow = self.createWorkflow(object)

    # Get a list of nodes that can execute, i.e. nodes
    # that do not depend on other results
    nodes = self.getInitialNodes(workflow, person = person)

    # The 'nodes' list contains a set of loose object metadata consisting of
    # what is found in the workflow description. This is usually just the id and
    # maybe the name/type, etc. We need more information about these nodes.

    # Do basic object discovery for the objects in the node list

    # Generate tasks for this node with its starting input
    tasks = self.generateNextTasks(nodes       = nodes,
                                   finishedJob = None,
                                   run         = run,
                                   person      = person)

    # Get the configuration for the host/port of this node.
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    # Creates a run record that keeps track of the workflow progress
    experiment = self.objects.parentFor(object)

    # Creates a job that initializes the workflow
    readOnlyToken = ""
    if person:
      account = self.accounts.retrieveAccount(identity = person.identity)
      readOnlyToken = self.accounts.generateToken(account, experiment or object, "readOnly")

    if readOnlyToken:
      readOnlyToken = f"?T={readOnlyToken}"

    for node, task in tasks:
      taskInfo = self.objects.infoFor(task)
      target = self.jobs.jobFromId(run.job_id).target
      job = self.createJob(task,
                           runOwner = run,
                           existingPath = None,
                           finalize = "workflows",
                           finalizeTag = f"occam://{thisHost}:{thisPort}/workflows/status/{(experiment or object).id}/{run.id}{readOnlyToken}",
                           person = person,
                           target = target)

      self.mapJobToRun(run.id, job.id, node.index)
      WorkflowManager.Log.write("Created job %s." % (job.id))

  def completeJob(self, job, person = None):
    """ Generate the next tasks based on the given job completing.
    """

    nodes = self.nextNodes(finishedJob = job,
                           person      = person)

    run = self.runFor(job.id)

    # Generate tasks for this node with new inputs
    tasks = self.generateNextTasks(nodes       = nodes,
                                   finishedJob = job,
                                   run         = run,
                                   person      = person)

    # Now, get the workflow object
    object = self.objects.parseObjectTag(run.workflow_tag)
    object = self.objects.resolve(object, person = person)

    # Get the configuration for the host/port of this node.
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    # Creates a run record that keeps track of the workflow progress
    experiment = self.objects.parentFor(object)

    # Creates a job that initializes the workflow
    readOnlyToken = ""
    if person:
      account = self.accounts.retrieveAccount(identity = person.identity)
      readOnlyToken = self.accounts.generateToken(account, experiment or object, "readOnly")

    if readOnlyToken:
      readOnlyToken = f"?T={readOnlyToken}"

    for node, task in tasks:
      taskInfo = self.objects.infoFor(task)
      target = self.jobs.jobFromId(run.job_id).target
      job = self.createJob(task,
                           runOwner = run,
                           existingPath = None,
                           finalize = "workflows",
                           finalizeTag = f"occam://{thisHost}:{thisPort}/workflows/status/{(experiment or object).id}/{run.id}{readOnlyToken}",
                           person = person,
                           target = target)
      self.mapJobToRun(run.id, job.id, node.index)
      WorkflowManager.Log.write("Created job %s." % (job.id))

  def removeRunFolder(self, run):
    """ Removes the run folders for the given workflow run.

    Arguments:
      run (RunRecord): Workflow run for which run folders should be removed.
    """

    if not run:
      return

    # Cleanup folders of all phases of the workflow.
    jobs = self.jobsFor(run.id)

    for job in jobs:
      # Retrieve the actor running this job
      person = self.jobs.personFor(job)

      # Retrieve the task for the job
      task = self.jobs.taskFor(job, person = person)
      taskInfo = self.objects.infoFor(task)

      # Have the JobManager remove the local files
      self.jobs.removeRunFolder(taskInfo, person = person)

  def finish(self, run, person):
    """ Completes a run as a success.
    """
    run.finish_time = datetime.datetime.now()

    session = self.database.session()

    self.database.update(session, run)
    self.database.commit(session)

    self.removeRunFolder(run)

    account = self.accounts.retrieveAccount(identity = person.identity)
    if ('When my workflows finish' in self.accounts.subscriptionsFor(account)):
      body = f"Workflow {run.workflow_name} has finished successfully."
      self.messages.sendMessage('Workflow Finished', body, account)

  def fail(self, run, person):
    """ Completes a run as a failure.
    """
    run.failure_time = datetime.datetime.now()

    session = self.database.session()

    self.database.update(session, run)
    self.database.commit(session)

    self.removeRunFolder(run)

    if person is None:
      return

    account = self.accounts.retrieveAccount(identity = person.identity)
    if ('When my workflows fail' in self.accounts.subscriptionsFor(account)):
      body = f"Workflow {run.workflow_name} has failed."
      self.messages.sendMessage('Workflow Failed', body, account)

  def objectForRun(self, run, person = None):
    """ Returns the containing object that has spawned the workflow run.

    Arguments:
      run (RunRecord): The run metadata.
      person (Person): The actor making the request.

    Returns:
      Object: The object that initiated the run.
    """

    # Retrieve the object that spawned the run
    object = self.objects.parseObjectTag(run.object_tag)
    return self.objects.resolve(object, person = person)

  def workflowForRun(self, runId, person = None):
    """ Returns the Object representing the workflow that is reflected in the run.

    Arguments:
      runId (str): The run identifier.
      person (Person): The actor making the request.

    Returns:
      Object: The main workflow object being used in this run.
    """

    # Retrieve the run metadata
    run_db = self.retrieveRun(runId)

    # Return the resolved workflow object.
    return self.objects.retrieve(id       = run_db.workflow_uid,
                                 revision = run_db.workflow_revision,
                                 link     = run_db.workflow_link,
                                 person   = person)

  def runsForObject(self, object, revision = None, person=None):
    """ Returns a RunRecord list for the given object.

    When the given revision is None, then this will return all runs. Otherwise,
    it will limit the search to the given revision.

    Arguments:
      object (Object): The object that initiated the run.
      revision (str): The revision of this object.
      person (Person): The actor making the request.

    Returns:
      list: A set of RunRecord objects for each existing run spawned by the given
            object.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')

    query = runs.select()
    query.where = (runs.object_uid == object.id)

    if revision:
      query.where = (runs.object_tag == self.objects.objectTagFor(object))

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    return [RunRecord(x) for x in rows]

  def mapJobToRun(self, run_id, job_id, nodeIndex):
    """ Attaches the given job to the given run.
    """

    from occam.workflows.records.run_job import RunJobRecord

    session = self.database.session()

    runJob_db  = RunJobRecord()
    runJob_db.run_id = run_id
    runJob_db.job_id = job_id
    runJob_db.node_index = nodeIndex

    self.database.update(session, runJob_db)
    self.database.commit(session)

    run = self.datastore.retrieveRunFor(job_id)
    run.job_id = job_id

    self.database.update(session, run)
    self.database.commit(session)

    return runJob_db

  def retrieveRun(self, runId):
    """ Returns the RunRecord for the given run id.

    Arguments:
      runId (str): The run identifier.

    Returns:
      RunRecord: The run metadata or None if it cannot be found.
    """

    from occam.workflows.records.run import RunRecord

    import sql

    session = self.database.session()

    runs = sql.Table('runs')

    query = runs.select()
    query.where = (runs.id == int(runId))

    self.database.execute(session, query)
    ret = self.database.fetch(session)

    if ret:
      ret = RunRecord(ret)

    return ret

  def runAndJobForURL(self, runURL):
    # Get the configuration for the host/port of this node.
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    # Parse URL
    parts = self.network.parseURL(runURL)

    # If this is a URL for our own local node, just retrieve the run normally
    if parts.netloc == f"{thisHost}:{thisPort}" or parts.netloc == f"localhost:{thisPort}":
      run = self.retrieveRun(parts.path.split('/')[-1])
      job = self.jobs.jobFromId(run.job_id)

      return run, job

    # Otherwise, we can retrieve the node information from the network
    metadata = self.network.getJSON(runURL)
    if metadata is None:
      return None

    run = metadata.get('run')
    job = metadata.get('job')

    runData = {
        "queue_time": Datetime.from8601(run.get('queueTime')),
        "finish_time": Datetime.from8601(run.get('finishTime')),
        "failure_time": Datetime.from8601(run.get('failureTime')),
        "workflow_tag": run.get('workflow', {}).get('tag'),
        "workflow_uid": run.get('workflow', {}).get('id'),
        "workflow_name": run.get('workflow', {}).get('name'),
        "object_tag": run.get('object', {}).get('tag'),
        "object_uid": run.get('object', {}).get('id'),
        "object_name": run.get('object', {}).get('name'),
        "identity": run.get('identity'),
        "id": run.get('id')
    }

    jobData = {
        "id": job.get('id'),
        "status": job.get('status')
    }

    from occam.workflows.records.run import RunRecord
    from occam.jobs.records.job import JobRecord
    return RunRecord(runData), JobRecord(jobData)

  def runFor(self, jobId):
    """ Returns the RunRecord corresponding to the given job.
    """

    return self.datastore.retrieveRunFor(jobId)

  def jobsFor(self, run_id):
    """ Return all job records created under the given workflow run.
    """

    jobs = self.nodesFor(run_id)
    ret = []
    for nodes in jobs["nodes"].values():
      for node in nodes['jobs']:
        job = self.jobs.jobFromId(node['id'])
        if job is not None:
          ret.append(job)

    return ret

  def jobsForRun(self, run_id):
    return self.datastore.retrieveJobsFor(runId=run_id)

  def nodesFor(self, runId, status = None, statusNot = None, nodeIndex = None):
    """ Yields nodes information about this run.
    """

    import sql

    session = self.database.session()

    runs = sql.Table('runs')
    runJobs = sql.Table('run_jobs')
    jobs = sql.Table('jobs')

    query = runJobs.select(runJobs.job_id, runJobs.node_index)
    query.where = (runJobs.run_id == runId)
    if nodeIndex is not None:
      query.where = query.where & (runJobs.node_index == nodeIndex)

    join = query.join(jobs, type_ = "LEFT")
    join.condition = (join.right.id == query.job_id)

    if status is not None:
      if not isinstance(status, list):
        status = [status]
      join.condition = join.condition & (join.right.status.in_(status))

    if statusNot is not None:
      if not isinstance(statusNot, list):
        statusNot = [statusNot]
      for statusCheck in statusNot:
        join.condition = join.condition & (join.right.status != statusCheck)

    query = join.select()

    self.database.execute(session, query)
    rows = self.database.many(session, size=1000)

    ret = {}

    for x in rows:
      if x['node_index'] is None:
        continue

      if x['status'] is None:
        continue

      ret[x['node_index']] = ret.get(x['node_index'], {"jobs": []})
      ret[x['node_index']]['jobs'].append({"id": x['job_id'],
                                           "status": x['status'],
                                           "kind": x['kind'],
                                           "startTime": x['start_time'] and x['start_time'].isoformat(),
                                           "queueTime": x['queue_time'] and x['queue_time'].isoformat(),
                                           "finishTime": x['finish_time'] and x['finish_time'].isoformat()})

    return {"nodes": ret}

  def isInputSourceSelf(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(index)[index]
    return pin[1] < 0

  def resolveInputSource(self, workflow, node, wireIndex, index):
    pin = node.followInputWire(wireIndex)[index]
    return workflow.nodes[pin[0]]

  def generateDefaultConfigurations(self, node, wireIndex, person = None):
    """ Creates, if needed, default configurations for the given node and wire.

    Args:
      node (Node): The workflow node to create the configurations for.
      wireIndex (int): The index of the input wire.
      person (Person): The person to authenticate as.

    Returns:
      list: A set of Object items for each configuration.
    """

    # Our goal is to have a list of configuration Objects
    ret = []

    # We have to resolve the object for the node
    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)
    inputMetadata = objectInfo.get('inputs', [])[wireIndex]

    # Get the base schema for this configuration
    schema = self.configurations.schemaFor(object, wireIndex, person = person)

    # Get the default configuration
    baseConfiguration = self.configurations.defaultsFor(schema)

    # This will permute the basic configuration
    for configuration in self.configurations.permute({}, baseConfiguration):
      WorkflowManager.Log.write("Generated default configuration for %s for %s" % (inputMetadata.get('name'), node.name))
      # Generate the configuration object and store that as a possible input
      # We will permute all the configuration inputs when we build tasks

      # This would generate a uuid that hashes the configuration
      #import uuid
      #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

      # Create the configuration object
      configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
        "file": inputMetadata.get('file', 'config.json'),
        "schema": {
          "id": node.id,
          "revision": node.revision,
          "file": inputMetadata.get('schema'),
          "type": "application/json",
          "name": objectInfo.get('name')
        }
      }, identity = person.identity)

      # Store the generated configuration
      self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
      self.objects.write.commit(configObject, person.identity, message="Adds configuration")
      self.objects.write.store(configObject, person.identity)

      # TODO: Make sure to tie the subset of updated parameters to the output
      ret.append(configObject)

    return ret

  def generateConfigurations(self, node, wireIndex, inputObject, person = None):
    # Our goal is to have a list of configuration Objects
    ret = []

    # We have to resolve the object for the node
    object = self.objects.retrieve(id       = node.id,
                                   revision = node.revision,
                                   person   = person)

    objectInfo = self.objects.infoFor(object)
    inputMetadata = objectInfo.get('inputs', [])[wireIndex]

    schema = self.configurations.schemaFor(object, wireIndex, person = person)
    data = self.configurations.dataFor(inputObject)
    baseConfiguration = self.configurations.defaultsFor(schema)
    subObjectInfo = self.objects.infoFor(inputObject)

    ranges = self.configurations.rangedValuesFor(data, schema)
    configurationSpace = self.configurations.expandRangedValue(ranges)
    for configuration in self.configurations.permute(configurationSpace, data):
      if configuration == data:
        # This permutation is the same as the base configuration
        # Which means, we aren't permuting!
        return [inputObject]

      WorkflowManager.Log.write("Generated configuration for %s for %s" % (inputMetadata.get('name'), node.name))

      # Generate the configuration object and store that as a possible input
      # We will permute all the configuration inputs when we build tasks

      # This would generate a uuid that hashes the configuration
      # We should ensure that common configurations are not constantly recreated
      #import uuid
      #print(uuid.uuid5(uuid.NAMESPACE_URL, json.dumps(configuration, sort_keys=True)))

      # Create the configuration object
      configObject = self.objects.write.create(name="configuration", object_type="configuration", info = {
        "file": inputMetadata.get('file', 'config.json'),
        "schema": {
          "id": objectInfo.get('id'),
          "revision": inputObject.revision,
          "file": inputMetadata.get('schema'),
          "type": "application/json",
          "name": objectInfo.get('name')
        },
        "generator": [{
          "id": inputObject.id,
          "revision": inputObject.revision,
          "file": subObjectInfo.get('file'),
          "name": subObjectInfo.get('name'),
          "type": subObjectInfo.get('type'),
        }]
      }, identity = person.identity)

      # Store the generated configuration
      self.objects.write.addFileTo(configObject, inputMetadata.get('file', 'config.json'), json.dumps(configuration))
      self.objects.write.commit(configObject, person.identity, message="Adds configuration")
      self.objects.write.store(configObject, person.identity)

      # TODO: Make sure to tie the subset of updated parameters to the output
      ret.append(configObject)

    return ret

  def nextNodes(self, finishedJob, person = None):
    """ This function returns a list of possible nodes to run after the given job completed.
    """

    # Get the Run this job is a part of, which represents the running workflow
    run = self.runFor(finishedJob.id)

    # Now, get the workflow object
    object = self.objects.parseObjectTag(run.workflow_tag)
    object = self.objects.resolve(object, person = person)

    if object is None:
      WorkflowManager.Log.error(
        "Could not find object %s@%s." %
          (run.workflow_uid, run.workflow_revision)
      )
      raise ValueError

    workflow = self.createWorkflow(object)

    # First, get the node for this job
    runJob = self.runJobFor(finishedJob.id)
    # TODO: REMOVE THIS
    #runJob.node_index = 1
    finishedNode = workflow.nodes[runJob.node_index]

    # Next, we want the possible outputs of this node, which will be nodes we
    # may wish to execute next.
    visited_nodes   = []
    candidate_nodes = []

    for idx, output in enumerate([finishedNode.self, *finishedNode.outputs]):
      idx = idx - 1
      for output_node_to in finishedNode.followOutputWire(idx):
        node_idx = output_node_to[0]
        if node_idx not in visited_nodes:
          visited_nodes.append(node_idx)
          node = workflow.nodes[node_idx]
          candidate_nodes.append((node, output_node_to[1],))

    return candidate_nodes

  def generateNextTasks(self, nodes, finishedJob, run, person = None, callback = None):
    """ This function generates new tasks based on the completion of a given job.
    """

    # Our goal is to create a list of tasks
    ret = []

    # Now, get the workflow object
    objectTag = self.objects.parseObjectTag(run.workflow_tag)
    object = self.objects.resolve(objectTag, person = person)

    if object is None:
      WorkflowManager.Log.error(
        "Could not find workflow object %s@%s." %
          (run.workflow_uid, run.workflow_revision)
      )
      raise ValueError

    experiment = self.objectForRun(run, person=person)

    workflow = self.createWorkflow(object)

    finishedNode = None

    if finishedJob:
      runJob = self.runJobFor(finishedJob.id)
      finishedNode = workflow.nodes[runJob.node_index]

    # For each of those possible running nodes, we want to look at any output
    # from previously executed jobs (not counting this finished node) and
    # determine how many times we will run this next node.

    # Our goal is to have a set of tasks (that we must permute and generate)
    taskSet = []

    # For each node to run...
    for node, wireIndex in nodes:
      # Our goal is to determine all inputs on each wire
      inputSet = [[]]

      # Retrieve an instance of the runnable object
      nodeObject = self.objects.retrieve(id       = node.id,
                                         revision = node.revision,
                                         person   = person)

      # For each input wire...
      for inputWireIndex, inputWire in enumerate(node.inputs):
        # Get the input nodes attached to that wire
        input_nodes = node.followInputWire(inputWireIndex)

        # Our goal is to gather all possible outputs
        # We will permute based on the previous finished jobs
        wireSet = [[]]

        # Deal with the default configuration
        # (when nothing is attached to a configuration wire)
        if inputWire.get('type') == 'configuration' and len(input_nodes) == 0:
          configObjects = self.generateDefaultConfigurations(node      = node,
                                                             wireIndex = inputWireIndex,
                                                             person    = person)

          for configObject in configObjects:
            # Add it is a 'generated output' (although we generated it just now)
            wireSet = [[SimpleNamespace(object = configObject)]]
        else:
          # When there are nodes attached to this node at this wire,
          # For each such attached node...
          for input_node_to in input_nodes:
            nodeIndex       = input_node_to[0] # The attached node
            outputWireIndex = input_node_to[1] # The wire on the attached node

            inputNode = workflow.nodes[nodeIndex]
            inputNodeSet = []

            if outputWireIndex == -1:
              # The incoming node is passing itself as input

              # If this is the node that just ran, only use this exact node
              jobs = []
              if finishedNode and nodeIndex == finishedNode.index:
                jobs = [finishedJob]
              else:
                jobs = self.jobsForNode(run.id, nodeIndex = nodeIndex)

              # For each 'self' attached object that was executed, add to the input set
              for job in jobs:
                # For each separate invocation of that object, add to our input set
                # for this wire.
                inputNodeSet.append([SimpleNamespace(job             = job,
                                                     object_uid      = inputNode.id,
                                                     object_revision = inputNode.revision)])

              # An object that is attached as 'self' that doesn't have a 'script' metadata
              # will not run, and therefore not have any job id
              if not jobs:
                # When that object did not need to run (non 'script')
                # just add it in the input set as the only input for this wire
                inputNodeSet.append([SimpleNamespace(job = None, object_uid = inputNode.id, object_revision = inputNode.revision)])
            elif finishedNode:
              # Look at what the incoming node produced on that wire

              # If this is the node that just ran, only use the new output
              if nodeIndex == finishedNode.index:
                # We pass a single group of output
                inputNodeSet.append([SimpleNamespace(job = None, object_uid = job.object_uid, object_revision = job.object_revision) for job in self.jobs.outputsFor(finishedJob, wireIndex = outputWireIndex)])
              else:
                # Gather finished jobs (that predate the finished one) pertaining to
                # the incoming nodes
                jobIds = self.jobsForNode(run.id, nodeIndex = nodeIndex)

                # Gather the outputs for all of these jobs
                # We will get an array where every item is a set of output for each previous job
                inputNodeSet.extend(
                  [
                    [
                      SimpleNamespace(job = None,
                                      object_uid = jobOutput.object_uid,
                                      object_revision = jobOutput.object_revision) for jobOutput in jobs
                    ] for jobs in self.jobs.outputsForAll(jobIds, wireIndex  = outputWireIndex,
                                                                  finishTime = finishedJob.finish_time)
                  ]
                )

            # At this point inputNodeSet will be an array of sets of output
            # Each item in the array should be used in a unique job (task)
            # So, we have to now maintain the unique possible jobs by permuting the wire set

            # First, pull out the object information to conform with other sections
            inputNodeSet = map(lambda outputSet:
                                 list(map(lambda jobOutput:
                                            SimpleNamespace(id       = jobOutput.object_uid,
                                                            revision = jobOutput.object_revision,
                                                            job      = jobOutput.job,
                                                            object   = None),
                                          outputSet)),
                               inputNodeSet)

            # Permute the wire set by generating a new wire set that contains this data
            newWireSet = []

            # For each new input, permute the existing known input sets
            newInputNodeSet = list(inputNodeSet)
            for newInput in newInputNodeSet:
              # This newInput is the set of input objects that needs to run within the task
              # We need to combine with the set of input objects already in the list
              for wireInputs in wireSet:
                wireInputSet = list(wireInputs)[:]
                wireInputSet.extend(list(newInput))
                newWireSet.append(wireInputSet)

            wireSet = newWireSet

        # pastOutputs has a set of possible input sets on that wire
        # That means, this might be an array of three items: [a], [b, c], [d]
        #   Which means it wants to spawn three jobs for this wire, one with [a]
        #   on it, one with [b, c] on it, and another with [d]
        # We have to now merge this with our complete understanding of this node.
        print("-------------- WIRE SET -----------------")
        print(wireSet)
        print("-------------- -------- -----------------")

        def resolveJobObject(jobOutputInfo):
          ret = None
          if jobOutputInfo.object:
            ret = jobOutputInfo.object
          else:
            ret = self.objects.retrieve(id       = jobOutputInfo.id,
                                        revision = jobOutputInfo.revision,
                                        person   = person)

          ret.job = None
          if hasattr(jobOutputInfo, 'job'):
            ret.job = jobOutputInfo.job

          return ret

        # Resolve each new input (as an iterator)
        newInputs = map(lambda outputSet:
                          list(map(resolveJobObject, outputSet)),
                        wireSet)

        # If this is a configuration, permute the input objects
        if inputWire.get('type') == 'configuration':
          newInputs = list(newInputs)
          permutedInputSet = []

          # For each configuration...
          for newInputSet in newInputs:
            for newInput in newInputSet:
              # Permute the configuration and return a list of config objects
              configObjects = self.generateConfigurations(node        = node,
                                                          wireIndex   = inputWireIndex,
                                                          inputObject = newInput,
                                                          person      = person)

              # Add those to our updated list
              for config in configObjects:
                permutedInputSet.append([config])

          # Replace the known list of inputs with the permuted configuration list
          newInputs = permutedInputSet

        newNodeSet = []

        # For each new input, permute the existing known input sets
        newInputs = list(newInputs)
        for newInput in newInputs:
          for nodeInputs in inputSet:
            nodeInputSet = list(nodeInputs)[:]
            nodeInputSet.append(newInput)
            newNodeSet.append(nodeInputSet)

        inputSet = newNodeSet

        # For each possible output for this node, permute the input set
        # Permute them... for every previous option, add each of these new inputs
        #nodeSet.append(list(newInputs))

      taskSet.append((node, inputSet,))

    # From this point, taskSet will be, sadly to some, an array of arrays.

    # [ (node, <-- Each unique runnable candidate node (tuple with node and inputs)
    #   [ <-- Each possible task for that node (permutations of inputs)
    #     [ <-- Each input wire for that node
    #       [ <-- Each object that will be given as input to that wire/node/task
    # ] ] ]) ] Whew!

    # Then, we can generate those tasks
    for node, inputSet in taskSet:
      # Get the object
      uuid     = node.id
      revision = node.revision

      obj = self.objects.retrieve(id       = uuid,
                                  revision = revision,
                                  person   = person)

      if obj is None:
        return None

      for inputs in inputSet:
        for input in inputs:
          for inputObject in input:
            if hasattr(inputObject, 'job') and inputObject.job:
              # We need to update this input's manifest
              inputObject.job = self.jobs.jobFromId(inputObject.job)
              task = self.jobs.taskFor(inputObject.job, person = person)
              task = self.objects.infoFor(task)

              rootPath = os.path.join(inputObject.job.path, "..")
              runReport = self.jobs.pullRunReport(task, rootPath)

              input_info = self.objects.infoFor(inputObject)

              # We want to lock the input's index to what it was before
              objectIndex = task['runs'].get('index')
              if objectIndex:
                input_info['index'] = objectIndex

              if isinstance(runReport, list) and len(runReport) > 0:
                runReport = runReport[0]

              if runReport:
                input_info['run'] = input_info.get('run', {})
                input_info['run'].update(runReport)

              # We then, also, want to add the input's runtime
              # dependencies as normal dependencies.
              input_info['dependencies'] = input_info.get('dependencies', [])
              input_info['dependencies'].extend(input_info.get('run', {}).get('dependencies', []))
              inputObject.info = input_info

        jobEntry = (node, self.manifests.run(object    = obj,
                                             id        = uuid,
                                             revision  = revision,
                                             inputs    = inputs,
                                             generator = experiment,
                                             person    = person),)

        ret.append(jobEntry)

    # Finally, we return the realized task set
    return ret

  def createJob(self, workflow, runOwner=None,
                                run=None,
                                existingPath=None,
                                initialize=None,
                                initializeTag=None,
                                finalize=None,
                                finalizeTag=None,
                                person=None,
                                target=None):
    """ This function creates a job for the input object.

    Arguments:
      object(Object): The object that will be run by the job
      runOwner(RunRecord): The run that is executing the job
      run(RunRecord): The run that the job is executing (if workflow, not task)
      person(Object): The person running the workflow

    Returns:
      JobRecord: the created job
    """

    objectInfo = workflow.info

    job = self.jobs.create(task = objectInfo,
                           taskId = workflow.id,
                           revision = workflow.revision,
                           person = person,
                           finalize = finalize,
                           finalizeTag = finalizeTag,
                           initialize = initialize,
                           initializeTag = initializeTag,
                           existingPath = existingPath,
                           target = target)
    return job

  def _runExecutedByJob(self, job_id):
    """ Get the run that a job is executing.

    Arguments:
      job_id(int): The id of the job

    Returns:
      RunRecord: The run the job is executing, or None.
    """

    import sql

    # Get the run that is executed by job_id
    # Get connection job->run to identify which run is executed by job_id
    runJob = self.runJobFor(jobId=job_id)
    if runJob is None:
      WorkflowManager.Log.warning( "Job %s is not running a workflow." % (job_id))
      return None

    # Get the run that job_id executes
    run = self.retrieveRun(runJob.run_id)
    if run is None:
      WorkflowManager.Log.error("Could not find run %s." % (runJob.run_id))
      return None

    return run

  def getInitialNodes(self, workflow, person=None):
    """ Returns the nodes that run first in the given workflow.

    All other jobs are determined after they finish running using nextNodes.

    Arguments:
      workflow (Workflow): The workflow in question.
      person (Person): The actor running the workflow.

    Returns:
      list: A list of object metadata dicts for each running node.
    """

    nodes = []
    visitedNodes=[]

    # Get all head nodes (nodes without connected inputs)
    headNodes = workflow.getHeadNodes()

    # For each head node, decide if it is running or not
    # If it is not running, it is a static data object and we follow the wire
    i = 0
    while i < len(headNodes):
      node = headNodes[i]

      # If we had visited this node, ignore it
      if node.index in visitedNodes:
        continue

      # Make sure we don't visit it again
      visitedNodes.append(node.index)

      # Attain the object information for this node
      node = self.resolveNode(node, person = person)

      # If one of those is a non executable input, follow the wire
      if not (self.isNodeRunnable(node, person=person) or
              self.isNodeWorkflow(node, person=person)):

        # Check every output
        outputs = []

        # The first output is the 'self' output, and then the normal outputs follow
        outputs.append(node.self)
        outputs.extend(node.outputs)

        # For each output port
        for portIndex, port in enumerate(outputs):
          # Look at each connected wire
          for wire in port.get('connections', []):
            subNodeIndex, inputIndex, position = wire.get('to')

            # Add this object to the 'headNodes' if we haven't seen it before
            if subNodeIndex not in visitedNodes:
              headNodes.append(workflow.nodes[subNodeIndex])
      else:
        # This is a running node. Add it to the return list
        nodes.append((node, -1,))

      i += 1

    return nodes

  def createRun(self, owner, workflow, person=None):
    """ Create a run record for the given workflow.

    Arguments:
      owner (Person): The person who owns the workflow.
      workflow (Object): The workflow being run.
      person (Person): The person requesting the run.

    Returns:
      RunRecord: The newly created run record.
    """

    from occam.workflows.records.run import RunRecord
    session = self.database.session()

    run_db = RunRecord()

    # Unlock at first
    run_db.lock = 0

    # Establish the origin workflow container
    ownerInfo = self.objects.infoFor(owner)
    run_db.object_tag  = self.objects.objectTagFor(owner)
    run_db.object_name = ownerInfo.get('name')
    run_db.object_uid  = owner.id

    run_db.workflow_name = workflow.name
    run_db.workflow_tag  = workflow.tag
    run_db.workflow_uid  = workflow.id

    # Establish when this is queued
    run_db.queue_time = datetime.datetime.now()

    # Establish who asked for this
    if person:
      run_db.identity = person.identity

    # Add to the database
    self.database.update(session, run_db)
    self.database.commit(session)

    return run_db

  def terminate(self, run, person):
    super_job = self.jobs.jobFromId(run.job_id)

    # Make sure the init process has finished!
    while (super_job.status != "finished") and (super_job.status != "failed"):
      import time
      time.sleep(2)
      WorkflowManager.Log.write("Waiting for init job (%d) to finish..." % run.job_id)
      super_job = self.jobs.jobFromId(run.job_id)

    # From the node information stored for this run of the workflow, find all
    # of the jobs corresponding to the run nodes and terminate them.
    nodeInfo = self.nodesFor(run.id, statusNot = ["failed", "finished"])
    for _, node in nodeInfo["nodes"].items():
      for job_info in node["jobs"]:
        # Retrieve the job by its id, fail if it is not found
        job = self.jobs.jobFromId(job_info["id"])
        if job is None:
          WorkflowManager.Log.error("Failed to find job %d within run %d" % (job_info["id"], run.id))
          return -1;

        runForJob = self.runFor(job.id)
        if runForJob.id == run.id:
          # The job we found belongs to this run, use the jobs manager to
          # terminate it.
          if self.jobs.terminate(job, person) != 0:
            WorkflowManager.Log.error("Failed to terminate job %d within run %d" % (job.id, run.id))
            return -1
        else:
          # The job we found belongs to a subworkflow, which also must be
          # terminated.
          if self.terminate(runForJob, person) != 0:
            WorkflowManager.Log.error("Failed to terminate run %d within run %d" % (runForJob.id, run.id))
            return -1

    self.fail(run, person)

    return 0;
