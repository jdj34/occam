# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.workflows.records.run_output import RunOutputRecord as RunOutput
from occam.workflows.records.run_output_object import RunOutputObjectRecord

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log
from occam.config import Config

from occam.manager import uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.configurations.manager import ConfigurationManager
from occam.workflows.manager      import WorkflowManager
from occam.jobs.manager           import JobManager
from occam.network.manager        import NetworkManager
from occam.databases.manager      import DatabaseManager

from urllib.parse import quote

@command('workflows', 'job-done',
  category      = 'Workflow Management',
  documentation = "Respond to a finished job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@argument("url", type=str, help = "The url of the workflow run metadata.")
@option("-r", "--remote-job", action = "store",
                              dest = "remote_job",
                              help = "The job identifier on the remote coordinator.")
@uses(ObjectWriteManager)
@uses(DatabaseManager)
@uses(ConfigurationManager)
@uses(WorkflowManager)
@uses(NetworkManager)
@uses(JobManager)
class JobDoneCommand:
  def do(self):
    # Get the local job
    job = self.jobs.jobFromId(self.options.job_id)

    # Get the run metadata by parsing the url
    # occam://host:port/runs/id
    run, remoteJob = self.workflows.runAndJobForURL(self.options.url)

    if not run:
      Log.error("Cannot find the given run")
      return 1

    # If it is remote, send the workflow job-done command to that coordinator
    config = Config().load()
    host = self.network.hostname()
    thisHost = config.get("daemon", {}).get("externalHost", host)
    thisPort = config.get("daemon", {}).get("port", None)

    # Parse URL
    parts = self.network.parseURL(self.options.url)

    # If this is a remote URL, ping the coordinator server indicated in the given URL
    if parts.netloc != f"{thisHost}:{thisPort}" and parts.netloc != f"localhost:{thisPort}":
      remoteURL = f"occam://{parts.netloc}/workflows/job-done/{self.options.remote_job}/{quote(self.options.url, safe='')}"
      self.network.get(remoteURL)
      return 0

    # Parse the tag and get the workflow object
    tag = self.objects.parseObjectTag(run.object_tag)

    # Get the job metadata for the job that has just completed
    job = self.jobs.jobFromId(self.options.job_id)

    # Fail if we cannot find the job
    if job is None:
      return -1

    # Finish the job!
    self.jobs.finished(job)

    # Get the actor that completed the job
    person = self.jobs.personFor(job)

    # Retrieve the workflow object itself
    workflow = self.objects.resolve(tag, person = person)

    if job.kind != "workflow":
      task = self.objects.retrieve(id       = job.task_uid,
                                   revision = job.task_revision,
                                   person   = person)

      # Get the task that tells us what was running
      taskInfo = self.objects.infoFor(task)

      # Get the workflow to tell us what initiated the run
      workflowInfo = self.objects.infoFor(workflow)

      # Get the experiment / container for the workflow
      experiment = self.objects.parentFor(workflow)

      # Keep track of the generator of this output
      generators = [{
        "name":     taskInfo.get('name'),
        "type":     taskInfo.get('type'),
        "id":       task.id,
        "revision": task.revision
      }]

      # If the workflow is within something, then we append it to the generators
      if experiment:
        experimentInfo = self.objects.infoFor(experiment)
        generators.append({
          "name":     experimentInfo.get('name'),
          "type":     experimentInfo.get('type'),
          "id":       experimentInfo.get('id'),
          "revision": experiment.revision
        })

      # Rake the outputs of the job
      outputs = self.jobs.outputsFor(job)

      # Get the run that is tracking the workflow progress
      run_job = self.workflows.runJobFor(job.id)

      # Tag each output as part of the workflow run
      wireRecords = {}

      for output in outputs:
        new_output = None
        if output.output_index in wireRecords:
          new_output = wireRecords[output.output_index]
          new_output.output_count += 1
        else:
          new_output = RunOutput()
          new_output.output_index = output.output_index
          new_output.output_count = 1
          new_output.run_id = run_job.run_id
          new_output.job_id = run_job.job_id
          new_output.node_index = run_job.node_index

        out_object_record = RunOutputObjectRecord()
        out_object_record.output_index = output.output_index
        out_object_record.object_uid = output.object_uid
        out_object_record.object_revision = output.object_revision
        out_object_record.run_id = new_output.run_id
        out_object_record.job_id = new_output.job_id

        # Commit the RunOutputObjectRecord
        session = self.database.session()
        self.database.update(session, out_object_record)
        self.database.commit(session)

      # Commit RunOutputRecords
      for _, new_output in wireRecords.items():
        session = self.database.session()
        self.database.update(session, new_output)
        self.database.commit(session)

    # Launch any tasks were dependent on the given job finishing.
    self.workflows.completeJob(job, person = person)

    # Determine if the workflow run is done.
    inprogressNodes = self.workflows.nodesFor(run.id, statusNot = ["failed", "finished", "ran"])
    succeededNodes = self.workflows.nodesFor(run.id, status = ["finished", "ran"])

    # As long as there are nodes that are still running, we don't finish
    if inprogressNodes['nodes']:
      return 0

    # TODO: Only mark a workflow failed if there exists a flow of pins where
    # no configurations succeeded. For now we only consider the workflow
    # failed if no nodes succeed.
    if succeededNodes['nodes']:
      self.workflows.finish(run, person)
      Log.done("Workflow %s run %s finished" % (run.workflow_uid, run.id))
    else:
      self.workflows.fail(run, person)
      Log.error("Workflow failed")

    return 0
