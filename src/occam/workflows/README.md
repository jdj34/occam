# The Workflows Component

This component handles workflows, configurations, and job generation.

## Workflows

Workflows are composed of object nodes. The object's inputs and outputs are
passed to one another in order of their appearance in the workflow. When a
workflow is run, the object nodes are traversed and jobs are launched to handle
execution of that portion of the workflow.  Configurations may indicate that
multiple permutations of a run are required using different configurations.

### Queueing
Workflows are queued for execution via the front-end or the `occam workflows
queue` command. When a workflow is queued, an init job is created to represent
the starting of the workflow. A run is then created and tied to that job.

### Cancelling
Workflows may be cancelled, which prevents additional jobs from being generated
for the workflow.
