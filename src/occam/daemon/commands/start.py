from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.daemon.manager  import DaemonManager

@command('daemon', 'start',
  category      = 'Services',
  documentation = "Starts a daemon in the background.")
@option("-p", "--port", action = "store",
                        dest   = "port",
                        help   = "determines the port on which to start the daemon")
@option("-s", "--host", action = "store",
                        dest   = "host",
                        help   = "determines the address on which to listen for daemon connections")
@uses(DaemonManager)
class DaemonCommand:
  """ This command starts a daemon on the given port.
  """

  def do(self):
    """ Perform the command.
    """

    self.daemon.start(host = self.options.host,
                      port = self.options.port,
                      processingTag = self.options.processing_tag)
    return 0
