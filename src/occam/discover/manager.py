# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import ipaddress
from types import SimpleNamespace

from occam.object   import Object
from occam.config   import Config
from occam.log      import loggable
from occam.semver   import Semver
from occam.datetime import Datetime

from occam.manager import manager, uses

from occam.builds.write_manager    import BuildWriteManager
from occam.databases.manager       import DataNotUniqueError
from occam.keys.write_manager      import KeyWriteManager
from occam.network.manager         import NetworkManager
from occam.nodes.manager           import NodeManager
from occam.objects.write_manager   import ObjectWriteManager
from occam.people.write_manager    import PersonWriteManager
from occam.permissions.manager     import PermissionManager
from occam.resources.write_manager import ResourceWriteManager
from occam.storage.manager         import StorageManager
from occam.versions.write_manager  import VersionWriteManager

from occam.keys.manager import KeyIdentityUnknownError

class DiscoveryOptions(SimpleNamespace):
  """ A set of discovery options that dictate what is pulled for an object.
  """

  def __init__(self, withResources=True, withDependencies=True,
                     withBuild=True, withBuildDependencies=False):
    """ Constructs an option set overriding default values when given.

    Arguments:
      withResources (bool): Also pulls resource objects listed in the object.
      withBuild (str or bool): If assigned a string, the string indicates the
                               build ID. When True, pulls a binary build
                               package for the object.
      withBuildDependencies (bool): Pulls objects necessary to build the object.
      withDependencies (bool): Pulls runtime dependencies for the object.
    """

    super().__init__(withBuild = withBuild,
                     withBuildDependencies = withBuildDependencies,
                     withResources = withResources,
                     withDependencies = withDependencies)

@loggable
@manager("discover")
@uses(StorageManager)
@uses(NodeManager)
@uses(NetworkManager)
@uses(KeyWriteManager)
@uses(PermissionManager)
@uses(ObjectWriteManager)
@uses(ResourceWriteManager)
@uses(VersionWriteManager)
@uses(BuildWriteManager)
@uses(PersonWriteManager)
class DiscoverManager:
  """ This manages mechanisms for finding objects in the world at large.
  """

  handlers = {}
  instantiated = {}

  def __init__(self):
    import occam.discover.plugins.ipfs

  @staticmethod
  def register(networkName, handlerClass):
    """ Adds a new discover backend type.

    Arguments:
      networkName (str): The name of the network protocol.
      handlerClass (object): The handler that implements the discovery interface.
    """

    DiscoverManager.handlers[networkName] = {
      'class': handlerClass
    }

  def handlerFor(self, networkName):
    """ Returns an instance of a handler for the given name.

    Arguments:
      networkName: The name of the network which indicates the plugin to use.

    Returns:
      object: An instance of a handler for that network backend.
    """

    if not networkName in DiscoverManager.handlers:
      DiscoverManager.Log.error("discovery network backend %s not known" % (networkName))
      return None

    # Instantiate a storage backend if we haven't seen it before
    if not networkName in DiscoverManager.instantiated:
      # Pull the configuration (from the 'stores' subpath and keyed by the name)
      subConfig = self.configurationFor(networkName)

      # Create a driver instance
      instance = DiscoverManager.handlers[networkName]['class'](subConfig)

      # If there is a problem detecting the backend, cancel
      # This will set the value in the instantiations to None
      # TODO: cache this in the stores configuration? or detection file?
      if hasattr(instance, 'detect') and callable(instance.detect) and not instance.detect():
        instance = None

      # Set the instance
      DiscoverManager.instantiated[networkName] = instance

    if DiscoverManager.instantiated[networkName] is None:
      # The driver could not be initialized
      return None

    return DiscoverManager.instantiated[networkName]

  def defaultBackend(self):
    """ Returns the default discovery backend.

    Returns:
      String: The name of the default network backend.
    """

    return self.configuration.get("default", "ipfs")

  def configurationFor(self, networkName):
    """ Returns the configuration for the given discovery network.
    
    This configuration is found within the occam configuration (config.yml)
    under the "discover" section under the given plugin name.

    Arguments:
      networkName (String): The name of the discovery network backend.

    Returns:
      dict: A set of configuration options, or empty dict if not found.
    """

    config = self.configuration
    subConfig = config.get(networkName, {})

    return subConfig

  def announce(self, id, token, backend=None):
    """ Announces a particular object id on the federation.

    Arguments:
      id (String): The object identifier to announce.
      token (String): The token representing the object or action.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      bool: Returns True when the announcement was made. It will return False
            if the requested network backend was not found or available.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return False

    # The handler can return a bool to denote success or failure
    ret = handler.announce(id, token)
    if isinstance(ret, bool):
      return ret

    return True

  def announceClient(self, name, port, backend=None):
    """ Announces a client service on the federation.

    Arguments:
      name (String): The name representing the client application.
      port (number): The port number the client is using.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      bool: Returns True when the announcement was made. It will return False
            if the requested network backend was not found or available.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return False

    # The handler can return a bool to denote success or failure
    ret = handler.announceClient(name, port)
    if isinstance(ret, bool):
      return ret

    return True

  def announceIdentity(self, id, publicKey, backend=None):
    """ Announces the public key with the identifier.

    Arguments:
      id (String): The identity URI representing the actor.
      publicKey (String): The public key data.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      bool: Returns True when the announcement was made. It will return False
            if the requested network backend was not found or available.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return False

    # The handler can return a bool to denote success or failure
    ret = handler.announceIdentity(id, publicKey)
    if isinstance(ret, bool):
      return ret

    return True

  def announceViewer(self, type, subtype, backend=None):
    """ Announces that there is a viewer for the given type and subtype.

    Arguments:
      type (String): The object type the object can view.
      subtype (String): The object subtype that the object can view.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      bool: Returns True when the announcement was made. It will return False
            if the requested network backend was not found or available.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return False

    id    = self.viewerIdFor(type, subtype, backend=backend)
    token = self.viewerTokenFor(type, subtype, backend=backend)

    # The handler can return a bool to denote success or failure
    ret = handler.announce(id, token)
    if isinstance(ret, bool):
      return ret

    return True

  def announceEditor(self, type, subtype, backend=None):
    """ Announces that there is a editor for the given type and subtype.

    Arguments:
      type (String): The object type the object can edit.
      subtype (String): The object subtype that the object can edit.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      bool: Returns True when the announcement was made. It will return False
            if the requested network backend was not found or available.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return False

    id    = self.editorIdFor(type, subtype, backend=backend)
    token = self.editorTokenFor(type, subtype, backend=backend)

    # The handler can return a bool to denote success or failure
    ret = handler.announce(id, token)
    if isinstance(ret, bool):
      return ret

    return True

  def announceProvider(self, environment, architecture, backend=None):
    """ Announces that there is a provider for the given environment and architecture.

    Arguments:
      environment (String): The environment the object provides.
      architecture (String): The architecture the object provides.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      bool: Returns True when the announcement was made. It will return False
            if the requested network backend was not found or available.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return False

    id    = self.providerIdFor(environment, architecture, backend=backend)
    token = self.providerTokenFor(environment, architecture, backend=backend)

    # The handler can return a bool to denote success or failure
    ret = handler.announce(id, token)
    if isinstance(ret, bool):
      return ret

    return True

  def retrieveToken(self, id, backend=None):
    """ Returns the information stored at a particular key on the given backend.

    Discovery backends typically can store a key/value pair. This function will
    do its best to retrieve the data for a particular token given by the 'id'
    field.

    Arguments:
      id (String): The key to search for.
      backend (String): The name of the backend to use, or None to use the
                        default.

    Returns:
      String: The byte data that is the stored value at that key or None if not
              found.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.retrieveToken(id)

  def nodesWithId(self, id, revision = None, backend=None, limit=10):
    """ Returns a list of nodes that are aware of the given id.

    Arguments:
      id (String): The object identifier or token to search for.
      revision (String): The revision of the object to search for.
      backend (String): The backend to specifically target or None to use the
                        default on the system.
      limit (number): The maximum number of nodes to find before returning.

    Returns:
      list: A set of NodeRecord elements describing nodes that answered our
            call.
    """

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return []

    candidates = handler.search(id)

    nodes = []
    for info in candidates:
      # Attempt to find a web client
      url = info.get('web')

      # See if we are behind a proxy
      info = self.nodes.retrieveInfo(url)
      if info is None:
        continue

      domain = None
      if 'domain' in info:
        # OK. Discover this domain
        domain = info['domain']

      # Try by IP address
      if not domain:
        for socketAddress in info.get('ip', []):
          if ipaddress.ip_address(socketAddress).is_private:
            continue
          domain = socketAddress
          break

      node = self.nodes.search(domain)
      if node is None:
        node = self.nodes.discover(domain, untrusted=True, quiet=True)
        # Discovery may fail.
        if node is None:
          continue

      nodes.append(node)

      if len(nodes) == limit:
        break

    # If we don't find nodes in the federation, we can elect to search nodes
    # directly...
    # TODO: This is likely... not a good plan!
    if len(nodes) == 0:
      nodes = self.nodes.findObject(id, revision = revision)

    return nodes

  def retrieveObjectInfo(self, id, revision = None, person = None, backend=None):
    """ Pulls object info specifically from the federation for the given object.

    Arguments:
      id (String): The object identifier.
      revision (String): The object revision, if known.
      person (Object): The actor attempting to retrieve the object.
      backend (String): The backend to use, or None to use the default.

    Returns:
      dict: The object metadata or None if not found or an error.
    """

    nodes = self.nodesWithId(id, revision = revision,
                                 backend = backend)

    if not nodes:
      return None

    return self.nodes.retrieveObjectInfoFrom(nodes[0], id, revision = revision,
                                                           person = person)

    data = self.retrieveFile(id, person, backend)
    if data:
      import json
      import codecs
      try:
        reader = codecs.getreader('utf-8')
        data = json.load(reader(data))
      except json.decoder.JSONDecodeError:
        data = None

    return data

  def retrieveJSON(self, id, revision = None, path = "object.json", person = None, backend=None):
    """ Retrieves a JSON document from the federation for the given object.

    Arguments:
      id (String): The object identifier.
      revision (String): The object revision, if known.
      path (String): The path of the file to retrieve as JSON.
      person (Object): The actor attempting to retrieve the object.
      backend (String): The backend to use, or None to use the default.

    Returns:
      dict: The JSON data or None if not found or an error.
    """

    data = self.retrieveFile(id, revision = revision,
                                 path = path,
                                 person = person,
                                 backend = backend)

    if data:
      import json
      import codecs
      try:
        reader = codecs.getreader('utf-8')
        data = json.load(reader(data))
      except json.decoder.JSONDecodeError:
        data = None

    return data

  def retrieveFile(self, id, revision = None,
                             path = "object.json",
                             person = None,
                             backend=None,
                             networkOptions=None):
    """ Retrieves the specified data from the federation for the given object.

    Arguments:
      id (String): The object identifier.
      revision (String): The object revision, if known.
      path (String): The path of the file to retrieve.
      person (Object): The actor attempting to retrieve the object.
      backend (String): The backend to use, or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      dict: The JSON data or None if not found or an error.
    """

    nodes = self.nodesWithId(id, revision = revision,
                                 backend = backend)

    if not nodes:
      return None

    return self.nodes.retrieveFileFrom(nodes[0], id, revision = revision,
                                                     path = path,
                                                     person = person,
                                                     options = networkOptions)

  def retrieveFileStat(self, id, revision = None,
                                 path = "object.json",
                                 person=None,
                                 backend=None,
                                 networkOptions=None):
    """ Retrieves the file metadata from the federation for the given object.

    Arguments:
      id (String): The object identifier.
      revision (String): The object revision, if known.
      path (String): The path of the file for which to retrieve metadata.
      person (Object): The actor attempting to retrieve the object.
      backend (String): The backend to use, or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      dict: The file metadata or None if not found or an error.
    """

    nodes = self.nodesWithId(id, revision = revision,
                                 backend = backend)

    if not nodes:
      return None

    return self.nodes.retrieveFileStatFrom(nodes[0], id, revision = revision,
                                                         path = path,
                                                         person = person,
                                                         options = networkOptions)

  def retrieveDirectory(self, id, revision = None,
                                  path = "",
                                  person = None,
                                  backend=None,
                                  networkOptions=None):
    """ Retrieves the directory listing from the federation for the given object.

    Arguments:
      id (String): The object identifier.
      revision (String): The object revision, if known.
      path (String): The path of the directory to list.
      person (Object): The actor attempting to retrieve the object.
      backend (String): The backend to use, or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      dict: The directory metadata or None if not found or an error.
    """

    nodes = self.nodesWithId(id, revision = revision,
                                 backend = backend)

    if not nodes:
      return None

    return self.nodes.retrieveDirectoryFrom(nodes[0], id, revision = revision,
                                                          path = path,
                                                          person = person,
                                                          options = networkOptions)

  def editorIdFor(self, type, subtype, backend):
    """ Encodes the key for an editor announcement.

    The key will encode a corresponding token generated elsewhere.

    Arguments:
      type (String): The object type that is edited.
      subtype (String): The object subtype that is edited.
      backend (String): The network backend to use or None to use the default.

    Returns:
      String: The key identifier or None if there was no handler.
    """

    if type is None:
      type = ""

    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.editorIdFor(type, subtype)

  def editorTokenFor(self, type, subtype, backend):
    """ Retrieves the raw data (unencoded) token for an editor announcement.

    Arguments:
      type (String): The object type that is edited.
      subtype (String): The object subtype that is edited.
      backend (String): The network backend to use or None to use the default.

    Returns:
      String: The token or None if there was no handler.
    """

    if type is None:
      type = ""

    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.editorTokenFor(type, subtype)

  def editorsFor(self, type, subtype = None, person=None,
                             backend=None, networkOptions=None):
    """ Discovers editors for the given type and optional subtype.

    Arguments:
      type (String): The object type that one wants to edit.
      subtype (String): The object subtype that one wants to edit.
      person (Object): The person making the request.
      backend (String): The network backend to use or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      list: A set of ObjectRecord elements describing possible candidates.
    """

    id = self.editorIdFor(type, subtype, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.editorsFor(nodes[0], type, subtype, person=person,
                                                          options=networkOptions)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for editor in data:
      # Create a editor/object record with the data
      editor['identity_uri'] = editor.get('identity')
      editor['object_type'] = editor.get('type')
      ret.append(ObjectRecord(editor))

    return ret

  def viewerIdFor(self, type, subtype, backend):
    """ Encodes the key for an viewer announcement.

    The key will encode a corresponding token generated elsewhere.

    Arguments:
      type (String): The object type that is viewed.
      subtype (String): The object subtype that is viewed.
      backend (String): The network backend to use or None to use the default.

    Returns:
      String: The key identifier or None if there was no handler.
    """

    if type is None:
      type = ""
    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.viewerIdFor(type, subtype)

  def viewerTokenFor(self, type, subtype, backend):
    """ Retrieves the raw data (unencoded) token for a viewer announcement.

    Arguments:
      type (String): The object type that is viewed.
      subtype (String): The object subtype that is viewed.
      backend (String): The network backend to use or None to use the default.

    Returns:
      String: The token or None if there was no handler.
    """

    if type is None:
      type = ""

    if subtype is None:
      subtype = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.viewerTokenFor(type, subtype)

  def viewersFor(self, type, subtype = None, person=None,
                             backend=None, networkOptions=None):
    """ Discovers viewers for the given type and optional subtype.

    Arguments:
      type (String): The object type that one wants to view.
      subtype (String): The object subtype that one wants to view.
      person (Object): The person making the request.
      backend (String): The network backend to use or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      list: A set of ObjectRecord elements describing possible candidates.
    """

    id = self.viewerIdFor(type, subtype, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.viewersFor(nodes[0], type, subtype, person = person,
                                                          options = networkOptions)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for viewer in data:
      # Create a viewer/object record with the data
      viewer['identity_uri'] = viewer.get('identity')
      viewer['object_type'] = viewer.get('type')
      ret.append(ObjectRecord(viewer))

    return ret

  def providerIdFor(self, environment, architecture, backend):
    """ Encodes the key for a provider announcement.

    Arguments:
      environment (String): The environment target for the provider.
      architecture (String): The architecture target for the provider.
      backend (String): The network backend to use or None to use the default.

    Returns:
      String: The token or None if there was no handler.
    """

    if environment is None:
      environment = ""

    if architecture is None:
      architecture = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.providerIdFor(environment, architecture)

  def providerTokenFor(self, environment, architecture, backend):
    """ Retrieves the raw data (unencoded) token for a provider announcement.

    Arguments:
      environment (String): The environment target for the provider.
      architecture (String): The architecture target for the provider.
      backend (String): The network backend to use or None to use the default.

    Returns:
      String: The token or None if there was no handler.
    """

    if environment is None:
      environment = ""

    if architecture is None:
      architecture = ""

    backend = backend or self.defaultBackend()

    handler = self.handlerFor(backend)
    if not handler:
      return None

    return handler.providerTokenFor(environment, architecture)

  def providersFor(self, environment, architecture, person=None,
                                                    backend=None,
                                                    networkOptions=None):
    """ Discovers providers for the given environment and architecture.

    Arguments:
      environment (String): The environment target for the desired provider.
      architecture (String): The architecture target for the desired provider.
      person (Object): The person making the request.
      backend (String): The network backend to use or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      list: A set of ObjectRecord elements describing possible candidates.
    """

    id = self.providerIdFor(environment, architecture, backend=backend)

    nodes = self.nodesWithId(id, backend=backend)

    if not nodes:
      return []

    data = self.nodes.providersFor(nodes[0], environment = environment,
                                             architecture = architecture,
                                             person = person,
                                             options = networkOptions)

    ret = []

    from occam.objects.records.object import ObjectRecord
    for provider in data:
      # Create a object record with the data
      provider['identity_uri'] = provider.get('identity')
      provider['object_type'] = provider.get('type')
      provider['remote'] = True
      ret.append(ObjectRecord(provider))

    return ret

  def status(self, id, revision = None, person = None,
                       backend=None, networkOptions=None):
    """ Retrieves the object status from the federation.

    Arguments:
      id (String): The object identifier that one wants the status for.
      revision (String): The revision of that object.
      person (Object): The actor making the request.
      backend (String): The network backend to use or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      dict: The object status or None if the object cannot be found.
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.statusFrom(nodes[0], option, person = person,
                                                   options = networkOptions)

  def history(self, option, person = None, backend=None, networkOptions=None):
    """ Retrieves a chronicle of changes to the given object from the federation.

    Arguments:
      id (String): The object identifier that one wants the status for.
      revision (String): The revision of that object.
      person (Object): The actor making the request.
      backend (String): The network backend to use or None to use the default.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      list: The list of actions made on the object.
    """

    nodes = self.nodesWithId(option.id, backend=backend)

    if not nodes:
      return None

    return self.nodes.historyFrom(nodes[0], option, person = person,
                                                    options = networkOptions)

  def resolve(self, option, person = None):
    """ Returns an Object based on a CLI object argument.

    If it is given a URL, it attempts to discover the object through the normal
    means. The scheme of the url will depict the storage layer to query.

    If it is not a URL, but an Occam id and revision, it will search for the
    object within the federation and discover it through the normal channels.
    """

    if option is None:
      return None

    if self.network.isURL(option.id):
      parts = self.network.parseURL(option.id)
      objInfo = self.storage.retrieve(parts.scheme, parts.netloc)

      ret = None
    else:
      # TODO: check the normal local repository and then react only if the
      #       object is not found.
      ret = self.objects.resolve(option, person = person)
      if ret is None:
        ret = self.discover(option.id, revision = option.revision,
                                       person = person)

    return ret

  def retrieveIdentity(self, uri, person=None, backend=None, nodes = None):
    """ Discovers the given identity.

    Arguments:
      uri (String): The identifier for the identity to query.
      person (Object): The actor making the request.
      backend (String): The network backend to use or None to use the default.
      nodes (list): A list of nodes to favor or None to search the federation.

    Returns:
      dict: The identity metadata or None on error or the identity is not found.
    """

    import base64, datetime

    # Retrieve public key (IPFS, etc)
    publicKey = self.retrieveToken(uri, backend=backend)

    identityInfo = None

    if not publicKey:
      # Discover identity the hard way... if necessary
      # TODO: only do this if there are no federated backends
      nodes = nodes or self.nodes.findIdentity(uri)

      if len(nodes) > 0:
        identityInfo = self.nodes.identityFrom(nodes[0], uri)
        if identityInfo:
          keyInfo = identityInfo["publicKey"]
          if (keyInfo.get('format') == "RSA" and keyInfo.get('encoding') == "PEM") or \
             ((keyInfo.get('format') == "Ed25519" or keyInfo.get('format') == "Curve25519") and \
              keyInfo.get('encoding') == "base64"):
            publicKey = keyInfo.get('data')
            type = keyInfo.get('format')
            encoding = keyInfo.get('encoding')
            published = Datetime.from8601(keyInfo.get('published'))

    if not publicKey:
      DiscoverManager.Log.write("cannot find identity")
      return None

    # Discover this identity
    identity = self.keys.write.discover(uri, publicKey, type, encoding, published)
    if not identity:
      # The KeyManager rejected this identity
      DiscoverManager.Log.error("key rejected")
      return None

    # Retrieve everything else
    nodes = nodes or self.nodesWithId(uri, backend=backend)

    if not nodes:
      return None

    identityInfo = identityInfo or self.nodes.identityFrom(nodes[0], uri)

    # Look at known signing keys (and their signatures)
    # Verify each of them and commit them
    for verifyKey in identityInfo.get('verifyingKeys', []):
      # {
      #   'key': { 'data': '', 'encoding': 'base64' },
      #   'signature': { 'data': '', 'encoding': 'base64' }
      # }
      try:
        keyInfo = verifyKey.get('key')
        id = keyInfo.get('id')
        DiscoverManager.Log.noisy(f"discovering verification key {id}")
        published = Datetime.from8601(keyInfo.get('published'))
        key = None

        if (keyInfo.get('format') == "RSA" and keyInfo.get('encoding') == "PEM") or \
           (keyInfo.get('format') == "Ed25519" and keyInfo.get('encoding') == "base64"):
          key = keyInfo.get('data')
          key_type = keyInfo.get('format')
          encoding = keyInfo.get('encoding')

        signatureInfo = verifyKey.get('signature')
        signature = None

        if (signatureInfo.get('format') == "PKCS1_v1_5" and signatureInfo.get('encoding') == "base64") or \
           (signatureInfo.get('format') == "Ed25519" and signatureInfo.get('encoding') == "base64"):
          signature = base64.b64decode(signatureInfo.get('data'))
          signature_type = signatureInfo.get('format')
          signature_digest = signatureInfo.get('digest')
      except Exception as e:
        # Invalid key if there is any strange encoding error
        raise e
        continue

      verifyKey = self.keys.write.discoverKey(uri, id, key, key_type, signature, signature_type, signature_digest, published, None, publicKey, type)
      if verifyKey:
        DiscoverManager.Log.noisy(f"accepted verification key {id}")
      else:
        DiscoverManager.Log.warning(f"rejected verification key {id}")

    # Also discover the Person object, if it exists
    if "person" in identityInfo:
      personInfo = identityInfo['person']
      if "id" in personInfo:
        person = self.discover(id = personInfo.get('id'),
                               revision = personInfo.get('revision'),
                               person = person,
                               nodes = nodes)

        if person:
          self.people.write.update(uri, person)

    return identityInfo

  def pullResource(self, node, objStat, id, revision=None,
                                            networkOptions=None,
                                            person=None):
    """ Pulls a resource object from the given node.

    Arguments:
      node (NodeRecord): The node to use to pull the resource.
      objStat (dict): The reported status of the resource.
      id (String): The resource object identifier to pull.
      revision (String): The revision of that resource.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.
      person (Object): The actor making the request.

    Returns:
      dict: The identity metadata or None on error or the identity is not found.
    """

    # Gather identity URI
    identity = objStat.get('identity')

    # Get the resource type (defaults to 'file')
    resourceType = objStat.get('subtype', ["file"])
    if not isinstance(resourceType, list):
      resourceType = [resourceType]
    resourceType = resourceType[0]

    DiscoverManager.Log.write(f"pulling {resourceType} resource")

    url = self.nodes.urlForNode(node, path = self.resources.urlFor(id, revision = revision, resourceType = resourceType, protocol = node.protocol))
    self.resources.write.pull(objStat, identity, overrideSource = url)

  def getAttributionIds(self, nodes, objInfo):
    """ Pull the signing identities of object authors and collaborators.

    Arguments:
      nodes (list): Nodes to find identities from.
      objInfo (dict): Object info of the object for which we want to get signing
        identities.
    """

    for ident in objInfo.get('authors', []) + objInfo.get('collaborators', []):
      # Acquire this identity as well
      if ((len(ident) == 46 and ident[0] == 'Q') or
          (len(ident) == 47 and ident[0] == '6')):
        self.retrieveIdentityIfMissing(ident, person=None, nodes=nodes)

  def getObjectPrereqs(self, node, objInfo, prereqSections, actionSections, person, pending={}):
    """ Attempt to get all of the prerequisites needed to use the given object.

    Arguments:
      objInfo (dict): Information describing the object for which prerequisites
        are wanted.
      prereqSections (list): A list of keys indicating what prerequisite object
        types need to be pulled. E.g.
        ['install', 'dependencies']
      actionSections (list): A list of keys indicating actions for which we
        want to get prerequisites. E.g. ['run', 'build']
      person (Person): Who is doing the object pulling.
      pending (list): A list of objects we are already planning to pull.

    """

    nodes = None
    if node:
      nodes = [node]

    # Collect object resources and pull them all
    for prereqSection in prereqSections:
      objectList = objInfo.get(prereqSection, [])

      for actionSection in actionSections:
        objectList.extend(
          objInfo.get(actionSection, {}).get(prereqSection, [])
        )

      # We want to get builds for dependencies.
      shouldGetBuild = (prereqSection == 'dependencies')
      for prereqObjInfo in objectList:
        # Try to find the dependency on the specific node we were told about.
        # Otherwise, look across the whole federation.
        for nodes in [[node], None]:
          localObject = self.discover(id = prereqObjInfo.get('id'),
                                      revision = prereqObjInfo.get('revision'),
                                      version = prereqObjInfo.get('version'),
                                      person = person,
                                      withBuild = shouldGetBuild,
                                      pending = pending,
                                      nodes = nodes)
          if localObject is not None:
            break

  def getPrereqSections(self, discoveryOptions):
    """ Determine the prerequisite types that need to be pulled.

    Arguments:
      discoveryOptions (DiscoveryOptions): Set of options listing what to pull.

    Returns:
      A list of keys to prerequisite lists of objects in the objInfo that
      should be pulled.
    """

    prereqSections = []

    if discoveryOptions.withResources:
      prereqSections.append('install')

    if discoveryOptions.withDependencies:
      prereqSections.append('dependencies')

    return prereqSections

  def getActionSections(self, discoveryOptions):
    """ Gives the action sections from which prerequisites should be pulled.

    Arguments:
      discoveryOptions (DiscoveryOptions): Set of options listing what to pull.

    Returns:
      A list of keys to sections of prerequisites in the objInfo that will
      enable actions for an object being pulled. For example, to run an object
      the "run" section's prerequisites need to be pulled.
    """

    actionSections = ["run"]
    if discoveryOptions.withBuildDependencies:
      actionSections.append("build")

    return actionSections

  def getObjVersionAndRevision(self, node, id, version=None, revision=None, networkOptions=None):
    """ Helper function that finds the version and revision for an object with
    the given id.

    If a revision is specified, the version and resolved version will not be
    returned.

    Arguments:
      node (NodeRecord): The node from which the information should be requested.
      id (String): The id of the object.
      version (String): The version to find.
      revision (String): The revision to find.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      versionInfos (list): List of versions of the object known to the node.
      version (string): The version passed in, or the latest version if the
        version argument was None. Will be None if the revision was specified.
      revision (string): The revision if it was passed in, or the revision of
        the resolved version if the revision argument was None.
      resolvedVersion (string): The version in versionInfos best matching the
        version string. Will be None if the revision was specified.
    """

    # Get a sorted (oldest to newest) list of object versions.
    versions = self.nodes.pullVersionInfoFrom(node, id, options = networkOptions)
    versionInfos = versions.get('versions', versions.get('tags', []))
    versionList = [versionInfo.get('version') for versionInfo in versionInfos]
    versionList = Semver.sortVersionList(versionList)

    # If given a revision, null the version.
    if revision is not None:
      return versionInfos, None, revision, None

    # If we need a version, we have to pull version information down to acquire
    # the right revision.
    if versionList and not version:
      # Get the latest version
      version = versionList[-1]

    resolvedVersion = version
    if version and not revision:
      resolvedVersion = Semver.resolveVersion(version, versionList)
      
      # Set resolvedVersion to the latest revision we find.
      for versionInfo in versionInfos:
        if versionInfo.get('version') == resolvedVersion:
          revision = versionInfo.get('revision')

    return versionInfos, version, revision, resolvedVersion

  def verifyVersion(self, obj, versionInfo, identity, nodes):
    """ Verify the signature of the given object.
    """

    import base64

    signatureInfo = versionInfo["signature"]
    published = Datetime.from8601(versionInfo.get('published'))
    signature = b''
    if signatureInfo.get('encoding') == "base64" and (
        signatureInfo.get('format') == "PKCS1_v1_5" or
        signatureInfo.get('format') == "Ed25519"):
      signature = base64.b64decode(
        signatureInfo.get('data', '').encode('utf-8')
      )
      signature_type = signatureInfo.get('format') or "PKCS1_v1_5"
      signature_digest = signatureInfo.get('digest') or "SHA512"

    verifyKeyId = signatureInfo.get('key', '')
    versionTag = versionInfo["version"]

    try:
      _ = self.keys.verifyingKeyFor(identity, verifyKeyId)
    except KeyIdentityUnknownError:
      self.retrieveIdentity(identity, nodes = [node])

    verified = self.keys.verifyTag(
      obj, signature, signature_type, signature_digest, identity, verifyKeyId,
      versionTag, published
    )
    if verified:
      # Store version tag
      self.versions.write.update(
        obj, versionTag, identity, published, signature, signature_type,
        signature_digest, verifyKeyId
      )
      DiscoverManager.Log.noisy(f"Discovered version {versionInfo['version']}")
    else:
      DiscoverManager.Log.warning("Version rejected due to signature mismatch.")

  def getBuild(self, obj, id, revision, version, person, pending, node):
    """ Pull the build for the given object if it doesn't already exist locally.
    """

  def verifyVersions(self, versionInfos, identity, id, revision, person, node):
    """ For each known version, verify and store the tag.
    """

    for versionInfo in versionInfos:
      # Verify the signature against the identity
      # Optionally: only allow identities with a relationship with the object identity
      if not (versionInfo.get('identity') == identity and "signature" in versionInfo):
        continue

      obj = self.objects.retrieve(id = id,
                                  revision = versionInfo.get('revision', revision),
                                  person = person)

      if obj is None:
        # We need to discover this version in order to verify it, so we pass
        # it up.
        continue

      self.verifyVersion(obj, versionInfo, identity, nodes=[node])

  def pullFromTaskList(self, nodes, objectInfo, networkOptions = None,
                                                person = None,
                                                pending = None):
    """ Pulls from a list that may contain a provider.

    A provider is an object that runs another. It will contain a 'running'
    metadata field that contains a list of processes which themselves have a
    list of supporting 'objects'.

    Arguments:
      nodes (list): The node list to pull from.
      objectInfo (dict): The object metadata.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.
      person (Object): The Person pulling this task.
      pending (dict): A dictionary of tokens pertaining to ongoing pulls.

    Returns:
      bool: True when the pull succeeded.
    """

    # Pull running objects
    for process in objectInfo.get('running', []):
      for object in process.get('objects', []):
        buildId = object.get('build', {}).get('id')
        self.discover(id = object.get('id'),
                      revision = object.get('revision'),
                      nodes = nodes,
                      withBuild = buildId,
                      networkOptions = networkOptions,
                      person = person,
                      pending = pending)

        # Pull input objects
        for port in object.get('inputs', []):
          for input in port.get('connections', []):
            self.discover(id = input.get('id'),
                          revision = input.get('revision'),
                          nodes = nodes,
                          networkOptions = networkOptions,
                          person = person,
                          pending = pending)

        # Pull any objects running in this environment layer
        if 'running' in object:
          self.pullFromTaskList(nodes, object, networkOptions = networkOptions,
                                               person = person,
                                               pending = pending)

    return True

  def pullTask(self, nodes, task, networkOptions=None,
                                  person=None,
                                  pending=None):
    """ Pulls everything needed to fulfill the given discovered task.

    Arguments:
      nodes (list): The node list to pull from.
      task (Object): The task object to pull objects from.
      person (Object): The Person pulling this task.
      pending (dict): A dictionary of tokens pertaining to ongoing pulls.
    """

    taskInfo = self.objects.infoFor(task)

    # If this is a build task, we want to ensure we pull the build requirements
    # for the object.
    if taskInfo.get('builds'):
      buildInfo = taskInfo.get('builds')
      self.discover(buildInfo.get('id'), revision = buildInfo.get('revision'),
                                         withDependencies = True,
                                         withBuildDependencies = True,
                                         nodes = nodes,
                                         networkOptions = networkOptions,
                                         person = person,
                                         pending = pending)

    # Go through the objects in the task and pull them at their respective
    # revisions.
    self.pullFromTaskList(nodes, taskInfo, networkOptions = networkOptions,
                                           person = person,
                                           pending = pending)

    return task

  def _pullObjectFromUpstream(self, id, revision, version,
                                                  resolvedVersion,
                                                  objInfo,
                                                  versionInfos,
                                                  node,
                                                  identity,
                                                  person,
                                                  discoveryOptions,
                                                  networkOptions,
                                                  pending):
    """ Pulls the requested object and its dependencies from a remote node.

    Arguments:
      id (String): The object identifier to pull.
      revision (String): The revision of that object.
      version (String): The version to satisfy, or None to choose the latest.
      objInfo (dict): The object info dictionary found on the remote node.
      versionInfos (list): A list of version info dictionaries for versions
        that satisfy the version requirements.
      node (NodeRecord): The node to use to pull the object.
      identity (identity): The identity of the person pulling this object.
      person (Object): The actor making the request.
      discoveryOptions (DiscoveryOptions): Set of options listing what to pull.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.
      pending (dict): A dictionary of tokens pertaining to ongoing pulls.
    """

    # Get the identities of everyone who contributed to the object.
    self.getAttributionIds(nodes=[node], objInfo=objInfo)

    # Log the object we are pulling.
    if objInfo.get('type') != 'task':
      DiscoverManager.Log.write(f"Pulling {objInfo.get('type')} "
                                f"{objInfo.get('name')} "
                                f"{resolvedVersion or ''}"
                                f"{('@' + revision) if revision else ''}")

    # Get a temporary cloned copy of the object.
    git = self.nodes.repositoryFrom(node, id, revision = revision,
                                              options = networkOptions)
    obj = Object(path     = git.path,
                 id       = id,
                 revision = revision,
                 info     = objInfo,
                 identity = identity)

    # Fetch all of the prerequisites needed to use the object.
    self.getObjectPrereqs(node,
                          objInfo,
                          self.getPrereqSections(discoveryOptions),
                          self.getActionSections(discoveryOptions),
                          person = person,
                          pending = pending)

    # Store the base object.
    try:
      self.objects.write.store(obj, identity=identity)
    except DataNotUniqueError:
      # This is OK, since the objects will be exactly the same. Somebody beat
      # us to the store with the same object.
      DiscoverManager.Log.warning(f"Object collision occured on while "
                                  F"pulling object {id}")

    # Make this object visible to all.
    self.permissions.update(id = id, canRead=True, canWrite=False, canClone=True)

    if isinstance(versionInfos, list):
      self.verifyVersions(versionInfos, identity, id, revision, person, node)

    # Get a reference to this object
    try:
      obj = self.objects.retrieve(id       = id,
                                  revision = revision,
                                  version  = version,
                                  person   = person)
      # TODO: Handle when the version requested isn't actually found.
      return obj
    except:
      return None

  def pullObject(self, node, objStat, id, revision=None,
                                          version=None,
                                          discoveryOptions=DiscoveryOptions(),
                                          networkOptions=None,
                                          person=None,
                                          pending=None):
    """ Pulls an object from the given node.

    Arguments:
      node (NodeRecord): The node to use to pull the object.
      objStat (dict): The reported status of the object.
      id (String): The object identifier to pull.
      revision (String): The revision of that object.
      version (String): The version to satisfy, or None to choose the latest.
      discoveryOptions (DiscoveryOptions): Set of options listing what to pull.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.
      person (Object): The actor making the request.
      pending (dict): A dictionary of tokens pertaining to ongoing pulls.

    Returns:
      dict: The identity metadata or None on error or the identity is not found.
    """

    # The object being requested may be a sub-object of an owner object. If so
    # we want to pull the owner object, not just the sub-object. This is
    # because the owner object describes the sub-object in a way where we
    # cannot pull the sub-object without it.
    id = objStat.get('owner', {}).get('id', id)

    # Make sure we have the signing identity for the object.
    identity = objStat.get('identity')
    self.retrieveIdentityIfMissing(identity, person, nodes = [node])

    # Figure out what versions of the object need to be pulled.
    versionInfos, version, revision, resolvedVersion = self.getObjVersionAndRevision(
      node, id, version, revision, networkOptions = networkOptions
    )

    # Get the revision from the object status, as a last resort (latest revision)
    revision = revision or objStat.get('revision')

    # Add the object to the pending block (by revision, then version)
    pending = pending or {}
    pending[f"{id}@{revision}"] = True

    # Add a 'pending' token for this version
    if resolvedVersion:
      pending[id] = pending.get(id, [])
      pending[id].append(resolvedVersion)

    # If we are requesting a specific version or revision, see if we already
    # know about this object locally. This lets us shortcut pulling. We can't
    # do this for non-versioned requests, or we will miss updates on remote
    # nodes.
    obj = None
    if revision is not None or version is not None:
      obj = self.objects.retrieve(id       = id,
                                  revision = revision,
                                  version  = version,
                                  person   = person)
      if obj:
        objInfo = self.objects.infoFor(obj)

        if not objInfo:
          DiscoverManager.Log.error(f"object {id} is present but its object "
                                    f"info is missing.")
          return None

        DiscoverManager.Log.write(f"Using existing {objInfo.get('type')} "
                                  f"{objInfo.get('name')} "
                                  f"{resolvedVersion or ''}"
                                  f"{('@' + revision) if revision else ''}")

    if obj is None:
      # Get the object info from upstream.
      objInfo = self.nodes.pullObjectInfoFrom(node, id, revision = revision,
                                                        options = networkOptions)
      # We could not discover the object.
      if objInfo is None:
        return None

      obj = self._pullObjectFromUpstream(id, revision,
                                             version,
                                             resolvedVersion,
                                             objInfo,
                                             versionInfos,
                                             node,
                                             identity,
                                             person,
                                             discoveryOptions,
                                             networkOptions,
                                             pending)
      if obj is None:
        return None

    revision = obj.revision

    # Now pull a build (if requested)
    if discoveryOptions.withBuild:
      buildId = None
      if isinstance(discoveryOptions.withBuild, str):
        buildId = discoveryOptions.withBuild

      owner = self.objects.ownerFor(obj, person = person)
      ownerInfo = self.objects.infoFor(owner)

      if ownerInfo.get('build', {}):
        build = self.pullBuild(node, owner, buildId = buildId,
                                            person = person,
                                            pending = pending,
                                            networkOptions = networkOptions)

        if build is None:
          # If we failed to pull the build from a specific node, we will try to
          # discover it from anywhere on the federation
          build = self.discoverBuild(id, revision = revision,
                                         version = version,
                                         networkOptions = networkOptions,
                                         person = person,
                                         pending = pending)
          if build is None:
            # We fail to pull the object if the build does not arrive. The
            # effect of this is that the object will be in the local store for
            # future pulls, but the caller can depend on discovery to fail when
            # a build isn't available.
            DiscoverManager.Log.noisy("Failed to discover a build.")
            return None

    # Return an instantiation of this Object (the original, not the owner)
    return obj

  def pullRuntimeDependencies(self, node, task, person = None, pending = None):
    """ Pulls any dependencies required to run an object from the given build task.

    Arguments:
      node (NodeRecord): The information gathered about this given node.
      task (Object): The 'task' object that represents the build manifest.
      person (Object): The person acquiring the build.
      pending (dict): A dictionary of tokens pertaining to ongoing pulls.

    Returns:
      bool: True upon successful pull.
    """

    # Retrieve task manifest
    taskInfo = self.objects.infoFor(task)

    # Pull out the 'dependencies' used to create the build.
    # Any of them that are marked 'run' or 'init' will be attached to the
    # object when it runs, so we need to retrieve them as though they were
    # normal object dependencies.
    buildTaskInfo = taskInfo.get('builds', {})
    dependencies = buildTaskInfo.get('init', {}).get('dependencies', []) + buildTaskInfo.get('dependencies', [])
    for dependency in dependencies:
      if dependency.get('inject') in ["run", "init"]:
        # Determine the version/revision of the dependency
        dependencyVersion = dependency.get('lock', dependency.get('version'))
        dependencyRevision = None
        if not dependencyVersion:
          dependencyRevision = dependency.get('revision')

        # See if we have a suitable local object already
        localObject = self.objects.retrieve(id = dependency.get('id'),
                                            revision = dependencyRevision,
                                            version = dependencyVersion)
        if not localObject:
          # Attempt to recursively discover this object
          self.discover(id = dependency.get('id'),
                        revision = dependencyRevision,
                        version = dependencyVersion,
                        person = person,
                        pending = pending,
                        nodes = [node])
        else:
          # Check for builds, and retrieve a new one
          builds = self.builds.retrieveAll(localObject)
          if not builds:
            self.discoverBuild(id = dependency.get('id'),
                               revision = dependencyRevision,
                               version = dependencyVersion,
                               person = person,
                               pending = pending,
                               nodes = [node])

    return True

  def pullBuild(self, node, obj, buildId=None,
                                 person=None,
                                 pending=None,
                                 verify=True,
                                 networkOptions=None):
    """ Discovers an object build on the network.

    Uses the various storage backends to hopefully find and retrieve the object
    build information. Will then pull down that built package and verify it.

    Arguments:
      node (NodeRecord): The information gathered about this given node.
      obj (Object): The object we want a build for.
      buildId (str): Pull the build with this Id. If not specified, the latest
                     build will be pulled.
      person (Object): The person object acquiring the build.
      pending (dict): A dictionary of tokens pertaining to ongoing pulls.
      verify (bool): When True, will verify the signature of the build.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.

    Returns:
      dict: The build information or None if the build failed to pull.
    """

    import base64, datetime

    # Revise what we are pulling to get the owner instead
    owner = self.objects.ownerFor(obj, person = person)

    # Gather identity URI
    identity = obj.identity

    # Get the object info
    objInfo = self.objects.infoFor(obj)

    # Pull the identity
    self.retrieveIdentityIfMissing(identity, person, nodes = [node])

    # Get the revision from the object status
    revision = obj.revision

    # Check for a local build of this object.
    builds = self.builds.retrieveAll(obj)
    for build in builds:
      if not buildId or buildId == build.build_id:
        DiscoverManager.Log.write(f"Using existing build for "
                                  f"{objInfo.get('type')} "
                                  f"{objInfo.get('name')} {obj.version or ''}")
        return build

    # Get a list of builds for this object.
    builds = self.nodes.pullBuildInfoFrom(node, owner.id, revision = revision,
                                                       options = networkOptions)
    if builds is None:
      # Could not find builds upstream.
      return None

    builds = builds.get('builds', [])

    # Gather builds from the originating identity
    if not buildId and len(builds) > 0:
      builds = list(filter(lambda build: build['identity'] == identity, builds))

    # Filter for a specific build if requested.
    if buildId:
      builds = list(filter(lambda build: build['id'] == buildId, builds))

    for build in builds:
      currentId = build['id']

      DiscoverManager.Log.write(f"Pulling build for {objInfo.get('type', '')} "
                                f"{objInfo.get('name')} {obj.version or ''}")

      # Check if this build already exists on our node. This can happen in
      # cases where pullBuildInfoFrom includes builds we know as builds of
      # other objects. For example, the g++ runtime and stdc++ library have
      # this issue.
      if self.builds.retrieveFromBuildId(taskId = currentId):
        DiscoverManager.Log.write(f"Using existing build for "
                                  f"{objInfo.get('type')} "
                                  f"{objInfo.get('name')} {obj.version or ''}")
        return build

      # Get a copy of the build task object.
      nodes = [node] if node else None
      task = self.discover(id = build.get('id'),
                    revision = build.get('revision'),
                    networkOptions = networkOptions,
                    person = person,
                    withBuild = False,
                    nodes = nodes)

      # Copy the build object into a temporary space.
      import tempfile
      buildPath = os.path.realpath(tempfile.mkdtemp(prefix="occam-", dir=Config.tmpPath()))
      buildPathInfo = self.nodes.buildFrom(node, owner.id, revision = revision,
                                                           buildId = currentId,
                                                           identity = identity,
                                                           destination = buildPath,
                                                           options = networkOptions)
      if buildPathInfo is None:
        # Could not pull
        return None

      buildPath = buildPathInfo['buildPath']
      package = buildPathInfo['buildPackagePath']

      # Verify it
      # Get the hash of the build
      if verify:
        if 'signature' not in build:
          DiscoverManager.Log.warning("Build rejected due to missing signature.")
          continue

        signatureInfo = build['signature']
        published = Datetime.from8601(build.get('published'))
        signed = Datetime.from8601(signatureInfo.get('signed'))
        verifyKeyId = signatureInfo.get('key', '')
        signature = b''
        if (signatureInfo.get('format') == "PKCS1_v1_5" and signatureInfo.get('encoding') == "base64") or \
           (signatureInfo.get('format') == "Ed25519" and signatureInfo.get('encoding') == "base64"):
          signature = base64.b64decode(signatureInfo.get('data', '').encode('utf-8'))
          signature_type = signatureInfo.get('format') or "PKCS1_v1_5"
          signature_digest = signatureInfo.get('digest') or "SHA512"

        try:
          _ = self.keys.verifyingKeyFor(build['identity'], verifyKeyId)
        except KeyIdentityUnknownError:
          self.retrieveIdentity(build['identity'], nodes = [node])

        if not self.builds.verify(owner, task, build['identity'], verifyKeyId, signature, signature_type, signature_digest, published, signed, buildPath = buildPath):
          DiscoverManager.Log.warning("Build rejected due to signature mismatch.")
          continue

      DiscoverManager.Log.noisy(f"Discovered build {currentId}.")

      # Pull build log
      buildLogDir = os.path.realpath(tempfile.mkdtemp(prefix="occam-"))
      buildLogPath = self.nodes.buildLogFrom(node, owner.id, revision = revision,
                                                             buildId = currentId,
                                                             destination = buildLogDir,
                                                             options = networkOptions)

      if buildLogPath is None:
        # Could not pull
        return None

      # Store it
      self.builds.write.pull(owner, build, buildPath, buildLogPath, task, buildPackagePath = package)

      # Delete the temporary path
      import shutil
      shutil.rmtree(buildPath)
      shutil.rmtree(buildLogDir)

      # Pull Run-time Dependencies from build
      self.pullRuntimeDependencies(node, task, person = person,
                                               pending = pending)

      # Success
      return build

    return None

  def retrieveIdentityIfMissing(self, uri, person, nodes=None):
    """ Checks to see if we have the keys for the given identity stored
    locally, and retrieves the identity if not.
    """

    if not self.keys.retrieve(uri):
      self.retrieveIdentity(uri, person=person, nodes=nodes)

  def discoverTask(self, id, revision=None, person = None, pending = None, networkOptions = None):
    """ Discovers a task and pulls the objects that are referenced.

    This is used in the case when you have a compute node that needs to prepare
    a task for execution.

    Arguments:
      id (String): The object identifier for the task.
      revision (String): The revision of the task, if known.
      person (Object): The Person retrieving the task.
      pending (dict): A record of all pending pulls.

    Returns:
      Object: The resolved task object.
    """

    # Add the task to the pending block
    pending = pending or {}
    pending[f"{id}@{revision}"] = True

    # Gather nodes for this id
    nodes = self.nodesFor(id)

    # Attempt to find the task object itself
    task = self.discover(id, revision = revision,
                             person = person,
                             nodes = nodes,
                             pending = pending,
                             networkOptions = networkOptions)

    # We found the task
    if task:
      # Pull the task objects
      self.pullTask(nodes, task, person = person,
                                 pending = pending,
                                 networkOptions = networkOptions)

    return task

  def nodesFor(self, id):
    """ Determines a set of nodes that report having the specified object.

    Arguments:
      id (String): The identifier string for the object sought after.

    Returns:
      list: A set of NodeRecord elements that describe candidate nodes.
    """

    nodes = None
    if self.network.isURL(id):
      # If it is a URL, we prioritize the given node
      parts = self.network.parseURL(id)

      # We discover the node
      node = self.nodes.discover(parts.scheme + "://" + parts.netloc)
      if node:
        # If that is an available node, we make it our list
        nodes = [node]

    if nodes is None:
      id = self.idFromURI(id)
      nodes = self.nodesWithId(id)

    return nodes

  def idFromURI(self, id):
    """ Pulls out the identifier from a URI.

    The URI can be just an identifier string. In this case, that same string is
    returned.

    Arguments:
      id (String): The identifier of the object specified as a URI.

    Returns:
      String: The raw identifier part of the URI.
    """

    if self.network.isURL(id):
      # If it is a URL, we prioritize the given node
      parts = self.network.parseURL(id)

      # Make the id the first section of the path
      try:
        id = parts.path.split('/')[1]
      except:
        DiscoverManager.Log.error(
          f"Determined uuid to be a URL, but could not parse: {parts.path}"
        )
        return None

    return id

  def discover(self, id, revision=None,
                         version=None,
                         withDependencies = True,
                         withBuild = True,
                         withBuildDependencies = False,
                         nodes = None,
                         networkOptions = None,
                         person = None,
                         pending = None):
    """ Discovers an object on the network.

    Uses the various storage backends to hopefully find and retrieve the object
    information. Returns the Object that has been discovered.

    Arguments:
      id (String): The identifier or URI of the object to discover.
      revision (String): The revision of the object.
      version (String): The version to satisfy, or None to choose the latest.
      withBuild (str or bool): If assigned a string, the string indicates the
                               build ID. When True, pulls a binary build
                               package for the object.
      withBuildDependencies (bool): Pulls dependencies required to build the
                                    object.
      nodes (list): The list of NodeRecord elements describing candidates to
                    pull from.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.
      person (Object): The actor pulling the object.
      pending (dict): A record of all pending pulls.

    Returns:
      Object: The resolved, discovered object.
    """

    from occam.connection_logic import retryOnReset
    # Gather nodes for this id
    if nodes is None:
      nodes = self.nodesFor(id)

    # Get the canonical id
    id = self.idFromURI(id)

    DiscoverManager.Log.noisy(f"Attempting to discover {id} {version or ''}{('@' + revision) if revision else ''}")

    discoveryOptions = DiscoveryOptions(withBuild = withBuild,
                                        withDependencies = withDependencies,
                                        withBuildDependencies = withBuildDependencies)

    # Start asking the hosts for object metadata
    for node in nodes:
      # TODO: Optionally, we can also start discovering the nodes via:
      #   self.nodes.discover(host)  # ... for host in hosts

      # TODO: We can maybe select from the list any nodes we already know
      #   or trust

      # Get the high level information for this object.
      try:
        rf = lambda: self.nodes.statusFrom(node = node,
                                           id = id,
                                           revision = revision,
                                           person = person,
                                           options = networkOptions)
        objStat = retryOnReset(rf)

        if objStat is None:
          continue

        # Discover the identity of this object owner.
        uri = objStat.get("identity")
        rf = lambda: self.retrieveIdentityIfMissing(uri, person, nodes = [node])
        retryOnReset(rf)

        rf = lambda: self.pullObject(node,
                                     objStat,
                                     id = id,
                                     revision = revision,
                                     version = version,
                                     discoveryOptions = discoveryOptions,
                                     networkOptions = networkOptions,
                                     person = person,
                                     pending = pending)
        if objStat.get('type') == "resource":
          rf = lambda: self.pullResource(node,
                                         objStat,
                                         id = id,
                                         revision = revision,
                                         networkOptions = networkOptions,
                                         person = person)
        obj = retryOnReset(rf)

        # TODO: How do we know which answer is the best between all of the
        # nodes (or should we be using an aggregate of all of this
        # information)? They all may have different histories for these
        # objects in theory.
        #
        # For now return the first valid object we find on the federation.
        if obj:
          return obj
      except:
        continue

    # Could not find the object (nor retrieve it)
    return None

  def discoverBuild(self, id, revision=None,
                              version=None,
                              nodes=None,
                              networkOptions=None,
                              person=None,
                              pending=None):
    """ Discovers an object build on the network.

    Uses the various storage backends to hopefully find and retrieve the object
    build information.

    Arguments:
      id (String): The identifier or URI of the object to discover a build.
      revision (String): The revision of the object to target.
      version (String): The version to satisfy, or None to choose the latest.
      nodes (list): The list of NodeRecord elements describing candidates to
                    pull from.
      networkOptions (NetworkOptions): The set of options to override all network
                                       requests. Generally only useful with URIs.
      person (Object): The actor pulling the object.
      pending (dict): A record of all pending pulls.

    Returns:
      dict: The build information or None if the build was not discovered.
    """

    DiscoverManager.Log.noisy(f"Attempting to discover build for {id} {version or ''}{('@' + revision) if revision else ''}")

    hosts = None

    if nodes is None:
      nodes = self.nodesFor(id)

    # Get the canonical id
    id = self.idFromURI(id)

    # Start asking the hosts for object metadata
    for node in nodes:
      obj = self.objects.retrieve(id = id, revision = revision, version = version)
      build = self.pullBuild(node, obj,
                             person = person,
                             pending = pending,
                             networkOptions = networkOptions)
      return build

    return None

def network(name):
  """ This decorator will register the given class as a discovery backend.
  """

  def register_network(cls):
    DiscoverManager.register(name, cls)
    cls = loggable("DiscoverManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_network
