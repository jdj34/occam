# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import socket
import select
import json

# To get the local hostname

from occam.log import loggable

from occam.manager import manager
from occam.config import Config

from urllib.error import URLError
from urllib.request import Request, URLopener, urlopen, HTTPError
from http.client import HTTPConnection
from urllib.parse import urlparse, quote, parse_qs

class HTTPSConnection(HTTPConnection):
  pass

@loggable
@manager("network")
class NetworkManager:
  """ This OCCAM manager handles downloading resources from the network.
  """

  def __init__(self):
    """ Initialize the network manager.
    """

    self.ports = {}
    self.portStart = 30001
    self.portEnd = 31000

    # Ensure that the port manager path is created
    self.portPath = self.configuration.get('paths', {}).get('ports', os.path.join(Config.root(), "ports"))
    if not os.path.exists(self.portPath):
      os.mkdir(self.portPath);

  def hostname(self):
    """ Returns the local hostname.
    """

    return socket.gethostname()

  def ip4(self):
    """ Returns the local ip address.
    """

    import socket
    # This is a frustratingly difficult problem.
    # From: https://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
    try:
      ret = (([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")] or [[(s.connect(("8.8.8.8", 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) + ["127.0.0.1"])[0]
    except (IndexError, ValueError, OSError):
      ret = None

    return ret

  def ip6(self):
    """ Returns the local ipv6 address.
    """

    try:
      ret = (([[(s.connect(("2001:4860:4860::8888", 53, 0, 0)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)]][0][1]]) + ["::1/128"])[0]
    except (IndexError, ValueError, OSError):
      ret = None
      
    return ret

  def parseURL(self, url):
    """ Retrieves the urlparts of the given url.

    Returns:
      urllib.parse.ParseResult: An object namespace that holds several
        properties that correspond to the parts of the url.
        Those properties are "scheme", "netloc", "path, "params", "query", and
        "fragment". More information can be found in the python documentation
        for urllib.parse.
    """

    return urlparse(url)

  def getFTP(self, url):
    """ Retrieves the given document over FTP.
    """

    # For pulling network http resources
    from ftplib import FTP

    urlparts = urlparse(url)
    path = os.path.dirname(urlparts.path)
    filename = os.path.basename(urlparts.path)

    ftp = FTP(urlparts.hostname)
    NetworkManager.Log.write(ftp.login())

    # Force binary mode
    NetworkManager.Log.write(ftp.sendcmd("TYPE I"))

    # Change directory
    NetworkManager.Log.write(ftp.cwd(path))

    # Retieve file
    socket, size = ftp.ntransfercmd('RETR %s' % (filename))

    # We must maintain the ftp connection and the socket for the incoming buffer
    class Socket:
      def __init__(self, socket, ftp):
        self.socket = socket
        self.ftp    = ftp

      def read(self, *args):
        return self.socket.recv(*args)

    # Return the buffer as a byte stream
    return Socket(socket, ftp), 'application/octet-stream', size

  def craftOccamPayload(self, url, headers=None):
    """ Returns the occam payload for the given Occam command url.

    Arguments:
      url (str): The URL representing the Occam command.

    Returns:
      dict: The command payload.
    """

    # Take apart the URL
    parts = self.parseURL(url)

    # Get the raw arguments from the path
    arguments = parts.path[1:].split('/')
    from urllib.parse import unquote
    arguments = [unquote(arg) for arg in arguments]

    # The first two 'arguments' are the component and command respectively
    component = arguments[0]
    command = arguments[1]

    # The rest are the normal positional arguments
    arguments = arguments[2:]

    # Now, take the query parameters and form the options set
    params = parse_qs(parts.query, keep_blank_values=True)

    options = {}
    for key, values in params.items():
      # We want to send stdin as actual stdin, not a really long option value.
      if len(key) == 1:
        key = f"-{key}"
      else:
        key = f"--{key}"

      # Interpret empty values as flags.
      if values[0] == '':
        options[key] = True
      else:
        options[key] = values[0]

    # Set token
    if headers:
      if headers.get('X_OCCAM_TOKEN'):
        options['-T'] = headers.get('X_OCCAM_TOKEN')
      if headers.get('X_OCCAM_PROCESSING_TAG'):
        options['-P'] = headers.get('X_OCCAM_PROCESSING_TAG')

    return {
      "component": component,
      "command": command,
      "arguments": arguments,
      "options": options
    }

  def postOccam(self, url, data, accept, headers=None, promoted=False):
    """ Post to another Occam instance's daemon.

    This can be used to submit commands to another node's daemon for processing. 

    The URL given is a crafted one that can be turned into an Occam command.

    For example, the command:

      occam objects view QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6 --start 42 --length 420

    Would be encoded as:

      occam://host:port/objects/view/QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6?start=42&length=420

    Arguments:
      url (String): URL to which to POST.
      data (String): Data to send as stdin to the command.
      accept (String): MIME type accepted as a response.
      headers (dict): Headers for the post request. These get translated into
        command line flags.
      promoted (bool): Whether or not the command should be promoted.

    Returns:
      (ConnectionWrapper, String, int): ConnectionWrapper that wraps the socket
        connection so it may be treated like a file handle, a string indicating
        the response type, and the size of the data returned.
    """
    response = None
    content_type = None

    # Parse out the socket address from the URL.
    parts = self.parseURL(url)

    port = 32000
    host = parts.netloc
    if ':' in parts.netloc:
      hostPortParts = parts.netloc.split(':')
      host = hostPortParts[0]
      port = int(hostPortParts[1])

    command = self.craftOccamPayload(url, headers = headers)
    if data is not None:
      command['stdin'] = len(data)

    if promoted:
      command['promote'] = True

    payload = (json.dumps(command) + "\n").encode('utf-8')

    # Open a socket
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.connect((host, port,))

    # Send data
    connection.sendall(payload)

    # Send 'stdin'
    if data is not None:
      connection.sendall(data)

    # Await result
    def readline():
      data = True
      buffer = b''
      while data:
        data = connection.recv(1)
        if data and data == b'\n':
          break
        buffer += data

      if not buffer:
        raise ConnectionResetError("Connection reset while reading POST response.")
      return int(buffer)

    try:
      headerSize = readline()

      # Read header
      header = json.loads(connection.recv(headerSize))

      # Read initial data length
      size = readline()
    except ConnectionResetError:
      # The caller needs to see this exception in order to know there is
      # opportunity for a retry.
      raise
    except Exception as e:
      NetworkManager.Log.error(f"Error while reading POST response: {e}")
      return None, None, None

    # Promoted commands do not have a 'size'
    if promoted:
      size = -1

    # We are now at the data in 'connection'
    # We leave open and return the socket.
    class ConnectionWrapper():
      def __init__(self, connection, size):
        self._socket = connection
        self._size = size
        self._position = 0

      def fileno(self):
        return self._socket.fileno()

      def write(self, data):
        """ Writes to the socket.
        """
        self._socket.sendall(data)

      def read(self, length = None):
        """ Reads from the socket the amount specified by length.

        If length is None, it will read any data that is available, blocking
        until there is data.

        Returns:
          bytes: The data read or an empty byte-string if there is no data left to read.
        """

        # We read everything
        if self._position == self._size:
          return b''

        if length is None:
          # Receive ALL data
          buffer = b''
          data = True
          while self._position != self._size and data:
            r, _, _ = select.select([self._socket], [], [])

            data = None
            if r:
              data = self._socket.recv(32000)

            if not data:
              break

            buffer += data
            self._position += len(data)

          return buffer

        buffer = self._socket.recv(length)
        self._position += len(buffer)
        return buffer

    ret = ConnectionWrapper(connection, size)

    # On error, return None since the resource cannot be found
    if header.get('status') == 'error':
      ret = None

    content_type = "application/octet-stream"
    return ret, content_type, size

  def getOccam(self, url, accept, scheme="occam", suppressError=False,
                                  doNotVerify=False, cert=None, headers=None):
    """ Retrieves the given document over Occam's daemon protocol.

    The URL given is a crafted one that can be turned into an Occam command.

    For example, the command:

      occam objects view QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6 --start 42 --length 420

    Would be encoded as:

      occam://host:port/objects/view/QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6?start=42&length=420
    """

    # The occam protocol 'GET' is just a post with no stdin data.
    return self.postOccam(url, None, accept, headers = headers)

  def getSSLContext(self, cafile = None, doNotVerify = False):
    """ Get the SSL context that should be used to connect over SSL/TLS.

    Arguments:
      cafile (String): Path to a concatenated .pem file containing all CAs
        allowed to act as a chain of trust for servers we connect to over TLS.

        If cafile is None, then the system will attempt to use the sources
        specified in the configuration's trustedCAs list. If no trustedCAs are
        specified the system default CAs will be used (these are determined at
        OpenSSL compile time).

      doNotVerify (bool): Whether or not connections should check for a valid
        TLS certificate.
    """
    import ssl

    # This results in a context that respects only the CAs in the cafile
    # specified, or one that respects the system's CAs if no cafile was
    # specified.
    ctx = ssl.create_default_context(cafile = cafile)

    if doNotVerify:
      ctx.check_hostname = False
      ctx.verify_mode = ssl.CERT_NONE

    if cafile is not None or doNotVerify:
      return ctx

    # If no cafile is specified use the configuration's trustedCAs.
    trustedCAs = self.configuration.get('trustedCAs', []) or []

    if len(trustedCAs) > 0:
      # Start out trusting no CAs.
      ctx = ssl.SSLContext(ssl.PROTOCOL_TLS)
      ctx.verify_mode = ssl.CERT_REQUIRED
      ctx.check_hostname = True

      # Add each source of trusted CAs.
      for capath in trustedCAs:
        try:
          if os.path.isdir(capath):
            ctx.load_verify_locations(capath=capath)
          else:
            ctx.load_verify_locations(cafile=capath)
        except (ssl.SSLError, FileNotFoundError) as e:
          NetworkManager.Log.error(f"Can't load CAs in {capath}: {e}")
    return ctx

  def getHTTP(self, url, accept, scheme=None, suppressError=False,
                                 doNotVerify=False, cert=None, headers=None):
    """ Retrieves the given document over HTTP.
    """

    data = None

    response = None
    content_type = None

    request = Request(url)
    if scheme is not None:
      request.type = scheme

    request.add_header('Accept', accept)
    request.add_header('User-Agent', 'occam')

    for k, v in (headers or {}).items():
      request.add_header(k, v)

    ctx = self.getSSLContext(cafile = cert, doNotVerify = doNotVerify)
    response = None
    size = 0
    content_type = None

    try:
      response = urlopen(request, context=ctx)
    except HTTPError as e:
      if e.code == 404:
        if not suppressError:
          NetworkManager.Log.error("cannot find resource: HTTP code %s" % (e.code))
      else:
        if not suppressError:
          NetworkManager.Log.error("cannot open resource: HTTP code %s" % (e.code))
    except URLError as e:
      if "unable to get local issuer certificate" in str(e.reason):
        # This can happen either when the server doesn't send the intermediate
        # certificates correctly, or if the root CA is unknown to the machine
        # doing the verification.
        NetworkManager.Log.error(f"cannot open resource: could not verify "
                                 f"SSL certificate of {url}: check that root CA "
                                 f"is present on local machine")
      elif not suppressError:
        NetworkManager.Log.error(f"cannot open resource: Network down {e.reason}")
    except socket.timeout as e:
      if not suppressError:
        NetworkManager.Log.error(f"cannot find resource: Socket timeout {e.strerror}")

      content_type = dict(response.info()).get('Content-Type', 'application/octet-stream')

    if response:
      try:
        content_type = content_type[0:content_type.index(';')]
      except (IndexError, ValueError, AttributeError):
        pass
      try:
        size = int(response.headers.get('Content-Length'))
      except (TypeError, ValueError):
        size = 0

    return response, content_type, size

  def get(self, url, accept='application/json', scheme=None, suppressError=False, doNotVerify=False, cert=None, headers=None):
    """ Retrieves the given document over HTTP or FTP or raw Occam Protocol.
    """

    urlparts = urlparse(url)

    if scheme is None:
      if urlparts.scheme == "":
        scheme = 'http'
      else:
        scheme = urlparts.scheme

    if scheme == "ftp":
      return self.getFTP(url)
    elif scheme == "occam":
      return self.getOccam(url, accept, scheme, suppressError, doNotVerify, cert, headers=headers)
    else:
      return self.getHTTP(url, accept, scheme, suppressError, doNotVerify, cert, headers=headers)

  def post(self, url, data, accept='application/json', scheme=None, suppressError=False, doNotVerify=False, cert=None, headers=None):
    """ Transmits data to the given URL over HTTP or FTP or raw Occam protocol.
    """

    urlparts = urlparse(url)

    if scheme is None:
      if urlparts.scheme == "":
        scheme = 'http'
      else:
        scheme = urlparts.scheme

    if scheme == "ftp":
      raise NotImplementedError("FTP post is not supported")
      #return self.postFTP(url)
    elif scheme == "occam":
      return self.postOccam(url, data, accept, headers=headers)
    else:
      raise NotImplementedError("HTTP post is not supported")

  def getJSON(self, url, accept="application/json", scheme=None, suppressError=False, cert=None, doNotVerify=False, headers=None):
    import codecs

    response, content_type, size = self.get(url, accept, scheme, suppressError, cert=cert, doNotVerify=doNotVerify, headers=headers)

    if response is None:
      return None

    reader = codecs.getreader('utf-8')
    data = json.load(reader(response))

    return data

  def isGitAt(self, url, cert=None):
    """ Returns True when the given url points to a git repository.
    """
    from occam.git_repository import GitRepository
    return GitRepository.isAt(url, cert=cert)

  def isObjectAt(self, url):
    pass

  def isURL(self, url):
    if not url:
      return False

    import re
    return re.match(r'^[a-z]+://', url)

  def allocatePort(self):
    """ Allocates a free port from the ranged configured on the system.

    This ensures that ports are not used more than once by an Occam
    process or virtual machine.
    """

    ret = None
    for port in range(self.portStart, self.portEnd):
      if not port in self.ports:
        path = os.path.join(self.portPath, str(port))
        if not os.path.exists(path):
          f = open(str(path), 'w+')
          f.close()

          self.ports[port] = True
          ret = port

          break

    return ret

  def freePort(self, port):
    """ Deallocates a port that was previously allocated by a call to allocatePort.
    """

    if port in self.ports:
      path = os.path.join(self.portPath, str(port))
      if os.path.exists(path):
        os.remove(path)

      del self.ports[port]

    return True
