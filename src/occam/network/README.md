# The Network Component

This component handles sending GET/POST requests to the server's network, as
well as allocation and deallocation of ports for intra-object network
communication.
