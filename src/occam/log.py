# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import i18n

from io import BytesIO

# json logging
# {
#   "type": "normal",
#   "message": "...",
#   "id": "...",
#   "params": { }
# }
# {
#   "type": "warning",
#   "message": "...",
#   "id": "...",
#   "params": { }
# }

# Log types:

# normal   - average, useful log info about what's going on / what was requested
# debug    - noisy info that is less useful for the day to day. for devs
# error    - error conditions!
# warning  - warning condition
# external - from other subprocesses we don't control
# done     - when things finish successfully

class Log:
  class Header:
    INVALID = 0
    STATUS  = 1

  class Action:
    INVALID = 0

  class Type:
    # Invalid or unknown type
    INVALID  = None

    # Normal everyday messages (useful!) (*)
    # This is just the normal output. This may have an ID when the logged
    # output is known. For instance, we might see a "normal" log type with
    # an id of 1234 which might correspond to 'building object Foo'. It will
    # have a params key 'object' that describes the object the line says we
    # are building.
    NORMAL   = "normal"

    # Debugging messages (not useful. for devs) (*)
    # May contain an ID and some parameters, but probably won't have i18n
    # support.
    DEBUG    = "debug"

    # Error messages (oh no!) (!)
    ERROR    = "error"

    # Warning messages (oh meh?? maybe oh no?) (!)
    WARNING  = "warning"

    # External messages (docker being noisy) ([])
    # These messages generally do not have ids. They are stdout/stderr of a
    # subprocess that OCCAM spawns to do some related task or run a VM.
    EXTERNAL = "external"

    # Completion messages (yay. success!!) (*)
    # These messages will appear when a command completes successfully. They
    # will have an associated DoneType for an ID.
    DONE     = "done"

  class DoneTypes:
    INVALID = 0 # Invalid or unknown type

    RUN     = 1 # Completed a run task
    BUILD   = 2 # Completed a build task
    CONSOLE = 3 # Completed a console task

  # Metadata per-thread about the logging destination
  threadInformation = {}

  @staticmethod
  def threadInfo():
    from threading import get_ident

    threadID = get_ident()

    if not threadID in Log.threadInformation:
      # Default Log settings
      Log.threadInformation[threadID] = {
        "first_line":     False,
        "verbose":        False,
        "store_output":   False,
        "errorMessage":   "",
        "stored_output":  BytesIO(),
        "percentMessage": "",
        "stdout":         sys.stdout.buffer,
        "stderr":         sys.stderr.buffer,
        "withinPercent":  False,
        "lastPercent":    -1,
        "jsonLogging":    False
      }

    return Log.threadInformation[threadID]

  @staticmethod
  def clear():
    """ Clears the stored output.
    """

    Log.threadInfo()["stored_output"] = BytesIO()

  @staticmethod
  def print(message, end="\n", file=sys.stdout.buffer):
    if Log.threadInfo()["withinPercent"]:
      Log.threadInfo()["withinPercent"] = False
      Log.threadInfo()["lastPercent"] = -1
      if not Log.threadInfo()["jsonLogging"]:
        file.write("\n".encode('utf-8'))

    if isinstance(message, str):
      message = message.encode('utf-8')
    file.write(message)
    file.write(end.encode('utf-8'))
    if hasattr(sys.stdout, 'buffer'):
      if file == sys.stdout.buffer:
        sys.stdout.flush()
      elif file == sys.stderr.buffer:
        sys.stderr.flush()
    else:
      file.flush()

  @staticmethod
  def initialize(options, storeOutput = False, stdout = None, stderr = None):
    if stdout is None:
      stdout = sys.stdout.buffer
    if stderr is None:
      stderr = sys.stderr.buffer

    if options.verbose:
      Log.threadInfo()["verbose"] = True
    if options.logtype == "json":
      Log.threadInfo()["jsonLogging"] = True
    Log.threadInfo()["store_output"] = storeOutput
    Log.threadInfo()["stdout"] = stdout
    Log.threadInfo()["stderr"] = stderr

    Log.clear()

  @staticmethod
  def header(message=None, key=None, **kwargs):
    if key is None:
      key = "null"

    if message is None:
      message = i18n.t(key, **kwargs)

    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("header", message, key=key)
    else:
      if Log.threadInfo()["first_line"]:
        Log.print("", file=Log.threadInfo()["stderr"])

      Log.first_line = True
      Log.print(message, file=Log.threadInfo()["stderr"])

  @staticmethod
  def output(message, padding="  ", end="\n"):
    """ Outputs the given message.

    When the log is configured to store output, the output is stored in buffer
    to be output later.

    Arguments:
      message(str): The message that should eventually be output.
      padding(str): Padding that precedes the message. This padding is output
        over stderr to separate the message as it should be read by a user vs
        how it should be read from a pipe.
      end(str): A string that should be output following the message. Unlike
        padding, this is output over stdout and is effectively part of the
        message.
    """
    if Log.threadInfo()["store_output"]:
      Log.threadInfo()["stored_output"].write((str(message) + end).encode('utf8'))
    else:
      Log.print(padding, end="", file=Log.threadInfo()["stderr"])
      Log.print(message, end=end, file=Log.threadInfo()["stdout"])

  @staticmethod
  def pipe(data, length=None):
    """ Outputs the contents of data.

    When the log is configured to store output, the output is stored in buffer
    to be output later.

    Arguments:
      data (str, bytes, or file handle): Data to be written. If data is a file
        handle, its contents are read and written.
      length (int): If data is a file handle, how much data is expected to be
        read. When length is specified the file handle will be immediately be
        read by (length) bytes. Otherwise the file handle will be read in 1000
        byte increments until no data is read.
    """
    # Encode to bytes if the string wasn't already encoded.
    if isinstance(data, str):
      data = data.encode('utf-8')

    if isinstance(data, bytes):
      if length:
        data = data[0:length]
      if Log.threadInfo()["store_output"]:
        Log.threadInfo()["stored_output"].write(data)
      else:
        Log.threadInfo()["stdout"].write(data)
        if Log.threadInfo()["stdout"] is sys.stdout.buffer:
          sys.stdout.flush()
    else:
      # Data was a file handle object from which we should read.
      bytesRead = 0
      while (length is None or length != bytesRead):
        amount = 1000
        if length:
          amount = min(1000, length)

        buff = data.read(amount)

        if length and (len(buff) + bytesRead > length):
          buff = buff[0:length-bytesRead]

        bytesRead += len(buff)

        if len(buff) == 0:
          break

        if Log.threadInfo()["store_output"]:
          Log.threadInfo()["stored_output"].write(buff)
        else:
          Log.threadInfo()["stdout"].write(buff)
          if Log.threadInfo()["stdout"] is sys.stdout.buffer:
            sys.stdout.flush()

  def storedOutput():
    output = Log.threadInfo()["stored_output"]
    Log.threadInfo()["stored_output"] = BytesIO()

    output.seek(0, 0)
    return output

  def errorMessage():
    return Log.threadInfo()["errorMessage"]

  @staticmethod
  def writeJSON(type, message, params={}, key="null", context=None):
    import json

    Log.print(json.dumps({
      "type": type,
      "message": message,
      "key": str(key),
      "params": params,
      "context": context
    }), file=Log.threadInfo()["stderr"], end="\n")

  @staticmethod
  def write(message=None, end="\n", key=None, context=None, params={}, **kwargs):
    if key is None:
      key = "null"

    if message is None:
      message = i18n.t(key, **kwargs)

    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("normal", message, key=key, params=(params or kwargs), context=context)
    else:
      if context:
        message = "%s: %s" % (context, message)
      Log.print(" \x1b[1;37m*\x1b[0m %s" % (message), file=Log.threadInfo()["stderr"], end=end)

  @staticmethod
  def external(message, source):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("external", message, params={"source": source})
    else:
      Log.print(" \x1b[1;37m[\x1b[0m%s\x1b[1;37m]\x1b[0m: %s" % (source, message), file=Log.threadInfo()["stderr"])

  @staticmethod
  def noisy(message, end="\n", context=None):
    if Log.threadInfo()["verbose"]:
      if Log.threadInfo()["jsonLogging"]:
        Log.writeJSON("debug", message, context=context)
      else:
        if context:
          message = "%s: %s" % (context, message)
        Log.print(" \x1b[38;5;240m* %s\x1b[0m" % (message), file=Log.threadInfo()["stderr"], end=end)

  @staticmethod
  def warning(message, context=None):
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("warning", message, context=context)
    else:
      if context:
        message = "%s: %s" % (context, message)
      Log.print(" \x1b[1;33m! Warning: %s\x1b[0m" % (message), file=Log.threadInfo()["stderr"])

  @staticmethod
  def error(message=None, key=None, context=None, **kwargs):
    if key is None:
      key = "null"

    if message is None:
      message = i18n.t(key, **kwargs)

    Log.threadInfo()["errorMessage"] = str(message)
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("error", message, context=context, key=key, params=kwargs)
    else:
      if context:
        message = "%s: %s" % (context, message)
      Log.print(" \x1b[1;31m! Error: %s\x1b[0m" % (message), file=Log.threadInfo()["stderr"])

  @staticmethod
  def done(message=None, key=None, **kwargs):
    if key is None:
      key = "null"

    if message is None:
      message = i18n.t(key, **kwargs)

    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("done", message, key=key, params=kwargs)
    else:
      Log.print(" \x1b[1;32m* Done: %s\x1b[0m" % (message), file=Log.threadInfo()["stderr"])

  @staticmethod
  def event(message, values={}):
    # Only happens in JSON logging (or when verbose mode is set)
    if Log.threadInfo()["jsonLogging"]:
      Log.writeJSON("event", message, params=values)
    elif Log.verbose:
      Log.print(" \x1b[38;5;240m+ Event: %s\x1b[0m" % (message), file=Log.threadInfo()["stderr"])

  @staticmethod
  def writePercentage(message, context=None):
    Log.threadInfo()["percentMessage"] = message
    Log.threadInfo()["percentContext"] = context

    Log.updatePercentage(0, message, context)

  @staticmethod
  def updatePercentage(percent, message=None, context=None):
    message = message or Log.threadInfo()["percentMessage"]
    context = context or Log.threadInfo()["percentContext"]

    percent = min(percent, 100.0)

    percentCode = str(int(percent)).rjust(3, ' ')
    if percentCode == Log.threadInfo()["lastPercent"]:
      return

    Log.threadInfo()["withinPercent"] = False

    Log.threadInfo()["stderr"].write(b"\x1b[G")
    sys.stderr.flush()

    percentLine = "#" * min(round((float(percent) / 100.0) * 20.0), 20)
    percentLine = percentLine.ljust(20, '.')

    lastMessage = Log.threadInfo()["percentMessage"]
    message = message + (" " * (len(lastMessage) - len(message)))

    Log.write("[%s%%] [%s] %s" % (percentCode, percentLine, message), context=context, params={ "progress": percent }, end='')

    Log.threadInfo()["percentMessage"] = message
    Log.threadInfo()["lastPercent"] = percentCode;
    Log.threadInfo()["withinPercent"] = True

    sys.stderr.flush()

  @staticmethod
  def outputBlock(text, padding="  "):
    for line in text.split('\n'):
      printed = ""
      for word in line.split(' '):
        if printed != "" and len(printed) + len(word) > (80 - len(padding)):
          Log.output("%s" % (printed), padding="")
          printed = ""
        printed += word + " "
      Log.output("%s" % (printed), padding="")

def loggable(context):
  def _loggable(cls, context):
    """
    Decorator that you can add to a class to give it the ability to log with
    context. It will add a Log class to your given class that contains methods
    that connect to the main logger.

    @loggable
    class Foo:
      ... etc ...

      def bar():
        # Prints "Foo: message"
        Foo.Log.write("message")

      def bar():
        # Prints "Foo: message" when in verbose mode
        Foo.Log.noisy("message")

      def baz():
        # Prints "Error: Foo: message"
        Foo.Log.error("message")

      def bop():
        # Prints "Warning: Foo: message"
        Foo.Log.warning("message")

    """

    class LogWrapper:
      def threadInfo():
        return Log.threadInfo();

      def write(message=None, key=None, end="\n", **kwargs):
        Log.write(message=message, key=key, end=end, context=context, **kwargs)

      def output(message, padding="  ", end="\n"):
        Log.output(message, padding = padding, end = end)

      def noisy(message, end="\n"):
        Log.noisy(message, end=end, context=context)

      def error(message=None, key=None, **kwargs):
        Log.error(message=message, key=key, context=context, **kwargs)

      def warning(message):
        Log.warning(message, context=context)

      def header(message=None, key=None, **kwargs):
        Log.header(message=message, key=key, **kwargs)

      def writePercentage(message):
        Log.writePercentage(message, context=context)

      def updatePercentage(percent, message=None):
        Log.updatePercentage(percent, message, context=context)

      def storedOutput():
        return Log.storedOutput()

      def errorMessage():
        return Log.errorMessage()

      def pipe(data):
        return Log.pipe(data)

    cls.Log = LogWrapper
    return cls

  if isinstance(context, str):
    def _loggableContext(cls):
      return _loggable(cls, context + "[" + cls.__name__ + "]")

    return _loggableContext
  else:
    return _loggable(context, context.__name__)
