# The Manifests Component

This component manages the creation and querying of tasks to build or run
objects.

The manifest manager can take an object and, based on its metadata, produce a
task to build or run that object. This task is a manifest that describes what
other objects should be present on the virtual machine which ultimately runs
the artifact.

Task generation goes through a few phases. The first phase takes the object and
queries for a path through other artifacts that can be used to run it. For
instance, a Linux-based application describes itself as such, and then the
query will find a Linux environment to be used as a base for that object. In
other examples, the artifact may require a particular software environment.
Such as a jar file, which may need a Java runtime. In this case, the path is
found through querying the BackendManager which keeps track of the these
relationships between objects and "Provider" objects in order to facilitate
execution diversity.

The second phase is resolving dependencies for each object in the chain and
adding those dependencies (and their dependencies and so on) to the task. Many
times, these are described in terms of tags (or versions) which will use a
style of semantic versioning to resolve to specific artifact revisions for each
dependency (for example, a software library at version >= 2.0 will resolve to
revision "fa63ea96cb258b110d70eb") The revisions are written to the task (along
with the version) to lock in that particular version of the object used.

The third phase resolves the rules written within object metadata. Most objects
are simple and do not have such rules, but others may write entries in their
metadata that are to be filled once the task is generated. These are of the
form {{ x }} where x is some key within the task, delimited by dots (.).  When
these are encountered, they are parsed and replaced with the value indicated by
the key written within. For example, {{ input.0.file }} would be replaced with
the file attached as the first input of that object. In this case, you can see
why this is powerful. This is only known at the time of task generation.
Furthermore, it can be filled in differently at any point, allowing tasks to be
somewhat generic where input can be given later.

When tasks are generated, they might be stored as an Object themselves and
assigned an identifier. They can be pushed to federated storage and queried
from other machines. In some cases, taskFor() can be called to simply return a
generated task without storing it as an object.

