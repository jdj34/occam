# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import OrderedDict

from occam.log import loggable
from occam.config import Config
from occam.object import Object

from occam.manager import uses, manager

from occam.objects.manager       import ObjectResolveVersionError
from occam.objects.write_manager import ObjectWriteManager
from occam.versions.manager      import VersionManager
from occam.storage.manager       import StorageManager
from occam.builds.manager        import BuildManager
from occam.backends.manager      import BackendManager
from occam.nodes.manager         import NodeManager
from occam.databases.manager     import DatabaseManager
from occam.permissions.manager   import PermissionManager
from occam.system.manager        import SystemManager
from occam.services.manager      import ServiceManager

import os
import json
import copy
import shutil
import time
import datetime

from occam.semver import Semver

from occam.key_parser import KeyParser

@loggable
@uses(ObjectWriteManager)
@uses(VersionManager)
@uses(StorageManager)
@uses(BuildManager)
@uses(BackendManager)
@uses(NodeManager)
@uses(DatabaseManager)
@uses(PermissionManager)
@uses(SystemManager)
@uses(ServiceManager)
@manager("manifests")
class ManifestManager:
  """ This component manages the creation and querying of tasks to build or run
  objects.
  """

  @staticmethod
  def deepMerge(destination, source):
    """
    """

    # TODO: lists should probably be appended

    for key, value in source.items():
      if isinstance(value, dict):
        # get node or create one
        node = destination.setdefault(key, {})
        ManifestManager.deepMerge(node, value)
      else:
        destination[key] = value

    return destination

  def __init__(self):
    self.parser = KeyParser()

  def buildAll(self, object, local=False, penalties=None, tasks = None, seen = None, originalPenalties = None, desiredBy = None, person=None):
    """ Build an object and recursively builds any necessary objects.

    This will employ a strategic system to build only what is necessary. It will
    avoid pitfalls such as cyclic dependencies that can happen when version matches
    resolve unexpectedly.
    """

    # This will be the returned list
    if tasks is None:
      tasks = []

    if originalPenalties is None:
      originalPenalties = copy.deepcopy(penalties)

    # Penalties is a hash consisting of arrays of revision strings per uuid keys
    # This is passed around to object resolvers to disallow objects of certain
    #   revisions being chosen as version resolves and such.
    if penalties is None:
      penalties = {}

    # This simply keeps track of the objects we have current seen
    if seen is None:
      seen = {}

    # Issue a build of this object
    while True:
      try:
        info = self.objects.infoFor(object)
        ownerInfo = info
        ManifestManager.Log.write(f"Building {info.get('type', 'object')} {info.get('name', 'unknown')} @ {object.revision}")

        if 'build' not in info:
          ownerInfo = self.objects.ownerInfoFor(object)
          if 'build' in ownerInfo:
            object = self.objects.ownerFor(object, person = self.person)

        # If this is already in penalties, then we are already building it
        # Just fall out as unresolved
        if object.id in penalties and object.revision in penalties[object.id]:
          return tasks, False

        if not object.id in penalties:
          penalties[object.id] = []
        if not object.revision in penalties[object.id]:
          penalties[object.id].append(object.revision)

        if not object.id in seen:
          seen[object.id] = {}
        if not object.revision in seen[object.id]:
          seen[object.id][object.revision] = desiredBy

        # Keep a copy of the penalties
        #oldPenalties = copy.deepcopy(penalties)

        task = self.build(object    = object,
                          id        = object.id,
                          revision  = object.revision,
                          local     = local,
                          penalties = penalties,
                          person    = person)
        tasks.append(task)
        break
      except BuildRequiredError as e:
        info = self.objects.infoFor(e.requiredObject)
        ManifestManager.Log.write(f"Cannot find built copy of {info.get('type', 'object')} {info.get('name', 'unknown')}")
        tasks, resolved = self.buildAll(e.requiredObject, penalties = penalties, tasks = tasks, seen = seen, originalPenalties = originalPenalties, desiredBy = object, person = person)
        if not isinstance(resolved, bool) or not resolved == True:
          return tasks, resolved
      except DependencyUnresolvedError as e:
        ManifestManager.Log.write(f"Cyclic dependency detected on {e.objectInfo.get('type', 'object')} {e.objectInfo.get('name', 'unknown')}")
        for k in list(penalties.keys()):
          if not k in originalPenalties:
            del penalties[k]
        penalties.update(originalPenalties)

        # Add all objects that requested this object to penalties
        owner = self.objects.ownerFor(e)
        owner_id = owner.id
        for revision, requestor in seen[owner_id].items():
          info = self.objects.infoFor(requestor)
          ownerInfo = info

          if 'build' not in info:
            ownerInfo = self.objects.ownerInfoFor(requestor)
            if 'build' in ownerInfo:
              requestor = self.objects.ownerFor(requestor, person = self.person)
          ManifestManager.Log.write(f"Penalizing {info.get('type', 'object')} {info.get('name', 'unknown')} @ {object.revision}")

          # Add the item that added this object originally
          if not object.id in penalties:
            penalties[object.id] = []
          if not requestor.revision in penalties[object.id]:
            penalties[object.id].append(requestor.revision)

        return tasks, 1

    return tasks, True

  def build(self, object=None, id=None, revision=None, local=False, penalties=None, backend=None, locks=None, person=None):
    """ Builds the given object by any means.
    """

    objectInfo = {}

    if object:
      objectInfo = self.objects.infoFor(object)
      id = object.id

      if not revision:
        revision = object.revision

    task = None
    if object is None:
      object = self.objects.retrieve(id=id, revision=revision, person=person)

    if object is None:
      ManifestManager.Log.error("Object not found.")
      return -1

    objectInfo = self.objects.infoFor(object)
    id = object.id

    if not "build" in objectInfo:
      ManifestManager.Log.write("No build information for this object.")
      return None

    task = self.taskFor(object, section="build", penalties=penalties, backend=backend, locks=locks, person=person)

    if task is None:
      ManifestManager.Log.write("Could not produce a task to build this object.")
      return None

    taskName = f"Task to build {objectInfo.get('name')} on {task.get('backend')}"
    taskObject = self.objects.write.create(name        = taskName,
                                           object_type = "task",
                                           subtype     = "build",
                                           info        = task)
    if person:
      db_obj = self.objects.write.store(taskObject, identity = person.identity)
      taskObject.id  = db_obj.id
      taskObject.uid = db_obj.uid

    # Set build task permissions to be permissive
    self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    return taskObject

  def run(self, object=None, id=None, revision=None, local=False, arguments=None, inputs=None, penalties=None, backend=None, generator=None, service=None, services=None, preferred=None, locks=None, person=None):
    """ Runs the given object.

    Args:
      service (string): The service to run for this object, given that it provides that service. Can be None.
    """

    if not person:
      ManifestManager.Log.error("Cannot create task: must be logged in.")
      return None

    ownerInfo = self.objects.ownerInfoFor(object)
    info      = self.objects.infoFor(object)

    if id is None:
      id = self.objects.idFor(object, person.identity)
      object.id = id

    if revision is None:
      revision = object.revision

    # TODO: Pull out the build infos if this object requires a build
    #       and use that task to run this object with the built version.
    buildTask = None
    if local == False and ("build" in info or "build" in ownerInfo) and object.link is None:
      buildTasks = self.builds.retrieveAll(object)
      if buildTasks is None:
        buildTasks = []

      if "build" in ownerInfo and "build" not in info:
        buildTasks.extend(self.builds.retrieveAll(self.objects.ownerFor(object, person = person)) or [])

      for buildRecord in buildTasks:
        # Build the object
        # TODO: issue build command
        buildTask = self.objects.retrieve(buildRecord.build_id, buildRecord.build_revision, person=person)

        if buildTask is None:
          continue

        # If we found a suitable build, just quit looking
        buildTask = self.objects.infoFor(buildTask)
        buildTask['id'] = buildRecord.build_id
        break

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error(f"Object {info.get('type')} {info.get('name')} must be built first.")
        return None

    if local == True and "build" in self.objects.infoFor(object):
      # Look for the buildtask in the local path
      buildTaskPath = os.path.join(object.path, ".occam", "local", "object.json")
      import json

      try:
        with open(buildTaskPath, "r") as f:
          buildTask = json.load(f)
      except:
        pass

      if buildTask is None:
        # TODO: discover the build task perhaps
        ManifestManager.Log.error("Object must be built locally first.")
        return None

    task = None

    # Get the object to run
    if object is None:
      ManifestManager.Log.error("Object not found.")
      return None

    task = self.taskFor(object, buildTask=buildTask, penalties = penalties, backend = backend, generator=generator, inputs=inputs, service=service, services=services, preferred=preferred, locks=locks, person=person)

    # Store task
    objectInfo = self.objects.infoFor(object)

    environment = task.get('environment')
    architecture = task.get('architecture')

    taskName = f"Task to run {objectInfo.get('name')} on {task.get('backend')}"
    if environment and architecture:
      taskName = f"Task to run {objectInfo.get('name')} on {environment}/{architecture}"

    taskObject = self.objects.write.create(name        = taskName,
                                           object_type = "task",
                                           subtype     = "run",
                                           info        = task)
    db_obj = self.objects.write.store(taskObject, identity = person.identity)
    taskObject.id  = db_obj.id
    taskObject.uid = db_obj.uid

    # For now the permissions will be permissive:
    # This task should belong to the object, however, and inherit permissions.
    self.permissions.update(id = taskObject.id, canRead=True, canWrite=True, canClone=True)

    if task is None:
      ManifestManager.Log.error("Could not determine how to run this object.")
      return None

    return taskObject

  def isBuildable(self, object):
    """ Returns True if the given object is buildable.
    """

    dependencyInfo = self.objects.infoFor(object)
    dependencyOwnerInfo = self.objects.ownerInfoFor(object)
    return 'build' in dependencyInfo or 'build' in dependencyOwnerInfo

  def retrieveBuildTaskFor(self, object, preferred, person = None):
    """ Retrieve the task object that built the given object.
    """

    buildId = None
    buildRevision = None
    buildTask = None
    if not self.isBuildable(object):
      return None

    # Look up a build or report that we need to build this object
    objectInfo = self.objects.infoFor(object)
    buildingObject = object
    if 'build' not in objectInfo:
      buildingObject = self.objects.ownerFor(object, person = person)

    buildingObjectInfo = self.objects.infoFor(buildingObject)
    buildIds = [{"id": x.build_id, "revision": x.build_revision} for x in self.builds.retrieveAll(buildingObject, preferred)]

    # For every build we know, we attempt to find that build's content
    for build in buildIds:
      path = self.objects.buildPathFor(buildingObject, build["id"])

      if path is not None:
        buildTask = self.objects.retrieve(id = build["id"], revision = build["revision"], person = person)

        if buildTask:
          # TODO: check to make sure locked versions match our expectations
          buildId = buildTask.id
          buildRevision = buildTask.revision
          buildTask = self.objects.infoFor(buildTask)
          break

    # TODO: when buildTask is None!
    if buildId is None:
      raise BuildRequiredError(objectInfo, buildingObject, f"cannot find a built task for {objectInfo.get('type')} {objectInfo.get('name')}")

    buildTask['id'] = buildId
    buildTask['revision'] = buildRevision

    return buildTask

  def completeInitSection(self, currentInit, currentProvider):
    """ Will finalize the given init section based on the given provider object info.

    Modifies currentInit in place.

    Returns:
      currentInit
    """

    providesInfo = currentProvider.get('provides', {})
    if isinstance(providesInfo, list):
      # TODO: handle multiple environments
      providesInfo = providesInfo[0]

    providerEnvVars = providesInfo.get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in currentInit.get('env', {}).items():
      asdf = value
      separator = value.get('separator')
      unique    = value.get('unique', False)

      if separator:
        # TODO: handle escapes
        items = separator.join(value.get('items', []))
      else:
        value = value.get('items', [])
        if len(value) == 0:
          items = ""
        else:
          items = value[-1]

      currentInit['env'] = currentInit.get('env', {})
      currentInit['env'][key] = items

    return currentInit

  def combineEnvSection(self, currentEnv, secondEnv):
    """ Will produce a combined 'env' metadata section from two given ones.

    Will add any addition items to the 'currentEnv' in place and return
    the same 'currentEnv'.
    """

    if not isinstance(currentEnv, dict) or not isinstance(secondEnv, dict):
      return currentEnv

    for key, value in secondEnv.items():
      # Canonize value as a dict with an items field as a list
      if not isinstance(value, dict):
        value = { "items": value }

      if not 'items' in value:
        value['items'] = []

      if not isinstance(value['items'], list):
        value['items'] = [value['items']]

      # If the key already exists, merge environment metadata properly
      if key in currentEnv:
        if not isinstance(currentEnv[key], dict):
          currentEnv[key] = {
            "items": currentEnv[key]
          }

        if not 'items' in currentEnv[key]:
          currentEnv[key]['items'] = []

        if not isinstance(currentEnv[key]["items"], list):
          currentEnv[key]["items"] = [currentEnv[key]["items"]]

        for subKey, subValue in value.items():
          # For items, which is a list of values, just extend the list
          if subKey == 'items':
            currentEnv[key]['items'].extend(value.get('items', []))
          elif not subKey in currentEnv[key]:
            # For other metadata, add it only if it doesn't already exist
            currentEnv[key][subKey] = subValue
      else:
        # Otherwise, just add it
        currentEnv[key] = value

    return currentEnv

  def parseInitSection(self, objectInfo, currentInit, currentProvider):
    """ Will modify the given current init section with the contents of the given init section.

    It will use currentProvider as a basis of understanding what certain environment variables mean.
    """

    # Retrieve the 'init' section of the given object
    objectInit = objectInfo.get('init')

    # Bail if the init section is not a dictionary
    if objectInit is None or (not isinstance(objectInit, OrderedDict) and not isinstance(objectInit, dict)):
      return currentInit

    # Retrieve 'env' section
    objectEnvVars = objectInit.get('env', {})

    # Bail if env section is invalid
    if objectEnvVars is None or (not isinstance(objectEnvVars, OrderedDict) and not isinstance(objectEnvVars, dict)):
      return currentInit

    providerEnvVars = currentProvider.get('provides', {}).get('init', {}).get('env', {})

    # Parse 'env' section
    for key, value in objectEnvVars.items():
      separator = None
      unique    = False

      if key in currentInit.get('env', {}):
        separator = currentInit['env'][key].get('separator')
        unique = currentInit['env'][key].get('unique', False)

      if isinstance(value, dict):
        separator = value.get('separator')
        unique = value.get('unique', False)

      if key in providerEnvVars:
        separator = providerEnvVars[key].get('separator', separator)
        unique    = providerEnvVars[key].get('unique', unique)

      if isinstance(value, dict):
        value = value.get('items', [])

      if isinstance(value, list):
        items = value
      elif separator:
        # TODO: handle escapes
        items = value.split(separator)
      else:
        items = [value]

      currentInit['env'] = currentInit.get('env', {})
      newItems = currentInit['env'].get(key, {}).get('items', [])
      newItems.extend(items)

      if unique:
        newItems = list(set(newItems))

      currentInit['env'][key] = {
        "unique":    unique,
        "separator": separator,
        "items":     newItems
      }

    return currentInit

  def resolveDependency(self, dependencyList, dependencyIndex, context, locks, person):
    """ Resolves the given dependency and returns its contents.
    """

    dependency = dependencyList[dependencyIndex]
    penalties = context['penalties']
    dependencyInfo = None
    dependencyObject = None

    # Pull out dependency index information about the currently resolved dependency
    # This keeps track of what we had done previously
    dependenciesIndex = context['dependenciesIndex']
    dependencyInfo = dependenciesIndex.get(dependency['id'], {})
    requestedVersion = ",".join(dependencyInfo.get('version', [])) or None
    priorDependencyInfo = dependencyInfo

    # Keep track of the reason why we failed first
    reason = None

    # Determine if we have already successfully resolved that dependency
    dependencyInfo = dependencyInfo.get('resolved')
    if dependencyInfo and 'version' in dependencyInfo:
      if Semver.resolveVersion(requestedVersion, [dependencyInfo['version']]):
        # Update the dependency list with the resolved dependency
        dependencyList[dependencyIndex] = copy.deepcopy(dependencyInfo)
        dependencyList[dependencyIndex]['requested'] = dependency['version']

        # Make sure it inherits the inject property
        if dependency.get('inject'):
          dependencyList[dependencyIndex]['inject'] = dependency['inject']
        return True

    # We have never resolved this dependency, or an updated version invalided it
    # Let's resolve it
    dependencyInfo = None
    resolved = False

    # Retain the version requirement that was satisfied
    if 'version' in dependency:
      dependency['requested'] = dependency['version']

    # We only 'retry' once when we find we need to penalize a sub-dependency
    lastAttempted = None

    while resolved is not True:
      # Resolve each dependency and pull out subDependencies
      ManifestManager.Log.noisy(f"looking for dependency {dependency.get('type')} {dependency.get('name')} @ {dependency.get('revision', requestedVersion)}")

      try:
        dependencyObject = self.objects.retrieve(id        = dependency.get('id'),
                                                 version   = requestedVersion,
                                                 revision  = dependency.get('revision'),
                                                 penalties = penalties,
                                                 person    = person)
      except ObjectResolveVersionError:
        # Cannot find the version requested...
        # The version is likely constricted by a previous resolve
        # So, we need to reject this version and propagate that information
        #   back up to the parent call.

        # If we fail here, it is because we cannot find another valid version
        # of this required object after attempting all the others.

        # If we already have a 'reason' for failing, it means one of the valid
        # versions we rejected met a version conflict that affects something we
        # already resolved elsewhere. We need to report that one instead, so
        # don't overwrite it!
        if reason is None:
          # Make a note of the reason we have failed here
          reason = dependency

          # Determine how we already resolved this conflicted dependency
          if dependenciesIndex.get(reason['id'], {}).get('resolved'):
            previous = dependenciesIndex[reason['id']]['resolved']
            reason = previous

        # Normalize resolved to be False
        resolved = False
        break

      if dependencyObject is None:
        ManifestManager.Log.noisy(f"failed to find {dependency.get('type')} {dependency.get('name')} @ {dependency.get('revision', requestedVersion)}")
        resolved = False
        break

      # Get the actual realized dependency information
      dependencyObjectInfo = self.objects.infoFor(dependencyObject)

      # Create the dependency information and copy things we want to keep
      dependencyInfo = {}
      dependencyInfo.update(dependency)
      dependencyInfo['revision'] = dependencyObject.revision
      if dependencyObject.version:
        dependencyInfo['version'] = dependencyObject.version

        # If there is a lock, then apply it
        if dependencyInfo.get('lock') in ['major', 'minor']:
          dependencyInfo['lock'] = dependencyInfo['requested'] + "," + Semver.generalize(dependencyObject.version, dependencyInfo.get('lock'))
        elif dependencyInfo.get('lock') in ['>=']:
          dependencyInfo['lock'] = dependencyInfo['requested'] + ",>=" + dependencyObject.version

      if dependencyObject.uid:
        dependencyInfo['uid'] = dependencyObject.uid

      for key in ['init', 'dependencies', 'install', 'owner', 'file', 'network',
                  'metadata', 'environment', 'architecture']:
        if key in dependencyObjectInfo:
          dependencyInfo[key] = dependencyObjectInfo[key]

      # If we are not building, we are running.
      # If we are running, we need a build to use to know where those binaries are.
      if self.isBuildable(dependencyObject) and not dependency.get('build', {}).get('id'):
        # Get a built copy
        try:
          buildTask = self.retrieveBuildTaskFor(dependencyObject, preferred=None, person = person)
        except BuildRequiredError as e:
          buildTask = None

        # If we can't find a build... we fail to resolve this dependency
        if buildTask is None:
          # Penalize this choice of resolving the version
          dependencyOwner = self.objects.ownerFor(dependencyObject, person = person)
          penalties[dependencyOwner.id] = penalties.get(dependencyOwner.id, []) + [dependencyObject.revision]
          ManifestManager.Log.write(f"Could not find a build task... rejecting {dependency.get('name', 'unknown')} {dependencyObject.version or dependencyObject.revision}.")
          continue

        # Set the build id for the build we will use
        dependencyInfo['build'] = {}
        dependencyInfo['build']['id'] = buildTask.get('id')
        dependencyInfo['build']['revision'] = buildTask.get('revision')
        dependencyInfo['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

      # In case of recursive dependencies, we need to remember how we are
      # resolving ourselves:
      pushedIndex = copy.deepcopy(dependenciesIndex)
      pushedPenalties = copy.deepcopy(penalties)

      # Resolve the dependencies of the dependencies
      resolved = self.resolveDependenciesFor(dependencyObject, dependencyInfo, context, locks, person, ignoreInject = (dependencyInfo.get('inject') == "ignore"))

      # If we could not resolve those dependencies... we didn't resolve the main
      # dependency this function was responsible for.
      if resolved is not True:
        # Restore our context
        dependenciesIndex = pushedIndex
        priorDependencyInfo = dependenciesIndex[dependency['id']]
        context['dependenciesIndex'] = dependenciesIndex

        penalties = pushedPenalties
        context['penalties'] = penalties

        if resolved is not False:
          # We noted a failure to resolve a version of a dependency...
          # Keep track of that while we attempt our own resolution
          reason = resolved

          # Normalize our resolution signature as False
          resolved = False

          if 'revision' in reason:
            # Determine how we already resolved this conflicted dependency
            penalties[reason['id']] = reason['revision']
          else:
            # We have never actually resolved this dependency, so this is a
            # normal everyday failure
            lastAttempted = dependencyObject.revision
        else:
          # Ensure we always penalize ourselves since it was not a version conflict
          lastAttempted = dependencyObject.revision

        if lastAttempted == dependencyObject.revision:
          # Penalize this choice of resolving the version
          dependencyOwner = self.objects.ownerFor(dependencyObject, person = person)
          penalties[dependencyOwner.id] = penalties.get(dependencyOwner.id, []) + [dependencyObject.revision]
          ManifestManager.Log.write(f"penalizing {dependency.get('name')} at revision {dependencyObject.revision}")

        lastAttempted = dependencyObject.revision

        if 'version' in dependency:
          ManifestManager.Log.noisy(f"failed to use {dependency.get('type')} {dependency.get('name')} version {dependency.get('version')}")

    # Update the dependency list with the resolved dependency
    if dependencyInfo and resolved:
      dependencyList[dependencyIndex] = dependencyInfo

      dependenciesIndex = context['dependenciesIndex']
      dependenciesIndex[dependency['id']] = dependenciesIndex.get(dependency['id'], {})
      dependenciesIndex[dependency['id']]['resolved'] = dependencyInfo

    # If we noted some version conflicts, return those instead
    if not resolved and reason:
      return reason

    return resolved

  def resolveDependenciesFor(self, object, objectInfo, context, locks, person, ignoreInject = False):
    """ Resolves the dependency list for the given object metadata.
    """

    dependenciesIndex = context['dependenciesIndex']

    # Resolve the list of dependencies
    resolved = True
    reason = None

    dependencies = objectInfo.get('dependencies', [])
    if 'init' in objectInfo:
      dependencies.extend(objectInfo['init'].get('dependencies', []))
      objectInfo['dependencies'] = dependencies

    dependencyLists = [(dependencies, "general")]

    # Also rake dependencies that are to be injected by the build task
    if not ignoreInject and objectInfo.get('build', {}).get('id'):
      # Get the dependencies that were used when the object was built
      buildTimeDependencies = objectInfo.get('build', {}).get('dependencies', [])

      # Do not consider if the inject parameter doesn't meet the given value
      buildTimeDependencies = list(
        filter(
          lambda x: x.get('inject') in ["init", "run"],
          buildTimeDependencies
        )
      )

      for buildDependency in buildTimeDependencies:
        # Remove existing build id so it will choose a new build if necessary
        if 'build' in buildDependency:
          del buildDependency['build']

        # Assert version requirements for the dependencies based on locks
        if 'lock' in buildDependency:
          if buildDependency not in ['major', 'minor']:
            buildDependency['version'] = buildDependency['lock']

            # And un-assert a revision
            if 'revision' in buildDependency:
              del buildDependency['revision']

        # Do not propagate the inject requirement into the new task
        del buildDependency['inject']

      # Reassert this list
      objectInfo.get('build', {})['dependencies'] = buildTimeDependencies

      # Append list of filtered build-time dependencies
      dependencyLists.append((buildTimeDependencies, "run-time",))
    else:
      if 'dependencies' in objectInfo.get('build', {}):
        del objectInfo['build']['dependencies']

    # Make a note of each dependency in this listing
    for dependencies, listType in dependencyLists:
      for i, dependency in enumerate(dependencies):
        # Determine the effective id for the dependency (allow it to inherit the
        # proper id/revision from its parent)
        if 'id' not in dependency:
          # This will occur if the dependency is provided by the object in a
          # sub-object, and therefore has no specified 'id'

          # First check if it is referring to its own owner
          if dependency.get('type') == objectInfo.get('type') and dependency.get('name') == objectInfo.get('name'):
            dependency['id'] = object.id
            dependency['uid'] = object.uid
          else:
            # The 'id' in this case is determined by the system relative to the
            # owning object.
            dependency['id'] = self.objects.idFor(object, object.identity, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))
            dependency['uid'] = self.objects.uidFor(object, subObjectType = dependency.get('type'), subObjectName = dependency.get('name'))

          # When a version is not explicit, we need to define a revision
          # In this case, we want the revision of the owning object
          if 'version' not in dependency:
            dependency['revision'] = dependency.get('revision', object.revision)

          # The 'version' should also be inherited by this dependency with the
          # version given by the owning object, when known.
          if 'version' not in dependency:
            if 'version' in objectInfo:
              dependency['version'] = objectInfo['version']
            if object.version:
              dependency['version'] = object.version

        if ignoreInject and 'inject' in dependency:
          del dependency['inject']

        # Add the intention to our ongoing dependency list
        if 'version' in dependency:
          # Override requested version with any 'lock' specified by the agent
          if locks and locks.get(dependency.get('id')):
            dependency['version'] = dependency.get('version', locks[dependency.get('id')]) + "," + locks[dependency.get('id')]
            dependency['version'] = ",".join(list(set(dependency['version'].split(","))))

          dependenciesIndex[dependency['id']] = dependenciesIndex.get(dependency['id'], {'version': []})
          dependencyInfo = dependenciesIndex[dependency['id']]
          dependencyVersion = dependency['version']
          for subVersion in dependencyVersion.split(','):
            if subVersion not in dependencyInfo['version']:
              dependencyInfo['version'].append(subVersion)

    # Go through each dependency source
    for dependencies, listType in dependencyLists:
      # And then through each individual dependency
      for i, dependency in enumerate(dependencies):
        ManifestManager.Log.noisy(f"Raking {listType} dependency: {dependency.get('type')} {dependency.get('name')} {dependency.get('version', '')}{('@' + dependency.get('revision', '')) if 'revision' in dependency else ''}")
        overwriteInfo = None

        resolved = self.resolveDependency(dependencies, i, context = context,
                                                           locks   = locks,
                                                           person  = person)
        if resolved is not True:
          if resolved is not False:
            # There was a version conflict
            # We must penalize that version and try ourselves again
            reason = resolved
          break

      if resolved is not True:
        break

    return resolved

  def resolveServicesFor(self, objectInfo, services, context):
    # Get the list of requested services
    requires = objectInfo.get('requires', {})
    for required, options in objectInfo.get('run', {}).get('requires', {}).items():
      if required not in requires:
        requires[required] = options
      else:
        ManifestManager.deepMerge(requires[required], options)

    # We can resolve some services with those given by a client.
    if objectInfo.get('environment') == "linux":
      for required in (services or []):
        if required not in requires:
          ManifestManager.Log.write(f"Adding client requested service requirement '{required}'")
          requires[required] = {}

    # Adds each required service
    for required, options in requires.items():
      environment  = objectInfo.get('environment')
      architecture = objectInfo.get('architecture')
      ManifestManager.Log.write(f"Fulfilling service requirement '{required}' for {environment}/{architecture}")

      knownServices = self.services.retrieve(environment, architecture, required)
      serviceObject = None
      for knownService in knownServices:
        serviceObject = self.objects.retrieve(id=knownService.internal_object_id, revision=knownService.revision, person=person)
        if serviceObject:
          serviceObjectInfo = self.objects.infoFor(serviceObject)
          ManifestManager.Log.write(f"Fulfilling service requirement '{required}' with {serviceObjectInfo.get('type')} {serviceObjectInfo.get('name')}")

          serviceObjectInfo['id'] = serviceObject.id
          serviceObjectInfo['uid'] = serviceObject.uid
          serviceObjectInfo['revision'] = serviceObject.revision

          serviceInfo = serviceObjectInfo.get('services', {}).get(required, {})
          ManifestManager.deepMerge(serviceObjectInfo, serviceInfo)

          # Adds a process to the current 'running' listing
          processInfo = self.addProcessToTask(currentInput, serviceObject, serviceObjectInfo, interactive=interactive, context=context, locks = locks, person=person)
          objectIndex = context['objectIndex']

          # Add the init section of the subprocess to the main process
          break
      if not knownServices or not serviceObject:
        # Cannot resolve a required service
        raise "cannot find service"

  def reduceDependencyInitSection(self, dependencyInfo):
    """ This will return a revised init section based on the one provided by a dependency.

    Dependencies do not need their entire init sections contained within the task manifest.
    This function will truncate the init section to only what is needed by the deployment
    backend.
    """

    # We will start from an empty dictionary
    ret = {}

    # Keep the 'link' section so the deployment backend knows to mount/copy directories
    # from objects into the root filesystem
    links = dependencyInfo.get('init', {}).get('link', [])
    if links:
      ret["link"] = links

    copies = dependencyInfo.get('init', {}).get('copy', [])
    if copies:
      ret["copy"] = copies

    env = dependencyInfo.get('init', {}).get('env', {})
    if env:
      ret["env"] = env

    # Return the minimized init section
    return ret

  def reduceDependency(self, process, dependencyCache, dependencyList, dependencyIndex):
    """ Shrinks the dependency information.
    """

    # Get the dependency list
    dependency = dependencyList[dependencyIndex]
    dependencies = dependency.get('dependencies', [])

    # Append all run-time dependencies
    buildTimeDependencies = dependency.get('build', {}).get('dependencies', [])
    if buildTimeDependencies:
      dependencies.extend(buildTimeDependencies)

    # Do we add ourselves to the initial list?
    token = f"{dependency.get('id')}@{dependency.get('revision')}"
    if token in dependencyCache:
      # Revise with lock/inject when necessary
      # Locks add an extra constraint
      if 'lock' in dependency or 'inject' in dependency:
        dependencyOrder = dependencyCache[dependency.get('id')]
        for item in dependencyOrder:
          index = item['index']
          if 'lock' in dependency:
            process['dependencies'][index]['lock'] = dependency['lock']
          if 'inject' in dependency:
            process['dependencies'][index]['inject'] = dependency['inject']
    else:
      # Add our actual dependency
      fullDependency = copy.deepcopy(dependency)
      process['dependencies'].append(fullDependency)
      dependencyCache[token] = fullDependency

      # Order dependencies within list correctly (by revision, as found,
      # and then by version, ascending.)

      # This list is stored via the id and it points to the list of versions
      # or revisions and the indices they are currently located.

      # When a new dependency is added, it will be added where it should go and
      # the rest will be propagated down. The new dependency starts at the end
      # of the list and will bubble up.

      # This will sort the dependency list in-place.
      dependencyCache[dependency.get('id')] = dependencyCache.get(dependency.get('id'), [])
      dependencyOrder = dependencyCache[dependency.get('id')]
      listItem = {}
      if 'version' in dependency:
        listItem['version'] = dependency.get('version')
      else:
        listItem['revision'] = dependency.get('revision')

      listItem['index'] = len(process['dependencies']) - 1
      dependencyOrder.append(listItem)

      # Bubble up this listItem
      index = len(dependencyOrder) - 2
      for currentItem in reversed(dependencyOrder[:-1]):
        # Done if we hit the revision items
        if 'revision' in listItem and 'revision' in currentItem:
          break
        elif 'version' in listItem and 'version' in currentItem:
          # Done if the current item is a lesser version than
          # the new item
          if Semver.versionMatches(currentItem['version'], '<', listItem['version']):
            break

        # Swap
        dependencyOrder[index] = listItem
        dependencyOrder[index+1] = currentItem

        tmp = process['dependencies'][listItem['index']]
        process['dependencies'][listItem['index']] = process['dependencies'][currentItem['index']]
        process['dependencies'][currentItem['index']] = tmp

        tmp = listItem['index']
        listItem['index'] = currentItem['index']
        currentItem['index'] = tmp

        # Look at the next entry
        index = index - 1

    # Reduce ourselves
    dependency = {
      'id': dependency.get('id'),
      'revision': dependency.get('revision'),
      'version': dependency.get('version'),
      'requested': dependency.get('requested'),
      'lock': dependency.get('lock'),
      'init': dependency.get('init', {}),
    }

    if dependency['requested'] is None:
      del dependency['requested']

    if dependency['lock'] is None:
      del dependency['lock']

    if dependency['version'] is None:
      del dependency['version']

    # Add back the dependency list
    if dependencies:
      dependency['dependencies'] = dependencies

    # Reduce each sub-dependency
    for i in range(0, len(dependencies)):
      self.reduceDependency(process, dependencyCache, dependencies, i)

    # Combine ENV section
    dependency['init'] = dependency.get('init', {})
    dependency['init']['env'] = dependency['init'].get('env', {})
    for i in range(0, len(dependencies)):
      self.combineEnvSection(dependency['init']['env'], dependencies[i].get('init', {}).get('env'))
      if 'env' in dependencies[i].get('init', {}):
        del dependencies[i]['init']['env']
    if len(dependency['init']['env']) == 0:
      del dependency['init']['env']

    # Update ourselves
    dependencyList[dependencyIndex] = dependency

  def reduceProcess(self, process):
    """ Shrinks down the process metadata to only what is necessary.
    """

    # We only need full information in the initial set of dependencies.
    dependencyCache = {}

    # All unique dependencies in subsections go into the main dependency list
    # for the process.
    dependencies = process.get('dependencies', [])
    process['dependencies'] = []

    # Append all run-time dependencies
    buildTimeDependencies = process.get('build', {}).get('dependencies', [])
    if buildTimeDependencies:
      dependencies.extend(buildTimeDependencies)

    # Go through and reduce each dependency
    for i, dependency in enumerate(dependencies):
      self.reduceDependency(process, dependencyCache, dependencies, i)

    # Retrieve the new list
    dependencies = process.get('dependencies', [])

    # The prior loop will aggregate all dependencies into our list
    # Reduce, now, each fully formed dependency
    for dependency in dependencies:
      # Get rid of the run-time dependencies of the sub-dependencies
      # And any other build information fluff
      if 'build' in dependency:
        build = {}
        for key in ['id', 'revision']:
          if key in dependency['build']:
            build[key] = dependency['build'][key]
        dependency['build'] = build

      # Reduce 'init' section
      if 'init' in dependency:
        dependency['init'] = self.reduceDependencyInitSection(dependency)

      # Get rid of any unnecessary keys
      keys = ["init", "build", "run", "id", "uid", "revision", "version",
              "name", "type", "dependencies", "network", "file", "metadata",
              "summary", "inject", "lock", "requested", "install", "stage"]
      for key in list(dependency.keys()):
        if key not in keys:
          del dependency[key]

    # Append it back
    if dependencies:
      process['dependencies'] = dependencies

  def addProcessToTask(self, currentEnvironment, object, objectInfo, isBuilding=False, buildTask=None, interactive=False, context={}, locks={}, person=None):
    """ Adds the process given by object to the current 'running' array.

    Side effects include the updating of any manifest context.

    Args:
      object (Object): The object being added.
      objectInfo (dict): The object metadata being added.
      interactive (Boolean): Whether or not we are running the object interactively.
      context (dict): Contains context metadata for VM generation.
      person (Person): The current Person that is authenticated.

    Returns:
      dict: The added process manifest.
    """

    currentEnvironment['running'] = currentEnvironment.get('running', [])
    currentRunning = {}
    currentRunning['objects'] = []
    currentEnvironment['running'].append(currentRunning)

    # inputInfo will be the copy of the object description that gets placed in the task
    # It will have only the minimal information the deployment backend needs to deploy the task
    inputInfo = {
      "id":           objectInfo.get('id'),
      "uid":          objectInfo.get('uid'),
      "name":         objectInfo.get('name'),
      "type":         objectInfo.get('type'),
      "revision":     objectInfo.get('revision'),
      "environment":  objectInfo.get('environment'),
      "architecture": objectInfo.get('architecture'),
      "dependencies": []
    }

    # Capture the network effects
    if not isBuilding and 'network' in objectInfo.get('run', {}):
      inputInfo['network'] = objectInfo['run']['network']

    for key in ['run', 'build', 'init', 'provides', 'install', 'basepath',
                'owner', 'file', 'outputs', 'inputs', 'network', 'lock',
                'requested', 'metadata', 'summary', 'stage']:
      if key in objectInfo:
        inputInfo[key] = objectInfo[key]

    if interactive:
      inputInfo['interactive'] = True

    # Mark the index in this object
    objectIndex = context['objectIndex']
    while objectIndex in context['reservedIndicies']:
      objectIndex += 1
    inputInfo['index'] = objectIndex
    objectIndex += 1
    context['objectIndex'] = objectIndex

    # For every dependency of this environment, add it to the previous
    # environment

    # Resolving the dependencies
    resolved = self.resolveDependenciesFor(object, objectInfo, context, locks, person)

    # We bail if we cannot resolve the dependencies
    if resolved is not True:
      raise DependencyUnresolvedError(resolved, "Cannot find dependency")

    # Shrink the process information down now that we have resolved everything
    self.reduceProcess(objectInfo)

    # Form the running object's init section from dependencies
    initSection = {}

    dependencies = objectInfo.get('dependencies')

    for dependency in dependencies:
      dependencyInfo = {
        "id":       dependency.get('id'),
        "uid":      dependency.get('uid'),
        "name":     dependency.get('name'),
        "type":     dependency.get('type'),
        "revision": dependency.get('revision'),
      }

      # For direct dependencies, we copy certain metadata to the task
      for key in ['init', 'inject', 'version', 'build', 'owner',
                  'file', 'network', 'paths', 'lock', 'summary', 'install',
                  'provides', 'environment', 'architecture', 'requested']:
        if key in dependency:
          dependencyInfo[key] = dependency[key]

      # Add this object to the context of the running process
      objectIndex = context['objectIndex']
      while objectIndex in context['reservedIndicies']:
        objectIndex += 1
      dependencyInfo['index'] = objectIndex
      objectIndex += 1
      context['objectIndex'] = objectIndex

      # Keep track of the dependencies in the current object
      inputInfo['dependencies'].append(dependencyInfo.copy())

      # Combine init section with main section, using the currentEnvironment
      self.parseInitSection(dependency, initSection, currentEnvironment)

      # Adds this object to the VM
      currentRunning['objects'].append(dependencyInfo)

    # Add prior processes' init sections
    processes = currentEnvironment["running"]
    for process in processes[:-1]:
      processInfo = process['objects'][-1]
      self.parseInitSection(processInfo, initSection, currentEnvironment)

    # TODO: add mount paths HERE instead of at the end of the task generation

    # Parse the {{ tags }} within the object
    self.parseTagsInTask(inputInfo)

    if 'dependencies' in inputInfo and len(inputInfo['dependencies']) == 0:
      del inputInfo['dependencies']

    # Add our own init section
    self.parseInitSection(inputInfo, initSection, currentEnvironment)
    self.completeInitSection(initSection, currentEnvironment)
    if 'run' in inputInfo:
      inputInfo['run'].update(initSection)

    currentRunning['objects'].append(inputInfo)

    return inputInfo

  def initializeTask(self, section, environmentGoal, architectureGoal, backendName, objectInfo, generator):
    """ Creates the basic task template for a new task manifest.

    Arguments:
      section (str): The phase being generated (run, build, etc)
      environmentGoal (str): The target environment of the task.
      architectureGoal (str): The target architecture for the task.
      backendName (str): The name of the backend for which this task will be deployed.
      objectInfo (dict): The object metadata being used.
      generator (Object): The generator of this task, if necessary. None if not.

    Returns:
      dict: The initial and mostly empty task manifest.
    """

    # Create blank task
    from uuid import uuid1
    task = {
      "type": "task",
      "environment":  environmentGoal  or 'native',
      "architecture": architectureGoal or 'native',
      "backend": backendName,
      "id": str(uuid1())
    }

    # Preserve the task's name
    if backendName:
      task["name"] = f"Virtual Machine to {section} {objectInfo.get('name')} with {backendName}"
    else:
      task["name"] = f"Virtual Machine to {section} {objectInfo.get('name')} on {environmentGoal}/{architectureGoal}"

    # Keep track of the source of the task (workflow, etc)
    if generator:
      task['generator'] = self.objects.infoFor(generator)
      task['generator']['revision'] = generator.revision
      task['generator']['id']  = generator.id
      task['generator']['uid'] = generator.uid

    # Add local machine information
    task['metadata'] = task.get('metadata', {})
    task['metadata'].update(self.system.machineInfo())

    return task

  def prepareObject(self, object, section, buildTask, person):
    objectInfo = self.objects.infoFor(object)

    # Make a copy of the object metadata
    objectInfo = copy.deepcopy(objectInfo)
    objectInfo['id'] = object.id
    objectInfo['uid'] = object.uid
    objectInfo['revision'] = object.revision

    # Append 'owner' information
    ownerObj = self.objects.ownerFor(object, person=person)
    if ownerObj.id != object.id:
      objectInfo['owner'] = objectInfo.get('owner', {
        "id": ownerObj.id,
        "uid": ownerObj.uid
      })

    if not object.link is None:
      # Retain information about the local stage being used
      objectInfo['stage'] = { 'id': object.link }

    # Put build dependencies in with normal dependencies
    if section == "build":
      # We ignore 'init' sections on builds
      objectInfo['init'] = {"dependencies": []}

    # Ensure the object we are running/building is using the section asked for.
    objectInfo[section] = objectInfo.get(section, {})

    # Combine dependencies

    generalDependencies = objectInfo.get('dependencies', [])
    objectInfo['dependencies'] = generalDependencies + objectInfo.get('init', {}).get('dependencies', []) + objectInfo[section].get('dependencies', [])

    # Capture any other metadata.
    if 'metadata' in objectInfo[section]:
      objectInfo['metadata'] = objectInfo[section]['metadata']

    # Capture the network effects
    if 'network' in objectInfo[section]:
      objectInfo['network'] = objectInfo[section]['network']

    # Capture what file is selected to run
    if 'file' in objectInfo[section]:
      objectInfo['file'] = objectInfo[section]['file']

    # If we are provided a build task, use that one specifically to run the object
    if buildTask:
      objectInfo['build'] = objectInfo.get('build', {})
      objectInfo['build']['id'] = buildTask.get('id')
      objectInfo['build']['revision'] = buildTask.get('revision')
      objectInfo['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

    # We, then, substitute the section requested with the 'run' section.
    if section != "run":
      objectInfo['run'] = objectInfo[section]
      del objectInfo[section]

    return objectInfo

  def taskFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run", arguments=None, buildTask=None, using=None, penalties=None, service=None, services=[], preferred=None, backend=None, locks=None, person=None):
    """ Generates a task manifest for the given object that will run the object
    on this system.

    Arguments:
      object (Object): The resolved object to create a task for. Can be a dict of metadata.
      revision (str): The revision of this object to create.
      indexStart (int): The object index to start labeling objects within the task.
      generator (Object): The resolved object that has created this task.
      environmentGoal (str): The environment in which to run this task.
      architectureGoal (str): The architecture on which to run this task.
      inputs (list): An array of inputs to give to this task.
      haveCapabilities (list): An array of capabilities provided by the requesting client.
      needCapabilities (list): An array of additional capabilities requested by the client.
      section (str): The phase to generate a task for. Defaults to "run".
      arguments (list): A list of arguments to append to the task's command.
      buildTask (dict): The build task used to build the provided object.
      penalties (dict): Metadata that prohibits certain revisions of dependencies being used.
      locks (dict): A dictionary keyed by object id that forces particular versions of dependencies.
      person (Object): The "person" creating this task.
    """

    # We keep track of "indexes" that have already been allocated by inputs
    reservedIndicies = []

    # Gather the reserved indicies
    for wireIndex, wire in enumerate(inputs or []):
      for input in wire:
        # Gather information about the input
        if isinstance(input, dict):
          if 'index' in input:
            reservedIndicies.append(input['index'])

    # Initialize penalties
    if penalties is None:
      penalties = {}

    # Gather object metadata
    if not isinstance(object, dict):
      objectInfo = self.objects.infoFor(object)
      ownerInfo = self.objects.ownerInfoFor(object)
      revision = object.revision
    else:
      objectInfo = object
      object = self.objects.retrieve(id=objectInfo.get('id'), revision=objectInfo.get('revision', revision), person=person)
      ownerInfo = {}

    # The starting index for tagging unique objects in the VM
    objectIndex = indexStart

    # Determine the environment and architecture target for the object
    sectionInfo = objectInfo.get(section, {})
    environment  = sectionInfo.get('environment', objectInfo.get('environment'))
    architecture = sectionInfo.get('architecture', objectInfo.get('architecture'))

    # Determine if there is an existing task for this object that we can use.

    # Gather providers and the native backend
    if environment == environmentGoal and architecture == architectureGoal:
      backendPath = []
    else:
      backendPath = self.backends.providerPath(environment, architecture, environmentGoal, architectureGoal, backend=backend, person = person)

    if backendPath is None:
      # Cannot find a provider path
      raise ProviderRequiredError(f"No suitable way to {section} this object has been found on {environment} on {architecture}", environment, architecture)

    nativeBackend = None
    if len(backendPath) > 0:
      nativeBackend = backendPath[-1]

    if architectureGoal is None and environmentGoal is None and (nativeBackend is None or isinstance(nativeBackend, Object)):
      # No native backend. We failed.
      raise BackendRequiredError(f"No backend found for {environment} on {architecture}", environment, architecture)

    # Determine backend details
    if isinstance(nativeBackend, Object) or nativeBackend is None:
      # The backend is still unknown, we are making a partial VM
      backendName = None
    else:
      # The backend is known. We are making a VM with the intent to run on
      # this backend.
      backendName = nativeBackend.__class__.name()

    # Once we have a native backend, build up the task manifest to run
    # on top of that backend

    task = None
    if not inputs:
      cacheKey = f"task-{section}-{backendName}"
      try:
        task = self.storage.retrieveDocument(object.uid, object.revision, key = cacheKey)
      except:
        task = None

    if task:
      ManifestManager.Log.noisy("Using a cached task")
      task = json.loads(task)
      # TODO: update with requested parameters
      return task

    # Generate a new task with the basics filled out
    task = self.initializeTask(environmentGoal = environmentGoal,
                               architectureGoal = architectureGoal,
                               section = section,
                               backendName = backendName,
                               objectInfo = objectInfo,
                               generator = generator)

    # Sanitize the object metadata returning a copy of it
    objectInfo = self.prepareObject(object, section = section,
                                            buildTask = buildTask,
                                            person = person)

    if section == "build":
      # Penalize resolving this object as its own dependency
      # (Unless it is locked in, which overrides this behavior)
      resolvedRevision = None
      if locks and objectInfo['id'] in locks:
        resolvedRevision, resolvedVersion = self.versions.resolve(object, locks[objectInfo['id']], penalties = penalties)

      if revision != resolvedRevision:
        penalties[objectInfo['id']] = penalties.get(objectInfo['id'], []) + [revision]

    # Determine if the object is interactive or not
    interactive = objectInfo.get('run', {}).get('interactive', False)
    if interactive:
      task['interactive'] = interactive

    # Ignore the backend class when we go through the objects
    lastIndex = -1
    if backendName is None:
      lastIndex = len(backendPath)

    # Enumerate through each object and append the 'revision' key
    # Note: we ignore the last item, which is the native BackendManager handler
    for i, obj in enumerate(backendPath[0:lastIndex]):
      info = self.objects.infoFor(obj)
      info['id'] = obj.id
      info['uid'] = obj.uid
      info['revision'] = obj.revision

      # Gather the 'owner' for this object, and embed this information
      ownerObj = self.objects.ownerFor(obj, person=person)
      if ownerObj.id != obj.id:
        info['owner'] = info.get('owner', {
          "id": ownerObj.id,
          "uid": ownerObj.uid
        })

    # For each provider, determine how to run each object
    providerList = backendPath[0:lastIndex]
    providerList = [object] + providerList

    # Keep track of the current provider and input
    lastInput = None
    currentEnvironment = task
    currentInput = task

    # For every intermediate Provider object, we add the item to the task
    for backendObject in reversed(providerList):
      # If we hit the intended object, we want to use our revised metadata
      if backendObject is object:
        backend = objectInfo
      else:
        # Otherwise, we pull out the metadata for that provider
        backend = self.objects.infoFor(backendObject)

      ManifestManager.Log.noisy(f"setting up {backend.get('name')} @ {backend.get('revision')}")

      # Add forced inputs
      if backend is objectInfo and inputs is not None and isinstance(inputs, list):
        ManifestManager.Log.write("Adding input to task")

        # Ready the list of inputs
        objectInfo['inputs'] = objectInfo.get('inputs', [])

        # Make sure the inputs are a list
        if not isinstance(objectInfo['inputs'], list):
          objectInfo['inputs'] = [objectInfo['inputs']]

        # For each input requested
        for wireIndex, wire in enumerate(inputs):
          for input in wire:
            # Gather information about the input
            inputInfo = input
            if not isinstance(input, dict):
              inputInfo = self.objects.infoFor(input)
              inputInfo['id']       = input.id
              inputInfo['uid']      = input.uid
              inputInfo['revision'] = input.revision
              if not input.link is None:
                # Retain information about the local stage being used
                inputInfo['stage'] = { 'id': input.link }

            if 'build' in inputInfo:
              if 'buildId' not in inputInfo:
                buildTask = self.retrieveBuildTaskFor(input, preferred, person = person)
                buildId = buildTask.get('id')

                inputInfo['buildId'] = buildId

            # Give the input a relative identifier
            while objectIndex in reservedIndicies:
              objectIndex += 1
            inputInfo['index'] = objectIndex
            objectIndex += 1

            # TODO: input dependencies??
            resolved = self.resolveDependenciesFor(input, inputInfo, context, locks, person)

            # We bail if we cannot resolve the dependencies
            if resolved is not True:
              raise DependencyUnresolvedError(resolved, "Cannot find dependency")

            objectInfo['inputs'] = objectInfo.get('inputs', [])
            objectInfo['inputs'].extend([{}] * (wireIndex + 1 - len(objectInfo['inputs'])))
            objectInfo['inputs'][wireIndex]['connections'] = objectInfo['inputs'][wireIndex].get('connections', [])
            objectInfo['inputs'][wireIndex]['connections'].append({
              "id":       inputInfo.get('id'),
              "uid":      inputInfo.get('uid'),
              "type":     inputInfo.get('type'),
              "name":     inputInfo.get('name'),
              "revision": inputInfo.get('revision'),
              "index":    inputInfo.get('index'),
            })

            objectInfo['dependencies'] = objectInfo.get('dependencies', [])
            objectInfo['dependencies'].extend(inputInfo.get('dependencies', []))

            for key in ['stage', 'version', 'buildId', 'owner', 'file', 'network', 'dependencies', 'run', 'init', 'build', 'metadata']:
              if key in inputInfo:
                objectInfo['inputs'][wireIndex]['connections'][-1][key] = inputInfo[key]

      # Keep track of all dependencies within this environment
      # We will throw away duplicates
      dependenciesIndex = {}

      # This contains all the state gathered while generating the task
      context = {'objectIndex': objectIndex, 'penalties': penalties, 'dependencies': {}, 'dependenciesIndex': dependenciesIndex, 'reservedIndicies': reservedIndicies}

      # Resolve each service requirement
      self.resolveServicesFor(backend, services = services,
                                       context  = context)

      # Adds a process to the current 'running' listing
      isBuilding = backend is objectInfo and section == 'build'
      buildTask = buildTask if backend is objectInfo else None

      if not isBuilding and not buildTask and self.isBuildable(backendObject) and not backend.get('build', {}).get('id') and not backend.get('stage'):
        # Get a built copy
        try:
          buildTask = self.retrieveBuildTaskFor(backendObject, preferred=None, person = person)
        except BuildRequiredError as e:
          buildTask = None

        backend['build']['id'] = buildTask.get('id')
        backend['build']['revision'] = buildTask.get('revision')
        backend['build']['dependencies'] = buildTask.get('builds', {}).get('dependencies', [])

      inputInfo = self.addProcessToTask(currentEnvironment, backendObject, backend, isBuilding = isBuilding,
                                                                                    buildTask = buildTask,
                                                                                    interactive = interactive,
                                                                                    context = context,
                                                                                    locks = locks,
                                                                                    person=person)
      objectIndex = context['objectIndex']

      # Follow the environment and then we will loop around and add objects to that
      # context as needed.

      # Environments are objects that will run dependencies. They are emulators or
      # OS environments.
      currentEnvironment['provides'] = {
        'environment':  inputInfo.get('environment'),
        'architecture': inputInfo.get('architecture')
      }

      if 'provides' in inputInfo or lastInput is None:
        lastInput = inputInfo
        currentEnvironment['running'] = currentEnvironment.get('running', [])
        currentRunning = currentEnvironment['running'][len(currentEnvironment['running'])-1]
        currentEnvironment = currentRunning['objects'][len(currentRunning['objects'])-1]
      else:
        pass
      currentInput = inputInfo

      if 'provides' in inputInfo:
        currentEnvironment = inputInfo
        dependencies = {}

      #capabilities |= set(backend.get('capabilities', []))

    # Determine capabilities/services/etc
    capabilities = []
    task['capabilities'] = list(capabilities)
    task['client'] = {}
    task['client']['capabilities'] = haveCapabilities
    task['client']['requires'] = needCapabilities

    # The last time this is assigned, it will indicate the index of the intended object
    runningObject = currentInput

    # Establish the 'volume' and 'mounted' paths for every object so they
    # know where they, and their inputs, exist in their environments
    basepath = None
    if 'backend' in task and task['backend'] is not None:
      if nativeBackend:
        basepath = {
          "id":           "backend",
          "environment":  task['provides'].get('environment'),
          "architecture": task['provides'].get('architecture'),
          "basepath":     nativeBackend.rootBasepath()
        }

    # Fill in the mount paths for each object in the task
    self.setVolumesInTask(task, basepath = basepath)

    # Finalize the tags
    self.parseTagsInTask(task, removeUnknown = True)

    # Have the task note what it is doing
    if section == "build":
      task['builds'] = runningObject
    else:
      task['runs'] = runningObject

    # Do not allow the task to 'provide' anything.
    # Note: Only applies to partial tasks (ones without native backends).
    if 'provides' in task:
      del task['provides']

    # Conserve the dependency resolution for this task

    # Conserve this task
    if not inputs:
      self.storage.pushDocument(object.uid, object.revision, key = cacheKey,
                                                             value = json.dumps(task))

    # Return the task manifest
    return task

  def printTask(self, task):
    """ Prints out the task information to the Log.
    """

    ManifestManager.Log.write(f"Task: {task.get('name', 'unnamed')}")

    def printTaskObject(obj):
      for wire in obj.get('inputs', []):
        for input in wire.get('connections', []):
          ManifestManager.Log.write(f"Input: {input.get('name', 'unnamed')}")
          if 'run' in input:
            ManifestManager.Log.write(f"Run: {input['run'].get('command', input['run'].get('script'))}")

          printTaskObject(input)

    printTaskObject(task)

  def backendsFor(self, object, revision=None, indexStart=0, generator=None, environmentGoal=None, architectureGoal=None, inputs=None, haveCapabilities=[], needCapabilities=[], section="run"):
    """ Returns a list of backends that can run the given object.
    """

    info = object
    if isinstance(object, Object):
      info = self.objects.infoFor(object)

    targetEnvironment  = info.get('environment')  or info.get(section, {}).get('environment')
    targetArchitecture = info.get('architecture') or info.get(section, {}).get('architecture')

    return self.backends.backendsFor(environment=targetEnvironment, architecture=targetArchitecture)

  def setVolumesInTask(self, objInfo, rootBasepath = None, basepath=None, rootMountPathIndex=None, mountPathIndex=None):
    """ This method takes a task's metadata and adds 'volume' and 'local' fields
    based on any 'basepath' fields it finds.
    """

    # Start at the root of the virtual machine and go through recursively
    # to any leaves.
    mountId = objInfo.get('id')
    if 'owner' in objInfo:
      mountId = objInfo['owner'].get('id', objInfo.get('id'))

    if rootBasepath is None:
      # TODO: get the rootbase path from the backend?
      rootBasepath = []

    if mountPathIndex is None:
      mountPathIndex = [0]

    if rootMountPathIndex is None:
      rootMountPathIndex = [0]

    # Set the volume and mounted for this entry
    if rootBasepath is not None and basepath is not None:
      rootMountPaths  = {}
      localMountPaths = {}
      for subBasepath in rootBasepath + [basepath]:
        rootMountPath = subBasepath.get('basepath', {}).get('mount', '/')
        if not isinstance(rootMountPath, str) and isinstance(rootMountPath, list):
          index = rootMountPathIndex[0]
          if index < len(rootMountPath):
            rootMountPath = rootMountPath[index]
            rootMountPathIndex[0] += 1
          else:
            # TODO: cannot create this task... place errors somewhere
            rootMountPath = "/"

        baseArchitecture = subBasepath.get('architecture', "unknown")
        baseEnvironment  = subBasepath.get('environment',  "unknown")

        rootMountPaths[baseArchitecture] = rootMountPaths.get(baseArchitecture, {})
        rootMountPaths[baseArchitecture][baseEnvironment] = rootMountPath

        localMountPath = subBasepath.get('basepath', {}).get('local', '/home/occam/local')

        localMountPaths[baseArchitecture] = localMountPaths.get(baseArchitecture, {})
        localMountPaths[baseArchitecture][baseEnvironment] = localMountPath

      mountPath = basepath.get('basepath', {}).get('mount', '/')
      if not isinstance(mountPath, str) and isinstance(mountPath, list):
        index = mountPathIndex[0]
        if index < len(mountPath):
          mountPath = mountPath[index]
          mountPathIndex[0] += 1
        else:
          # TODO: cannot create this task... place errors somewhere
          mountPath = "/"

      objInfo['paths'] = {
        "volume": rootMountPaths,
        "mount":  mountPath,
        "separator": basepath.get('basepath', {}).get('separator', '/'),

        "local":  localMountPaths,
        "localMount": basepath.get('basepath', {}).get('local', '/home/occam/local'),

        "taskLocal":  basepath.get('basepath', {}).get('taskLocal', '/home/occam/task'),

        "cwd": basepath.get('basepath', {}).get('cwd', basepath.get('basepath', {}).get('local', '/home/occam/task'))
      }

      if len(rootBasepath) > 0:
        objInfo["paths"]["mountLocal"] = rootBasepath[-1].get('basepath', {}).get('objectLocal', '/home/occam/local'),

    if 'provides' in objInfo:
      if rootBasepath is None:
        rootBasepath = []
      if basepath is not None:
        rootBasepath = rootBasepath[:]
        rootBasepath.append(basepath)
      basepath = {
        "id": objInfo.get('id'),
        "basepath": objInfo.get('basepath', (basepath or {}).get('basepath', {})),
        "architecture": objInfo.get('provides', {}).get('architecture'),
        "environment":  objInfo.get('provides', {}).get('environment'),
      }

    # Recursively go through the rest of the entries:
    inputs = objInfo.get('inputs', [])
    if isinstance(inputs, list):
      for wire in inputs:
        for input in wire.get('connections', []):
          self.setVolumesInTask(input, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    for subTask in objInfo.get('running', []):
      for inputObject in subTask.get('objects', []):
        self.setVolumesInTask(inputObject, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Do the outputs, too
    outputs = objInfo.get('outputs', [])
    if isinstance(outputs, list):
      for wire in outputs:
        for output in wire.get('connections', []):
          self.setVolumesInTask(output, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Dependencies
    for dep in objInfo.get('dependencies', []):
      self.setVolumesInTask(dep, rootBasepath, basepath, rootMountPathIndex, mountPathIndex)

    # Update cwd
    if 'run' in objInfo and 'cwd' in objInfo['run']:
      objInfo['paths']['cwd'] = objInfo['run']['cwd']

    if 'run' in objInfo and 'script' in objInfo['run'] and 'command' not in objInfo['run']:
      objInfo['run']['command'] = objInfo['run']['script']

    if 'run' in objInfo and 'command' in objInfo['run']:
      if not isinstance(objInfo['run']['command'], list):
        objInfo['run']['command'] = [objInfo['run']['command']]

      if not objInfo['run']['command'][0].strip().startswith("/"):
        objInfo['run']['command'][0] = objInfo['paths']['mount'] + objInfo['paths']['separator'] + objInfo['run']['command'][0].strip()

  def updateTag(self, value, originalKey, root):
    # Determine the root of the key
    # Determine the relative key
    # Update the tag with relative keys

    # For each tag, reflect the relative key
    try:
      index = 0
      start = value.index("{{", index)

      while start >= 0:
        end   = value.index("}}", start)
        key   = value[start+2:end].strip()

        if key == "{{":
          # Escaped curly brace section
          replacement = "{{"
        else:
          try:
            path = self.parser.path(originalKey)

            # Determine the root
            newRoot = root
            current = root
            keyPrefix = ""
            currentPrefix = ""
            for subKey in path:
              current = current[subKey]
              if isinstance(subKey, int):
                currentPrefix = currentPrefix + "[" + str(subKey) + "]"
              else:
                currentPrefix = currentPrefix + "." + subKey

              if isinstance(current, dict) and "id" in current:
                newRoot = current
                keyPrefix = currentPrefix

            if keyPrefix.startswith("."):
              keyPrefix = keyPrefix[1:]

            okey = key
            if keyPrefix:
              key = keyPrefix + "." + key

            replacement = "{{ " + key + " }}"
          except:
            replacement = "{{ %s }}" % (key)

        if replacement is None:
          replacement = ""

        replacement = str(replacement)

        try:
          value = value[0:start] + replacement + value[end+2:]
        except:
          value = value

        # Adjust where we look next
        index = start + len(replacement)

        # Pull the index of the next {{ section
        # If there isn't one, it will fire an exception
        start = value.index("{{", index)

    except ValueError as e:
      pass

    if value is None:
      value = ""
    return value

  def parseTagsInTask(self, value, root=None, parent=None, environment=None, removeUnknown=False):
    """ This method goes through every entry in the task info and replaces curly
    brace tags with what they request.

    These replacements are completed to create the task manifest for creating
    a virtual machine. Each tag is based on the root of any object metadata.
    The root of an object is any section with an 'id' tag. The following shows
    how the changes made above are actually the same regardless of where the
    object is inserted into the task manifest as a whole.

    Examples:

      A section such as this::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample {{ paths.mount }}",
          "bar": "{{ paths.mount }}/usr/lib"
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
          },
          "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
          "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
        }

      And, in a more complex example where some items are self referencing from their parent::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample {{ paths.mount }}",
              "bar": "{{ paths.mount }}/usr/lib"
            }
          ]
        }

      Becomes::

        {
          "paths": {
            "mount": "/occam/1133bb33-5274-11e5-b1d4-dc85debcef4e"
          },
          "input": [
            {
              "paths": {
                "mount": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e"
              },
              "foo": "./sample /occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e",
              "bar": "/occam/dd44fcce-5274-11e5-b1d4-dc85debcef4e/usr/lib"
            }
          ]
        }

      As a special case, ``{{ {{ }}`` will escape double curly brackets,
      although double right curly brackets don't require it::

        {
          "foo": "./sample 'foo bar {{{{}}baz}}'"
        }

      Creates the task manifest with the curly brace intact::

        {
          "foo": "./sample 'foo bar {{baz}}'"
        }
    """

    if root is None:
      root = value

    if isinstance(value, dict):
      # Go through a chunk of metadata

      newRoot = root
      if 'id' in value:
        newRoot = value

      # Handle complex objects first
      for k, v in value.items():
        if isinstance(v, dict):
          self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

      # Then lists
      for k, v in value.items():
        if isinstance(v, list):
          self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

      # Then strings
      for k, v in value.items():
        if isinstance(v, str):
          value[k] = self.parseTagsInTask(v, root=newRoot, removeUnknown=removeUnknown)

    elif isinstance(value, list):
      # Go through a list of items

      i = 0
      for v in value:
        value[i] = self.parseTagsInTask(v, root=root, removeUnknown=removeUnknown)
        if value[i] == "" and v != "":
          # Delete empty strings in lists
          del value[i]
        else:
          i += 1

    elif isinstance(value, str):
      # This is a string, so we parse it for curly brace sections
      try:
        index = 0
        start = value.index("{{", index)

        while start >= 0:
          end   = value.index("}}", start)
          key   = value[start+2:end].strip()

          commands = []
          replaced = True

          if key == "{{":
            # Escaped curly brace section
            replacement = "{{"
          else:
            key, *commands = [x.strip() for x in key.split('|')]
            try:
              replacement = self.parser.get(root, key)
            except:
              replaced = False
              if removeUnknown:
                replacement = ""
              else:
                replacement = "{{ %s }}" % (key)

          if replacement is None:
            replacement = ""

          replacement = str(replacement)

          # Update keys *within* the replacement text
          replacement = self.updateTag(replacement, key, root)

          # If there are no more keys, then we're good to execute the commands
          if "{{" not in replacement:
            for command in commands:
              parts = [x.strip() for x in command.split(" ", 1)]
              func = parts[0]
              args = ""
              if len(parts) > 1:
                args = parts[1]

              import shlex

              # Parse args (splits on ' except when escaped)
              args = shlex.split(args)

              if func == "substring":
                if len(args) == 0:
                  args = [0]
                if len(args) == 1:
                  args.append(len(replacement))
                if len(args) > 2:
                  args = args[0:2]

                args = [int(x) for x in args]

                args[0] = min(args[0], len(replacement))
                args[1] = min(args[1], len(replacement))

                replacement = replacement[args[0]:args[1]]
              elif func == "basename":
                rmext = None
                if len(args) == 1:
                  import re
                  if args[0] == ".*":
                    rmext = re.compile(re.escape(".") + ".*$")
                  else:
                    rmext = re.compile(re.escape(args[0]) + "$")

                sep = root.get('basepath', {}).get('separator', "/")
                old = replacement

                if os.sep != sep:
                  repsep = "\\\\"
                  if os.sep == "/":
                    repsep = "\\/"
                  replacement = replacement.replace(os.sep, repsep).replace(sep, os.sep)

                replacement = os.path.basename(replacement)

                if rmext:
                  replacement = rmext.sub("", replacement)
              elif func == "extname":
                replacement = os.path.splitext(replacement)[1]
              elif func == "replace":
                replacement = replacement.replace(args[0], args[1])
              elif func == "dospath":
                # We can assume sep is a "\\" (whew!)
                # We can then split the paths up
                if os.sep != "\\":
                  tmppath = replacement.replace("/", "%/%").replace("\\", os.sep).replace("%/%", "\\/")

                # Shorten all directories and filenames when appropriate
                tmppath = tmppath.split(os.sep)
                newpath = []
                for subpath in tmppath:
                  subpath = subpath.replace(" ", "")
                  name, ext = os.path.splitext(subpath)
                  if len(name) > 8 or len(ext) > 4:
                    subpath = name[0:6] + "~1" + ext[0:4]
                  newpath.append(subpath)

                replacement = "/".join(newpath)
                replacement = replacement.replace("\\/", "%/%").replace(os.sep, "\\").replace("%/%", "/")

          try:
            value = value[0:start] + replacement + value[end+2:]
          except:
            value = value

          # Adjust where we look next
          index = start
          if not replaced:
            index = start + len(replacement)

          # Pull the index of the next {{ section
          # If there isn't one, it will fire an exception
          start = value.index("{{", index)

      except ValueError as e:
        pass

    if value is None:
      value = ""
    return value

  def rakeTaskObjects(self, objInfo):
    """ Returns an array of every object metadata for the given task metadata.
    """

    ret = []

    for wire in objInfo.get('inputs', []):
      for input in wire.get('connections', []):
        ret.append(input)
        ret += self.rakeTaskObjects(input)

    return ret

class ManifestError(Exception):
  """ Base class for all manifest errors.
  """

  def __init__(self, message):
    self.message = message

  def __str__(self):
    return self.message

class ProviderRequiredError(ManifestError):
  """ Error is generated when a provider path cannot be resolved.
  """

  def __init__(self, message, environment, architecture):
    super().__init__(message)
    self.environment = environment
    self.architecture = environment

class BackendRequiredError(ManifestError):
  """ Error is generated when a backend cannot be resolved.
  """

  def __init__(self, message, environment, architecture):
    super().__init__(message)
    self.environment = environment
    self.architecture = environment

class DependencyUnresolvedError(ManifestError):
  """ Error is generated when a dependency cannot be resolved.
  """

  def __init__(self, objectInfo, message):
    super().__init__(message)
    self.objectInfo = objectInfo

class BuildRequiredError(ManifestError):
  """ Error is generated when a build is required.
  """

  def __init__(self, objectInfo, requiredObject, message):
    super().__init__(message)
    self.objectInfo = objectInfo
    self.objectInfo['id'] = requiredObject.id
    self.objectInfo['uid'] = requiredObject.uid
    self.objectInfo['revision'] = requiredObject.revision
    self.objectInfo['version'] = requiredObject.version
    self.requiredObject = requiredObject
