# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table, index

@table("associations")
class AssociationRecord:
  """ A record that reflects that the given object is associated with the given criteria for the given person.
  """

  schema = {

    # Foreign Key to Object that is associated

    "internal_object_id": {
      "foreign": {
        "table": "objects",
        "key":   "id"
      }
    },

    # Foreign Key to Identity (can be NULL to apply system-wide)

    "identity_uri": {
      "foreign": {
        "table": "identities",
        "key":   "uri"
      }
    },

    # Fields

    # The object revision.
    "revision": {
      "type": "string",
      "length": 128
    },

    # The type of association ("viewer", "editor", "provider", etc)
    "association_type": {
      "type": "string",
      "length": 32
    },

    # The object type (for viewers/editors) or architecture (for providers)
    "major": {
      "type": "string",
      "length": 128
    },

    # The object subtype (for viewers/editors) or environment (for providers)
    "minor": {
      "type": "string",
      "length": 128
    },

    # A timestamp of when this association was made
    "published": {
      "type": "datetime",
    },

    # Constraints

    "relationship": {
      "constraint": {
        "type": "unique",
        "columns": ["internal_object_id", "identity_uri", "association_type", "major", "minor"],
        "conflict": "replace"
      }
    }
  }

@index(AssociationRecord, "major")
class AssociationMajorIndex:
  ids = ['major']

@index(AssociationRecord, "minor")
class AssociationMinorIndex:
  ids = ['minor']

@index(AssociationRecord, "association_type")
class AssociationAssociationTypeIndex:
  ids = ['association_type']

@index(AssociationRecord, "identities")
class AssociationIdentityIndex:
  ids = ['identity_uri']
