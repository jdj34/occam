# The Ingest Component

This component handles turning artifacts in upstream repositories into Occam objects.

Ingest currently supports Python packages and maven packages. This means that
Occam can turn a Python package (in wheel / .whl format) stored on PyPI into an
Occam object that can be executed when provided with a Python interpreter in
the execution environment.
