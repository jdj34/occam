# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.log import loggable

from occam.manager import manager, uses

from occam.objects.write_manager  import ObjectWriteManager
from occam.versions.write_manager import VersionWriteManager
from occam.manifests.manager      import ManifestManager
from occam.builds.write_manager   import BuildWriteManager
from occam.jobs.manager           import JobManager
from occam.keys.write_manager     import KeyWriteManager, KeySignatureExistsError
from occam.permissions.manager    import PermissionManager

@loggable
@manager("ingest")
@uses(ObjectWriteManager)
@uses(BuildWriteManager)
@uses(VersionWriteManager)
@uses(ManifestManager)
@uses(JobManager)
@uses(PermissionManager)
@uses(KeyWriteManager)
class IngestManager:
  """
  """

  handlers = {}

  def __init__(self):
    import importlib

    # Look at configuration at import the plugins specified
    pluginList = self.configuration.get('plugins', [])
    for pluginName in pluginList:
      importlib.import_module(pluginName)

  @staticmethod
  def register(name, handlerClass):
    """ Registers the given class as an importable package with the given name.
    """

    IngestManager.handlers[name] = handlerClass

  def handlerFor(self, packageType):
    """ Returns an instance of a handler for the given package type.
    """

    if not packageType in self.handlers:
      return None

    subConfig = self.configurationFor(packageType)

    # Create a driver instance
    instance = IngestManager.handlers[packageType](subConfig)

    return instance

  def configurationFor(self, packageType):
    """ Returns the configuration for the given package type.

    Returns the configuration for the given storage that is found within the
    occam configuration (config.yml) under the "ingest" section under the
    given package plugin name.
    """

    config = self.configuration
    subConfig = config.get(packageType, {})

    return subConfig

  def pullAll(self, packageType, person, force, **kwargs):
    """ Invokes the given plugin to ingest all packages.

    All extra keyword arguments are passed to the plugin underneath.

    Arguments:
      packageType (str): The type of package which indicates which plugin to use.
      packageName (str): The name or URL of the package to ingest.
      person (Person): The actor requesting the ingest.
      force (bool): Whether or not to pull the object even if it already exists.

    Returns:
      list: A set of Object elements pertaining to the ingested objects.

    Raises:
      IngestPluginNotFoundError: When the packageType is not known or usable.
    """

    handler = self.handlerFor(packageType)

    if handler is None:
      raise IngestPluginNotFoundError(packageType)

    packageNames = handler.queryAll(person.identity, **kwargs)

    ret = []
    for packageName in packageNames:
      obj = self.pull(packageType, packageName, person, force, **kwargs)
      if obj is not None:
        ret.append(obj)

    return ret

  def pull(self, packageType, packageName, person, force, pending = None, **kwargs):
    """ Invokes the given plugin to ingest the given package.

    All extra keyword arguments are passed to the plugin underneath.

    Arguments:
      packageType (str): The type of package which indicates which plugin to use.
      packageName (str): The name or URL of the package to ingest.
      person (Person): The actor requesting the ingest.
      force (bool): Whether or not to pull the object even if it already exists.

    Returns:
      Object: The ingested object or None if the object could not be ingested.

    Raises:
      IngestPluginNotFoundError: When the packageType is not known or usable.
    """

    return self._pull(packageType, packageName, person = person, force = force, limit = 10, pending = pending, **kwargs)

  def _pull(self, packageType, packageName, person, force, limit = 10, pending = None, **kwargs):
    limit = limit - 1
    if limit == 0:
      return None

    handler = self.handlerFor(packageType)

    if handler is None:
      raise IngestPluginNotFoundError(packageType)

    values = handler.query(person.identity, packageName, pending = pending, **kwargs)

    if values is None:
      return None

    if isinstance(values, list) or isinstance(values, tuple):
      objectInfo, options = values
    else:
      objectInfo, options = values, {}

    # Check to see if we already have the object
    # If so, don't continue
    tag = options.get("tag")
    try:
      obj = self.objects.retrieve(id = self.objects.idFor(obj=None, identity = person.identity, objectInfo = objectInfo), version = tag, person = person)
    except:
      obj = None

    if obj is not None and not force:
      IngestManager.Log.noisy("Already know about this object.")
      return obj

    # Create the object
    IngestManager.Log.write("Creating %s %s %s" % (objectInfo.get('type'), objectInfo.get('name'), tag or ""))
    obj = self.objects.write.create(objectInfo.get('name', packageName), objectInfo.get('type', packageType), info = objectInfo)

    # Clone the object to a particular place to complete the ingestion
    localObject = self.objects.temporaryClone(obj, person = person)[0]

    # Gather resources
    self.objects.write.pullResources(localObject, person.identity)

    if 'files' in options:
      files = options['files']
      for path in files:
        with open(path, 'rb') as f:
          self.objects.write.addFileTo(localObject, os.path.basename(path), f.read())

    self.objects.write.commit(localObject, person.identity, message="Adds file content")
    self.objects.write.store(localObject, person.identity)
    self.permissions.update(id = localObject.id, canRead=True, canWrite=False, canClone=True)

    localObject = self.objects.retrieve(id = localObject.id, revision = localObject.revision, person = person)

    objectInfo = self.objects.infoFor(localObject)

    report = None
    if 'run' in options and options['run']:
      report = "run"
    if 'build' in options and options['build']:
      report = "build"

    if 'ingest' in options:
      nextPending = (pending or {}).copy()
      nextPending[localObject.id] = objectInfo
      IngestManager.Log.write("Ingesting: {}".format([x.get('packageName') for x in options['ingest']]))
      for subObject in options['ingest']:
        IngestManager.Log.write("Recursively ingesting {}".format(subObject.get('packageName')))
        self.pull(person = person, force = False, pending = nextPending, **subObject)

    innerLimit = 10
    while report and innerLimit > 0:
      # Run the object and pass along the stdout
      if report == "run":
        IngestManager.Log.header("Running for {}".format(objectInfo.get('name')))
        try:
          task = self.manifests.run(localObject, person = person)
        except:
          # If it cannot perform the task, fail
          IngestManager.Log.error("Cannot create a run task.")
          return None
      else:
        IngestManager.Log.header("Building for {}".format(packageName))
        foo = self.objects.infoFor(localObject)
        try:
          task = self.manifests.build(localObject, person = person)
        except:
          # If it cannot perform the task, fail
          IngestManager.Log.error("Cannot create a build task.")
          return None

      import io, json
      output = io.BytesIO(b"")
      taskInfo = self.objects.infoFor(task)
      originalTaskId = task.id
      opts = self.jobs.deploy(taskInfo, task.revision, person = person, stdout = output)
      runReport = self.jobs.execute(*opts)

      if report == "build" and runReport.get('runReport', {}).get('status') != "failed":
        elapsed = runReport['time']
        buildPath = runReport.get('paths').get(taskInfo.get('builds').get('index')).get('buildPath')
        buildLogPath = os.path.join(os.path.dirname(runReport.get('paths').get('task')), 'task', 'objects', str(taskInfo.get('builds').get('index')), 'stdout.0')

        self.builds.write.store(person, localObject, task, buildPath, elapsed, buildLogPath=buildLogPath)

      # TODO: look at the stdout in the object's output
      dataLength = output.tell()
      output.seek(0)
      data = output.read(dataLength).decode('utf-8')

      values = self.handlerFor(packageType).report(person.identity, report, options or {}, objectInfo, packageName, data, runReport, pending = pending, **kwargs)

      # Clean-up
      self.jobs.removeRunFolder(taskInfo, person = person)

      if values is None:
        break

      if isinstance(values, list) or isinstance(values, tuple):
        objectInfo, options = values
      else:
        objectInfo, options = values, {}

      obj = self.objects.write.create(objectInfo.get('name', packageName), objectInfo.get('type', packageType), info = objectInfo)

      # Clone the object to a particular place to complete the ingestion
      localObject = self.objects.temporaryClone(obj, person = person)[0]

      # Gather resources
      self.objects.write.pullResources(localObject, identity = person.identity)

      files = []
      if 'files' in options:
        files = options['files']
        for path in files:
          with open(path, 'rb') as f:
            self.objects.write.addFileTo(localObject, os.path.basename(path), f.read())

      self.objects.write.commit(localObject, person.identity, message="Adds file content")
      self.objects.write.store(localObject, person.identity)
      self.permissions.update(id = localObject.id, canRead=True, canWrite=False, canClone=True)

      localObject = self.objects.retrieve(id = localObject.id, revision = localObject.revision, person = person)

      objectInfo = self.objects.infoFor(localObject)

      report = None
      if 'run' in options and options['run']:
        report = "run"
      if 'build' in options and options['build']:
        report = "build"

      if 'ingest' in options:
        nextPending = (pending or {}).copy()
        nextPending[localObject.id] = objectInfo
        IngestManager.Log.write("Ingesting: {}".format([x.get('packageName') for x in options['ingest']]))
        for subObject in options['ingest']:
          IngestManager.Log.noisy("Recursively ingesting {}".format(subObject.get('packageName')))
          self.pull(person = person, force = False, pending = nextPending, **subObject)

      innerLimit = innerLimit - 1

      if 'tag' in options:
        tag = options.get("tag")

    if tag:
      # Tag the version in the note store
      try:
        signature_digest, signature_type, signature, verifyKeyId, published = self.keys.write.signTag(localObject, person.identity, tag)
        IngestManager.Log.write("Signed version tag as %s" % (person.identity))
      except KeySignatureExistsError as e:
        pass

      self.versions.write.update(localObject,
                                 tag              = tag,
                                 identity         = person.identity,
                                 published        = published,
                                 signature        = signature,
                                 signature_type   = signature_type,
                                 signature_digest = signature_digest,
                                 verifyKeyId      = verifyKeyId)

    return localObject

def package(name):
  """ This decorator will register the given class as a package type.
  """

  def register_package(cls):
    IngestManager.register(name, cls)
    cls = loggable("IngestManager")(cls)

    def init(self, subConfig):
      self.configuration = subConfig

    cls.__init__ = init

    return cls

  return register_package

class IngestError(Exception):
  """ Base class for all ingest errors.
  """

class IngestPluginNotFoundError(IngestError):
  """ When an ingest plugin cannot be found.
  """

  def __init__(self, pluginName):
    """ Creates a new IngestPluginNotFoundError
    """

    self.pluginName = pluginName

  def __str__(self):
    return "Plugin `" + self.pluginName + "` not found or enabled in your Occam config."
