export CC="gcc"
export CXX="g++"

mkdir -p path
ln -s /usr/bin/gcc path/cc
ln -s /usr/bin/g++ path/cpp
export PATH=$PATH:$PWD/path

mkdir -p /tmp

if [ "$2" == "2" ]; then
  pip2 install "$1" --root="$PWD/../build" -vvv --disable-pip-version-check "${@:4}"
else
  echo "Running pip install"

  #/bin/env python3 -m pip install "$1" --root="$PWD/build" -vvv --disable-pip-version-check --no-build-isolation --no-index "${@:4}" || exit 1
  /bin/env python3 -m pip install "$1" -vvv --disable-pip-version-check --no-build-isolation --no-index --no-dependencies "${@:4}" || exit 1

  echo ""
  echo "Occam Ingest: Parsing the pip output and building package."
  echo ""

  echo "Running pip show"
  echo ""

  # Remove 'extra' sources once installed... so we just look at the file list
  # and install the files pip says are part of the package to the build output
  # path.
  /bin/env python3 -m pip show -f "$3" > files.txt

  echo "Parsing pip show output"
  echo ""

  /bin/env python3 occam-py-copy.py $PWD/../build

  echo ""
  echo "Done"
fi
