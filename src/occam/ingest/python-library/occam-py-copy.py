import os, sys, shutil

parse = False

basePath = None

with open('files.txt') as f:
  for line in f:
    line = line.strip("\n")

    if line.startswith("Version:"):
      print(line)
    elif line.startswith("Location:"):
      line = line[10:]
      basePath = line
    elif line.startswith("Files:"):
      parse = True
    elif parse and line.startswith(" "):
      line = line.strip()
      srcPath = os.path.realpath(os.path.join(basePath, line))
      destPath = os.path.join(sys.argv[1], srcPath[1:])

      os.makedirs(os.path.dirname(destPath), exist_ok=True)
      shutil.copyfile(srcPath, destPath)
