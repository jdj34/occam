# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.ingest.manager import IngestManager, package

from occam.network.manager import NetworkManager
from occam.objects.manager import ObjectManager

from occam.manager import uses

from occam.semver import Semver

import uuid as UUID
import os, re

@package("python-library")
@uses(ObjectManager)
@uses(NetworkManager)
class PythonLibrary:
  """ This aids in importing python packages into Occam for preservation.
  """

  PYPI_IDENTITY = "6Mv1SXJiuM9brN8cwU3jCJa7VEjXL776iYmssW7RtdLnrkY"

  INDEX_SOURCE = "https://pypi.org/simple/"

  CANONICAL_SOURCE = "https://pypi.python.org/pypi/%s"

  JSON_MIRRORS=[
    ("https://pypi.python.org/pypi/%s/json", "https://pypi.python.org/pypi/%s/%s/json",),
  ]

  def __queryAll(self):
    """ Retrieves a listing of all packages.
    """

    PythonLibrary.Log.write(f"Parsing package listing at {PythonLibrary.INDEX_SOURCE}")

    data, content_type, size = self.network.get(PythonLibrary.INDEX_SOURCE)

    PythonLibrary.Log.writePercentage("Parsing...")

    ret = []
    position = 0
    for line in data.readlines():
      position += len(line)
      PythonLibrary.Log.updatePercentage(position / size * 100)

      line = line.strip()
      if line.startswith(b'<a'):
        name = line.split(b'>', 1)[1].split(b'<', 1)[0].decode('utf-8')
        ret.append(name)

    PythonLibrary.Log.updatePercentage(100)

    return ret

  def __queryMetadata(self, pythonLibraryName, version = None, pythonVersion = None):
    """ Retrieves the JSON PyPI metadata for the given python package that matches the given version and pythonVersion.

    Returns:
      None when the data cannot be found.
    """

    # Get the baseline
    metadata = self.__retrieveMetadata(pythonLibraryName)

    if metadata is None:
      return None

    # Gather the versions
    releases = list(metadata.get('releases', {}).keys())

    # Sort the releases
    releases = Semver.sortVersionList(releases)

    # Go through the releases until we find something we can use
    for release in reversed(releases):
      if release != metadata.get('info', {}).get('version'):
        metadata = self.__retrieveMetadata(pythonLibraryName, release)

      requiresPython = (metadata.get('info', {}).get('requires_python', '') or '').replace('*', 'x')
      # Must match python version
      if not pythonVersion or not requiresPython or Semver.resolveVersion(requiresPython, [pythonVersion]):
        # Must match requested version range
        if not version or Semver.resolveVersion(version, [release]):
          metadata['version'] = release
          return metadata

    return None

  def __retrieveMetadata(self, pythonLibraryName, version = None):
    """ Retrieves the JSON PyPI metadata for the given python package at the given package version.

    Returns:
      None when the data cannot be found.
    """

    for baseURL in PythonLibrary.JSON_MIRRORS:
      url = baseURL[0] % (pythonLibraryName)
      if version:
        url = baseURL[1] % (pythonLibraryName, version)

      PythonLibrary.Log.noisy(f"Pulling metadata at {url}")

      try:
        data = self.network.getJSON(url)
      except:
        data = None

      if data is not None:
        return data

    return None

  def sourceURLFor(self, pythonLibraryName):
    """ Returns the canonical source url.
    """

    sourceURL = PythonLibrary.CANONICAL_SOURCE % (pythonLibraryName.lower())

    return sourceURL

  def uidFor(self, occamName, pythonLibraryName, sourceURL):
    """ Returns the id for a particular source package name.
    """

    return self.objects.uidFor(None, objectInfo = {
      "name": occamName,
      "type": "python-library",
      "source": sourceURL
    })

  def idFor(self, identity, occamName, pythonLibraryName, sourceURL):
    """ Returns the id for a particular source package name.
    """

    return self.objects.idFor(None, identity, objectInfo = {
      "name": occamName,
      "type": "python-library",
      "source": sourceURL
    })

  def queryAll(self, identity, pythonVersion = "3"):
    """ Returns a list of all packages.
    """

    return self.__queryAll()

  def query(self, identity, pythonLibraryName, pending = None, pythonVersion = "3", version = None):
    """ Returns the id and initial metadata for the package.
    """

    # At first, we do not know the canonical name. It helps us find the metadata
    # if the name is listed on a package repository such as PyPI.
    # We will negotiate with the package index and get the canonical name later.
    name = None
    sourceURL = self.sourceURLFor(pythonLibraryName)

    # When a library target is specified as name@url, it must negotiate
    # the repository and remember the canonical name.
    if '@' in pythonLibraryName:
      name, pythonLibraryName = pythonLibraryName.split('@', 1)
    resourceURL = None
    resourceType = None

    if self.network.isURL(pythonLibraryName):
      # Acquire a python library from a URL

      sourceURL = pythonLibraryName

      # It might be a git repository.
      if self.network.isGitAt(pythonLibraryName):
        # For a git repository, we assume it has some kind of setup.py and we
        # will use that as its resource and to determine the metadata after it
        # builds a package.

        # Gather the canonical name
        if name:
          pythonLibraryName = name
        else:
          pythonLibraryName = os.path.basename(sourceURL)

        resourceURL = sourceURL
        resourceType = "git"
        metadata = {}
        version = "git"
      else:
        # Fail, for now
        # TODO: handle URL to tarball of python package or wheel
        return None
    else:
      # A package listing from PyPI

      metadata = self.__queryMetadata(pythonLibraryName, version = version, pythonVersion = pythonVersion)

      if metadata is None and version is not None:
        # Also look for the version ending in '.0'
        # Python considers 1.24.0 to be equal to 1.24, so it comes up sometimes
        metadata = self.__queryMetadata(pythonLibraryName, version = version + ".0", pythonVersion = pythonVersion)

      if metadata is None:
        # Cannot find the metadata
        return None

      if version:
        # Set the version we are using to the version found
        version = metadata.get('version', version)

      # Gather version listing
      releases = list(metadata.get('info', {}).get('releases', {}).keys())

      # Use the 'latest' version listed in the metadata
      if version is None:
        version = metadata.get('info', {}).get('version')

      # If there is no listed latest, use the biggest version listed as a release
      if version is None and len(releases) > 0:
        version = Semver.sortVersionList(releases)[-1]

      # If we cannot determine which version to query, then we cannot find the package
      if version is None:
        return None

      # Determine its canonical name
      pythonLibraryName = metadata['info']['name']

      # Get the URL of the desired release

      # First, gather a list of all downloadable distributions for the given version
      distributions = metadata.get('releases', {}).get(version, [])

      # And find the source distribution
      chosenDistribution = None
      for distribution in distributions:
        # TODO: handle python versions better
        if pythonVersion[0] == "3" and (distribution.get('python_version') == "cp37" or distribution.get('python_version') == "py3" or distribution.get('python_version') == "py2.py3" or distribution.get('python_version') == "cp37"):
          url = distribution.get('url')
          if url:
            # If it has a URL, choose this one if the platform matches
            if ("manylinux" in distribution.get('filename', url) or "-none-" in distribution.get('filename', url)) and not "macosx" in distribution.get('filename', url):
              # And the architecture
              if "x86_64" in distribution.get('filename', url) or "-any." in distribution.get('filename', url):
                chosenDistribution = distribution
                break

        #if pythonVersion[0] == "2" and (distribution.get('python_version') == "cp26" or distribution.get('python_version') == "cp27"):

        if distribution.get('packagetype') == "sdist" and distribution.get('python_version') == "source":
          url = distribution.get('url')
          if url:
            # If it has a URL, choose this one
            chosenDistribution = distribution
            break

      # We could not find a download that we accept
      if chosenDistribution is None:
        PythonLibrary.Log.write(f"Cannot find an acceptable distribution for {pythonLibraryName}.")
        return None

      resourceURL = chosenDistribution.get('url')

    objectName = pythonLibraryName
    if pythonVersion[0] == "2":
      objectName = objectName + "-2"

    uid = self.uidFor(objectName, pythonLibraryName, sourceURL)
    id  = self.idFor(identity, objectName, pythonLibraryName, sourceURL)

    PythonLibrary.Log.write("uid: {}".format(uid))
    PythonLibrary.Log.write(" id: {}".format(id))

    filename = ""
    ext = ""
    if resourceType is None:
      resourceType = "application/gzip"
      if ext == ".zip" or ext == ".whl":
        resourceType = "application/zip"
      elif ext == ".xz":
        resourceType = "application/x-xz"
      elif ext == ".bz2":
        resourceType = "application/x-bzip2"
      elif ext == ".lz":
        resourceType = "application/x-lzip"

    isWheel = False
    directoryName = os.path.basename(resourceURL) + "/"
    if resourceType != "git":
      filename, ext = os.path.splitext(os.path.basename(resourceURL))

      directoryName, ext = filename, ext
      if os.path.splitext(directoryName)[1] == ".tar":
        directoryName, ext = os.path.splitext(directoryName)

      if ext == ".whl":
        directoryName = directoryName + ext
        isWheel = True
      else:
        directoryName = directoryName + "/"

    # Create the object manifest that will determine the requirements
    #   by running an isolated setup.py
    objectInfo = {
      "type": "python-library",
      "name": objectName,

      "source": sourceURL,

      "environment": "linux",
      "architecture": "x86-64",

      "run": {
        "install": [{
          "type": "resource",
          "subtype": resourceType,
          "source": resourceURL,
          "name": "Python Source for %s %s" % (pythonLibraryName, version or "(Unknown Version)"),
          "to": os.path.basename(resourceURL)
        }],
        "dependencies": [{
          "id": "QmPxP6yHYWbSA3z3sFVQVp67xUmAmKAD7kLUCc7j3JJjvi",
          "type": "library",
          "name": "png",
          "version": "1.x"
        }, {
          "type": "collection",
          "name": "build",
          "id": "QmQ1i5VjdxdU7dWCkhpM7ccVDCBNPX2swTLQSHRiCW8sK1",
          "version": "1.0"
        }, {
          "type": "application",
          "name": "unzip",
          "id": "QmPvYt44E95j78i7MSi3rJrMZ6K3Jyj677Yg3Q8wruEcKE",
          "version": "6.x"
        }, {
          "type": "compiler",
          "name": "g++",
          "id": "QmVseooVZ4dn2Vo4ikwKXzmBcqDH9avt4YWYtKmujP4KC8",
          "version": ">5",
          "inject": "ignore"
        }],
        "command": ["/bin/sh", "{{ paths.mount }}/fakebuild.sh", "{{ paths.mount }}", directoryName, pythonVersion[0]]
      }
    }

    if not isWheel and resourceType != "git":
      objectInfo["run"]["install"][0]["actions"] = {"unpack": "."}

    if pythonVersion[0] == "2":
      objectInfo['run']['dependencies'].append({
        "id": "QmZA4Hs6pWgUpoSGAzgv1H4JuCGGdNjxgBHXsE2Zo4GV3n",
        "type": "language",
        "name": "python2",
        "version": ">=2.x",
        "lock": "minor"
      })
    else:
      pythonIndex = len(objectInfo['run']['dependencies'])
      objectInfo['run']['dependencies'].append({
        "id": "QmRt6cEP2MsyxaAvyzHCH9FFwCn3NAtLRzbJdW4tVPBkSV",
        "type": "language",
        "name": "python",
        "version": ">=3.6.1",
        "lock": "minor"
      })

      if pythonLibraryName != "wheel":
        # Require wheel (expect when requesting 'wheel' package)
        subSourceURL = self.sourceURLFor("wheel")
        id = self.idFor(PythonLibrary.PYPI_IDENTITY, "wheel", "wheel", subSourceURL)
        objectInfo['run']['dependencies'].append({
          "id": id,
          "type": "python-library",
          "name": "wheel",
          "version": ">=" + pythonVersion + ".0"
        })

      if pythonLibraryName != "pytoml" and pythonLibraryName != "wheel":
        # Require toml (expect when requesting 'wheel'/'toml' package)
        subSourceURL = self.sourceURLFor("pytoml")
        id = self.idFor(PythonLibrary.PYPI_IDENTITY, "pytoml", "pytoml", subSourceURL)
        objectInfo['run']['dependencies'].append({
          "id": id,
          "type": "python-library",
          "name": "pytoml",
          "version": ">=" + pythonVersion + ".0"
        })

        # TODO: ensure we pull in pip/toml/etc for the asking identity
        # and use those IDs here

        if pythonLibraryName != "pip":
          subSourceURL = self.sourceURLFor("pip")
          id = self.idFor(PythonLibrary.PYPI_IDENTITY, "pip", "pip", subSourceURL)
          # Require pip (expect when requesting 'pip' package, or toml/wheel)
          objectInfo['run']['dependencies'].append({
            "id": id,
            "type": "python-library",
            "name": "pip",
            "version": ">=" + pythonVersion + ".0"
          })

          # A hard requirement of 'argparse' just messes everything up. Pip
          # ignores the existence of argparse as an installed module specifically
          # and, as a consequence, always tries to install it from the cache or
          # network. Therefore, it has to be specified in its source form.
          # FOR EVERY PACKAGE JUST IN CASE. THANKS A LOT.
          if pythonLibraryName != "argparse":
            subSourceURL = self.sourceURLFor("argparse")
            argparseId = self.idFor(PythonLibrary.PYPI_IDENTITY, "argparse", "argparse", subSourceURL)
            argparseObject = self.objects.retrieve(id = argparseId, version = ">=" + pythonVersion + ".0")
            argparseInfo = self.objects.infoFor(argparseObject)
            argparseResource = argparseInfo.get('build', {}).get('install', [None])[0]
            if argparseResource:
              objectInfo['run']['install'].append(argparseResource)
              objectInfo['run']['command'].append(argparseResource['to'])

    requiresPython = (metadata.get('info', {}).get('requires_python', '') or '').replace('*', 'x')
    if requiresPython:
      objectInfo['run']['dependencies'][pythonIndex]['version'] += "," + requiresPython

    license = metadata.get('info', {}).get('license')
    if license:
      objectInfo['license'] = license

    summary = metadata.get('info', {}).get('summary')
    if summary:
      objectInfo['description'] = summary

    description = metadata.get('info', {}).get('description')
    if description:
      if summary:
        objectInfo['summary'] = summary
      objectInfo['description'] = description

    website = metadata.get('info', {}).get('home_page')
    if website:
      objectInfo['website'] = website

    author = metadata.get('info', {}).get('author')
    if author:
      objectInfo['authors'] = [author]

    # We need to parse the result of running this object to determine the requirements

    # TODO: check the md5 given by the metadata
    # TODO: use signatures?

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "python-library"))

    buildScriptPath  = os.path.join(basePath, "fakebuild.sh")
    ingestPythonPath = os.path.join(basePath, "ingest.py")
    parseWheelPythonPath  = os.path.join(basePath, "parse-wheel.py")
    parseTomlPythonPath  = os.path.join(basePath, "parse-toml.py")
    parseSetupCfgPythonPath  = os.path.join(basePath, "parse-setup-cfg.py")

    return objectInfo, {
      "files": [
        buildScriptPath,
        ingestPythonPath,
        parseWheelPythonPath,
        parseTomlPythonPath,
        parseSetupCfgPythonPath,
      ],
      "run": True,
      "tag": pythonVersion + "." + version
    }

  def report(self, identity, runType, options, objectInfo, packageName, data, runReport, pending = None, pythonVersion = "3", version = None):
    """ Finishes the process.
    """

    if runReport.get('runReport', {}).get('status') == "failed":
      # We do not accept failure, here
      return None

    if runType == "build":
      return None

    packageName = objectInfo.get('name')

    import json, re

    setupArguments = {}

    try:
      setupArguments = json.loads(data)
    except:
      # Cannot parse the output
      pass

    requirements = setupArguments.get("requirements")

    # Clone the existing object
    # We will modify it with new requirements and dependencies
    newObjectInfo = objectInfo.copy()

    # First, we need to see if there are any dependencies that are required
    # to do an installation
    setupDependencies = False

    if requirements is None:
      # Failed to parse
      # TODO: try an earlier version
      return None

    if isinstance(requirements, dict):
      if "setup_requires" in requirements:
        # Add the requirements to the new form of object and have it re-run eventually
        setupDependencies = True
        requirements = requirements["setup_requires"]
        PythonLibrary.Log.write(f"Repeating due to hidden setup requirement: {str(requirements)}")

    # We will use this regular expression to parse python versions
    splitter = re.compile(r"^([^><=~!]+)(.*)$")
    versionSplitter = re.compile(r"^\s*([><=~!]+)(.*)$")

    # Create the object manifest that will determine the requirements
    #   by running an isolated setup.py
    section = 'run'

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "python-library"))
    buildScriptPath  = os.path.join(basePath, "fakebuild.sh")
    ingestPythonPath = os.path.join(basePath, "ingest.py")
    parseWheelPythonPath = os.path.join(basePath, "parse-wheel.py")
    parseTomlPythonPath  = os.path.join(basePath, "parse-toml.py")
    parseSetupCfgPythonPath  = os.path.join(basePath, "parse-setup-cfg.py")
    filesList = [buildScriptPath, ingestPythonPath, parseWheelPythonPath, parseTomlPythonPath, parseSetupCfgPythonPath]

    if not setupDependencies:
      directoryName = newObjectInfo['run']['command'][3]
      extraArgs = newObjectInfo['run']['command'][5:]
      if "--ignore-setup-dependencies" in extraArgs:
        extraArgs.remove("--ignore-setup-dependencies")

      buildScriptPath = os.path.join(basePath, "build.sh")
      copyScriptPath = os.path.join(basePath, "occam-py-copy.py")
      filesList = [buildScriptPath, copyScriptPath]

      section = 'build'
      newObjectInfo['build'] = newObjectInfo['run']

      # Reform manylinux2014 to manylinux2010 (for older pip/python before 2019)
      packageFile = newObjectInfo['build']['install'][0]['to']
      if 'manylinux2014' in packageFile:
        packageFile = packageFile.replace('manylinux2014', 'manylinux2010')
        newObjectInfo['build']['install'][0]['to'] = packageFile

      newObjectInfo['build']['command'] = ["/bin/sh", "{{ paths.mount }}/build.sh", directoryName, pythonVersion[0], packageName]
      newObjectInfo['build']['command'].extend(extraArgs)
      del newObjectInfo['run']

      newObjectInfo['build']['dependencies'] = newObjectInfo['build'].get('dependencies', [])
    else:
      if "--ignore-setup-dependencies" not in newObjectInfo['run']['command']:
        newObjectInfo['run']['command'].append("--ignore-setup-dependencies")

    linkType = "link"
    if packageName == "argparse":
      linkType = "copy"

    newObjectInfo['init'] = {
      linkType: [
        {
          "source": "usr",
          "to":     "/usr"
        }
      ]
    }

    ingestRequests = []

    if requirements:
      # First, reform the set of requests into canonical names
      canonical = []

      for request in requirements:
        extra = ""
        if ';' in request:
          request, extra = request.split(';', 1)
        extra = extra.strip()

        url = None
        if '@' in request:
          # It is requesting from a URL
          request, url = request.split('@', 1)

          if 'git+' in url:
            # It is a git repo
            url = url.split('+')[1]

          if '#' in url:
            # Remove the query string
            url = url.split('#')[0]

        result = splitter.match(request)
        if result is None:
          # TODO: error? it is a malformed requirement
          continue

        # Capture interesting conditionals
        extras = extra.split('and')
        bail = False
        for extra in extras:
          extra_result = splitter.match(extra)
          if extra_result:
            key = extra_result.group(1).strip()
            value = extra_result.group(2).strip()

            value_result = versionSplitter.match(value)
            condition = ""
            if value_result:
              condition = value_result.group(1).strip()
              value = value_result.group(2).strip()
              value = value.replace('"', '').replace("'", '')

            # TODO: handle 'and' clauses and such... Yuck

            # Handle 'sys_platform == "win32"', etc
            if key == 'sys_platform' or key == 'platform_system':
              if condition == "==" and value != "linux":
                # We don't want this (yet)
                bail = True
                break

              if condition == "!=" and value == "linux":
                # We don't want this (yet)
                bail = True
                break

            # Handle 'extra == "test"', etc
            if key == 'extra':
              # We don't want this (yet)
              bail = True
              break

            if key == 'python_version':
              value = extra_result.group(2).strip()
              value = value.replace('"', '').replace("'", '')

              # Yuck. When it comes to conditional dependencies, like this, it's really messy
              if not pythonVersion or not Semver.resolveVersion(value, [pythonVersion]):
                bail = True
                break

        if bail:
          continue

        name       = result.group(1).strip()
        subVersion = result.group(2).strip()

        # Check the optional group name
        parts = name.split('[', 1)
        if len(parts) > 1:
          continue

        if name in ['os', 'sys', 'math']:
          # Ignore common python packages
          continue

        # If it is not a URL, we can easily find the metadata in PyPI
        if not url:
          # Gather its canonical name
          metadata = self.__retrieveMetadata(name)
          name = metadata['info']['name']

        PythonLibrary.Log.noisy(f"Found dependency {name}")

        if subVersion == "":
          subVersion = None

        # Python versioning uses * as a wildcard
        # I'd rather reform that, even though '*' works in semver
        if subVersion:
          subVersion = subVersion.replace('*', 'x')

          # Get rid of the spaces
          subVersion = subVersion.replace(' ', '')

          # Python versioning expects that ".0" at the end matches a version without it
          # For instance "1.24.0" should also match "1.24", which it does not in Occam
          # semver... for reasons. (The reason is related to patch versioning.)

          # Therefore, we need to turn something like '<3.0,>=2.7.0' into
          # '<3,>=2.7' and '==2.7' to turn into '==2.7 || ==2.7.0', etc
          # '!=2.7' needs to be '!=2.7,!=2.7.0' ... phew
          versionRanges = subVersion.split(',')
          for i in range(len(versionRanges)):
            versionRange = versionRanges[i].strip()
            if versionRange.endswith(".0"):
              if versionRange.startswith("<") or versionRange.startswith(">"):
                # '<3.0' -> '<3'
                # '>=2.7.0' -> '>=2.7'
                versionRange = versionRange[:-2]
              elif versionRange.startswith("!"):
                # '!=2.7.0' -> '!=2.7,!=2.7.0'
                versionRanges.append(versionRange[:-2])
              else:
                # '==2.7.0' -> '==2.7.0 || ==2.7'
                versionRange = versionRange + " || " + versionRange[:-2]
            elif versionRange.startswith("!"):
              # '!=2.7' -> '!=2.7,!=2.7.0'
              versionRanges.append(versionRange + ".0")
            elif versionRange.startswith("="):
              # '==2.7' -> '==2.7 || ==2.7.0'
              versionRange = versionRange + " || " + versionRange + ".0"

            versionRanges[i] = versionRange

          subVersion = ",".join(versionRanges)

        occamName = name
        if pythonVersion[0] == "2":
          occamName = occamName + "-2"

        canonical.append((name, occamName, subVersion, url))

      # Next, form the dependency metadata and deal with dependency cycles
      for request in canonical:
        # Unpack
        name, occamName, subVersion, url = request

        subSourceURL = url
        if url is None:
          subSourceURL = self.sourceURLFor(name)
        uid = self.uidFor(occamName, name, subSourceURL)
        id  = self.idFor(identity, occamName, name, subSourceURL)
        pypiId = self.idFor(PythonLibrary.PYPI_IDENTITY, occamName, name, subSourceURL)

        ingestName = name
        if url:
          ingestName = f"{name}@{url}"

        if id in (pending or {}):
          # This package does the UNTHINKABLE and requires a library that itself
          # requires us! Why does Python allow such atrocities? One example is
          # 'fixtures' which requires 'testtools' which requires 'fixtures'.

          # Blasphemy.

          # In these cases, we need to not include the cycle in our dependency
          # chain. Instead, we must include a version of the package in our
          # build dependency listing as a resource. Pip will build an install
          # if we point it to both packages' wheels/tarballs.

          # We add the package as a build resource and then add the package
          # as an argument to our build script. The script then adds it as a
          # command-line argument to pip alongside the normal package.

          # Get what we know of the pending object we are trying to depend upon
          info = pending[id]

          # Place the resource as part of our own
          resource = info.get('build').get('install')[0]
          newObjectInfo[section]['install'].append(resource)

          # Add it to the command
          if section == 'build':
            newObjectInfo['build']['command'].append(resource['to'])

          # Collect the dependencies of the required package
          cycleBuildDependencies = info.get('build').get('dependencies', [])

          # We can ignore the first 8 since those are all the same (up to 'pip')
          cycleBuildDependencies = cycleBuildDependencies[8:]

          # If it is not part of our current set of requests, we can add it
          for check in cycleBuildDependencies:
            if check.get('name') == objectInfo.get('name'):
              # Do not consider ourselves
              continue

            found = False
            for subReq in canonical:
              if subReq[2] == check.get('id'):
                # Already taken care of here
                found = True
                break

            if not found:
              # We need this dependency, so we'll add it
              newObjectInfo[section]['dependencies'].append(check)

          # Then, we can add the general dependencies
          cycleDependencies = info.get('dependencies', [])

          # If it is not part of our current set of requests, we can add it
          for check in cycleDependencies:
            if check.get('name') == objectInfo.get('name'):
              # Do not consider ourselves
              continue

            found = False
            for subReq in canonical:
              if subReq[2] == check.get('id'):
                # Already taken care of here
                found = True
                break

            if not found:
              # We need this dependency, so we'll add it
              newObjectInfo['dependencies'] = newObjectInfo.get('dependencies', [])
              newObjectInfo['dependencies'].append(check)

              # We still add it as an ingest request
              ingestRequests.append({
                "packageType": "python-library",
                "packageName": ingestName,
                "pythonVersion": pythonVersion
              })
        else:
          subName = name
          if pythonVersion[0] == "2":
            subName = subName + "-2"

          dependencyInfo = {
            "id": id,
            "uid": uid,
            "name": subName,
            "type": "python-library"
          }

          if subVersion:
            # Update version specification to prepend python version
            versionRanges = subVersion.split(',')
            for i in range(len(versionRanges)):
              versionRange = versionRanges[i].strip()
              versionSubRanges = versionRange.split('||')
              for j in range(len(versionSubRanges)):
                versionSubRange = versionSubRanges[j].strip()
                conditional = re.split('[^>=<!~]', versionSubRange, 1)[0]
                versionSubRanges[j] = conditional + pythonVersion + "." + versionSubRange[len(conditional):]
              versionRanges[i] = " || ".join(versionSubRanges)
            compiledVersion = ','.join(versionRanges)
            dependencyInfo['version'] = compiledVersion
          elif url:
            dependencyInfo['version'] = "=" + pythonVersion + ".git"
          else:
            dependencyInfo['version'] = ">=" + pythonVersion + ".0"

          # See if we have the requested object as a PyPI object. Use that, if so.
          # If not, we will assume our own identity will ingest it and then request
          # it recursively later.
          checkObject = None
          try:
            checkObject = self.objects.retrieve(id = pypiId, version = dependencyInfo['version'])
          except:
            checkObject = None

          if checkObject:
            # We have a pypi version of the object
            dependencyInfo['id'] = pypiId
          else:
            # If it cannot be found already, ask the IngestManager to recursively
            # gather it.
            ingestRequests.append({
              "packageType": "python-library",
              "packageName": ingestName,
              "pythonVersion": pythonVersion
            })

            if subVersion:
              ingestRequests[-1]['version'] = subVersion

          if section == 'run':
            # Add to the tally when we have to do a second requirements parsing
            # pass. (when we see setup_requirements)
            # Remember: These go into the build dependencies upon the next stage
            newObjectInfo['run']['dependencies'].append(dependencyInfo)
          else:
            # If it needs 'pip', we ignore that requirement since it makes things
            # really, really messy. We just keep that in the build dependencies.
            subSourceURL = self.sourceURLFor("pip")
            pipId = self.idFor(identity, "pip", "pip", subSourceURL)

            if dependencyInfo.get('id') == pipId:
              newObjectInfo['build']['dependencies'].append(dependencyInfo)
            else:
              newObjectInfo['dependencies'] = newObjectInfo.get('dependencies', [])
              newObjectInfo['dependencies'].append(dependencyInfo)

    basePath = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "python-library"))

    report = {
      "files": filesList,
      "ingest": ingestRequests
    }

    report[section] = True

    return newObjectInfo, report

  def craft(self):
    """ Creates an Occam object from the given package resource.
    """

    # For Python packages, they use a build system written in python where the
    # packages are described in python code. This is unfortunate since the
    # determination of requirements/dependencies has to be done by executing
    # that python configuration.

    # Therefore, the first step (if the python package is not a wheel, see
    # below) is to create an initial object to download the python resource
    # and run the setup.py a particular way to parse the requirements.

    # If the python package is not the obsolete "egg" format, and is instead
    # a "wheel" format, the requirements are already listed and the code is
    # already compiled. The "wheel" packages are like binary distributions
    # where "egg" packages are source.

    # The second step (or first, for "wheel" packages) is to then use the
    # dependency list to create the object with a "build" and "init"
    # section. The "build" process will run the setup.py and then install
    # the files to the built object.

    # We then need to recursively gather the required packages.
