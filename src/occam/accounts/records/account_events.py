# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.databases.manager import table

@table("account_events")
class AccountEvent:
  """ Stores information about user account events.
  """

  schema = {

    # The username.
    "username": {
      "foreign": {
        "key": "username",
        "table": "accounts"
      }
    },

    # The originating IP address.
    "ip_address": {
      "type": "string",
      "length": 46,
      "null": False
    },

    # The event we are logging. This could be a successful login, failed login,
    # or password reset.
    "event_type": {
      "type": "string",
      "length": 32,
      "values": ["login_success", "login_failure", "password_change"],
      "null": False
    },

    # The time of the last failed login attempt.
    "event_time": {
      "type": "datetime",
      "null": False
    }
  }
