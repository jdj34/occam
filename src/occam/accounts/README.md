# The Accounts Component

This component manages the accounts that sponsor objects, create jobs, and
share research within Occam.

## Utility Accounts

Every object needs to be included in the system via an account. That is to say,
someone must be accountable for the object in terms of quota, TOS compliance,
etc. In the case of PyPI and other upstream object ingestion, it is recommended
to create a utility / service account that will be the sponsor. This account
would ideally have a higher (or unlimited) quota for object storage.
