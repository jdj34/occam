# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql

from occam.databases.manager import uses, datastore

from occam.objects.database import ObjectDatabase

@uses(ObjectDatabase)
@datastore("accounts")
class AccountDatabase:
  """ Manages the database interactions for the Account component.
  """

  def queryMembershipsFor(self, identity, key = None, keyAs = None):
    """ Retrieves a query the returns a row for each MembershipRecord associated with the given Person Object.

    Arguments:
      identity (str): The identity to query memberships for.
      key (str): The key (if any) to retrieve for the query.
      keyAs (str): What to rename the key as.
    """

    if keyAs is None:
      keyAs = key

    memberships = sql.Table("memberships")

    if key is None:
      query = memberships.select()
    else:
      query = memberships.select(memberships.__getattr__(key).as_(keyAs))

    query.where = (memberships.identity_uri == identity)
    query.where = query.where & (memberships.reference_count > 0)

    return query

  def queryMembershipFor(self, group, member):
    """ Retrieves a query for the specific row pertaining to the MembershipRecord
        associated with the given Person group and Person member.

    Arguments:
      group (str): The identity associated with the group.
      member (str): The identity to look up within the group.

    Returns:
      sql.Query: The query to retrieve the membership record.
    """

    query = self.queryMembershipsFor(member)
    query.where = query.where & (memberships.base_identity_uri == group)

    return query

  def retrieveMembershipsFor(self, identity):
    """ Retrieves a list of each MembershipRecord associated with the given identity.

    Arguments:
      identity (str): The identity URI to look up.

    Returns:
      list: A set of MembershipRecord items for each membership of this identity.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsFor(identity)

    self.database.execute(session, query)

    records = self.database.many(session, size=1000)
    return [MembershipRecord(x) for x in records]

  def retrieveMembershipObjectsFor(self, person):

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsFor(person, key = "base_identity_uri", keyAs = "id")
    return self.objects.retrieveObjectsForIds(query)

  def queryMembershipsOf(self, identity, key = None, keyAs = None):
    """ Retrieves a query the returns a row for each MembershipRecord associated
        with the given (group) identity.

    Arguments:
      identity (str): The identity URI to look up for the group.
      key (str): The key, if any, to retrieve from the record.
      keyAs (str): What to rename that key to.

    Returns:
      sql.Query: The query to retrieve all membership records for given identity.
    """

    if keyAs is None:
      keyAs = key

    memberships = sql.Table("memberships")

    if key is None:
      query = memberships.select()
    else:
      query = memberships.select(memberships.__getattr__(key).as_(keyAs))

    query.where = (memberships.base_identity_uri == identity)
    query.where = query.where & (memberships.reference_count > 0)

    return query

  def retrieveMembershipsOf(self, identity):
    """ Retrieves a list of each MembershipRecord that has the given identity as a member.

    Arguments:
      identity (str): The identity to look up.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsOf(identity)

    self.database.execute(session, query)

    records = self.database.many(session, size=1000)
    return [MembershipRecord(x) for x in records]

  def _retrieveMembershipObjectsOf(self, identity):
    """ Returns the Object representing the members of the given group.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipsOf(person, key = "identity_uri", keyAs = "id")
    return self.objects.retrieveObjectsForIds(query)

  def retrieveMembershipFor(self, group, member):
    """ Returns the MembershipRecord for the given group and member.

    Arguments:
      group (str): The identity URI for the group.
      member (str): The identity URI for the member of the group.

    Returns:
      MembershipRecord: The record of the membership of the member with the group.
    """

    from occam.accounts.records.membership import MembershipRecord

    session = self.database.session()

    query = self.queryMembershipFor(group, member)

    self.database.execute(session, query)
    record = self.database.fetch(session)
    if record:
      record = MembershipRecord(record)

    return record

  def insertMembership(self, group, member):
    import sql.conditionals

    # Create Membership record to link group to the member

    # Then, create a Membership record to link that member to all owners of that group
    # Or, increment the record if it already exists (since you can be a member
    #   of a particular subgroup more than once through different intermediaries)
    # Poll until the record is created/updated.

    # This SQL query will perform an upsert with an increment in SQLite3
    #"with new (identity_uri, base_identity_uri) as ( values(%, %) ) insert or replace into memberships (id, person_object_id, base_identity_uri, reference_count) select old.id, new.person_object_id,  new.base_identity_uri, (old.reference_count+1) from new left join memberships as old on new.person_object_id = old.person_object_id and new.base_identity_uri = old.base_identity_uri"

    # Or??
    #"begin; insert into blah if not exists(); update blah = blah + 1; commit;"

    # Or this:
    #"insert or replace into memberships (id, identity_uri, base_identity_uri, reference_count) values ((coalesce((select id from memberships where person_object_id = 0 and base_identity_uri = 1), NULL)), 0, 1, coalesce((select reference_count from memberships where person_object_id = 0 and base_identity_uri = 1), 0) + 1);"

    # We go for, instead however, a two command transaction, for ease of implementation and to conform with python-sql
    # and the most portable SQL:

    session  = self.database.session()

    memberships = sql.Table("memberships")

    objectQuery = self.objects.queryObjectRecordFor(member, key = "id")
    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")
    baseMembershipQuery = self.queryMembershipsFor(group).select(memberships.base_identity_uri)

    # Insert the initial record for the given member and group

    query = memberships.insert()
    query.columns = [memberships.identity_uri, memberships.base_identity_uri]
    query.values = sql.Select(columns = [objectQuery, baseObjectQuery])
    query.values.where = sql.operators.Not(sql.operators.Exists(memberships.select(sql.Literal(1), where = (memberships.identity_uri.in_(objectQuery)) & (memberships.base_identity_uri.in_(baseObjectQuery)))))

    # INSERT INTO memberships (identity_uri, base_identity_uri) VALUES
    #   (SELECT <objectQuery.id>, <baseObjectQuery.id> WHERE NOT EXISTS
    #     (SELECT 1 FROM 'memberships' WHERE
    #        memberships.identity_uri IN <objectQuery> AND memberships.base_identity_uri IN <baseObjectQuery>))
    # (IN is used in the final clause because the sql query creation is messed up when it is '=')
    self.database.execute(session, query)

    # Insert the extra records for membership in the group's groups

    # We need to reference the table a different way so it gets a new alias (that is, it is assigned a different "AS x")
    membershipsB = sql.Table("memberships")
    query = memberships.insert()
    query.columns = [memberships.identity_uri, memberships.base_identity_uri]
    subQuery = memberships.select(objectQuery, memberships.base_identity_uri)
    subQuery.where = (memberships.identity_uri.in_(baseObjectQuery))
    subQuery.where = subQuery.where & sql.operators.Not(sql.operators.Exists(membershipsB.select(sql.Literal(1), where = (membershipsB.identity_uri.in_(objectQuery)) & (membershipsB.base_identity_uri == (memberships.base_identity_uri)))))
    query.values = subQuery

    # INSERT INTO memberships (identity_uri, base_identity_uri) VALUES
    #   (SELECT <objectQuery.id>, base_identity_uri AS 'a' WHERE
    #     a.identity_uri = <baseObjectQuery.id> AND NOT EXISTS
    #       (SELECT 1 FROM 'memberships' AS 'b' WHERE b.identity_uri IN <objectQuery> AND
    #         b.base_identity_uri = a.base_identity_uri))
    self.database.execute(session, query)

    # Increment the records

    query = memberships.update(columns = [memberships.reference_count], values=[memberships.reference_count + sql.Literal(1)])

    query.where = (memberships.identity_uri.in_(objectQuery))
    query.where = query.where & ((memberships.base_identity_uri.in_(baseMembershipQuery)) | (memberships.base_identity_uri.in_(baseObjectQuery)))

    # UPDATE 'memberships' (reference_count) VALUES (reference_count + 1) WHERE
    #   identity_uri = <objectQuery.id> AND
    #   (base_identity_uri IN (<baseMembershipQuery>) OR
    #     base_identity_uri = <baseObjectQuery.id>) AND
    #   reference_count > 0
    self.database.execute(session, query)

    # Retrieve the field
    query = self.queryMembershipFor(group, member)
    self.database.execute(session, query)

    # Perform the transaction
    self.database.commit(session)

    record = self.database.fetch(session)
    if record:
      from occam.accounts.records.membership import MembershipRecord
      record = MembershipRecord(record)

    return record

  def deleteMembership(self, group, member):
    """ Removes the member from the given group.
    """

    # Gather all membership records pointing from that person to the group in question
    # Decrement their reference counts
    # Delete them if they are now zero

    session  = self.database.session()

    memberships = sql.Table("memberships")

    # Decrement reference counts for the existing records
    # First, the one between member and group
    # And also, all memberships between member and base objects of group
    # This is easier if there exists a membership between person -> themselves

    query = memberships.update(columns = [memberships.reference_count], values = [memberships.reference_count - sql.Literal(1)])

    objectQuery = self.objects.queryObjectRecordFor(member, key = "id")
    baseObjectQuery = self.objects.queryObjectRecordFor(group, key = "id")
    baseMembershipQuery = self.queryMembershipsFor(group).select(memberships.base_identity_uri)

    query.where = (memberships.identity_uri.in_(objectQuery))
    query.where = query.where & ((memberships.base_identity_uri.in_(baseMembershipQuery)) | (memberships.base_identity_uri.in_(baseObjectQuery)))
    query.where = query.where & (memberships.reference_count > 0)

    # UPDATE 'memberships' (reference_count) VALUES (reference_count - 1) WHERE
    #   identity_uri = <objectQuery.id> AND
    #   (base_identity_uri IN (<baseMembershipQuery>) OR
    #     base_identity_uri = <baseObjectQuery.id>) AND
    #   reference_count > 0
    self.database.execute(session, query)

    # Delete all memberships with 0 in the reference_count

    # If another query beats us to this and upserts the reference_count + 1, this is ok
    # This will only trigger when the reference_count is in fact 0
    # We don't necessarily care that this record is destroyed as we only pull out
    # records when the reference_count > 0, but clean-up is good and can obscure
    # membership when people are removed, which may be useful for privacy etc.
    # (Although, the transaction should be atomic, so not a real worry)
    query = memberships.delete()

    query.where = (memberships.identity_uri.in_(objectQuery))
    query.where = query.where & (memberships.reference_count == 0)

    # DELETE FROM 'memberships' WHERE
    #   identity_uri IN <objectQuery> AND
    #   reference_count = 0
    self.database.execute(session, query)

    # Commit the transaction
    self.database.commit(session)

  def queryAccounts(self, key = None):
    """ Returns a general SQL query for all account records.

    Arguments:
      key (str): The key to pull from the query.

    Returns:
      sql.Query: The SQL query to retrieve any AccountRecord.
    """

    accounts = sql.Table("accounts")
    if key:
      query = accounts.select(accounts.__getattr__(key))
    else:
      query = accounts.select()

    return query

  def queryAccount(self, username=None, email=None, identity=None, key=None):
    """ Returns an SQL query to retrieve the Account for the given criteria.

    Arguments:
      username (str): A username.
      email (str): An email.
      identity (str or sql.Query): The URI of the identity or a query.
      key (str): The key to pull from the query.

    Returns:
      sql.Query: The SQL query to retrieve the AccountRecord.
    """

    accounts = sql.Table("accounts")
    if key:
      query = accounts.select(accounts.__getattr__(key))
    else:
      query = accounts.select()

    query.where = sql.Literal(True)

    if not isinstance(identity, str) and identity is not None:
      query.where = (accounts.identity_key_id.in_(identity))
    elif identity is not None:
      query.where = (accounts.identity_key_id == identity)

    if username is not None:
      query.where = query.where & (accounts.username == username)

    if email is not None:
      query.where = query.where & (accounts.email == email)

    return query

  def queryPeople(self):
    """ Returns an SQL query to retrieve all Objects representing the Person
        for all accounts.

    Returns:
      sql.Query: The SQL query to retrieve all items each as an ObjectRecord.
    """

    subQuery = self.queryAccounts(key = "person_id")

    return self.objects.queryObjects(id = subQuery)

  def queryPerson(self, identity):
    """ Returns an SQL query to retrieve the Object for the Person associated
        with the given identity.

    Arguments:
      identity (str): The URI of the identity.

    Returns:
      sql.Query: The SQL query to retrieve the ObjectRecord.
    """

    subQuery = self.queryAccount(identity = identity, key = "identity_uri")

    return self.objects.queryObjects(id = subQuery)

  def retrieveAccount(self, username=None, email=None, identity=None):
    """ Retreives the Account for the given identity.

    Arguments:
      username (str): A username.
      email (str): An email.
      identity (str): The identity URI of the account.

    Returns:
      AccountRecord: The information about the account.
    """

    from occam.accounts.records.account import AccountRecord

    session = self.database.session()

    query = self.queryAccount(username = username,
                              email = email,
                              identity = identity)

    self.database.execute(session, query)
    row = self.database.fetch(session)
    if row is None:
      return None

    return AccountRecord(row)

  def retrieveAccountEvents(self, username=None, ipAddress=None,
                                                 eventTypes=[],
                                                 fromTime=None,
                                                 latest=False):
    """ Retrieves account event records.

    Arguments:
      username (str): The username for the account involved in the event.
      ipAddress (str): The IP address where the event originated.
      fromTime (datetime): Time after which we want to see records.
      latest (bool): Whether only the most recent record should be returned.

    Returns:
      list: Account event records matching the given parameters.
    """
    from occam.accounts.records.account_events import AccountEvent

    session = self.database.session()

    events = sql.Table("account_events")

    query = events.select()
    query.where = sql.Literal(True)
    if username:
      query.where = (events.username == username)
    if ipAddress:
      query.where = query.where & (events.ip_address == ipAddress)
    if eventTypes:
      # If the list of event types we want to see is greater than one we will use
      # SQL boolean logic to determine a match.
      subquery = (events.event_type == eventTypes[0])
      if len(eventTypes) > 1:
        for t in eventTypes[1:]:
          subquery = subquery | (events.event_type == t)

      query.where = query.where & subquery

    if fromTime:
      query.where = query.where & (events.event_time >= fromTime)

    query.order_by = sql.Desc(events.event_time)

    if latest:
      query.limit = 1

    size = 100
    self.database.execute(session, query)
    rows = self.database.many(session, size = size)
    return [AccountEvent(x) for x in rows]

  def retrieveAllAccounts(self):
    """ Retrieves all accounts in the system.

    Returns:
      list: List of AccountRecords.
    """

    from occam.accounts.records.account import AccountRecord

    # Open a database session
    session = self.database.session()

    # Query for all 'email' keys
    query = self.queryAccounts()

    # Execute the query
    self.database.execute(session, query)

    # Get up to 5000 accounts
    records = self.database.many(session, size=5000)

    # Since email is None if not provided
    return [AccountRecord(account) for account in records]

  def updateAccount(self, identity, roles=None, subscriptions=None,
                                                personId=None,
                                                resetToken=None):
    """ Updates the Account record with the given information.

    Arguments:
      identity (str): The identity URI of the account.
      roles (list): A list of strings containing the roles for the account.
      subscriptions (list): A list of strings containing subscription events
        for which the account owner wants notifications.
      personId (str): The unique ID of the Person to associate to this Account.
      resetToken: The reset token the user can use to reset the account
        password.
    """

    from occam.accounts.records.account import AccountRecord

    session = self.database.session()

    query = self.queryAccount(identity = identity)

    # Retrieve the record
    self.database.execute(session, query)

    row = self.database.fetch(session)
    if row is None:
      return

    # Update fields within the row.
    row = AccountRecord(row)

    if resetToken:
      row.reset_token = resetToken

    if personId:
      row.person_id = personId

    if roles is not None:
      row.roles = ';' + ';'.join(roles) + ';'

    if subscriptions is not None:
      row.subscriptions = ';' + ';'.join(subscriptions) + ';'

    # Update
    self.database.update(session, row)

    # Commit the transaction
    self.database.commit(session)

  def queryAuthors(self, id, identity=None, kind="authorships", delete=False):
    """ Retrieves the sql.Query to get every author of the given object.

    Arguments:
      id (str): The object id.
      kind (str): The type of author relationship.
      delete (bool): Returns a DELETE query instead of a SELECT.

    Returns:
      sql.Query: The query to retrieve every Object for the author list.
    """

    authorships = sql.Table(kind)

    query = authorships.select(authorships.identity_uri)
    if delete:
      query = authorships.delete()

    query.where = authorships.internal_object_id == id

    if identity:
      query.where = query.where & (authorships.identity_uri == identity)

    return self.objects.queryObjects(id = query)

  def retrieveAuthors(self, id, kind = "authorships"):
    """ Retrieves the person Objects for every author of the given object.

    Arguments:
      id (str): The object id.
      kind (str): The type of author relationship.

    Returns:
      list: A list of ObjectRecord items for each author.
    """

    from occam.objects.records.object import ObjectRecord

    session = self.database.session()

    query = self.queryAuthors(id, kind = kind)

    self.database.execute(session, query)

    rows = self.database.many(session, size=20)
    return list(map(lambda x: ObjectRecord(x), rows))

  def addAuthor(self, id, identity, kind = "authorships"):
    """ Creates a record associating the given object with the given identity.

    Arguments:
      id (str): The object id.
      identity (str): The identity URI to add as an author.
      kind (str): The type of author relationship.
    """

    from occam.accounts.records.authorship import AuthorshipRecord
    from occam.accounts.records.collaboratorship import CollaboratorshipRecord

    session = self.database.session()

    if kind == "authorships":
      record = AuthorshipRecord()
    else:
      record = CollaboratorshipRecord()

    record.identity_uri = identity
    record.internal_object_id = id

    self.database.update(session, record)
    self.database.commit(session)

  def removeAuthor(self, id, identity, kind = "authorships"):
    """ Removes the record associating the given object with the given identity.

    Arguments:
      id (str): The object id.
      identity (str): The identity URI to remove as an author.
      kind (str): The type of author relationship.
    """

    session = self.database.session()

    query = self.queryAuthors(id, identity = identity, kind = kind, delete = True)
    self.database.execute(session, query)

  def updateEmail(self, account_db, email, expiration, token):
    """ Updates the email.

    Arguments:
      account_db (AccountRecord): The account to update.
      email (str): The new email address to store.
      expiration (datetime): The datetime when the token will be rejected.
      token (str): The reset token that needs to be matched.
    """

    session = self.database.session()

    # If we have no other known email, record it immediately
    if not account_db.email:
      account_db.email = email

    # Record the intended new email
    account_db.email_pending = email
    account_db.email_token = token
    account_db.email_token_expiration = expiration

    # Update the account record
    self.database.update(session, account_db)
    self.database.commit(session)

  def setEmailVisibility(self, account_db, isPublic):
    """ Sets email visibility for an account.
  
    Arguments:
      account_db (AccountRecord): The account to update.
      isPublic (bool): The value email_public will take.
    """

    account_db.email_public = isPublic

    # Update the account record
    session = self.database.session()
    self.database.update(session, account_db)
    self.database.commit(session)

  def setPasswordResetToken(self, account_db, expiration, token):
    """ Marks the account as requesting a password reset.

    Arguments:
      account_db (AccountRecord): The account to update.
      expiration (datetime): The datetime when the token will be rejected.
      token (str): The reset token that needs to be matched.
    """

    account_db.reset_token = token
    account_db.reset_token_expiration = expiration

    session = self.database.session()
    self.database.update(session, account_db)
    self.database.commit(session)

  def verifyEmail(self, account_db):
    """ Marks the account as having a validated email.

    Arguments:
      account_db (AccountRecord): The account to update.
    """

    # Copy the pending email to the official email
    account_db.email = account_db.email_pending
    account_db.email_verified = True

    session = self.database.session()
    self.database.update(session, account_db)
    self.database.commit(session)

  def updatePassword(self, account_db, hashed_password):
    """ Updates the hashed password for the given account.

    Arguments:
      account_db (AccountRecord): The account to update.
      hashed_password (str): The hashed password string to store.
    """

    session = self.database.session()

    # Create password auth
    account_db.hashed_password = hashed_password

    self.database.update(session, account_db)
    self.database.commit(session)

  def createAccount(self, username = None,
                          person_id = None,
                          hashed_password = None,
                          identity = None,
                          secret = None,
                          email = None,
                          roles = None,
                          subscriptions = None,
                          active = True):
    """ Creates a new AccountRecord with the given information.

    Arguments:
      username (str): The username for the account.
      person_id (str): The object id for the Person associated with the account.
      hashed_password (str): The hashed form of the password.
      identity (str): The identity URI of the actor.
      secret (str): The secret for the account.
      email (str): The email address attached to the account.
      roles (list): A set of roles to apply to the account.
      subscriptions (list): A set of email subscriptions to apply to the account.
      active (bool): Whether or not the account is active.
    """

    from occam.accounts.records.account import AccountRecord

    # Set password (if exists)
    account = AccountRecord()

    # Update the properties
    account.active = active
    account.email = email
    account.secret = secret
    account.identity_key_id = identity
    account.hashed_password = hashed_password
    account.person_id = person_id
    account.username = username

    # Update roles
    account.roles = ';' + ';'.join(roles or []) + ';'

    # Update subscriptions
    account.subscriptions = ';' + ';'.join(subscriptions or []) + ';'

    # Save
    session = self.database.session()
    self.database.update(session, account)
    self.database.commit(session)

    # Return the new account
    return account

  def createAccountEvent(self, username, ipAddress, eventType, eventTime):
    """ Creates a new account event record with the given information.

    Arguments:
      username (str): The username used to attempt a login.
      ipAddress (str): The client IP address requesting authentication.

    Returns:
      AccountEvent: The new event record.
    """
    from occam.accounts.records.account_events import AccountEvent

    event = AccountEvent()

    # Update the properties.
    event.username = username
    event.ip_address = ipAddress
    event.event_type = eventType
    event.event_time = eventTime

    # Save.
    session = self.database.session()
    self.database.update(session, event)
    self.database.commit(session)

    # Return the new event.
    return event

  def countAccounts(self):
    """ Returns the number of accounts on the system.
    """

    import sql

    accounts = sql.Table('accounts')

    session = self.database.session()
    query = accounts.select(sql.aggregate.Count(sql.Literal(1)))

    self.database.execute(session, query)
    return next(iter(self.database.fetch(session).values()))
