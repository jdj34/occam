# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.manager import uses

from occam.commands.manager import command, option, argument

from occam.accounts.manager import AccountManager

@command('accounts', 'whoami')
@uses(AccountManager)
class WhoamiCommand:
  """ Describes the person currently authenticated.
  """

  def do(self):

    ident = None
    if self.person:
      ident = {
        "name": self.person.name,
        "identity": self.person.identity,
        "id": self.person.id,
        "uid": self.person.uid,
      }
    Log.output(f"{ident}")

    return 0
