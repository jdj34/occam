# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import getpass, json, datetime

from occam.log import Log

from occam.manager import uses

from occam.config import Config

from occam.commands.manager import command, option, argument

from occam.accounts.manager import AccountManager
from occam.objects.manager import ObjectManager
from occam.messages.manager import MessageManager

@command('accounts', 'login')
@argument("username_or_email", type = str)
@option("-p", "--password",       dest   = "password",
                                  action = "store",
                                  help   = "the password to use for the account")
@option("-t", "--generate-token", dest   = "generate_token",
                                  action = "store_true",
                                  help   = "when given, outputs the authentication token instead of authenticating a terminal session")
@option("-f", "--filter",         dest   = "filter",
                                  action = "store",
                                  help   = "will only output the given parameter: token, identity")
@option("-i", "--address",        dest   = "ip_address",
                                  action = "store",
                                  help   = "when given, the IP address of the client device")
@uses(MessageManager)
@uses(AccountManager)
@uses(ObjectManager)
class LoginCommand:
  """ Authenticates via password in the terminal creating an authentication token.

  When you are on the command-line, you can use occam login to authenticate the
  current user. It will store the authentication token in the user's home path.
  When Occam runs any command that requires authentication, it will read that
  auth token (usually in .occam/auth.json) and use that token as though it were
  passed as "-T".
  
  You can also simply generate a token which can be used to authenticate an action
  in any terminal session. The stdout of the login command will be the auth token
  you would pass as "-T" to any occam command. You also need to pass the "-A" (or
  --as) argument with the id of the Person you are trying to be, as the secret
  key used to decrypt the token is per-Person, not per-Node.
  """

  def do(self):
    import i18n

    Log.header("Authenticating")

    username = None
    email = None
    usingEmail = self.accounts.isEmailAddressValid(self.options.username_or_email)
    if usingEmail:
      email = self.options.username_or_email
    else:
      username = self.options.username_or_email

    # Get the password
    password = None
    if self.options.password is not None:
      password = self.options.password
    else:
      Log.write(key = "occam.accounts.commands.login.awaitingPassword")
      prompt = i18n.t(key = "occam.accounts.commands.login.passwordPrompt",
                      username = self.options.username_or_email)
      password = getpass.getpass(prompt)

    # Retrieve the account information for this username.
    account = self.accounts.retrieveAccount(username = username, email = email)
    if account is None:
      if usingEmail:
        Log.error("email or password is invalid")
      else:
        Log.error("username or password is invalid")
      return -1

    # Get IP address
    ip = self.options.ip_address
    if ip is None:
      ip = "localhost"

    # Send a warning if the account has seen a lot of failures from this IP.
    # The point is to alert the user that someone is trying to unsuccessfully
    # get into the account. In the event that they weren't the one trying to
    # log in, they should be made aware of it. Ideally we would also be made
    # aware of it and be able to ban the originating IP.
    #
    # NOTE: We only send on the maxFailCount, not above the maxFailCount
    # because if someone is trying to breach the account we do NOT want the
    # user to be spammed with email for each attempt over the threshold.

    pwConfig = Config().load().get("passwords", {})
    loginFailAlertThreshold = pwConfig.get("loginFailAlertThreshold", 0)
    failures = self.accounts.loginFailuresSinceSuccess(account.username,
                                                       ipAddress = ip)

    if loginFailAlertThreshold > 0 and failures == loginFailAlertThreshold:
      self.accounts.sendExcessiveLoginFailMessage(account,
                                                  ipAddress = ip,
                                                  failureCount = loginFailAlertThreshold )

    if self.accounts.isAccountLocked(account.username, ipAddress = ip):
      Log.error("Too many failed login attempts, please try again later.")
      return -1

    # Determine if credentials match
    if not self.accounts.authenticate(username = account.username,
                                      password = password):
      # Record failed login. If possible, send message to account holder after
      # 5 attempts
      self.accounts.recordAccountEvent(username = account.username,
                                      ipAddress = ip,
                                      eventType = "login_failure",
                                      eventTime = datetime.datetime.now())
      if usingEmail:
        Log.error("email or password is invalid")
      else:
        Log.error("username or password is invalid")
      return -1

    # Generates a session token for the current terminal for that account
    if self.options.generate_token:
      # Retrieve the Person that holds the name and metadata for the account
      personObject = self.accounts.retrievePerson(account.identity_key_id)
      personInfo = self.objects.infoFor(personObject)
      personInfo['id'] = personObject.id

      # Generate an authentication token
      token = self.accounts.generateToken(account, personObject, "person")

      Log.write("auth token: ", end="")
      output = {
        "token": token,
        "identity": account.identity_key_id,
        "person": personInfo,
        "roles": personObject.roles
      }

      if self.options.filter == "token":
        Log.output(output.get('token'))
      elif self.options.filter == "identity":
        Log.output(output.get('identity'))
      else:
        Log.output(json.dumps(output))
    else:
      self.accounts.generateAuthentication(account)

    self.accounts.recordAccountEvent(username = account.username,
                                    ipAddress = ip,
                                    eventType = "login_success",
                                    eventTime = datetime.datetime.now())
    Log.done("successfully authenticated as %s" % (account.username))
    return 0
