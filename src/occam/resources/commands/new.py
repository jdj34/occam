# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.resources.write_manager import ResourceWriteManager
from occam.discover.manager        import DiscoverManager

from occam.object import Object
from occam.log import Log

import os
import json

@command('resources', 'new',
  category      = 'Resource Inspection',
  documentation = "Creates and mirrors a new resource.")
@argument("type", type = str)
@argument("name", type = str)
@argument("source", type = str)
@uses(ResourceWriteManager)
@uses(DiscoverManager)
class ResourcesNewCommand:
  """ The new command will ingest a resource.
  """

  def do(self):
    if self.person is None or not hasattr(self.person, 'id'):
      Log.error("Must be authenticated to store new objects")
      return -1

    info = {
      'source': self.options.source,
      'name': self.options.name,
      'subtype': self.options.type,
      'type': 'resource'
    }

    id, uid, revision, subDependencies, data_path = self.resources.write.pull(info, self.person.identity)

    ret = info
    ret['id'] = id
    ret['uid'] = uid
    ret['revision'] = revision

    Log.output(json.dumps(ret))
    return 0
