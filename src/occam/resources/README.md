# The Resources Component

This component handles resource installation.

This is the process OCCAM uses to resolve "install" sections of any
Object.

When a resource is newly pulled from source, the uid and revision/hash
are not known. So, it is pulled to a temporary place near where the
resource would be stored and then moved when the name is known. An Object
and Resource record is created as necessary.

For files, the resource data is a binary blob. Other types of resources
are also possible. Git resources are directories that are maintained by
the git data structure. In these cases, the resource is versioned by its
own mechanism. Files and binary blobs, on the other hand, are versioned
through an external mechanism: a hash.

Therefore, each repository plugin can decide how it handles versioning.
The FileResource plugin, for example, will be given a path to its
resource data as stored by the StorageManager and will return a path
to the requested revision as ``<base path>/<revision hash>``. Whereas a
GitResource will simple parrot the ``<base path>`` as revisions are kept
as metadata within the git resource rooted at that path.

This is why, as strange as it may look when viewing one over the others,
the resource plugin asks for a path and then repeats that path back to
the other subroutines within the resource implementation. A FileResource,
when :meth:`~occam.resources.plugins.file.FileResource.retrieve` is called, 
passing it ``<base path>/<resource hash>`` allows it to form a direct
path to the resource at ``<base path>/<resource hash>/data`` whereas the
GitResource gets passed ``<base path>`` and remotely executes git to pull
out the file data with ``git show`` using the ``<base path>`` as the
working directory.

Examples of different types of Resources are available in plugins. The
FileResource is an example of a binary blob. The TarResource inherits the
functionality of a File but adds archival and directory traversal within the
resource. GitResource shows you a versioned resource that is self-maintained.
DockerResource gives an example of a resource that is self-stored using some
external tooling and is not installed as part of an Object's local filesystem.
