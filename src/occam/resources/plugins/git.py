# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.object import Object
from occam.log    import loggable
from occam.git_repository import GitRepository as GitRepo

from occam.resources.manager import resource, ResourceManager

from occam.network.manager import NetworkManager

from occam.manager import uses

# urljoin for resolving relative urls
try: from urllib.parse import urljoin
except ImportError: from urlparse import urljoin

from uuid import uuid4

@resource('git')
@uses(NetworkManager)
@loggable
class GitResource:
  """ This is an OCCAM resource engine for handling repeatable Git retrieval
  and storage.
  """

  def pathFor(self, uuid, path, revision=None):
    """ Returns the path to the stored git repository on disk (if it would exist)
    for the given uuid.
    """

    # The repository is stored in the base path
    return path

  def urlFor(self, uuid, revision, protocol=None):
    """ Determines the URL for the given resource data.

    This URL can be used as an 'alternative source' or mirror of the data and
    can be used when pulling the resource from a remote node.
    """

    # This URL is used even over an occam daemon protocol
    return f"{uuid}/{revision}"

  def actions(self, uuid, revision, path, resourceInfo, identity, destination):
    """ Performs any actions that may be required upon installation.
    """

    return None

  def retrieve(self, uuid, revision):
    """
    Retrieves the stored git repository at the given uuid at the given
    revision.
    """

    gitPath = self.pathFor(uuid, revision)
    obj = self.occam.objects.retrieve(uuid)

    if self.exists(uuid, revision):
      return gitPath

    return None

  def install(self, uuid, revision, path, resourceInfo, identity, destination):
    """ Installs the git repository via the given uuid and revision to the given path.
    """

    git_path = os.path.join(path, "repository")
    git = GitRepo(git_path, revision=revision)
    GitResource.Log.noisy("Cloning git repository at revision %s from %s to %s" % (revision, git_path, destination))
    git.clone(destination)

    installedGit = GitRepo(destination)
    installedGit.reset(revision)
    installedGit.submoduleInit()
    installedGit.submoduleUpdate()

    # Ensure we also link up submodules
    submodules = installedGit.gitmodules()
    config     = installedGit.config()

    source = resourceInfo.get('source', '')
    if not source.endswith("/"):
      # It needs to end with a '/' for urljoin to work properly
      source = source + "/"

    #for key in submodules:
    #  if key.startswith('submodule '):
    #    tokens = list(filter(None, key.split(' ')))
    #    if len(tokens) > 1:
    #      submoduleName = tokens[1]
    #      submoduleURL = submodules[key].get('url')

          # Reform URL relative to this source
    #      submoduleURL = urljoin(source, submoduleURL)

          # Get the resource object by searching by URL
     #     submoduleObject = self.resources.retrieveResource(resourceType='git', source=submoduleURL)
     #     if not submoduleObject is None:
     #       submoduleID = submoduleObject.uid

            # Store submodule
     #       GitResource.Log.noisy("Pointing submodule %s to %s" % (submoduleName, submoduleID))

     #       config['submodule %s' % (submoduleName)] = {}
     #       config['submodule %s' % (submoduleName)]['url'] = internalURL

    # Update git config to point to our version of any known submodules
    # May break when submodules are unknown
    installedGit.configUpdate(config)

    return True

  def pull(self, uuid, revision, name, source, to, path, identity, cert=None, headers=None, existingPath=None):
    """ Retrieves and stores the given git resource according to the given resource info.
    
    Returns a reference of the Object in the store. The certificate path
    necessary to retrieve the resource over HTTPS can be given.
    """

    name = name or "Git Repository"

    git = None
    gitPath = None
    dependencies = []
    git_obj = None

    # This is the path to the aggregated commit pool
    poolPath = os.path.join(path, "repository")

    # This is the path to the index unique to a particular identity
    identityPath = os.path.join(path, identity)

    # Initialize the commit pool
    if not GitRepo.exists(poolPath):
      os.mkdir(poolPath)
      GitRepo.create(poolPath)

    # Initialize the archived index
    alternates = "../../../repository"
    if not GitRepo.exists(identityPath):
      os.mkdir(identityPath)
      GitRepo.create(identityPath, alternates=alternates)

    sources = []
    if existingPath:
      sources.append(existingPath)

    if self.network.isGitAt(source, cert=cert):
      sources.append(source)

    for sourceURL in sources:
      if self.network.isGitAt(sourceURL, cert=cert):
        # Pull in content from the referenced source
        GitResource.Log.write(f"Pulling resource from network at: {sourceURL}")
      else:
        # Pull in content from the local source
        GitResource.Log.write(f"Pulling resource from local filesystem at: {sourceURL}")

      # Add a remote from the pool
      git = GitRepo(poolPath)

      from uuid import uuid4
      remote_name = str(uuid4())

      git.addRemote(remote_name, sourceURL)

      GitResource.Log.writePercentage("Fetching updates")

      def progress(reason):
        GitResource.Log.updatePercentage(reason["percent"],
                                         message="Fetching updates: " + reason["reason"])

      git.fetch(remote_name, tags=True, progressCallback=progress)
      git.rmRemote(remote_name)

      # Create a reference branch for the given identity
      git = GitRepo(identityPath)

      # We don't have a revision, so pull the most recent
      if not revision:
        remoteGit = GitRepo(sourceURL)
        revision = remoteGit.head()

      # Look up if the parent revision is tracked, and if so,
      # replace that tag with this revision
      parentRevision = git.parent(revision)

      if parentRevision and git.hasBranch("archived-" + parentRevision):
        GitResource.Log.noisy("Deleting revision branch %s at %s" % (parentRevision, poolPath))
        git.deleteRef("refs/heads/archived-%s" % (parentRevision))

      # Maintain a reference to this revision in the index
      GitResource.Log.noisy("Generating a revision branch %s at %s" % (revision, poolPath))
      git.updateRef("refs/heads/archived-%s" % (revision), revision)

      # We also want to copy the refs/tags from the alternates path
      # This way, they get cloned appropriately from mirrored repositories
      # Only mirrored external git repositories have tags (from GitLab, etc)
      #   on the commit pool.
      try:
        srcTagsPath = os.path.join(git.path, "..", "repository", ".git", "refs", "tags")
        destTagsPath = os.path.join(git.path, ".git", "refs", "tags")

        for tag in os.listdir(srcTagsPath):
          if not os.path.exists(destTagsPath):
            os.mkdir(destTagsPath)

          with open(os.path.join(srcTagsPath, tag), "r") as src:
            with open(os.path.join(destTagsPath, tag), "w+") as dest:
              dest.write(src.read())
      except FileNotFoundError:
        pass

      # Look at submodules
      git = GitRepo(sourceURL)
      config = git.config()
      gitmodules = git.gitmodules()
      dependencies = []

      # Archive submodules
      for key in config:
        if key.startswith('submodule '):
          tokens = list(filter(None, key.split(' ')))
          if len(tokens) > 1:
            submoduleName = tokens[1]
            submoduleURL = config[key].get('url')

            if key in gitmodules:
              submodulePath = gitmodules[key]['path']

            # Store submodule
            GitResource.Log.write("Found submodule %s at %s" % (submoduleName, submoduleURL))

            submoduleInfo = {
              'name': '%s (%s)' % (name, submoduleName),
              'source': submoduleURL,
              'type': 'resource',
              'subtype': 'git',
              'revision': git.submoduleRevisionFor(submodulePath),
              'to': os.path.join(to, submodulePath)
            }

            dependencies.append(submoduleInfo)

      revision = revision or git.head()

    return revision, dependencies, path

  def exists(self, uuid, path, revision=None):
    """ Returns True if a git repository containing the given revision is stored
    in the object store. If revision is not specified, it just looks for the
    presence of the git repository.
    """

    gitPath = self.pathFor(uuid, path, revision)
    poolPath = os.path.join(gitPath, "repository")
    gitMetadataPath = os.path.join(poolPath, '.git')

    # Determine if the given revision already exists
    if not os.path.exists(gitMetadataPath):
      return False

    if revision and GitRepo.hasRevision(poolPath, revision):
      return True

    return False

  def update(self, uuid, source):
    """ Updates the git repository object stored in the git store.
    """

    # Git updates just pull down any new commits into a random branch name.
    # These commits will be preserved by the branching semantics of git.
    # We can then return a new HEAD, if there is one.

    info = obj.objectInfo()
    object_type = info['type']

    if info.get('storable') == False:
      return

    uuid = info['id']
    path = self.git.objects.pathFor(uuid, 'git')

    # Get the repository's actual file path in the git store
    repo_path = os.path.join(path, info['file'])

    # Interface to Git for this repository in the git store
    store_git = GitRepo(repo_path)

    GitResource.Log.noisy("fetching from %s to %s" % (git.path, repo_path))

    # Create random remote and branch names
    remote_name = str(uuid4())
    new_branch_name = str(uuid4())

    # Add the remote to the local git repository
    GitResource.Log.noisy("adding remote %s -> %s" % (remote_name, git.path))
    store_git.addRemote(remote_name, git.path)

    # Fetch local branches
    store_git.fetchRemoteBranches(remote_name)

    # Checkout that branch from the remote
    GitResource.Log.noisy("checking out %s from %s/%s" % (new_branch_name, remote_name, git.branch()))
    store_git.checkoutBranch(new_branch_name, git.branch(), remote_name)

    # Remove generated remote name
    store_git.rmRemote(remote_name)

    # Done

  def clonable(self):
    """
    Git repositories are indeed clonable.
    """

    return True

  def clone(self, uuid, revision, name, source):
    """
    Clones the repository and returns a new resource tag.
    """

    GitResource.Log.write("Forking git repository %s (%s)" % (name, uuid))

    # Git updates just pull down any new commits into a random branch name.
    # These commits will be preserved by the branching semantics of git.
    # We can then return a new HEAD, if there is one.

    path = self.occam.objects.pathFor(uuid, 'git')

    # Interface to Git for this repository in the git store
    store_git = GitRepo(path)

    # Create a new identifier
    newUUID = Object.uuid('')

    # Create git object
    self.store(store_git, newUUID, revision, name, source, to)

    # Done
    return {
      'id':       newUUID,
      'revision': revision,
      'name':     name,
      'source':   source
    }

  def currentRevision(self, path):
    """
    Returns the current revision of the object. This is used to detect changes.
    When this revision differs from the one an object current is using during
    a commit, then it may be updated in that object. For instance, in an 'occam
    update' command.
    """
    git = GitRepo(os.path.join(path, "repository"))
    if git is None:
      return None

    return git.head()

  def commit(self, uuid, revision, name, source, path):
    """
    Will commit the resource at the current path to the store as an updated
    revision if it has changed. It will return the updated resource info.
    """

    dirty = False

    git = GitRepo(path)

    newRevision = self.currentRevision(path)

    # If there is a problem, just say it didn't change
    if newRevision is None or git is None:
      newRevision = revision

    # If it changed, push new content up to the store
    if newRevision != revision:
      GitResource.Log.write("Pushing updated git content")
      dirty = True

      # Git updates just pull down any new commits into a random branch name.
      # These commits will be preserved by the branching semantics of git.

      store_path = self.occam.objects.pathFor(uuid, 'git')

      # Interface to Git for this repository in the git store
      store_git = GitRepo(store_path)

      GitResource.Log.noisy("fetching from %s to %s" % (git.path, store_path))

      # Create random remote and branch names
      remote_name = str(uuid4())
      new_branch_name = str(uuid4())

      # Add the remote to the local git repository
      GitResource.Log.noisy("adding remote %s -> %s" % (remote_name, git.path))
      store_git.addRemote(remote_name, git.path)

      # Fetch local branches
      store_git.fetchRemoteBranches(remote_name)

      # Checkout that branch from the remote
      GitResource.Log.noisy("checking out %s from %s/%s" % (new_branch_name, remote_name, git.branch()))
      store_git.checkoutBranch(new_branch_name, git.branch(), remote_name)

    return {
      'id':       uuid,
      'revision': newRevision,
      'name':     name,
      'source':   source
    }, dirty

  def retrieveDirectory(self, uuid, revision, path, subpath):
    """ Retrieves the directory listing of the given revision.
    """

    return GitRepo(os.path.join(path, "repository"), revision=revision).retrieveDirectory(subpath)

  def retrieveFileStat(self, uuid, revision, path, subpath):
    """ Retrieves the file status information for the given path in the given revision.
    """

    return GitRepo(os.path.join(path, "repository"), revision=revision).retrieveFileStat(subpath)

  def retrieveFile(self, uuid, revision, path, subpath, start=0, length=None):
    """ Retrieves the file data found within the resource at the given revision.
    """

    return GitRepo(os.path.join(path, "repository"), revision=revision).retrieveFile(subpath, start=start, length=length)

  def stat(self, uuid, revision, path):
    """ Retrieves the file status of the resource.
    """

    # Git repositorites are directories:
    git = GitRepo(os.path.join(path, "repository"), revision=revision)
    gitStat = git.retrieveSize()

    return {
      "name": "repository",
      "type": "tree",
      "size": gitStat.get('size-pack'),
    }

  def retrieve(self, uuid, revision, path, start=0, length=None):
    """ Retrieves the resource data.
    """

    # Git repositories are directory based, they are not retrievable.
    return None

  def retrieveHistory(self, uid, revision, path):
    git = GitRepo(os.path.join(path, "repository"))
    if git is None:
      return None

    return git.history()

  def traversable(self, uid, revision, path):
    """ Returns True since git is always treated as a directory.
    """

    return True
