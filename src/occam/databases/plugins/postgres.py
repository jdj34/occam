# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.databases.manager import database, DataNotUniqueError

from occam.log import loggable

@loggable
@database('postgres')
class Postgres:
  """ This implements an PostgreSQL backend.
  """

  def __init__(self, configuration):
    """ Initializes the PostgreSQL driver.
    """

    from threading import local
    Postgres.localThread = local()

    self.connections = {}

    self.configuration = configuration

  def disconnect(self):
    Postgres.localThread.occam_postgres_connection = None

  def connect(self):
    import psycopg2

    # PARSE_DECLTYPES : parses TIMESTAMP into datetime
    name = self.configuration.get('name', 'occam')
    user = self.configuration.get('user', 'occam')
    host = self.configuration.get('host', 'localhost')
    port = self.configuration.get('port', 5432)
    password = self.configuration.get('password', 'occam')
    Postgres.localThread.occam_postgres_connection = psycopg2.connect(f"dbname={name} user={user} host={host} port={port} password={password}")
    self.setFlavor()

  def setFlavor(self):
    """ Sets the flavors for this thread.
    """

    import sql
    sql.Flavor.set(sql.Flavor(paramstyle='format', ilike=True))

  def session(self):
    """ Returns an PostgreSQL cursor.
    """

    import psycopg2
    import psycopg2.extras

    connected = getattr(Postgres.localThread, 'occam_postgres_connection', None)
    if not connected:
      self.connect()

    try:
      cursor = Postgres.localThread.occam_postgres_connection.cursor(
        cursor_factory = psycopg2.extras.RealDictCursor
      )
    except psycopg2.InterfaceError as e:
      self.connect()
      return self.session()

    return cursor

  def execute(self, session, query, setID=False, postamble=None):
    """ Executes the given query upon the given session.
    """

    import psycopg2
    
    updatedQuery = list(tuple(query))
    if postamble:
      updatedQuery[0] += postamble
      Postgres.Log.noisy("UPDATED query: ", updatedQuery[0])
    if setID:
      updatedQuery[0] += " RETURNING id"
      Postgres.Log.noisy("UPDATED query: ", updatedQuery[0])

    ret = None
    try:
      ret = session.execute(*updatedQuery)
    except psycopg2.IntegrityError as e:
      self.rollback(session)
      if "violates unique constraint" in str(e):
        raise DataNotUniqueError(str(e)) from None
    except psycopg2.InterfaceError as e:
      self.connect()
      return self.execute(session, query, setID=setID)
    except Exception as e:
      self.rollback(session)
      Postgres.Log.write("Failing Query:")
      Postgres.Log.write(str(tuple(query)))
      raise e

    return ret

  def fetch(self, session, size=1):
    """ Gets the next row from the query result for this session.
    """

    return session.fetchone()

  def many(self, session, size=1):
    """ Gets the next rows from the query result for this session. Will pull
        the number of rows indicated by 'size'. It may return less than the
        requested number of rows when not enough are available.
    """

    return session.fetchmany(size)

  def updateSerial(self, session, table, column_name):
    """ Updates the table's identity serial value if necessary.
    """
    # Per https://stackoverflow.com/questions/244243/how-to-reset-postgres-primary-key-sequence-when-it-falls-out-of-sync/23390399#23390399
    query = f"SELECT setval(pg_get_serial_sequence('{table}','{column_name}'), COALESCE(max({column_name}) + 1, 1), false) from {table};"
    session.execute(query)

  def commit(self, session):
    """ Commits the given session.
    """

    return Postgres.localThread.occam_postgres_connection.commit()

  def renameTable(self, session, tableOld, tableNew):
    """ Alters the given table's name.
    """
    Postgres.Log.write(f"Renaming table {tableOld} to {tableNew}")
    query = f"ALTER TABLE {tableOld} RENAME TO {tableNew}"
    session.execute(query)

  def migrate(self, session, tableName, schema):
    """ Alters the given table's given columns.
    """
    import psycopg2

    Postgres.Log.write(f"Migrating table {tableName}")

    # We have to form this command ourselves since python-sql doesn't have it
    # and, to be honest, sqlite3's alter table support is less than good.

    for key, value in schema.items():
      prelude  = "ALTER TABLE %s ADD COLUMN " % (tableName)
      epilogue = ";"

      line = self._columnLine(tableName, key, value)

      query = prelude + line + epilogue

      try:
        session.execute(query)
      except psycopg2.errors.DuplicateColumn as e:
        if "duplicate column" in str(e):
          Postgres.Log.warning(f"Column {key} already exists.")

  def _typeToDatabaseType(self, tableName, keyName, value, foreign = False):
    """ Returns the PostgreSQL type for the given general type.
    """

    type = value.get('type')

    if type is None:
      return None

    if type == "string":
      if 'length' not in value:
        return "VARCHAR"
      elif isinstance(value.get('length', 128), int):
        return "VARCHAR(%s)" % (value.get('length', 128))
      else:
        raise TypeError("string attribute length is not an integer")
    elif type == "binary":
      if 'length' not in value:
        return "BYTEA"
      elif isinstance(value.get('length', 128), int):
        # PostgreSQL does not have a length modifier on BYTEA
        # We need a CHECK constraint instead
        return "BYTEA CHECK(length(%s) <= %s)" % (keyName, value.get('length', 128))
      else:
        raise TypeError("binary attribute length is not an integer")
    elif type == "integer":
      if foreign == False and value.get('serial'):
        return "SERIAL"

      return "INTEGER"
    elif type == "text" or type == "json":
      return "TEXT"
    elif type == "datetime":
      return "TIMESTAMP"
    elif type == "boolean":
      return "INTEGER"

    return None
  
  def _columnLine(self, tableName, key, value):
    line = ""
    if value.get('foreign'):
      line = line + "FOREIGN KEY ("

    if value.get('constraint'):
      line = line + "CONSTRAINT "
      key = tableName + "_" + key

    line = line + key

    if value.get('constraint'):
      line = line + " " + value['constraint'].get('type', "unique").upper()
      if 'columns' in value['constraint']:
        line = line + " (" + ",".join(value['constraint'].get('columns', [])) + ")"

    if value.get('foreign'):
      line = line + ") REFERENCES %s (%s)" % (value['foreign'].get('table'), value['foreign'].get('key'))

    if value.get('type'):
      pgType = self._typeToDatabaseType(tableName, key, value)
      line = line + " " + pgType

    if value.get('primary') and not value.get('constraint'):
      line = line + " PRIMARY KEY"
      if value.get('null') != False:
        line = line + " NOT NULL"

    if value.get('null') == False:
      line = line + " NOT NULL"

    if value.get('unique') == True:
      line = line + " UNIQUE"

    if 'default' in value:
      default = value.get('default')
      if value.get('type') == "string" or value.get('type') == "text":
        default = "'%s'" % (default)
      elif value.get('type') == "boolean":
        default = 0 if default == False else 1
      line = line + " DEFAULT %s" % (str(default))

    return line

  def index(self, session, tableName, tableSchema, indexName, indexSchema):
    """ Creates the index (or alters the index) with the given table/index schema.
    """

    Postgres.Log.write(f"Creating index {tableName}-{indexName}")

    # The canonical name to Postgres
    canonicalName = f"{tableName}_{indexName}_index"

    prelude = f"CREATE INDEX IF NOT EXISTS {canonicalName} ON {tableName} ("
    epilogue = ");"

    line = ""

    for id in indexSchema.ids:
      line = line + id + " "

    query = prelude + line.strip() + epilogue

    if os.getenv("OCCAM_QUERY"):
      Postgres.Log.write(query)

    session.execute(query)

  def create(self, session, tableName, tableSchema, classList):
    """ Creates the table (or alters the table) with the given table schema.
    """

    # We have to form this command ourselves since python-sql doesn't have it

    Postgres.Log.write(f"Creating table {tableName}")

    prelude  = "CREATE TABLE IF NOT EXISTS %s (" % (tableName)
    epilogue = ");"

    query = prelude
    foreign = ""
    constraints = ""

    for key, value in tableSchema.items():
      line = self._columnLine(tableName, key, value)

      if value.get('foreign'):
        foreign = foreign + line + ","

        foreignKeyValue = value

        while 'foreign' in foreignKeyValue:
          referencedTable = classList[foreignKeyValue['foreign'].get('table')]
          foreignKeyValue = referencedTable[foreignKeyValue['foreign'].get('key')]
          foreignKeyType = self._typeToDatabaseType(tableName, key, foreignKeyValue, foreign=True)

        query = query + (" %s %s," % (key, foreignKeyType))
      elif value.get('constraint'):
        constraints = constraints + line + ","
      else:
        query = query + line + ","

    query = query + foreign
    query = query + constraints

    if query[-1] == ",":
      query = query[:-1]

    query = query + epilogue

    if os.getenv("OCCAM_QUERY"):
      Postgres.Log.write(query)

    session.execute(query)

  def lastRowID(self, session):
    """ Retrieves the last row id, typically that was just inserted.
    """

    return session.fetchone().get('id')

  def rowCount(self, session):
    """ Retrieves the number of rows affected by the last update.
    """

    return session.rowcount

  def massageUpdate(self, query, record, table, schema):
    """ Returns an updated query for an UPDATE style update for the given record.
    """

    return None

  def massageInsert(self, query, record, table, schema):
    """ Returns an updated query for an INSERT style update for the given record.
    """

    # We will add the ON CONFLICT style command to the end of the INSERT
    # (if necessary)
    postamble = None

    for k,v in schema.items():
      if isinstance(v, dict) and v.get('constraint'):
        if v['constraint'].get('type') == "unique":
          if v['constraint'].get('conflict') == "replace":
            # Build out the item list
            items = ""
            for k2,v2 in schema.items():
              if isinstance(v2, dict) and v2.get('type'):
                items = items + f" {k2}=EXCLUDED.{k2},"

            # Remove the trailing comma
            if items:
              items = items[:-1]

            # Recall that constraints are named with the table name as a prefix
            # since constraint names are global in Postgres
            postamble = f" ON CONFLICT ON CONSTRAINT {table}_{k} DO UPDATE SET {items}"

    return postamble

  def massageValues(self, schema, updated):
    """ Returns reformed values based on the table schema and the given keys.
    """

    import psycopg2

    def massageValue(schema, key, value):
      ret = value

      if value is True:
        ret = 1
      elif value is False:
        ret = 0

      if isinstance(value, bytes):
        ret = psycopg2.Binary(value)

      if isinstance(value, dict):
        import json
        ret = json.dumps(value)

      return ret

    for key, value in updated.items():
      updated[key] = massageValue(schema, key, value)

    return updated

  def rollback(self, session):
    """ Rolls back the current transaction.
    """

    import psycopg2

    ret = None
    try:
      ret = Postgres.localThread.occam_postgres_connection.rollback()
    except psycopg2.InterfaceError as e:
      self.connect()

    return ret
