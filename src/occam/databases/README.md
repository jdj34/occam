# The Databases Component

This component handles reading and writing from a database.

Currently SQLite3 and Postgres are supported as database back-ends.

The pattern for performing a database operation is:

```
import sql

# Create a session
session = self.database.session()

# Create a valid SQL query. Ideally you will use the SQL library.
someTable = sql.Table("someTable")
cols = [someTable.path]
vals = [path]
query = someTable.update(where = (someTable.id == id), columns = cols, values = vals)

# Execute the query through the database component, which will transparently
# work with a database back-end to achieve query execution.
count = self.database.execute(session, query)

# The changes are not permanent until they are committed.
self.database.commit(session)
```

Database changes should be implemented per component in a module called
database.py in the component folder.

Database plugin choice is done through the config.yml in the Occam root folder.
