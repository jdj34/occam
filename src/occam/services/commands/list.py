# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.services.manager import ServiceManager

@command('services', 'list',
  category      = 'Service Management',
  documentation = "Lists the available services.")
@option("-s", "--service",      help = "Filter by the given name",
                                dest = "service")
@option("-a", "--architecture", help = "Filter by the given architecture",
                                dest = "architecture")
@option("-e", "--environment",  help = "Filter by the given environment",
                                dest = "environment")
@uses(ServiceManager)
class ServiceListCommand:
  def do(self):
    ret = []

    rows = self.services.retrieveAll(self.options.environment,
                                     self.options.architecture,
                                     self.options.service)

    ret = [{
      "id":           row.internal_object_id,
      "environment":  row.environment,
      "architecture": row.architecture,
      "service":      row.service
    } for row in rows]

    Log.output(json.dumps(ret))
    return 0
