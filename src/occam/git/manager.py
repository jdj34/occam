# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os, json, codecs, io

from occam.config     import Config
from occam.log        import loggable

from occam.git_repository import GitRepository

from occam.manager import manager, uses

from occam.accounts.manager import AccountManager
from occam.objects.manager import ObjectManager
from occam.accounts.manager import AccountManager
from occam.resources.manager import ResourceManager
from occam.storage.manager import StorageManager

@loggable
@uses(AccountManager)
@uses(ObjectManager)
@uses(ResourceManager)
@uses(AccountManager)
@uses(StorageManager)
@manager("git")
class GitManager:
  """ This OCCAM manager handles git access and retrieval.
  """

  @staticmethod
  def popen(command, stdout=None, stdin=None, stderr=None, cwd=None, env=None):
    import subprocess
    if stdout is None:
      stdout = subprocess.DEVNULL

    if stderr is None:
      stderr = subprocess.DEVNULL

    return subprocess.Popen(command, stdout=stdout, stdin=stdin, stderr=stderr, cwd=cwd, env=env)

  def issueService(self, stdin, path, service):
    """ Issues the requested service and returns a bytestream.
    """

    import subprocess
    command = ['git', service, '--stateless-rpc', path]
    p = GitManager.popen(command, stdin = subprocess.PIPE, stdout = subprocess.PIPE, cwd=path)
    p.stdin.write(stdin.read())
    p.stdin.close()

    return p.stdout

  def publishGitHead(self, stdin, path):
    """ Returns the HEAD data as a bytestream.
    """

    return open(os.path.join(path, "HEAD"), "rb")

  def packetWrite(self, line):
    """ Forms a git protocol packet from the given bytestring.
    """

    return "{:04x}".format(len(line) + 4).encode('utf-8') + line

  def publishInfoRefs(self, stdin, path):
    """ Returns the info refs as a bytestream.
    """

    try:
      return open(os.path.join(path, "info", "refs"), "rb")
    except:
      return io.BytesIO(b'')

  def publishUploadPack(self, stdin, path, service):
    """ Returns a git info ref upload-pack object as a bytestream.
    """

    # Write header
    ret = io.BytesIO(b'')
    ret.write(self.packetWrite(("# service=%s\n" % (service)).encode('utf-8')))
    ret.write(b"0000")

    # Write the pack contents

    import subprocess
    command = ['git', 'upload-pack', '--stateless-rpc', '--advertise-refs', path]
    p = GitManager.popen(command, stdout = subprocess.PIPE, cwd = path)
    ret.write(p.stdout.read())
    p.communicate()

    ret.seek(0, io.SEEK_SET)

    return ret

  def publishObjectsInfoFile(self, stdin, path, file):
    """ Returns the particular object info pack as a bytestream.
    """

    return open(os.path.join(path, "objects", "info", file), "rb")

  def publishObjectsPack(self, stdin, path, prefix):
    """ Returns the pack object file.
    """

    return open(os.path.join(path, "objects", "pack", prefix))

  def publishObjectsFile(self, stdin, path, prefix, suffix):
    """ Returns the loose object file for the given prefix and suffix as a bytestream.
    """

    return open(os.path.join(path, "objects", prefix, suffix))

  def handleHTTPRequest(self, connection, request, buffer):
    """ Handles a git HTTP request.

    Arguments:
      connection (socket): The open socket to the git client.
      request (String): The main HTTP request line.
      buffer (bytes): The rest of the request body.
    """

    # Parse request line
    request = request.rstrip('\r\n')
    words = request.split(' ')

    # Split into the parts we want
    verb = words[0]
    path = words[1]

    # Parse headers
    import http.client
    import io
    requestBody = buffer.split(b'\r\n\r\n')[1]
    headers = http.client.parse_headers(io.BytesIO(buffer))

    contentLength = headers.get('Content-Length')
    if contentLength:
      contentLength = int(contentLength)

      # Read the rest of the body, if necessary
      if contentLength > len(requestBody):
        requestBody += connection.recv(contentLength - len(requestBody))

    # Reform the URL
    if not path.startswith("/"):
      path = "/" + path
    url = f"http://domain{path}"

    # Parse the URL
    import urllib.parse
    urlparts = urllib.parse.urlparse(url)
    path = urlparts.path

    # Parse the path into object id/revision and the git action
    parts = path[1:].split('/')
    id = parts[0]
    revision = parts[1]
    path = '/'.join(parts[2:])

    person = None
    token = headers.get('X_OCCAM_TOKEN')
    if token:
      tokenInfo = self.accounts.decodeToken(token)

      if 'identity' in tokenInfo:
        # I think it is fine to just allow person to become None if it does
        # not authenticate.
        person = self.accounts.currentPerson(token = token)

    # Resolve object
    object = self.objects.retrieve(id = id, revision = revision, person = person)

    # We want to resolve its local repository path
    gitPath = None
    if object is None:
      # Check resource for id
      object = self.resources.infoFor(id = id)

      # Check resource for uid
      if not object:
        object = self.resources.infoFor(uid = id)

      if object and "git" in object["subtype"]:
        gitPath = self.storage.resourcePathFor(object["uid"])
        gitPath = os.path.join(path, "repository")
    else:
      gitPath = self.storage.repositoryPathFor(uuid     = object.uid,
                                               revision = object.revision,
                                               identity = object.identity)

    if not gitPath:
      # Send back a 404
      connection.send(b'HTTP/1.1 404 Not Found\r\nStatus: 404\r\nContent-Length: 0\r\n\r\n')
      return

    params = urllib.parse.parse_qs(urlparts.query)

    contentType = 'application/octet-stream'
    body = b''
    if verb == "GET" and path == 'info/refs':
      if 'service' in params:
        # GET /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/info/refs?service=git-upload-pack HTTP/1.1
        service = params['service'][0]

        contentType = f"application/x-{service}-advertisement"
        data = self.publishUploadPack(None, gitPath, service).read()

        # We need to inject the requested revision
        body = b''
        for line in data.split(b'\n'):
          # Special case an empty upload-pack
          # This is so we can embed some revisions
          if line == b"00000000":
            body += b"0000"
            break

          if line == b"0000":
            break

          body += line + b'\n'

        if revision:
          # Inject the revision as the HEAD
          gitHash = GitRepository.hexFromMultihash(revision)

          injectRef = b'%s refs/heads/snapshot\n' % (gitHash.encode('utf-8'))
          injectRef = (b"%x" % (len(injectRef) + 4)).rjust(4, b'0') + injectRef
          body += injectRef

          injectRef = b'%s HEAD\n' % (gitHash.encode('utf-8'))
          injectRef = (b"%x" % (len(injectRef) + 4)).rjust(4, b'0') + injectRef
          body += injectRef

        body += b'0000'
      else:
        # GET /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/info/refs HTTP/1.1
        # This is just publishing the info/refs file
        contentType = 'text/plain'
        body = self.publishInfoRefs(None, gitPath)
    elif verb == "GET" and path.startswith('objects/pack/pack-'):
      # GET /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/objects/pack/pack-{prefix}.{suffix} HTTP/1.1
      # Retrieves pack files
      if not path.endswith("idx") and not path.endswith("pack"):
        # 404
        connection.send(b'HTTP/1.1 404 Not Found\r\nStatus: 404\r\nContent-Length: 0\r\n\r\n')
        return

      # Get prefix/suffix
      tokens = path.split('/')[-1].split('-')
      prefix = tokens[0]
      suffix = tokens[1]

      contentType = "application/x-git-packed-objects"
      if suffix == "idx":
        contentType = "application/x-git-packed-objects-toc"

      data = self.publishObjectsPack(None, gitPath, prefix)
    elif verb == "POST":
      # POST /QmfY1XACrB8cew7UALD7KEnPyp8dNznfEA5BPP5xFJztg6/5dswamd8qY1tfHt4javZmYLDvmBvp9/git-upload-pack HTTP/1.1
      # TODO: the post body might be compressed if Content-Encoding is gzip
      contentType = "application/x-git-upload-pack-result"
      body = self.issueService(io.BytesIO(requestBody), gitPath, "upload-pack").read()

    connection.send(b'HTTP/1.1 200 OK\r\nStatus: 200\r\nConnection: keep-alive\r\nContent-Type: %s\r\nContent-Length: %d\r\n\r\n' % (contentType.encode('utf-8'), len(body)))
    connection.send(body)
