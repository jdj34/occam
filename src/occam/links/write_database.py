# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

from occam.links.database import LinkDatabase

@loggable
@datastore("links.write", reader=LinkDatabase)
@uses(LinkDatabase)
class LinkWriteDatabase:
  """ Manages the mutable database interactions for link (object relationship lists) management.
  """

  def createLink(self, sourceObjectId,
                       sourceIdentityUri,
                       targetObjectId,
                       targetObjectRevision,
                       relationship,
                       published,
                       trackingId,
                       sourceRevision = None):
    """ Creates and returns a new LinkRecord object with the given content.

    Args:
      sourceObjectId (str): The identifier for the source object.
      targetObjectId (str): The identifier for the target object.
      targetObjectRevision (str): The revision for the target object.
      relationship (str): The relationship type.
      published (datetime): The datetime when the link is created.
      trackingId (str): The id of the staged cache where this object is updated.

    Returns:
      LinkRecord: The new record.
    """

    from occam.links.records.link import LinkRecord

    # Create a session
    session = self.database.session()

    # Create an empty record
    link = LinkRecord()

    # Start filling out the record
    link.source_identity_uri = sourceIdentityUri
    link.source_object_id = sourceObjectId
    link.source_revision  = sourceRevision
    link.target_object_id = targetObjectId
    link.target_revision  = targetObjectRevision
    link.relationship     = relationship
    link.published        = published
    link.local_link_id    = trackingId

    # Submit the record
    self.database.update(session, link)
    self.database.commit(session)

    return link

  def deleteLinks(self, id                   = None,
                        sourceObjectId       = None,
                        sourceIdentityUri    = None,
                        sourceRevision       = None,
                        targetObjectId       = None,
                        targetObjectRevision = None,
                        relationship         = None):
    """ Deletes the first matching link.

    Args:
      id (str): The identifier for the link.
      sourceObjectId (str): The identifier for the source object.
      targetObjectId (str): The identifier for the target object.
      targetObjectRevision (str): The revision for the target object.
      relationship (str): The relationship type.

    Returns:
      LinkRecord: The deleted key or None if no such key is found.
    """

    # Create a session
    session = self.database.session()

    rows = self.retrieveLinks(id                   = id,
                              sourceObjectId       = sourceObjectId,
                              sourceIdentityUri    = sourceIdentityUri,
                              sourceRevision       = sourceRevision,
                              targetObjectId       = targetObjectId,
                              targetObjectRevision = targetObjectRevision,
                              relationship         = relationship)
    if rows:
      self.database.delete(session, rows[0])
      self.database.commit(session)
      return rows[0]

    return None

  def truncateLinks(self, sourceObjectId, sourceIdentityUri, relationship, limit):
    """ Ensures that there are only the given number of links.

    It will, in the case there are too many links, remove the oldest links
    via the 'published' field.

    Args:
      sourceObjectId (str): The identifier for the source object.
      relationship (str): The relationship type.
      limit (int): The maximum number of links to allow.
    """

    # Create a session
    session = self.database.session()

    # Delete all records at index limit+1 when ordered by published

    subQuery = self.links.queryLinks(sourceObjectId    = sourceObjectId,
                                     sourceIdentityUri = sourceIdentityUri,
                                     relationship      = relationship,
                                     key               = "id")

    subQuery.offset = limit

    links = sql.Table("links")
    query = links.delete()
    query.where = (links.id.in_(subQuery))

    # Execute the query
    self.database.execute(session, query)
    self.database.commit(session)

  # Local link management
  
  # Local links are a type of storage cache.
