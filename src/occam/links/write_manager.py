# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.config  import Config

from occam.manager import uses, manager
from occam.log     import loggable

from occam.databases.manager   import DatabaseManager
from occam.storage.manager     import StorageManager
from occam.permissions.manager import PermissionManager
from occam.caches.manager      import CacheManager
from occam.links.manager       import LinkManager
from occam.objects.manager     import ObjectManager

@loggable
@uses(DatabaseManager)
@uses(StorageManager)
@uses(PermissionManager)
@uses(ObjectManager)
@uses(CacheManager)
@uses(LinkManager)
@manager("links.write", reader=LinkManager)
class LinkWriteManager:
  """ This manages the creation and updating of links.
  """

  def create(self, source_db,
                   relationship,
                   target_object_db,
                   target_revision,
                   limit = None,
                   local_link_id = None,
                   person = None,
                   source_revision = None):
    """ Creates a link between two objects.

    This is used primarily to connect a Person to an Object. For instance, a
    "bookmark" link will let people save objects at a particular revision to
    a list. This way, they can recall them at some point.

    The main list of active work for a Person is the "collected" relation. When
    one removes an item from their dashboard, that item then becomes "archived"
    for one particular example of this might be used by an Occam client.

    If the link already exists, the relationship will be updated.

    You cannot add a link to an object you cannot see.
    You cannot add a link from an object you cannot edit.

    Args:
      source_db (ObjectRecord, IdentityRecord): The object or identity record to link from.
      relationship (str): The relationship between the objects.
      target_object_db (ObjectRecord): The object record to link to.
      target_revision (str): The revision of the target object.
      limit (int): The maximum number of links for this relation.
        Defaults to None which implies an infinite number. When the limit is
        set, it will add this link, and if the limit is exceeded, truncate the
        oldest stored link.
      local_link_id (str): The identifier of the local link.
      person (Person): The person adding the link, for permission checking.
      source_revision (str): The revision of the source object.

    Returns:
      LinkRecord: The resulting link record or None if the record cannot be created.
    """

    from occam.objects.records.object import ObjectRecord

    sourceObjectId = None
    sourceIdentityUri = None

    if isinstance(source_db, ObjectRecord):
      if not self.permissions.can("write", source_db.id, person=person):
        LinkWriteManager.Log.error(f"Person {person.id} has no write permission for {source_db.id}")
        return None

      sourceObjectId = source_db.id
    else:
      # Pull the identity record uri
      if person.identity == source_db.uri:
        sourceIdentityUri = source_db.uri

      # Cannot set a link if the actor is not the one linking
      if sourceIdentityUri is None:
        return None

    # Need to have read access to the object being linked
    if not self.permissions.can("read", target_object_db.id, person=person):
      print("no read")
      return None

    # Create a new LinkRecord

    import datetime
    published = datetime.datetime.now()
    link = self.datastore.write.createLink(sourceObjectId       = sourceObjectId,
                                           sourceIdentityUri    = sourceIdentityUri,
                                           sourceRevision       = source_revision,
                                           targetObjectId       = target_object_db.id,
                                           targetObjectRevision = target_revision,
                                           relationship         = relationship,
                                           published            = published,
                                           trackingId           = local_link_id)

    if limit and limit > 0:
      self.datastore.write.truncateLinks(sourceObjectId       = sourceObjectId,
                                         sourceIdentityUri    = sourceIdentityUri,
                                         relationship         = relationship,
                                         limit                = limit)

    # Invalidate the cache key
    self.cache.delete(self.links._cacheKey(sourceObjectId or sourceIdentityUri, relationship, order = "ascending"))
    self.cache.delete(self.links._cacheKey(sourceObjectId or sourceIdentityUri, relationship, order = "descending"))

    return link

  def createLocalLink(self, db_object, revision, path, linkId=None):
    """ Creates a link between a local data object and a local path.
    
    This record will allow changes that happen within the system
    or seen by the outside to be reflected at the current path automatically
    in some situations.

    When path is None, the tracked object is placed in a tracked object pool
    somewhere within the occam system's file storage. These are system-tracked
    objects and have some special semantics. For instance, they collapse
    whenever revisions eventually match other tracked objects. They also
    regenerate when they are missing, as opposed to non-system local links
    which are destroyed when the path they track goes away.

    Returns the LocalLinkRecord created.
    """

    from uuid import uuid4
    if linkId is None:
      linkId = str(uuid4())

    # Create a suitable path outside of the object store.
    if path is None:
      path = self.links.pathFor(linkId = linkId)

    from occam.links.records.local_link import LocalLinkRecord

    session = self.database.session()

    local_link = LocalLinkRecord()

    # Assign keys to local link.
    local_link.id = linkId
    local_link.path = path
    local_link.target_object_id = db_object.id
    local_link.revision = revision

    LinkManager.Log.noisy("creating local link between object (%s) at %s" % (db_object.id, path))

    self.database.update(session, local_link)
    self.database.commit(session)

    return local_link

  def updateLocalLink(self, link, newRevision):
    """ Updates the status of the local version based on a stored revision.

    It will collapse internal tracked objects when the newRevision is one that
    is already known and tracked elsewhere. This can happen when objects are
    reverted. Essentially, it just deletes it and lets the current LocalLink
    handle tracking the object from now on.
    """

    import sql

    session = self.database.session()

    link.revision = newRevision

    self.database.update(session, link)
    self.database.commit(session)

    # Update any minor links that are attached to this local link
    links = sql.Table('links')

    query = links.update(where = (links.local_link_id == link.id), columns = [links.target_revision], values = [link.revision])

    self.database.execute(session, query)
    self.database.commit(session)

    query = links.select(where = (links.local_link_id == link.id))
    
    self.database.execute(session, query)
    self.database.commit(session)
    
    links = self.database.many(session)

    from occam.links.records.link import LinkRecord
    for link in links:
      # TODO: simplify this as a query instead
      # TODO: or just retrieve the last row when we can
      link = LinkRecord(link)
      self.cache.delete(self.links._cacheKey(link.source_object_id, link.relationship, order = "ascending"))
      self.cache.delete(self.links._cacheKey(link.source_object_id, link.relationship, order = "descending"))

    return link

  def destroy(self, link_db):
    """ Destroys the given link.
    """

    ret = self.datastore.write.deleteLinks(id = link_db.id)

    # Invalidate the cache key
    self.cache.delete(self.links._cacheKey(link_db.source_object_id, link_db.relationship, order = "ascending"))
    self.cache.delete(self.links._cacheKey(link_db.source_object_id, link_db.relationship, order = "descending"))

    return ret

  def destroyLocalLink(self, link_id):
    """ Destroys any local link (if it exists) for the given link.
    """

    import sql
    
    session = self.database.session()

    localLinks = sql.Table("local_links")

    query = localLinks.delete(where = (localLinks.id == link_id))

    self.database.execute(session, query)
    self.database.commit(session)

    return True

  def initializeLocalLinkBuild(self, object):
    """ Initializes the build metadata for the given object.

    A build occurs on a copy of the object's current state. When you invoke
    a build, the new state is copied into this directory while keeping any
    generated files intact (See #cleanLocalLinkBuild) for subsequent builds.

    This protects ongoing work from being damaged by rogue build scripts and
    human error while also allowing for incremental development within the
    archive system.

    Returns the path to the object data for the local build.
    """

    from distutils import dir_util

    # Copy the contents of the object to the build's own path
    buildPath = os.path.join(object.path, "..", "metadata", "builds", "object")
    os.makedirs(buildPath, exist_ok = True)

    # "Clone" into this object path

    # Since object data is indexed and not actually part of the git repository,
    # we have to copy the files over.

    # distutils.dir_util.copy_tree(src, dst, update = True) copies only newer files.
    sourcePath = object.path
    # We need to clear the internal directory cache since we may have removed the
    # directory at some point:
    # (https://stackoverflow.com/questions/9160227/dir-util-copy-tree-fails-after-shutil-rmtree/28055993)
    dir_util._path_created = {}
    dir_util.copy_tree(sourcePath, buildPath, update=True)

    # Install the object resources
    self.objects.install(object, buildPath, build = True)

    return buildPath

  def cleanLocalLinkBuild(self, object):
    """ Removes cached build information from the given tracked object.

    It will remove all generated files and reset the state back to the indexed
    state of the local object.

    When a build happens with a local object, the build can be done
    incrementally. The previous state leaks into any subsequent builds. This
    helps with development of new objects. However, it is not appropriate for
    preservation quality and you can get into a bad state. This resets the build
    path back to a known state. This is very much like a strict "make clean"
    operation.
    """

    import shutil

    # Delete the build metadata
    buildRootPath = os.path.join(object.path, "..", "metadata", "builds")
    if os.path.exists(buildRootPath):
      shutil.rmtree(buildRootPath)
