# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from occam.config  import Config

from occam.manager import uses, manager
from occam.log     import loggable

from occam.databases.manager   import DatabaseManager
from occam.storage.manager     import StorageManager
from occam.permissions.manager import PermissionManager
from occam.caches.manager      import CacheManager

@loggable
@uses(DatabaseManager)
@uses(StorageManager)
@uses(PermissionManager)
@uses(CacheManager)
@manager("links")
class LinkManager:
  """ Manages relationships among objects and synchronization of objects and local clones.

  Essentially, some objects can be created in a directory and then modified
  outside of that context. Perhaps, through a web-based client. This manager
  establishes a link between objects and their local content ensuring that if
  they are updated, they change locally as well.

  This is useful for caching and ensuring that, for instance, a workset that
  is being modified isn't cloned repeatedly. It also gives a single point for
  all collaborators to modify an object.

  Other relationships tie objects together. One such type of relationship is
  one between a Person and an Object. Such as a recently used items list,
  bookmarks, and any active work. A client can make links and use them to
  organize objects appropriately.

  Links are not propagated across the federation.
  """

  def _cacheKey(self, object_id, relationship,
                                 order = "ascending",
                                 reverse = False):
    """ Internal method to produce the cache key for the given object and relationship.

    Arguments:
      object_id (string): The object id of the source or target of the relationship.
      relationship (string): The 'kind' of link.
      order (string): The order of the list.
      reverse (bool): Whether the source object was being sought.
    """

    searching_for = "t"
    if reverse:
      searching_for = "s"
    return f"links-{searching_for}-{object_id}-{relationship}-{order}"

  def retrieve(self, source_db, relationship, target_object_db=None, target_revision = None):
    """ Retrieves a list of LinkRecords for the given source and relationship.

    You may optionally provide the target to filter to just the single link
    (if it exists) between source and target. It will return a list of one
    link (or an empty list.)
    """

    from occam.objects.records.object import ObjectRecord
    from occam.keys.records.identity import IdentityRecord

    sourceObjectId = None
    sourceIdentityUri = None
    if isinstance(source_db, ObjectRecord):
      sourceObjectId = source_db.id
    elif isinstance(source_db, IdentityRecord):
      sourceIdentityUri = source_db.uri
    else:
      raise TypeError("source_db not ObjectRecord or IdentityRecord")

    if target_object_db and not isinstance(target_object_db, ObjectRecord):
      raise TypeError("Target parameter not type of ObjectRecord")

    if target_revision and not isinstance(target_revision,str):
      target_revision = str(target_revision)

    targetObjectId = None

    if not isinstance(relationship, str):
      relationship = str(relationship)

    if target_object_db:
      targetObjectId = target_object_db.id

    return self.datastore.retrieveLinks(sourceObjectId       = sourceObjectId,
                                        sourceIdentityUri    = sourceIdentityUri,
                                        targetObjectId       = targetObjectId,
                                        targetObjectRevision = target_revision,
                                        relationship         = relationship)

  def retrieveObjects(self, source_db=None,
                            target_db=None,
                            relationship=None,
                            order="ascending",
                            sourceRevision=None,
                            targetRevision=None,
                            reverse=False):
    """ Retrieves an ObjectRecord list for the links for the given source and relationship.
    """

    import json
    from occam.objects.records.object import ObjectRecord
    from occam.keys.records.identity import IdentityRecord

    sourceObjectId = None
    sourceIdentityUri = None
    # Interpret source_db. It must be in a known format or optionally None if
    # we are performing a reverse lookup.
    if isinstance(source_db, ObjectRecord):
      sourceObjectId = source_db.id
    elif isinstance(source_db, IdentityRecord):
      sourceIdentityUri = source_db.uri
    elif not (source_db is None and reverse):
      raise TypeError("source_db not ObjectRecord or IdentityRecord")

    # When doing reverse lookup the target is required.
    if reverse and not isinstance(target_db, ObjectRecord):
      raise TypeError("target_db not ObjectRecord or IdentityRecord")
    
    if relationship:
      cacheObjId = sourceObjectId or sourceIdentityUri
      # If we are searching by target use that to construct the cache key.
      if reverse:
        cacheObjId = target_db.id
      cacheKey = self._cacheKey(cacheObjId, relationship, order, reverse)

      data = self.cache.get(cacheKey)

      if not data is None:
        import codecs
        return [ObjectRecord(x) for x in json.loads(codecs.decode(data, 'utf8'))]

    session = self.database.session()

    import sql

    links   = sql.Table('links')
    objects = sql.Table('objects')

    join = links.join(objects)
    if reverse:
      join.condition = (links.source_object_id == join.right.id)
    else:
      join.condition = (links.target_object_id == join.right.id)

    query = join.select(links.id.as_('link_id'),
                        links.source_revision,
                        links.target_revision,
                        links.relationship,
                        links.local_link_id,
                        join.right.id,
                        join.right.object_type,
                        join.right.subtype,
                        join.right.name,
                        join.right.organization,
                        join.right.description,
                        join.right.updated,
                        join.right.published,
                        join.right.tags,
                        join.right.environment,
                        join.right.architecture,
                        join.right.capabilities)

    query.where = sql.Literal(True)
    if sourceObjectId:
      query.where = (links.source_object_id == sourceObjectId)
    elif sourceIdentityUri:
      query.where = (links.source_identity_uri == sourceIdentityUri)

    if sourceRevision:
      query.where = query.where & (links.source_revision == sourceRevision)

    if targetRevision:
      query.where = query.where & (links.target_revision == targetRevision)

    if relationship:
      query.where = query.where & (links.relationship == relationship)

    if target_db:
      query.where = query.where & (links.target_object_id == target_db.id)
    
    if order == "ascending":
      query.order_by = sql.Asc(links.id)
    else:
      query.order_by = sql.Desc(links.id)

    self.database.execute(session, query)

    ret = self.database.many(session, size=100)

    if relationship:
      self.cache.set(cacheKey, json.dumps(ret))

    return [ObjectRecord(x) for x in ret]

  def path(self):
    """ Returns the path to the local tracked-object store.
    """
    
    return self.configuration.get('path', os.path.join(Config.root(), 'links'))

  #link_id is never used in this function
  def pathFor(self, linkId):
    """ Returns the path for the tracked object base on the linkId specified.

    Arguments:
      linkId (str): String of at least 4 characters specifying the identifer
        for a local link.
    """

    # Find the full path to the stored invocation metadata
    # Ex: .occam/links/2-code/2-code/full-code
    
    code = linkId
    code1 = code[0:2]
    code2 = code[2:4]

    path = os.path.join(self.path(), code1, code2, code)
    os.makedirs(path, exist_ok = True)

    return path

  def retrieveLocalLinks(self, obj, path = None, linkId = None):
    """ Retrieves the LocalLink, if it exists, for the given object.

    This should be done for every read/write of an Object that might be done.
    If there is a local link, it serves as a cached repository for that Object.

    That is, and in particular collaborations or large project worksets, this
    is used not only for command line usage that goes along with any web or GUI
    client... it is also used internally or by clients to create a quicker way
    of modifying an object repeatedly. That way, the object does not need to be
    cloned and then modified and then stored each time.
    """

    import sql

    session = self.database.session()

    from occam.links.records.local_link import LocalLinkRecord

    links = sql.Table("links")
    subQuery = links.select(links.local_link_id, where = (links.id == linkId))

    localLinks = sql.Table("local_links")
    objects    = sql.Table("objects")

    query = localLinks.select(where = (localLinks.target_object_id == obj.id) & (localLinks.revision == obj.revision))

    if linkId:
      #query.where = query.where & (localLinks.id.in_(subQuery))
      query.where = query.where & (localLinks.id == linkId)

    if path:
      query.where = query.where & (localLinks.path == path)

    self.database.execute(session, query)

    return [LocalLinkRecord(x) for x in self.database.many(session)]

  def __retrieveFileStatFrom(self, path):
    """ Returns the item info for the given file.

    Arguments:
      path (str): The path to a local file.

    Returns:
      dict: The item metadata.
    """

    itemInfo = {
      'name': os.path.basename(path),
      'type': 'tree' if os.path.isdir(path) else 'blob'
    }

    if os.path.islink(path):
      itemInfo['mode'] = os.lstat(path)
      itemInfo['link'] = 'symbolic'
      itemInfo['size'] = os.lstat(path).st_size
    else:
      itemInfo['mode'] = os.stat(path)
      itemInfo['size'] = os.path.getsize(path)

    itemInfo['mime'] = self.storage.mimeTypeFor(itemInfo['name'])

    return itemInfo

  def retrieveDirectoryFrom(self, obj, path = "/"):
    """ Retrieves a directory listing for the given local object.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to list.

    Returns:
      dict: The directory listing metadata (or None if the path is not found.)
    """

    if not obj.link:
      return None

    objectPath = os.path.join(obj.path, path[1:])

    if not os.path.exists(objectPath):
      return None

    data = {'items': []}

    for file in os.listdir(objectPath):
      itemPath = os.path.join(objectPath, file)
      itemInfo = self.__retrieveFileStatFrom(itemPath)
      data['items'].append(itemInfo)

    return data

  def retrieveDirectoryFromBuildFrom(self, obj, path = "/"):
    """ Retrieves a directory listing for the given local object's build.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to list.

    Returns:
      dict: The directory listing metadata (or None if the path is not found.)
    """

    if not obj.link:
      return None

    buildPath = os.path.join(obj.path, "..", "metadata", "builds", "build", path[1:])

    if not os.path.exists(buildPath):
      return None

    data = {'items': []}

    for file in os.listdir(buildPath):
      itemPath = os.path.join(buildPath, file)
      itemInfo = self.__retrieveFileStatFrom(itemPath)
      data['items'].append(itemInfo)

    return data

  def retrieveFileFrom(self, obj, path = "/"):
    """ Retrieves file data from a local link for the given path.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to view.

    Returns:
      File: A stream of the file data.
    """

    if not obj.link:
      return None

    objectPath = os.path.join(obj.path, path[1:])
    if not os.path.exists(objectPath):
      return None

    return open(objectPath, "rb")

  def retrieveFileFromBuildFrom(self, obj, path = "/"):
    """ Retrieves file data for the given local object's build.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to view.

    Returns:
      File: A stream of the file data.
    """

    if not obj.link:
      return None

    buildPath = os.path.join(obj.path, "..", "metadata", "builds", "build", path[1:])
    if not os.path.exists(buildPath):
      return None

    return open(buildPath, "rb")

  def retrieveFileStatFrom(self, obj, path = "/"):
    """ Retrieves file data from a local link for the given path.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to view.

    Returns:
      File: A stream of the file data.
    """

    if not obj.link:
      return None

    objectPath = os.path.join(obj.path, path[1:])
    if not os.path.exists(objectPath):
      return None

    return self.__retrieveFileStatFrom(objectPath)

  def retrieveFileStatFromBuildFrom(self, obj, path = "/"):
    """ Retrieves file metadata for the given local object's build.

    Arguments:
      obj (Object): The object to use. It should be local (have a link field.)
      path (str): The file path to poll.

    Returns:
      dict: The file metadata.
    """

    if not obj.link:
      return None

    buildPath = os.path.join(obj.path, "..", "metadata", "builds", "build", path[1:])
    if not os.path.exists(buildPath):
      return None

    return self.__retrieveFileStatFrom(buildPath)
