# The Nodes Component

This component handles tracking and updating information an Occam node knows
about other nodes withing the federation.

The discover component uses this component to find unresolved objects on other
nodes.
