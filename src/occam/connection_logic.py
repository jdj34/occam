# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Helper functions for working with connections.
def retryOnReset(f, retryLimit = 5):
  """ Retry the given function on connection reset until the limit is reached.
  """

  for attempt in range(retryLimit):
    try:
      return f()
    except ConnectionResetError as e:
      if attempt >= retryLimit - 1:
        raise RuntimeError(f"Connection to remote node "
                           f"{self.options.from_uri} could not be "
                           f"established.").with_traceback(e.__traceback__) from None
