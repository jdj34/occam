# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sql
import sql.aggregate

from occam.log import loggable
from occam.databases.manager import uses, datastore

@loggable
@datastore("jobs")
class JobDatabase:
  """ Manages the database interactions for job management.
  """

  def queryJobs(self, id=None, ids=None, status=None, targets=None, kind=None,
                      task_id=None, finishTime=None, key=None):
    """ Returns a instantiated query object for retrieving job records.

    Arguments:
      id (str): The identifier for the job.
      ids (list): A list of identifiers for jobs.
      status (list): A set of job status strings to filter the listing.
      targets (list): A set of target strings to filter the listing.
      kind (str): The registered job 'kind', for instance 'build'.
      task_id (str): The object ID for the running task.

    Returns:
      sql.Query: The query to be executed.
    """

    jobs = sql.Table("jobs")
    if key:
      query = jobs.select(jobs.__getattr__(key))
    else:
      query = jobs.select()

    query.where = sql.Literal(True)

    if id is not None:
      query.where = query.where & (jobs.id == id)

    if ids is not None:
      query.where = query.where & (jobs.id.in_(ids))

    if task_id is not None:
      query.where = query.where & (jobs.task_uid == task_id)

    if kind is not None:
      query.where = query.where & (jobs.kind == kind)

    if status is not None:
      statusCondition = sql.Literal(False)
      for statusString in status:
        statusCondition = statusCondition | (jobs.status == statusString)
      query.where = query.where & statusCondition

    if targets is not None:
      targetCondition = sql.Literal(False)
      for target in targets:
        targetCondition = targetCondition | (jobs.target == target)
      query.where = query.where & targetCondition

    if finishTime is not None:
      query.where = query.where & (jobs.finish_time < finishTime)

    query.order_by = sql.Desc(jobs.queue_time)

    return query

  def retrieveJobCounts(self, status=None, targets=None):
    """ Returns a dictionary of aggregate statistics for the filtered jobs.

    Arguments:
      status (list): A set of job status strings to filter the listing.
      targets (list): A set of target strings to filter the listing.

    Returns:
      dict: A dictionary keyed by job status containing aggregate statistics.
    """

    session = self.database.session()

    # The base query
    query = self.queryJobs(id = None, status = status, targets = targets)
    withQuery = sql.With()
    withQuery.query = query

    # We need a subquery for each possible status
    unionQuery = None
    statuses = ["queued", "started", "initialized", "ran", "finished", "failed"]
    for token in statuses:
      subQuery = withQuery.select(sql.As(sql.aggregate.Count(withQuery), token),
                                  sql.As(withQuery.status, 'status'))
      subQuery.where = (withQuery.status == token)
      subQuery.group_by = withQuery.status

      # Union the queries together
      if unionQuery is None:
        unionQuery = subQuery
      else:
        unionQuery = unionQuery | subQuery

    # We gather the rows
    mainQuery = unionQuery.select()
    mainQuery.with_ = [withQuery]
    self.database.execute(session, mainQuery)
    rows = self.database.many(session, size=1000)

    # We reform the data into a normal dictionary
    ret = {}

    # With default values of 0
    for token in statuses:
      ret[token] = 0

    # And then update with the aggregate values retrieved
    for i, row in enumerate(rows):
      # There is a weird bug in python-sql that makes every As 'queued',
      # so this should work even if that behavior changes to something
      # more expected: (Reforms to use the status as the key)
      ret[row['status']] = row[list(row.keys())[0]]

    # Return the aggregate information; a dictionary with several keys for each
    # possible status with the number of records found.
    return ret

  def retrieveJobs(self, id=None, status=None, targets=None,
                         kind=None, task_id=None):
    """ Returns a set of JobRecord items that match the given criteria.

    Arguments:
      id (str): The identifier for the job.
      status (list): A set of job status strings to filter the listing.
      targets (list): A set of target strings to filter the listing.
      kind (str): The registered job 'kind', for instance 'build'.
      task_id (str): The object ID for the running task.

    Returns:
      list: A set of JobRecord items.
    """

    # Create a session
    from occam.jobs.records.job import JobRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryJobs(id, status = status, targets = targets,
                               kind = kind, task_id = task_id)

    # Execute the query
    self.database.execute(session, query)

    # Return the result
    return [JobRecord(x) for x in self.database.many(session, size=100)]

  def pullJob(self, scheduler="occam"):
    """ Will atomically return a JobRecord for the next job to run.

    This will pull a job while also marking it as 'started'.

    Returns:
      JobRecord: An available Job or None if no jobs are on the queue.
    """

    import sql
    import datetime

    from occam.jobs.records.job import JobRecord

    jobs = sql.Table('jobs')

    session = self.database.session()

    # Find queued jobs with no dependencies
    query = jobs.select()
    query.where = ((jobs.status == "queued") & (jobs.scheduler == scheduler))

    self.database.execute(session, query)

    rows = self.database.many(session)

    # For each one, attempt to switch the status
    # If successful, then return that job
    for row in rows:
      row = JobRecord(row)
      row.status = "started"
      row.start_time = datetime.datetime.today()

      # Update the job's db row
      query = jobs.update(where   = (jobs.status == "queued") & (jobs.id == row.id),
          columns = [jobs.status, jobs.start_time],
          values  = [row.status, row.start_time])

      # Count the number of updated rows (should be 1)
      count = self.database.execute(session, query)
      self.database.commit(session)

      # If count was 0, then it didn't update, otherwise, return the job:
      if count == 1:
        # Successfully updated the job record
        # Return the record
        return row

    # If there are no jobs, then return None
    return None

  def updateJob(self, id, path=None,
                          status=None,
                          startTime=None,
                          initTime=None,
                          runTime=None,
                          finishTime=None,
                          schedulerId=None):
    """ Updates the given job record.

    Arguments:
      id (str): The job identifier.
      path (str): When not None, updates the job path.

    Returns:
      bool: True if the record was updated and False otherwise.
    """

    import sql
    jobs = sql.Table("jobs")

    # Create a session
    session = self.database.session()

    # Set up columns and values tuples
    cols = []
    vals = []

    if path is not None:
      cols.append(jobs.path)
      vals.append(path)

    if status is not None:
      cols.append(jobs.status)
      vals.append(status)

    if startTime is not None:
      cols.append(jobs.start_time)
      vals.append(startTime)

    if initTime is not None:
      cols.append(jobs.init_time)
      vals.append(initTime)

    if runTime is not None:
      cols.append(jobs.run_time)
      vals.append(runTime)

    if finishTime is not None:
      cols.append(jobs.finish_time)
      vals.append(finishTime)

    if schedulerId is not None:
      cols.append(jobs.scheduler_identifier)
      vals.append(schedulerId)

    query = jobs.update(where = (jobs.id == id), columns = cols, values = vals)

    count = self.database.execute(session, query)
    self.database.commit(session)

    if count == 1:
      return True

    # Did not update
    return False

  def createJob(self, taskBackend=None,
                      taskName=None,
                      taskId=None,
                      taskRevision=None,
                      interactive=None,
                      initialize=None,
                      initializeTag=None,
                      finalize=None,
                      finalizeTag=None,
                      scheduler=None,
                      target=None,
                      identity=None,
                      status=None,
                      kind=None,
                      queueTime=None,
                      path=None):
    from occam.jobs.records.job import JobRecord

    # Create a session
    session = self.database.session()

    # Create an empty record
    job = JobRecord()

    # Start filling out the record
    if taskBackend:
      job.task_backend = taskBackend

    if taskName:
      job.task_name = taskName

    if taskRevision:
      job.task_revision = taskRevision

    if taskId:
      job.task_uid = taskId

    if path:
      job.path = path

    if scheduler:
      job.scheduler = scheduler

    if identity:
      job.identity = identity

    if status:
      job.status = status

    if kind:
      job.kind = kind

    if queueTime:
      job.queue_time = queueTime

    if initialize:
      job.initialize = initialize

    if initializeTag:
      job.initialize_tag = initializeTag

    if finalize:
      job.finalize = finalize

    if finalizeTag:
      job.finalize_tag = finalizeTag

    if interactive:
      job.interactive = interactive

    if target:
      job.target = target

    # Submit the record
    self.database.update(session, job)
    self.database.commit(session)

    return job

  def createJobOutput(self, id, objectId, objectRevision, wireIndex, itemIndex):
    """ Adds a record of the output object for this job.

    Arguments:
      id (str): The identifier for the job that produced the output.
      objectId (str): The id for the object that was produced and stored.
      objectRevision (str): The revision for the object that was produced and stored.
      wireIndex (int): The index of the output wire.
      itemIndex (int): The position of the item on that wire.

    Returns:
      JobOutputRecord: The saved record.
    """
    # Create a session
    from occam.jobs.records.job_output import JobOutputRecord
    session = self.database.session()

    record = JobOutputRecord()

    record.job_id            = id
    record.object_uid        = objectId
    record.object_revision   = objectRevision
    record.output_index      = wireIndex
    record.output_item_index = itemIndex

    # Submit the record
    self.database.update(session, record)
    self.database.commit(session)

    return record

  def queryJobOutputs(self, id, wireIndex=None, itemIndex=None):
    """ Retrieves a query to find all known outputs for the given job.

    Arguments:
      id (str): The identifier of the job to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      sql.Query: The respective query.
    """

    outputs = sql.Table("job_outputs")

    query = outputs.select()
    query.where = (outputs.job_id == id)

    if itemIndex:
      query.where = query.where & (outputs.output_item_index == itemIndex)

    if wireIndex:
      query.where = query.where & (outputs.output_index == wireIndex)

    return query

  def retrieveJobOutputs(self, id, wireIndex=None, itemIndex=None):
    """ Retrieves all known outputs for the given job.

    Arguments:
      id (str): The identifier of the job to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      list: A list of JobOutputRecord objects.
    """

    # Create a session
    from occam.jobs.records.job_output import JobOutputRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryJobOutputs(id, wireIndex = wireIndex,
                                     itemIndex = itemIndex)

    # Execute the query
    self.database.execute(session, query)

    # Return the result
    return [JobOutputRecord(x) for x in self.database.many(session, size=100)]

  def queryJobOutputsForAll(self, ids, wireIndex=None, itemIndex=None, finishTime=None):
    """ Retrieves a query to find all known outputs for the given job.

    Arguments:
      ids (list): A list of identifiers for the jobs to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      sql.Query: The respective query.
    """

    outputs = sql.Table("job_outputs")

    query = outputs.select(order_by = sql.Asc(outputs.job_id))

    if finishTime:
      subQuery = self.queryJobs(ids = ids, finishTime = finishTime, key = "id")
      query.where = (outputs.job_id.in_(subQuery))
    else:
      query.where = (outputs.job_id.in_(ids))

    if itemIndex:
      query.where = query.where & (outputs.output_item_index == itemIndex)

    if wireIndex:
      query.where = query.where & (outputs.output_index == wireIndex)

    return query

  def retrieveJobOutputsForAll(self, ids, wireIndex=None, itemIndex=None, finishTime=None):
    """ Retrieves all known outputs for the given set of jobs.

    Arguments:
      ids (list): A list of identifiers for the jobs to query.
      wireIndex (int): The index of the wire to filter.
      itemIndex (int): The index of the item to filter.

    Returns:
      list: A list of JobOutputRecord objects.
    """

    # Create a session
    from occam.jobs.records.job_output import JobOutputRecord
    session = self.database.session()

    # Retrieve the query
    query = self.queryJobOutputsForAll(ids, wireIndex  = wireIndex,
                                            itemIndex  = itemIndex,
                                            finishTime = finishTime)

    # Execute the query
    self.database.execute(session, query)

    # Return the result
    return [JobOutputRecord(x) for x in self.database.many(session, size=100)]
