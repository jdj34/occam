# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime

from occam.config import Config

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.accounts.manager import AccountManager
from occam.jobs.manager     import JobManager
from occam.nodes.manager    import NodeManager, NetworkOptions
from occam.network.manager import NetworkManager

import json

@command('jobs', 'report',
    category = "Job Management",
    documentation = "Reports the job status to a coordinator node")
@argument("remote_job_id", action = "store",
                           type = str,
                           help = "the job id for the job on the coordinator node")
@argument("coordinator_uri", action = "store",
                             type = str,
                             help = "URI of the coordinator we should notify")
@option("-s", "--status", action = "store",
                          dest = "status",
                          type = str,
                          help = "the status to report. Defaults to current status")
@option("-j", "--job-id", action = "store",
                          dest = "job_id",
                          type = str,
                          help = "the local job to report")
@option("-e", "--external-token",
        action = "store",
        dest   = "external_token",
        help   = "token to use when pinging the coordinator node")
@uses(AccountManager)
@uses(JobManager)
@uses(NodeManager)
@uses(NetworkManager)
class ReportCommand:
  """ Notifies the coordinator at the given URI that the status of a job has changed.
  """

  def do(self):
    header = (
      f"Notifying {self.options.coordinator_uri} that job "
      f"{self.options.remote_job_id} "
    )

    if self.options.job_id:
      header = f"{header}(locally job {self.options.job_id}) "
    header = f"{header} has changed."

    Log.header(header)

    # Get the host and port we need to give to the coordinator so it knows
    # where to find results.
    config = Config().load()
    host = self.network.hostname()
    computeHost = config.get("daemon", {}).get("externalHost", host)
    computePort = config.get("daemon", {}).get("port", 32000)

    # Generate an access token for information retrieval
    account = self.accounts.retrieveAccount(username = self.person.name)
    token = self.accounts.generateToken(account, self.person, "person")

    # Send the 'ping' to the coordinator for this job
    node = self.nodes.discover(self.options.coordinator_uri)
    networkOptions = NetworkOptions(externalToken = self.options.external_token)
    fromUri = f"occam://{computeHost}:{computePort}"

    status = self.options.status

    # If we are reporting about a job that has gotten far enough as to have a
    # job ID, get the aggregate status before reporting back to the
    # coordinator.
    if self.options.job_id:
      job = self.jobs.jobFromId(self.options.job_id)
      if not job:
        Log.error("Cannot find the given job.")
        return 1

      # We may not be trying to record a new job status, if that is the case
      # use the current status of the job.
      status = status or job.status

      if status == "started" and job.status != "started":
        # If we have yet to officially 'start' this job, mark it started now
        startTime = datetime.now()
        if not self.jobs.updateJob(job.id, status="started", startTime=startTime):
          Log.error("Unable to start job, could not set job status to 'started'.")
          return -1

    self.nodes.pingForJob(node, remoteJobId = self.options.remote_job_id,
                                fromUri = fromUri,
                                status = status,
                                jobId = self.options.job_id,
                                token = token,
                                options = networkOptions)

    return 0
