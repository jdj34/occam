# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.jobs.manager import JobManager
from occam.objects.manager import ObjectManager

@command('jobs', 'new',
    category = "Job Management",
    documentation = "Queues a task to run.")
@argument("task", type="object")
@option("-i", "--interactive", action = "store_true",
                               help   = "Queues the job to run as an interactive process.")
@option("-j", "--json", dest    = "to_json",
                        action  = "store_true",
                        help    = "Returns result as a json document.")
@option("-t", "--target", action  = "store",
                          dest    = "target",
                          help    = "Scheduler target to use for job scheduling. 'null' for an unqueued job.")
@uses(JobManager)
@uses(ObjectManager)
class NewCommand:
  """ Creates a new job to run the given task in the background.
  """

  def do(self):
    Log.header("Creating and queuing a job")
    task = self.objects.resolve(self.options.task, person=self.person)
    job_id = self.jobs.queue(
      task = task,
      person = self.person,
      interactive = self.options.interactive,
      target = self.options.target,
    )

    if job_id is None:
      return -1

    if self.options.to_json:
      import json

      ret = {
        "id": job_id
      }

      Log.output(json.dumps(ret))
    else:
      Log.write("job id: ", end = "")
      Log.output(str(job_id), padding = "")

    return 0
