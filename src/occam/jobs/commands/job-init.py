# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.jobs.manager          import JobManager

@command('jobs', 'job-init',
  category      = 'Job Management',
  documentation = "The default initialize handler for a started job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@option("-r", "--remote-job", action = "store",
                              dest = "remote_job",
                              help = "The job identifier on the remote coordinator.")
@uses(JobManager)
class JobsJobInitCommand:
  def do(self):
    """ Performs the 'jobs job-init' command, the default job initializer.
    """

    # Get the job
    job_id = self.options.job_id
    job = self.jobs.jobFromId(self.options.job_id)

    # If the job failed (or cancelled), then do not update the status
    if job.status == "failed":
      # Return success, in this case
      return 0

    # Report initializing the job.
    self.jobs.initialized(job)
    return 0
