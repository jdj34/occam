# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import tty
import termios

from occam.log import Log

from occam.commands.manager import command, option, argument

from occam.manager import uses

from occam.objects.manager   import ObjectManager
from occam.jobs.manager      import JobManager

@command('jobs', 'connect',
    category = "Job Management",
    documentation = "Connects your terminal to the given job")
@argument("job_id", type=str, help = "The identifier for the job you wish to connect to.")
@uses(JobManager)
class JobConnectCommand:
  """ Connects the terminal to the given job.
  """

  def do(self):
    job_id = self.options.job_id
    job = self.jobs.jobFromId(job_id)

    if job is None:
      Log.error(f"Job {job_id} does not exist.")
      return 1

    old_tty = None

    try:
      stdin = self.stdin
      if self.stdin is sys.stdin.buffer:
        stdin = sys.stdin

        # save original tty setting then set it to raw mode
        try:
          if hasattr(sys.stdin, "buffer"):
            old_tty = termios.tcgetattr(sys.stdin)
            tty.setraw(sys.stdin.fileno())
        except:
          pass
      else:
        sys.stdin = stdin

      # Connect current stdin/stdout to the job stdin/stdout
      self.jobs.connect(job)
    finally:
      # restore tty settings back
      if old_tty:
        termios.tcsetattr(self.stdin, termios.TCSADRAIN, old_tty)

    return 0
