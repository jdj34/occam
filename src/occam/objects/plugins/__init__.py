# Allows plugin directory to be extended via external plugins
from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
