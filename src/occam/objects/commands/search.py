# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from occam.log              import Log
from occam.manager          import uses

from occam.commands.manager import command, option

from occam.objects.manager  import ObjectManager

@command('objects', 'search',
  category      = "Object Discovery",
  documentation = "Look up object details only from our local knowledge.")
@option("-t", "--type",         action  = "store",
                                dest    = "object_type",
                                help    = "search for objects with the specified type")
@option("-s", "--subtype",      action  = "append",
                                dest    = "subtype",
                                nargs   = "?",
                                help    = "search for objects with the specified subtype")
@option("-n", "--name",         action  = "store",
                                dest    = "name",
                                help    = "search for objects with the specified name")
@option("-i", "--id",           action  = "store",
                                dest    = "id",
                                help    = "search for the object with the specified id")
@option("-u", "--uid",          action  = "store",
                                dest    = "uid",
                                help    = "search for the object with the specified uid")
@option("-g", "--tag",          action  = "append",
                                dest    = "tags",
                                nargs   = "?",
                                help    = "search for objects with these tags")
@option("-e", "--environment",  action  = "append",
                                dest    = "environments",
                                type    = str,
                                help    = "search for objects with the specified environment")
@option("-a", "--architecture", action  = "append",
                                dest    = "architectures",
                                type    = str,
                                help    = "search for objects with the specified architecture")
@option("--excludeEnvironment", dest    = "excludeEnvironments",
                                action  = "append",
                                type    = str,
                                help    = "filter out objects by the given environment")
@option("--excludeArchitecture", dest    = "excludeArchitectures",
                                action  = "append",
                                type    = str,
                                help    = "filter out objects by the given architecture")
@option("--environments",       action  = "store",
                                dest    = "searchEnvironments",
                                help    = "search for known environments")
@option("--architectures",      action  = "store",
                                dest    = "searchArchitectures",
                                help    = "search for known architectures")
@option("--types",              dest    = "return_types",
                                action  = "store",
                                type    = str,
                                default = None,
                                nargs   = "?",
                                help    = "search types matching the given string")
@option("-x", "--excludeType",  dest    = "exclude_types",
                                action  = "append",
                                type    = str,
                                help    = "filter objects by their type")
@option("-v", "--views",        action  = "store",
                                dest    = "views",
                                nargs   = "+",
                                help    = "search for objects that can view the specified type. a subtype may follow.")
@option("-p", "--provides",     action  = "store",
                                dest    = "provides",
                                nargs   = 2,
                                help    = "search of objects that provide the given environment and architecture")
@option("-d", "--identity",     action  = "store",
                                dest    = "identity",
                                type    = str,
                                help    = "search for objects signed by the given identity.")
@option("-f", "--full",         dest    = "full",
                                action  = "store_true",
                                help    = "returns full aggregate information about the search")
@option("-j", "--json",         dest    = "to_json",
                                action  = "store_true",
                                help    = "returns result as a json document")
@uses(ObjectManager)
class SearchCommand:
  def do(self):
    viewsType    = None
    viewsSubtype = None

    if self.options.views:
      viewsType = self.options.views[0]
      if len(self.options.views) == 2:
        viewsSubtype = self.options.views[1]

    if not self.options.return_types is None:
      Log.header(key="occam.objects.commands.search.listingTypes")

      types = self.objects.searchTypes(query = self.options.return_types)

      if self.options.to_json:
        Log.output(json.dumps(types))
      else:
        for object_type in types:
          Log.output(object_type)
    elif not self.options.searchEnvironments is None:
      Log.header(key="occam.objects.commands.search.listingEnvironments")

      rows = self.objects.searchEnvironments(query = self.options.searchEnvironments)

      if self.options.to_json:
        Log.output(json.dumps(rows))
      else:
        for result in rows:
          Log.output(result)
    elif not self.options.searchArchitectures is None:
      Log.header(key="occam.objects.commands.search.listingArchitectures")

      rows = self.objects.searchArchitectures(query = self.options.searchArchitectures)

      if self.options.to_json:
        Log.output(json.dumps(rows))
      else:
        for result in rows:
          Log.output(result)
    else:
      if self.options.name is None:
        self.options.name = ""

      identity = ""
      if self.person:
        identity = self.person.identity
        if hasattr(self.person, "roles") and 'administrator' in self.person.roles:
          identity = None

      data = self.objects.fullSearch(object_type          = self.options.object_type,
                                     subtype              = self.options.subtype or [],
                                     name                 = self.options.name,
                                     id                   = self.options.id,
                                     uid                  = self.options.uid,
                                     viewsType            = viewsType,
                                     viewsSubtype         = viewsSubtype,
                                     provides             = self.options.provides,
                                     environments         = self.options.environments,
                                     excludeEnvironments  = self.options.excludeEnvironments,
                                     architectures        = self.options.architectures,
                                     excludeArchitectures = self.options.excludeArchitectures,
                                     identity             = self.options.identity,
                                     excludeTypes         = self.options.exclude_types or [],
                                     tags                 = self.options.tags or [],
                                     canBeReadBy          = identity)

      Log.header(key="occam.objects.commands.search.header")

      ret = data

      if self.options.to_json:
        ret['objects'] = [{"uid":          obj.uid,
                           "id":           obj.id,
                           "revision":     obj.revision,
                           "name":         obj.name,
                           "owner": {
                             "id": obj.owner_id or obj.id
                           },
                           "identity":     obj.identity_uri,
                           "summary":      obj.description,
                           "type":         obj.object_type,
                           "subtype": list(filter(None, obj.subtype[1:-1].split(';'))),
                           "architecture": obj.architecture,
                           "organization": obj.organization,
                           "environment":  obj.environment,
                           "images": [] if not obj.images else obj.images.split(';'),
                          } for obj in data['objects']
                         ]

        ret['numTypes'] = ret['num_types']
        del ret['num_types']
        ret['numEnvironments'] = ret['num_environments']
        del ret['num_environments']
        ret['numArchitectures'] = ret['num_architectures']
        del ret['num_architectures']
        ret['numTags'] = ret['num_tags']
        del ret['num_tags']

        if self.options.full:
          Log.output(json.dumps(ret))
        else:
          Log.output(json.dumps(ret['objects']))
      else:
        for obj in data['objects']:
          Log.output((obj.object_type or "object") + " " + (obj.name or "unknown"))
          Log.output(obj.id,  padding="      id: ")

      Log.write(key="occam.objects.commands.search.found", count=data['count'])

    return 0
