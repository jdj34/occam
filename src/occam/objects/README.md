# The Objects Component

This component handles Occam object storage, object retrieval, and a number of
helper functions related to object metadata.

## Occam Objects

Occam objects are made of 3 elements: metadata (in the form of an "object.json" file), a Git repository, and object data (anything you want the object to contain). The history of an object's metadata and data is tracked using a Git repository.

An object's metadata describes what an object provides, and what it requires to run reproducibly. The object.json file will contain 9 sections:
  * Information - contains the human friendly as well as system populated identifying information about the object:
    * name - The human friendly name of the object.
    * type - The type of object. This might be "simulator" or "image". Used to recommend viewers and other type sensitive components of the system to the user.
    * subtype - The subtype of the object. Further specifies the type of an object. For example, "JPEG" for a "image" type.
    * version - The version of an object. This may be used to distinguish between object versions for the purposes of dependency specification.
    * uid - System populated field giving a unique identifier to the object
    * revision - System populated field indicating the version of the object
  * Inputs / Configurations - Describes the inputs of an object. This lets the object be composed into workflows. Inputs will be provided by the previous connecting node in the workflow.
  * Outputs - Describes the outputs of an object.
  * Dependencies - Describes the other objects in the system that must be provided in the execution environment in order to use this object.
  * Build Information - Describes how to create a build for the object.
  * Run Information - Describes how to run the object when in an execution environment.
  * Execution Requirements - Describes an environment and architecture the object must be run inside. The system will use this information to find appropriate backends and emulators to layer in order to allow for execution.
  * Environment Initialization - Describes how to copy and symlink data into the execution environment, as well as what changes must be made to environment variables in the container to achieve successful execution.
