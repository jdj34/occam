# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log      import loggable
from occam.manager  import manager, uses
from occam.datetime import Datetime

import os

from occam.objects.manager    import ObjectManager
from occam.builds.manager     import BuildManager
from occam.storage.manager    import StorageManager
from occam.network.manager    import NetworkManager
from occam.system.manager     import SystemManager
from occam.keys.write_manager import KeyWriteManager

@loggable
@manager("builds.write", reader=BuildManager)
@uses(ObjectManager)
@uses(BuildManager)
@uses(StorageManager)
@uses(NetworkManager)
@uses(SystemManager)
@uses(KeyWriteManager)
class BuildWriteManager:
  """ This manages the storage of new builds of objects.
  """

  def __localBuildPath(self, object):
    """ Returns the path of the build task.

    Arguments:
      object (Object): An instance of a linked Occam object to clean.
    """

    if not object.link:
      return None

    return os.path.join(object.path, "..", "metadata", "builds", "vm")

  def clean(self, object):
    """ Cleans the build state of a linked object.

    Arguments:
      object (Object): An instance of a linked Occam object to clean.
    """

    if not object.link:
      return None

    # Look at the build path for this object and delete it.
    path = self.__localBuildPath(object)

  def deploy(self, object, person, command = None):
    """ Deploys a build for the given object.

    Arguments:
      object (Object): An instance of an Occam object to build.
      person (Person): The person that is performing the build.
      command (str): The command to invoke (default: use object metadata.)
    """

    cwd = None
    if object.link:
      # A local object under a linked path.
      # A linked path retains its build state and build task.
      pass

  def sign(self, identity, object, task, published=None):
    """ Signs the given build.

    Arguments:
      identity (str): The identity of the signing actor.
      object (Object): The Occam object instance for this build.
      task (Object): The Occam object representing the build task.

    Returns:
      BuildRecord: The modified build record containing the signature.
    """

    # Get the build hash
    buildHash = self.builds.retrieveHash(object, task.id)

    import datetime
    published = published or datetime.datetime.utcnow()

    # Generate a signature
    signature_digest, signature_type, signature, signed, verifyKeyId = self.keys.write.signBuild(object, identity, task, buildHash, published)

    return self.datastore.write.updateSignature(id               = object.id,
                                                revision         = object.revision,
                                                buildId          = task.id,
                                                buildRevision    = task.revision,
                                                identity         = identity,
                                                verifyKeyId      = verifyKeyId,
                                                published        = published,
                                                signed           = signed,
                                                signature        = signature,
                                                signature_type   = signature_type,
                                                signature_digest = signature_digest)

  def pull(self, obj, buildInfo, buildPath, buildLogPath, buildTask, buildPackagePath = None):
    """ Stores an existing build in the build repository.

    Arguments:
      obj (Object): The Occam object instance for the build.
      buildInfo (dict): The build status information.
      buildPath (str): The path on the local disk to the built package.
      buildLogPath (str): The path on the local disk to the build log.
      buildTask (Object): The Occam object representing the build task.
      buildPackagePath (str): A path to a prepared compressed package, if any.

    Returns:
      BuildRecord: The newly created record of this build.
    """

    import datetime
    import base64

    id = obj.id
    uid = obj.uid
    revision = obj.revision
    buildId = buildInfo.get('id')
    buildUid = buildInfo.get('uid')
    buildRevision = buildInfo.get('revision')
    elapsed = buildInfo.get('elapsed')
    identity = buildInfo.get('identity')
    published = buildInfo.get('published')
    published = Datetime.from8601(published)

    backend = buildInfo.get('backend')
    host = buildInfo.get('host')
    port = buildInfo.get('port')

    signatureInfo = buildInfo.get('signature', {})
    verifyKeyId = signatureInfo.get('key')
    signed = signatureInfo.get('signed')
    signed = Datetime.from8601(signed)
    signature = b''
    if signatureInfo.get('encoding') == "base64":
      signature = base64.b64decode(signatureInfo.get('data', '').encode('utf-8'))

    signature_type = signatureInfo.get('format') or "PKCS1_v1_5"
    signature_digest = signatureInfo.get('digest') or "SHA512"

    # Store the build in the file store
    taskInfo = self.objects.infoFor(buildTask)
    self.storage.pushBuild(uid, revision=revision, buildId=buildId, path=buildPath, logPath=buildLogPath)

    # Retrieve hash
    buildHash = self.builds.retrieveHash(obj, buildTask.id)

    # Get the filter token
    filterToken = self.builds.filterFor(taskInfo)

    if buildPackagePath:
      # Retrieve stored path
      buildPath = self.storage.pathFor(uid, revision, buildId)

      # Determine the path to the compressed package
      tarpath = buildPath + ".tar.xz"

      # Move the file
      import shutil
      shutil.move(buildPackagePath, tarpath);

    # Store a record of the build
    return self.datastore.write.createBuild(id               = id,
                                            uid              = uid,
                                            revision         = revision,
                                            buildId          = buildId,
                                            buildUid         = buildUid,
                                            buildRevision    = buildRevision,
                                            buildHash        = buildHash,
                                            filterToken      = filterToken,
                                            backend          = backend,
                                            elapsed          = elapsed,
                                            host             = host,
                                            port             = port,
                                            identity         = identity,
                                            verifyKeyId      = verifyKeyId,
                                            signed           = signed,
                                            signature        = signature,
                                            signature_type   = signature_type,
                                            signature_digest = signature_digest,
                                            published        = published)

  def store(self, person, object, task, buildPath, elapsed, buildLogPath=None):
    """ Stores the given build given the build task.

    Args:
      person (Person): The actor that built the object.
      object (Object): The object that was built.
      task (Object): The task that built this object.
      buildPath (str): The path on disk of the resulting build.
      elapsed (int): The time in milliseconds of the build.
      buildLogPath (str): The filename of the build log.

    Returns:
      BuildRecord: The record of the build.
    """

    # Get the system configuration
    system = self.system.retrieve()

    # Retrieve the host/port that built the object
    host = system.host or self.network.hostname()
    port = system.port or 9292

    # Get the task manifest
    taskInfo = self.objects.infoFor(task)

    # Retrieve the built object from the task manifest
    builtObjectInfo = taskInfo.get('builds')

    backend = taskInfo.get('backend')

    # Retrieve identifiers for that object
    id       = builtObjectInfo.get('id')
    uid      = builtObjectInfo.get('uid')
    revision = builtObjectInfo.get('revision')

    # Store the build in the file store
    self.storage.pushBuild(uid, revision=revision, buildId=task.id, path=buildPath, logPath=buildLogPath)

    # Get the build hash
    buildHash = self.builds.retrieveHash(object, task.id)

    import datetime
    built = datetime.datetime.utcnow()

    if person:
      identity = person.actingIdentity or person.identity

    # Generate a signature
    signature_digest = None
    signature_type = None
    signature = None
    signed = None
    verifyKeyId = None
    if not person.actingIdentity:
      signature_digest, signature_type, signature, signed, verifyKeyId = self.keys.write.signBuild(object, identity, task, buildHash, built)

    filterToken = self.builds.filterFor(taskInfo)

    # Store a record of the build
    return self.datastore.write.createBuild(id               = id,
                                            uid              = uid,
                                            revision         = revision,
                                            buildId          = task.id,
                                            buildUid         = task.uid,
                                            buildRevision    = task.revision,
                                            buildHash        = buildHash,
                                            filterToken      = filterToken,
                                            backend          = backend,
                                            elapsed          = elapsed,
                                            host             = host,
                                            port             = port,
                                            identity         = identity,
                                            verifyKeyId      = verifyKeyId,
                                            signed           = signed,
                                            signature        = signature,
                                            signature_type   = signature_type,
                                            signature_digest = signature_digest,
                                            published        = built)
