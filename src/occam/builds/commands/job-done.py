# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from occam.commands.manager import command, option, argument

from occam.object import Object
from occam.log    import Log

from occam.manager import uses

from occam.objects.manager       import ObjectManager
from occam.jobs.manager          import JobManager
from occam.builds.write_manager  import BuildWriteManager
from occam.messages.manager      import MessageManager

@command('builds', 'job-done',
  category      = 'Build Management',
  documentation = "Respond to a finished build job.")
@argument("job_id", type=str, help = "The identifier for the job.")
@option("-r", "--remote-job", action = "store",
                              dest = "remote_job",
                              help = "The job identifier on the remote coordinator.")
@uses(MessageManager)
@uses(ObjectManager)
@uses(JobManager)
@uses(BuildWriteManager)
class BuildsJobDoneCommand:
  def do(self):
    """ Performs the 'builds job-done' command.
    """

    # Get the job
    job_id = self.options.job_id
    job = self.jobs.jobFromId(self.options.job_id)

    # If the job failed (or cancelled), then do not commit the build
    if job.status == "failed":
      if self.person:
        self.emailBuildUpdate("fail", job_id)
      return 0

    # Get the task executed
    task = self.jobs.taskFor(job)

    if task is None:
      # Cannot find the task
      Log.error("Cannot find the referenced task.")
      return -1

    # Retrieve the task manifest
    taskInfo = self.objects.infoFor(task)

    # Retrieve the object from the task
    objectInfo = taskInfo.get('builds')
    object = self.objects.retrieve(id       = objectInfo.get('id'),
                                   revision = objectInfo.get('revision'),
                                   person   = self.person)

    if object is None:
      # We cannot find the object that was built
      Log.error("Cannot find the referenced object.")
      return -1

    owner = self.objects.ownerFor(object, person = self.person)

    # Get the eventual build path
    buildPath = self.jobs.storage.buildPathFor(owner.uid,
                                               revision = objectInfo.get('revision'),
                                               buildId  = task.info.get("id"),
                                               create   = True)

    start_time = job.start_time or datetime.datetime.utcnow()
    run_time   = job.run_time or datetime.datetime.utcnow()
    elapsed    = (run_time - start_time).total_seconds()

    buildLogPath = self.jobs.logPathFor(job.id)

    # Store the build
    Log.write(f"Storing build {task.id}")
    self.builds.write.store(self.person, owner, task, buildPath, elapsed, buildLogPath)

    # Compress the build
    Log.write(f"Compressing build {task.id}")
    self.builds.compress(owner.uid, owner.revision, task.id, buildPath = buildPath)

    # Finish the job
    self.jobs.finished(job)
    if self.person:
      self.emailBuildUpdate("finish", job_id)

  def emailBuildUpdate(self, update, job_id):
    """ Emails the authenticated account if it is subscribed to build updates

    Arguments:
      update (str): The type of update to send.
      job_id (int): The job id associated with the build.
    """
    account = self.accounts.retrieveAccount(identity = self.person.identity_uri)

    if (f'When my builds {update}' in self.accounts.subscriptionsFor(account)):
      body = f"Build {job_id} has {update}ed."
      self.messages.sendMessage(f'Build {update}', body, account)
