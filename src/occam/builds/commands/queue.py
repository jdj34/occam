# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

import json
from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.write_manager  import BuildWriteManager
from occam.manifests.manager import ManifestManager, BuildRequiredError
from occam.jobs.manager      import JobManager
from occam.links.write_manager import LinkWriteManager

from occam.manager import uses

@command('builds', 'queue',
  category      = 'Build Management',
  documentation = "Queues a build of the given object.")
@argument("object", type="object", nargs="?")
@option("-f", "--from-clean",  dest   = "from_clean",
                               action = "store_true",
                               help   = "will build from a clean environment.")
@option("-t", "--target", action  = "store",
                          dest    = "target",
                          help    = "Scheduler target to use for job scheduling. 'null' for an unqueued job.")
@uses(ObjectManager)
@uses(BuildWriteManager)
@uses(ManifestManager)
@uses(LinkWriteManager)
@uses(JobManager)
class BuildsQueueCommand:
  def do(self):
    if self.person is None:
      Log.error("Must be authenticated to build.")
      return -1

    if self.options.object is None:
      # Default id is '+'
      obj = self.objects.resolve(SimpleNamespace(id       = "+",
                                                 revision = None,
                                                 version  = None,
                                                 uid      = None,
                                                 path     = None,
                                                 index    = None,
                                                 link     = None,
                                                 identity = None),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    Log.header("Queuing build")

    if obj is None:
      # Cannot resolve the object
      Log.error("Could not find the object.")
      return -1

    if obj.link:
      if self.options.from_clean:
        self.links.write.cleanLocalLinkBuild(obj)
      buildPath = self.links.write.initializeLocalLinkBuild(obj)

    # Get a task
    resolved  = False
    penalties = {}

    task = self.manifests.build(obj, id = obj.id, revision = obj.revision, local = False, penalties = penalties, person = self.person)
    tasks = [task]
    resolved = True

    # Queue the first build
    task = list(reversed(tasks))[0]

    taskInfo = self.objects.infoFor(task)
    originalTaskId = task.id
    taskInfo['id'] = task.id

    job = self.jobs.create(taskInfo, task.id, revision = task.revision, person = self.person, finalize = "builds", target = self.options.target)

    ret = {
      "id": job.id
    }

    Log.output(json.dumps(ret))

    return 0
