# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2014-2020 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from occam.log    import Log
from occam.object import Object

from types import SimpleNamespace

from occam.commands.manager import command, option, argument

from occam.objects.manager import ObjectManager
from occam.builds.manager  import BuildManager
from occam.links.manager   import LinkManager

from occam.manager import uses

@command('builds', 'list',
  category      = 'Build Management',
  documentation = "Lists builds for the object or directories within.")
@argument("object", type="object", nargs="?", help="A buildable object")
@argument("task",   type="object", nargs="?", help="The build task")
@option("-j", "--json", dest   = "to_json",
                        action = "store_true",
                        help   = "returns result as a json document")
@option("-l", "--long", dest   = "list_details",
                        action = "store_true",
                        help   = "lists the metadata for each file in a table listing")
@uses(ObjectManager)
@uses(BuildManager)
@uses(LinkManager)
class BuildsListCommand:
  def listBuilds(self, obj):
    Log.header("Listing known builds")

    # List existing tags
    builds = self.builds.retrieveAll(obj)

    if not builds and not self.options.to_json:
      Log.write("No builds")

    if self.options.to_json:
      import json
      import base64

      ret = {
        "builds": []
      }

      for build in builds:
        buildInfo = {
          "id":        build.build_id,
          "uid":       build.build_uid,
          "revision":  build.build_revision,
          "elapsed":   build.elapsed,
          "backend":   build.backend,
          "hash":      build.build_hash,
          "host":      build.host,
          "port":      build.port,
          "identity":  build.identity_uri,
          "published": build.published.isoformat() + "Z",
        }
        if build.signed:
          buildInfo["signature"] = {
            "signed": build.signed.isoformat() + "Z",
            "data": base64.b64encode(build.signature).decode('utf-8'),
            "encoding": "base64",
            "format": "PKCS1_v1_5",
            "digest": "SHA512",
            "key": build.verify_key_id
          }
        ret["builds"].append(buildInfo)
      Log.output(json.dumps(ret))
    else:
      for build in builds:
        Log.write("%s: %s" % (build.published.isoformat(), build.build_id,))

    return 0

  def retrieveDirectoryFrom(self, obj, task, path):
    data = self.builds.retrieveDirectoryFrom(obj, task.id, path)

    return data

  def listBuildDirectory(self, obj, task, path):
    data = self.retrieveDirectoryFrom(obj, task, path)

    if data is None:
      Log.error("Could not find the given path.")
      return -1

    if self.options.to_json:
      import json

      if self.options.list_details:
        Log.output(json.dumps(data['items']))
      else:
        Log.output(json.dumps([item['name'] for item in data['items']]))
      return 0

    for i, item in enumerate(data['items']):
      if i > 0:
        Log.output("  ", end="", padding="")
      Log.output(item['name'], end="", padding="")

    Log.output("\n", end="", padding="")

    return 0

  def do(self):
    if self.options.object is None:
      obj = self.objects.resolve(ObjectManager.parseObjectIdentifier("+"),
                                 person = self.person)
    else:
      obj = self.objects.resolve(self.options.object,
                                 person = self.person)

    if obj is None:
      Log.error("Could not find the object.")
      return -1

    obj = self.objects.ownerFor(obj, person = self.person)

    if self.options.task or (self.options.object and self.options.object.path):
      # Allow not specifying a task, but rather a path
      # This will pull from the object's default build
      if not self.options.task:
        # List existing tags
        builds = self.builds.retrieveAll(obj)

        if not builds:
          Log.write("No builds")

        taskId = builds[0].build_id
        taskPath = self.options.object.path or ""
        task = ObjectManager.parseObjectIdentifier(f"{taskId}{taskPath}")
      else:
        task = self.options.task

      taskObject = self.objects.resolve(task,
                                        person = self.person)

      if taskObject is None:
        Log.error("Could not find the task.")
        return -1

      build = self.builds.retrieve(obj, task)

      if build is None:
        Log.error("Cannot find build.")
        return -1

      obj.revision = build.revision

      return self.listBuildDirectory(obj, taskObject, taskObject.file or "/")

    return self.listBuilds(obj)
