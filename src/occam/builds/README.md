# The Builds Component

This component handles the retrieval of build objects, as well as the
compression of builds.

## Build

A build is a specialized Occam object specifying a built binary/library created
from a buildable object. Builds are added to a backend container environment
for execution.
