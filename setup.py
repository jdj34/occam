#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import io, sys

from os.path import basename, dirname, join, splitext, exists

from glob import glob

from setuptools import setup, find_packages

from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
  user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

  def initialize_options(self):
    TestCommand.initialize_options(self)
    self.pytest_args = ''

  def run_tests(self):
    import shlex
    #import here, cause outside the eggs aren't loaded
    import pytest
    errno = pytest.main(shlex.split(self.pytest_args))

    # If there is a report.json, generate tests.json
    if exists("report.json"):
      import json

      f = open("report.json")
      report = json.load(f)["report"]
      wf = open("tests.json", "w+")

      # Create the conforming test report
      output = {}

      # Copy over environment metadata
      output["metadata"] = report["environment"]

      # Set up unit test group
      output["groups"] = {}
      output["groups"]["unit"] = {"name": "Unit Tests"}
      output["groups"]["unit"]["describes"] = {}

      for test in (report["tests"] or []):
        # Get test group
        group_name = test["name"].split("/")[1]
        if group_name not in output["groups"]:
          output["groups"][group_name] = {}
        group = output["groups"][group_name]

        # Place in appropriate place in hierarchy

        # Divide up test name

        # The name looks like:
        # file/path.py::TestClass::()::TestSub::()::TestSubSub::test_should_x"

        # We will first ignore the first item (the test path)
        sections = test["name"].split("::")[1:]

        # Prepend the test file
        test_file = test["name"].split("::")[0].split("/", 2)[2]

        # Reform test_file
        test_file = join(dirname(test_file), basename(test_file)[:-8])
        test_file = "Testoccam." + test_file.replace("/", ".")

        sections = [test_file, "()"] + sections

        section_iter = iter(sections)
        pairs = zip(section_iter, section_iter)

        for i, (name, function) in enumerate(pairs):
          name = name[4:]
          # Is a class/function etc
          if "describes" not in group:
            group["describes"] = {}
          item_type = None
          if i == 0:
            item_type = "module"
          elif i != (len(sections)/2) - 1:
            item_type = "class"
          else:
            item_type = "function"
            name = name[:1].lower() + name[1:]
          if name not in group["describes"]:
            group["describes"][name] = {"name": name}

            if item_type:
              group["describes"][name]["type"] = item_type

          group = group["describes"][name]

          if function != "()":
            function = function[5:]
            function = function.replace("_", " ")
            if "tests" not in group:
              group["tests"] = []
            group["tests"].append({"type": test["outcome"],
                                   "time": test["duration"],
                                   "number": test["run_index"],
                                   "it": function})

      wf.write(json.dumps(output, indent=2))

    sys.exit(errno)


setup(name='occam',
      version="0.0.1",
      packages=find_packages('src'),
      package_dir={'': 'src'},
      license="AGPL 3",
      author="wilkie",
      author_email="wilkie@xomb.org",
      url="https://gitlab.com/occam-archive/occam",
      py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
      include_package_data=True,
      cmdclass = {'test': PyTest},
      extras_require={
            # Requirements for Postgres support.
            "Postgres": ["psycopg2>=2.8.2"],
      }
)
