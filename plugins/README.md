# Occam Plugins

You can make your own extensions to Occam to add features, backends, and commands through
the creation of an Occam component. Add your component (a Python project under the occam
namespace) into the plugins directory.

These will automatically be placed in your PYTHONPATH, effectively, and added to the global module path.

You still need to place them in your `$OCCAM_ROOT/config.yml` under the components section. For instance, for `occam-social`, you will place occam.social in that list.

Once this is done, this component will be visible to the Occam system and parsed and used to supply new commands and features and component plugins.

See the [occam-component](https://gitlab.com/occam-archive/occam-component) repository
for a simple boiler-plate occam component to start your own plugins.

What follows are some existing plugins.

### Social

To allow threaded comments on different objects, you can install the `occam-social` plugin.
[Find it on GitLab](https://gitlab.com/occam-archive/occam-social).

### Search

To make use of full text search and advanced indexing, you can opt to use the `occam-search`
plugin. Currently, it supports Elasticsearch as a search index backend.
[Find it on GitLab](https://gitlab.com/occam-archive/occam-search).
