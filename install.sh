#!/bin/bash
set -eu

# Installs to a particular OS

# We require 'which'
if ! which which > /dev/null 2> /dev/null; then
	echo "The 'which' application is required."
	exit 1
fi

# Detect package manager
if which apt > /dev/null 2> /dev/null; then
	echo "Ubuntu Detected"
	./scripts/install/ubuntu.sh
	exit 0
elif which pacman > /dev/null 2> /dev/null; then
	echo "Arch Detected"
	./scripts/install/arch.sh
	exit 0
fi

if hash yum 2>/dev/null; then
	echo "CentOS Detected"
	./scripts/install/centos.sh
	exit 0
fi

echo "Could not detect the OS. Consult the install documentation or prepare, manually, the process outlined in the scripts in ./scripts/install to install OS packages and then run ./scripts/install/common.sh to complete installation from there."
